package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.CitaTerapia;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Personal;

import java.util.List;

public interface CitaTerapiaService {

    /**
     * Lista las citas programadas por el area social, que aun no han sido atendidos por el area de rehabilitacion
     * @return
     */
    List<CitaTerapia> ListarCitasNoAtendidas();

    CitaTerapia save(CitaTerapia citaTerapia);

    boolean deleteCitaTerapia(Personal id);

    /**
     * Retorna un registro de las citas programadas que tiene un paciente
     * @param idexpediente
     * @return
     */
    //List<CitaTerapia> getCitaTerapiaPaciente(Expediente idexpediente);

    /**
     * Retorna  la agenda de atencio que tiene un personal especifico.
     * @param idpersonal
     * @return
     */
    List<CitaTerapia> getAgendaAtencionPorPersonal(Personal idpersonal);
}
