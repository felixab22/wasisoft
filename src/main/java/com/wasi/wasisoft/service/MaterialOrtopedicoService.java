package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.MaterialOrtopedico;
import com.wasi.wasisoft.model.PrestamoMaterial;

import java.util.List;

public interface MaterialOrtopedicoService {
    /**
     * Guarda un MaterialOrtopedico
     *
     * @param materialOrtopedico
     * @return el rol guardado
     */
    MaterialOrtopedico save(MaterialOrtopedico materialOrtopedico);

    /**
     * Recupera la lista de materiales Ortopedicos
     *
     * @return lista de materiales Ortopedicos
     */
    List<MaterialOrtopedico> findAll();

    /**
     * Elimina un area con el id recibido
     *
     * @param id
     */
    void deleteMaterialOrtopedico(Long id);

    // METODOS DEL REGISTRO PRESTAMO DE MATERIALES ORTOPEDICOS

    /**
     * Registra y actualiza Prestamo o venta de material
     * @param prestamoMaterial
     * @return
     */
    PrestamoMaterial savePrestamoMaterial(PrestamoMaterial prestamoMaterial);

    /**
     * Lista los materiales prestados (Cuando tipooperacion = 1, operacion Garantia)
     * @return
     */
    List<PrestamoMaterial> getMaterialesEnGarantia();

    /**
     * Lista los materiales que se han vendido. (Cuando tipooperacion = 0, operacion venta)
     * @return
     */
    List<PrestamoMaterial> getMaterialesVendidos();

    /**
     * Lista de materiales que estan en alquiler
     * @return
     */
    List<PrestamoMaterial> getMaterialesEnAlquiler();

    /**
     * Elimina un registro de prestamo de material ortopedico
     * @param idprestamomaterial
     */
    void deletePrestamoMaterial(Long idprestamomaterial);

    /**
     * Devuelve una lista de materiales ortopedicos, que posee un paciente.
     * @param idexpediente
     * @return
     */
    List<PrestamoMaterial> getPrestamoMaterialPorPaciente(Expediente idexpediente);
}

