package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaAto;
import com.wasi.wasisoft.model.FichaSalud;

import java.util.List;

/**
 * Created by HeverFernandez on 26/04/2018.
 */
public interface FichaAtoService {

    FichaAto saveFichAto(FichaAto fichaAto);

    void deleteFichaAto(Long idfichaato);

    /**
     * Lista FICHA ATO de un paciente por años.
     * @param idexpediente
     * @param anio
     * @return
     */
    List<FichaAto> getFichaAtoPaciente(Expediente idexpediente, String anio);

    /**
     * Lista los años de las fichas registradas.
     * @param idexpediante
     * @return
     */
    List<FichaAto> ListarFichaAtoPacientePorFecha(Expediente idexpediante);
}
