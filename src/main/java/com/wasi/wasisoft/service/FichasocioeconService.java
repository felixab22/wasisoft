package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichasocioecon;
import com.wasi.wasisoft.model.Situacionecon;
import com.wasi.wasisoft.model.VisitaDomiciliaria;
import com.wasi.wasisoft.util.ResponseArray;

import java.util.List;

public interface FichasocioeconService {

    Fichasocioecon save(Fichasocioecon fichasocioecon);
    List<Fichasocioecon> findAll();
    void deleteFichasocioecon(Long id_fichasocioecon);

    /**
     * Devuelve la ficha socio economica de un paciente que se busca mediante su id de expediente.
     * @param idexpediente
     * @return
     */
    List<Fichasocioecon> obtenerFichaSocioEconomica(Expediente idexpediente);

//    VisitaDomiciliaria save(VisitaDomiciliaria visitaDomiciliaria);
//    List<VisitaDomiciliaria> findAllVisitaDomiciliaria();

    //Metodos para la generacion de reportes
    ResponseArray reporteByTipoFamilia();
    ResponseArray reportByUbicacionVivienda();
    ResponseArray reportByTenenciaVivienda();
    ResponseArray reportByTipoSeguro();
    ResponseArray reportByNivelEducativo();
    ResponseArray reportByTipoInstitucionEducativa();
    ResponseArray reportByCertificadoDiscapacidad();


}
