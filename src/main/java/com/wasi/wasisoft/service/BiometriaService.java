package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Biometria;
import com.wasi.wasisoft.model.FichaTerapiaFisica;

import java.util.List;

public interface BiometriaService {

    /**
     * Registra y actualiza un registro
     * @param biometria
     * @return
     */
    Biometria saveBiometria(Biometria biometria);

    /**
     * Elimina un registro de biometria
     * @param idbiometria
     */
    void deleteBiometria(Long idbiometria);

    /**
     * Lista las fechas de registro de las fichas de biometria de un paciente
     * @param idficha
     * @return
     */
    List<Biometria> ListarBiometriaPorAnios(FichaTerapiaFisica idficha);

    /**
     * Muestra la biometria de un paciente en un año en especifico
     * @return
     */
    List<Biometria> getBiometriaPaciente(FichaTerapiaFisica idfichaterapiafisica, String anio);
}
