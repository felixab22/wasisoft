package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Marcha;
import com.wasi.wasisoft.model.MarchaPierna;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface MarchaService {

    Marcha saveMarcha(Marcha marcha);

    void deleteMarcha(Long idmarcha);

    /**
     * Obtiene la ficha marcha de un paciente en un año dado
     * @param idfichaterapia
     * @param anio
     * @return
     */
    List<Marcha> getMarchaPaciente(FichaTerapiaFisica idfichaterapia, String anio);

    /**
     * Retorna los años de registro de las fichas marcha de un paciente
     * @param idficha
     * @return
     */
    List<Marcha> listarFichaMarchaPorFechas(FichaTerapiaFisica idficha);

    //  METODOS DE LA CLASE MARCHAPIERNA
    //============================================================================================================

    MarchaPierna saveMarchaPierna(MarchaPierna marchaPierna);

    void deleteMarchaPierna(Long idmarchapierna);

    /**
     * Obtiene datos del seguimiento de la pierna (0 = pierna derecha, 1 = pierna izquierda)
     * @param idmarcha
     * @param posicionpierna
     * @return
     */
    List<MarchaPierna> getMarchaPierna(Marcha idmarcha, Integer posicionpierna);

}
