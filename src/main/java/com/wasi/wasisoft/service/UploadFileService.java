package com.wasi.wasisoft.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by AITAMH on 25/01/2019.
 */
@Service
public class UploadFileService {

    private static final Log LOGGER = LogFactory.getLog(UploadFileService.class);

    Calendar calendario = Calendar.getInstance();

    int anio = calendario.get(Calendar.YEAR);
    private String upload_folder = "D:\\sofware\\files\\"+anio;

    public void saveFile(MultipartFile file) throws IOException {
        String nombre = file.getOriginalFilename();
        String nombrearea,codigo = "";
        nombrearea = nombre.split("-")[0];
        codigo = nombre.split("-")[1];
        File folder = new File(upload_folder+"//"+nombrearea+"//"+codigo);
        if (!folder.exists()) {
            folder.mkdir();
            LOGGER.info(" PARAMETROS CLASE SERVICIO : '" + "DIRECTORIO CREADA CORRECTAMENTE : "  );
        }
        else
            LOGGER.info(" PARAMETROS CLASE SERVICIO : '" + "EL DIRECTORIO SELECCIONADO YA EXISTE : "  );

        if(!file.isEmpty()){
            byte[] bytes = file.getBytes();
            Path path = Paths.get(folder+"//"+ file.getOriginalFilename());
            Files.write(path,bytes);
        }
    }

//    public void saveMultipleFiles(List<MultipartFile> files) throws IOException {
//        for(MultipartFile file: files){
//            if(file.isEmpty()) continue;
//            byte[] bytes = file.getBytes();
//            Path path = Paths.get(upload_folder + file.getOriginalFilename());
//            Files.write(path,bytes);
//        }
//    }

    public List<String> getListFiles(String nombrearea, String codigo) {
        File uploadDir = new File(upload_folder+"//" +nombrearea+"//"+codigo);
        System.out.println("DIRECCION DE ARCHIVOS " +uploadDir );
        File[] files = uploadDir.listFiles();

        List<String> list = new ArrayList<String>();
        if (files.length != 0) {
            for (File file : files) {
                list.add(file.getName());
            }
            LOGGER.info(" PARAMETROS METODO GETLISTFILES CON ARCHIVOS : " + list.size() + " ------" + list);
            return list;
        }
        else {
            LOGGER.info(" PARAMETROS METODO GETLISTFILES : " + list.size()  );
            return list;
        }
    }

    public void downloadPDFResource( HttpServletResponse response, String fileName,String nombrearea, String codigo) throws IOException {
        LOGGER.info(" PARAMETROS METODO GETLISTFILES : Nombre del area "+nombrearea + " Codigo : "+ codigo + "Nombre del archivo --" + fileName );
        File file = new File(upload_folder+"//"+nombrearea+"//"+codigo+"//"+ fileName);

        if (file.exists()) {

            //get the mimetype
            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) {
                //unknown mimetype so set the mimetype to application/octet-stream
                mimeType = "application/octet-stream";
            }

            response.setContentType(mimeType);

            response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

            //Here we have mentioned it to show as attachment
//            response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));

            response.setContentLength((int) file.length());

            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

            FileCopyUtils.copy(inputStream, response.getOutputStream());
        }
    }

    public boolean deleteFile(String fileName,String nombrearea, String codigo){
        LOGGER.info(" PARAMETROS METODO GETLISTFILES : Nombre del area "+nombrearea + " Codigo : "+ codigo + "Nombre del archivo --" + fileName );
        File file = new File(upload_folder+"//"+nombrearea+"//"+codigo+"//"+ fileName);

        if (file.exists()){
            boolean status = file.delete();
            if (!status){
                System.out.println("El archivo se encuentra, pero no se pudo eliminar");
                return false;
            }else {
                System.out.println("El archivo se elimino correctamente");
                return true;
            }
        }else {
            System.out.println("No se encuentra el archivo solicitado");
            return false;
        }
    }
}
