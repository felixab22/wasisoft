package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.VisitaDomiciliaria;

import java.util.List;

public interface VisitaDomiciliariaService {

    /**
     * Lista todas los registros de las visitas domiciliarias
     * @return
     */
    List<VisitaDomiciliaria> findAll();

    /**
     * Registra y actualiza la tabla Visitadomiciliaria
     * @param visitaDomiciliaria
     * @return
     */
    VisitaDomiciliaria save(VisitaDomiciliaria visitaDomiciliaria);

    /**
     * Elimina un registro de la tabla visita Domiciliaria
     * @param idVisitaDomiciliaria
     */
    void deleteVisitaDomiciliaria(Long idVisitaDomiciliaria);

    /**
     * Devuelve La lista de  las visitas domiciliarias, de un paciente
     * @param idexpediente
     * @return
     */
    List<VisitaDomiciliaria> findByIdexpediente(Expediente idexpediente);

    /**
     * Lista las viistas domiciliarias de un paciente, dado su DNI
     * @param dni
     * @return
     */
    List<VisitaDomiciliaria> obtenerVisitasPorDni(int dni);

    /**
     * Lista las visitas domiciliarias de un paciente, dado su HISTORIAL
     * @param historial
     * @return
     */
    List<VisitaDomiciliaria> obtenerVisitasPorHistorial(String historial);
}
