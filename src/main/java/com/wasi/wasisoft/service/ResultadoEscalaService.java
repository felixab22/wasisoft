package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.ResultadoEscala;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface ResultadoEscalaService {

    ResultadoEscala saveResultadoEscala(ResultadoEscala resultadoEscala);

    void deleteResultadoEscala(Long idResultado);

    /**
     * Obtiene La ficha de los resultados de escala de un paciente en un año dado
     * @param idfichaterapiafisica
     * @param anio
     * @return
     */
    List<ResultadoEscala> getResultadoEscalaPaciente(FichaTerapiaFisica idfichaterapiafisica, String anio);

    /**
     * Lista los años de registro de las fichas de resultado de un paciente.
     * @param idficha
     * @return
     */
    List<ResultadoEscala> ListResultadoEscalaPorFechas(FichaTerapiaFisica idficha);
}
