package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Apoderado;
import com.wasi.wasisoft.model.Expediente;

import java.util.List;

public interface ApoderadoService {

    /**
     * Registra y actualiza registro en la tabla apoderado
     * @param apoderado
     * @return
     */
    Apoderado save(Apoderado apoderado);

    /**
     * Lista todo los apoderados registrados.
     * @return
     */
    List<Apoderado> findAll();

    /**
     * Elimina un apoderado, de un paciente
     * @param id_apoderado
     */
    void deleteApoderado(Long id_apoderado);

    /**
     * Lista los apoderados que tiene un paciente
     * @param idexpediente
     * @return
     */
    List<Apoderado> findByIdexpediente(Expediente idexpediente);
}
