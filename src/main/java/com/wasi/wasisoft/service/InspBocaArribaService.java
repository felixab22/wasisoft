package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.InspBocaArriba;

import java.util.List;

public interface InspBocaArribaService {
    /**
     * Registra y actualiza
     * @param inspBocaArriba
     * @return
     */
    InspBocaArriba save(InspBocaArriba inspBocaArriba);

    /**
     * Retorna la Inspeccion de boca arriba de un paciente
     * @param idfichaterapia
     * @return
     */
    List<InspBocaArriba> findInspBocaArribaByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapia, String anio);

    /**
     * Elimina un registro de la bd
     * @param idinspeccion
     */
    void deleteInspeccionBocaArriba(Long idinspeccion);

    List<InspBocaArriba> listarFichasInspecionPasados(FichaTerapiaFisica idfichaterapia);
}
