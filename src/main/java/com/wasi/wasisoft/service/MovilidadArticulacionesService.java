package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Movilidad;
import com.wasi.wasisoft.model.MovilidadArticulaciones;

import java.util.List;

public interface MovilidadArticulacionesService {

    /**
     * Registra y actualiza
     * @param movilidadArticulaciones
     * @return
     */
    MovilidadArticulaciones save(MovilidadArticulaciones movilidadArticulaciones);

    /**
     * Retorna la Movilidad de las articulaciones de un paciente
     * @param idfichaTerapiaFisica
     * @return
     */
    List<MovilidadArticulaciones> getMovilidadArticulacionesPaciente(FichaTerapiaFisica idfichaTerapiaFisica, String anio);

    /**
     * Elimina un registro
     * @param idmovilidad
     */
    void deleteMovilidadArticulaciones(Long idmovilidad);

    /**
     * Lista las fechas de las fichas de movilidad de articulaciones de un paciente.
     * @param idficha
     * @return
     */
    List<MovilidadArticulaciones> listarMovilidadArticulacionesPorFechas(FichaTerapiaFisica idficha);

    //METODOS DE  MOVILIDAD DEL PACIENTE

    /**
     * Registra y actualiza en la tabla movilidad
     * @param movilidad
     * @return
     */
    Movilidad saveMovilidadPaciente(Movilidad movilidad);

    /**
     * Elimina un registro de movilidad
     * @param idmovilidad
     */
    void deleteMovilidadPaciente(Long idmovilidad);

    /**
     * Retorna todos los seguimientos de movilidad que tiene el paciente
     * @param movilidadArticulaciones
     * @return
     */
    List<Movilidad> getMovilidadSeguimiento(MovilidadArticulaciones movilidadArticulaciones);

    /**
     * Retorna la movilidad de seguimiennto uno del paciente.
     * @return
     */

}
