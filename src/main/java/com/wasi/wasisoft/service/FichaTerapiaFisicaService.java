package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaTerapiaFisica;

import java.util.List;

public interface FichaTerapiaFisicaService {

    FichaTerapiaFisica save(FichaTerapiaFisica fichaTerapiaFisica);

    /**
     * Lista los pacientes que
     * @return
     */
//    List<FichaTerapiaFisica> findAllPacienteNoAtendido();

    /**
     *
     * @param idfichaterapia
     */
    void deleteFichaTerapiaFisica(Long idfichaterapia);
    /**
     * Genera una lista de los pacientes que estan siendo atendido en el area de rehabilitacion.
     */
    List<FichaTerapiaFisica> ListarPacientesAtedidos();

    /**
     * Devuelve la ficha de terapia fisica de un paciente que se busca mediante su id de expediente.
     * @param idexpediente
     * @return
     */
    List<FichaTerapiaFisica> obtenerFichaTerapiaFisica(Expediente idexpediente);

}
