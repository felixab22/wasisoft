package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Asistencia;
import com.wasi.wasisoft.model.Personal;

import java.util.List;

public interface AsistenciaService {

    /**
     * Registra y actualiza
     * @param asistencia
     * @return
     */
    Asistencia saveAsistencia(Asistencia asistencia);

    /**
     * Lista todas las inasistencias que tiene un paciente.
     * @return
     */
    List<Asistencia> ListarAsistencia(String paciente);

    /**
     * List el record de asistencia de cada paciente
     * @param paciente
     * @return
     */
    List<Asistencia> ListarInasistencias(String paciente);

    /**
     * Lista el record de asistencia de los pacientes de un personal
     * @param idpersonal
     * @return
     */
    List<Asistencia> ListarAsistenciaPorPersonal(Personal idpersonal);

    /**
     * Lista completa de inasistencias de todos los pacientes.
     * @return
     */
    List<Asistencia> findAll();
    /**
     * Si la cantidad de inasistencias es igual a 3, se elimina la lista inasistencias para ese paciente
     * @param idinasistencia
     */
//    void deleteInasistenecia(Long idinasistencia);


}
