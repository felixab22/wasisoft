package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.DiagnosticoFinal;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.PlanTrabajo;

import java.util.List;

/**
 * Created by HeverFernandez on 17/04/2018.
 */
public interface DiagnosticoFinalService {

    DiagnosticoFinal saveDiagnosticoFinal(DiagnosticoFinal diagnosticoFinal);

    void deleteDiagnosticoFinal(Long iddiagnostico);

    /**
     * Obtiene el diagnostico final de un paciente en un año dado
     * @param idfichaterapia
     * @param anio
     * @return
     */
    List<DiagnosticoFinal> getDiagnosticoFinal(FichaTerapiaFisica idfichaterapia, String anio);

    /**
     * Obtiene la lista de los años de diagnostico final de un paciente
     * @param idficha
     * @return
     */
    List<DiagnosticoFinal> listaDiagnosticoFinalPorFechas(FichaTerapiaFisica idficha);

    // METODOS DEL PLAN DE TRABAJO
    //===============================================

    PlanTrabajo savePlanTrabajo(PlanTrabajo planTrabajo);

    void deletePlanTrabajo(Long idplantrabajo);

    List<PlanTrabajo> getPLanDeTrabajoDelPaciente(DiagnosticoFinal iddiagnosticofinal);
}
