package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Notificacion;
import com.wasi.wasisoft.util.ResponseArray;

import java.util.List;

/**
 * Created by airo on 23/01/2019.
 */
public interface NotificacionService {

    Notificacion save(Notificacion notificacion);

    List<Notificacion> findAll();

    /**
     * Elimina
     * @param idnotificacion
     */
    void deleteNotificacion(Long idnotificacion);

    /**
     * Muestra el numero de veces de inasistencia de un paciente
     * @param nombrepaciente
     * @return
     */
    List<Notificacion> CantidadInasistencias(String nombrepaciente);

    /**
     * Lista la cantidad de inasistencias de los pacientes.
     * @return
     */
    List<Notificacion> reportByInasistencias();
}
