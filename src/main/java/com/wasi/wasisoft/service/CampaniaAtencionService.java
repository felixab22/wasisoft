package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.CampaniaAtencion;
import com.wasi.wasisoft.model.RegistroCampania;

import java.util.List;

/**
 * Created by airo on 1/02/2019.
 */
public interface CampaniaAtencionService {

    CampaniaAtencion saveCampania(CampaniaAtencion campaniaAtencion);

    List<CampaniaAtencion> listCampaniaAtencion();

    void deleteCampania(Long idcampania);

    /*
    Registro de campañas de atencion
     */
    RegistroCampania saveRegistroCampania(RegistroCampania registroCampania);

    /**
     * Lista los pacientes participantes de una campaña en una fecha indicada
     * @param fecha
     * @return
     */
    List<RegistroCampania> ListRegistroCampaniaByFecha(String fecha);

    /**
     * Lista los pacientes participantes de una campaña especifica.
     * @param idcampania
     * @return
     */
    List<RegistroCampania> ListRegistroCampaniaByCampaniaAtencion(CampaniaAtencion idcampania);

    /**
     * Retorna el el paciente que participo en una campaña de atencion.
     * @param nombre
     * @return
     */
    RegistroCampania PacienteRegistradoEnCampania(String nombre);
}
