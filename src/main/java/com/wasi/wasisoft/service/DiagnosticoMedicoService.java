package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.DiagnosticoMedico;
import com.wasi.wasisoft.model.FichaTerapiaFisica;

import java.util.List;

public interface DiagnosticoMedicoService {

    /**
     * Registra y actualiza un registro de la Tabla DiagnosticoMedico
     * @param diagnosticoMedico
     * @return
     */
    DiagnosticoMedico save(DiagnosticoMedico diagnosticoMedico);

    /**
     * Muestra una lista de los diagnostico medicos q tiene un paciente, mediante una busqueda por idfichaterapiafisica
     * @param idfichaterapiafisica
     * @return
     */
    List<DiagnosticoMedico> getDiagnosticoMedico(FichaTerapiaFisica idfichaterapiafisica);

    /**
     * Elimina un diagnostico de un pàciente.
     * @param idDiagnosticoMedico
     */
    void deleteDiagnosticoMedico(Long idDiagnosticoMedico);
}
