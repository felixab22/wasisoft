package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Evpsicologica;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaPsicologia;
import sun.util.resources.ga.LocaleNames_ga;

import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
public interface FichaPsicologiaService {

    FichaPsicologia saveFichaPsicologia(FichaPsicologia fichaPsicologia);

    void deleteFichaPsicologia(Long idficha);

    List<FichaPsicologia> getFichaPsicologiaPaciente(Expediente idexpediente);

    //Evaluacion psicologica de familiares del paciente

    Evpsicologica saveEvPsicologica(Evpsicologica evpsicologica);

    void deleteEvPsicologica(Long idevpsico);

    List<Evpsicologica> findAllByIdfichapsicologia(FichaPsicologia idfichapsico);
}
