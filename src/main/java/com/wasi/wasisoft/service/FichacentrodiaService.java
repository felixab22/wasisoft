package com.wasi.wasisoft.service;

        import com.wasi.wasisoft.model.CuidadoAlim;
        import com.wasi.wasisoft.model.Expediente;
        import com.wasi.wasisoft.model.Fichacentrodia;

        import java.util.List;

/**
 * Created by HeverFernandez on 27/04/2018.
 */
public interface FichacentrodiaService {

    Fichacentrodia saveFichcebtrodia(Fichacentrodia fichacentrodia);

    void deleteFichacentrodia(Long idfichacentrodia);

    /**
     * Obtiene la ficha de centro del dia del pacinte.
     * @param cuidadoAlim
     * @return
     */
    List<Fichacentrodia> getFichacentrodiaPaciente(CuidadoAlim cuidadoAlim);

}
