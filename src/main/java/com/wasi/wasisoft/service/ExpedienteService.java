package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Ubigeo;
import com.wasi.wasisoft.util.ResponseArray;

import java.util.List;

public interface ExpedienteService {

    /**
     * Registra y actualiza expediente
     * @param expediente
     * @return
     */
    Expediente save(Expediente expediente);

    /**
     * Lista todos los expediente activos y no activos
     * @return
     */
    List<Expediente> findAll();

    /**
     * Eliminar un regisro de expediente
     * @param id_expediente
     */
    void deleteExpediente(Long id_expediente);

    /**
     * Obtiene un registro de expediente mediante un codigo de historia.
     * @param codigoHistoria
     * @return
     */
    List<Expediente> findByCodigohistorial(String codigoHistoria);

    //List<Expediente> findByNombreAndPrimer_apellido(String nombre, String primerapellido);

    /**
     * Busca el paciente por dni
     * @param dni
     * @return
     */
    List<Expediente> findByDni(int dni);

    //Reportes
    ResponseArray reporteByDiagnostico();
    ResponseArray reporteByEdadPaciente();
    ResponseArray reporteBySexoPaciente();

}
