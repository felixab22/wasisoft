package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Situacionpaciente;

import java.util.List;

public interface SituacionpacienteService {

    Situacionpaciente save(Situacionpaciente situacionpaciente);

    List<Situacionpaciente> findByIdexpediente(Expediente idexpediente);

    /**
     * Lista los pacientes activos.
     * @return
     */
    List<Situacionpaciente> findAllExpedienteByPacienteactivo();

    /**
     * Lista los pacientes activos y aceptados
     * @return
     */
    List<Situacionpaciente> getPacienteActivoAndAceptado();
}
