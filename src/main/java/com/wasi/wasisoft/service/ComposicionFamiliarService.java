package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.ComposicionFamiliar;
import com.wasi.wasisoft.model.Expediente;

import java.util.List;

public interface ComposicionFamiliarService {

    List<ComposicionFamiliar> findAll();
    ComposicionFamiliar save(ComposicionFamiliar composicionFamiliar);
    void deleteComposicionFamiliar(Long idcompfamiliar);

    List<ComposicionFamiliar> findByIdexpediente(Expediente idexpediente);
}
