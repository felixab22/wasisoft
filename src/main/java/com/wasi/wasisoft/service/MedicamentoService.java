package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Medicamento;
import com.wasi.wasisoft.model.MedicamentoVenta;

import java.util.List;

public interface MedicamentoService {

    /**
     * Guarda un Medicamento
     *
     * @param medicamento
     * @return el rol guardado
     */
    Medicamento save(Medicamento medicamento);

    /**
     * Recupera la lista de medicamentos
     *
     * @return lista de medicamentos
     */
    List<Medicamento> findAll();

    /**
     * Elimina un area con el id recibido
     *
     * @param id
     */
    void deleteMedicamento(Long id);

    //========METODOS DE LA VENTA Y DISTRIBUCION DE MEDICAMENTOS====================
    MedicamentoVenta saveMedicamentoVenta(MedicamentoVenta medicamentoVenta);

    void deleteMedicamentoVenta(Long idmedicamentoventa);

    List<MedicamentoVenta> getAllVentaMedicamento();
}
