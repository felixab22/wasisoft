package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Fuerza;
import com.wasi.wasisoft.model.FuerzaSeguimiento;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface FuerzaService {
    /**
     * Guarda y elimina
     * @param fuerza
     * @return
     */
    Fuerza saveFuerza(Fuerza fuerza);

    /**
     * Elimina un registro
     * @param idfuerza
     */
    void deleteFuerza(Long idfuerza);

    /**
     * Obtiene una ficha de fuerza de un paciente
     * @param idfichaterapiafisica
     * @param anio
     * @return
     */
    List<Fuerza> getFuerzaPaciente(FichaTerapiaFisica idfichaterapiafisica, String anio);

    /**
     * Lista los años de las fichas de fuerza
     * @param idficha
     * @return
     */
    List<Fuerza> listarFichasFuerzaPorFechas(FichaTerapiaFisica idficha);

    // METODOS DE LOS SEGUIMIENTOS DE LA FUERZA
    //=========================================================================

    FuerzaSeguimiento saveFuerzaSeguimiento(FuerzaSeguimiento fuerzaSeguimiento);

    void deleteSeguimientoFuerza(Long idseguimientofuerza);

    List<FuerzaSeguimiento> getSeguimientoFuerzaPaciente(Fuerza idfuerza);

}
