package com.wasi.wasisoft.service;

import com.wasi.wasisoft.model.Espasticidad;
import com.wasi.wasisoft.model.FichaTerapiaFisica;

import java.util.List;

/**
 * Created by Espasticidad on 16/04/2018.
 */
public interface EspasticidadService {
    /**
     * Guarda y actualiza
     * @param espasticidad
     * @return
     */
    Espasticidad saveEspasticidad(Espasticidad espasticidad);

    /**
     * Elimina una ficha de espasticidad
     * @param idespasticidad
     */
    void deleteEspasticidad(Long idespasticidad);

    /**
     * Lista las fechas de registro de fichas de espasticidad de un paciente.
     * @param idficha
     * @return
     */
    List<Espasticidad> ListarEspasticidadPorAnios(FichaTerapiaFisica idficha);

    /**
     * Muestra la ficha de espasticidad de un paciente en un año dado
      * @param idfichaterapiafisica
     * @param anio
     * @return
     */
    List<Espasticidad> getEspasticidadPaciente(FichaTerapiaFisica idfichaterapiafisica, String anio);
}
