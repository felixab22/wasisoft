package com.wasi.wasisoft.service;


import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.model.User;

import java.util.List;

public interface PersonalService {

    /**
     * Guarda un usuario
     *
     * @param personal
     * @return el usuario guardado
     */
    Personal save(Personal personal);

    /**
     * Recupera la lista de usuarios
     *
     * @return lista de usuarios
     */
    List<Personal> findAll();

    /**
     * Elimina un usuario con el id recibido
     *
     * @param id
     */
    void deletePersonal(Long id);

    List<Personal> findPersonalByAreaRehabilitacion();

    List<Personal> findAllByIdarea(Area idarea);

    /**
     * Muestra la lista de lo personales activos
     */
    List<Personal> listPersonalActivo();

    /**
     * Muestra la lista de los personales inactivos o dados de baja
     */
    List<Personal> listPersonalInactivo();

    /**
     * Obtiene datos del personal dado el username del User
     * @param username
     * @return
     */
    Personal obtenerPersonalPorUsuario(String username);

    /**
     * Obtiene datos del personal dado su id usuario
     * @param iduser
     * @return
     */
    Personal getPersonalByUser(User iduser);
}
