package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.VisitaDomiciliariaDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.VisitaDomiciliaria;
import com.wasi.wasisoft.service.VisitaDomiciliariaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisitaDomiciliariaServiceImpl implements VisitaDomiciliariaService {

    @Autowired
    protected VisitaDomiciliariaDao visitaDomiciliariaDao;

    @Override
    public List<VisitaDomiciliaria> findAll() {
        return this.visitaDomiciliariaDao.findAll();
    }

    @Override
    public VisitaDomiciliaria save(VisitaDomiciliaria visitaDomiciliaria) {
        return this.visitaDomiciliariaDao.save(visitaDomiciliaria);
    }

    @Override
    public void deleteVisitaDomiciliaria(Long idVisitaDomiciliaria) {
        this.visitaDomiciliariaDao.deleteById(idVisitaDomiciliaria);
    }

    @Override
    public List<VisitaDomiciliaria> findByIdexpediente(Expediente idexpediente) {
        return this.visitaDomiciliariaDao.findByIdexpediente(idexpediente);
    }

    @Override
    public List<VisitaDomiciliaria> obtenerVisitasPorDni(int dni) {
        return this.visitaDomiciliariaDao.obtenerVisitasPorDni(dni);
    }

    @Override
    public List<VisitaDomiciliaria> obtenerVisitasPorHistorial(String historial) {
        return this.visitaDomiciliariaDao.obtenerVisitasPorHistorial(historial);
    }
}
