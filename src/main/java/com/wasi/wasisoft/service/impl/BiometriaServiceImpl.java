package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.BiometriaDao;
import com.wasi.wasisoft.model.Biometria;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.BiometriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class BiometriaServiceImpl implements BiometriaService {

    @Autowired
    protected BiometriaDao biometriaDao;

    private Calendar calendario = Calendar.getInstance();
    private  Integer anio = calendario.get(Calendar.YEAR);
    private String cadenaanio = Integer.toString(anio);

    @Override
    public Biometria saveBiometria(Biometria biometria) {
        return this.biometriaDao.save(biometria);
    }

    @Override
    public List<Biometria> getBiometriaPaciente(FichaTerapiaFisica idfichaterapiafisica, String cadenaanio) {
        return this.biometriaDao.findByIdfichaterapiafisica(idfichaterapiafisica, cadenaanio);
    }

    @Override
    public void deleteBiometria(Long idbiometria) {
        this.biometriaDao.deleteById(idbiometria);
    }

    @Override
    public List<Biometria> ListarBiometriaPorAnios(FichaTerapiaFisica idficha) {
        return this.biometriaDao.listBiometriaPorFechas(idficha);
    }
}
