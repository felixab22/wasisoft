package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.MaterialOrtopedicoDao;
import com.wasi.wasisoft.dao.PrestamoMaterialDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.MaterialOrtopedico;
import com.wasi.wasisoft.model.PrestamoMaterial;
import com.wasi.wasisoft.service.MaterialOrtopedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class MaterialOrtopedicoServiceImpl implements MaterialOrtopedicoService{

    @Autowired
    protected MaterialOrtopedicoDao materialOrtopedicoDao;

    @Autowired
    protected PrestamoMaterialDao prestamoMaterialDao;

    private Calendar calendario = Calendar.getInstance();
    private  Integer anio = calendario.get(Calendar.YEAR);
    private String cadenaanio = Integer.toString(anio);

    @Override
    public MaterialOrtopedico save(MaterialOrtopedico materialOrtopedico) {
        return this.materialOrtopedicoDao.save(materialOrtopedico) ;
    }

    @Override
    public List<MaterialOrtopedico> findAll() {
        return this.materialOrtopedicoDao.findAll();
    }

    @Override
    public void deleteMaterialOrtopedico(Long id) {
        this.materialOrtopedicoDao.deleteById(id);
    }

    @Override
    public PrestamoMaterial savePrestamoMaterial(PrestamoMaterial prestamoMaterial) {
        return this.prestamoMaterialDao.save(prestamoMaterial);
    }

    @Override
    public List<PrestamoMaterial> getMaterialesEnGarantia() {
        return this.prestamoMaterialDao.getMaterialPrestado();
    }

    @Override
    public List<PrestamoMaterial> getMaterialesVendidos() {
        return prestamoMaterialDao.getMaterialVendido(cadenaanio);
    }

    @Override
    public List<PrestamoMaterial> getMaterialesEnAlquiler() {
        return prestamoMaterialDao.getMaterialEnAlquiler();
    }

    @Override
    public void deletePrestamoMaterial(Long idprestamomaterial) {
        this.prestamoMaterialDao.deleteById(idprestamomaterial);
    }

    @Override
    public List<PrestamoMaterial> getPrestamoMaterialPorPaciente(Expediente idexpediente) {
        return this.prestamoMaterialDao.findByIdexpediente(idexpediente);
    }
}
