package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.CuidadoAlimDao;
import com.wasi.wasisoft.dao.FichaSaludDao;
import com.wasi.wasisoft.dao.VisdomintegralDao;
import com.wasi.wasisoft.dao.VisitaMedicaDao;
import com.wasi.wasisoft.model.*;
import com.wasi.wasisoft.service.FichaSaludService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
@Service
public class FichaSaludServiceImpl  implements FichaSaludService{

    @Autowired
    protected FichaSaludDao  fichaSaludDao;

    @Autowired
    protected VisitaMedicaDao visitaMedicaDao;

    @Autowired
    protected VisdomintegralDao visdomintegralDao;

    @Autowired
    protected CuidadoAlimDao cuidadoAlimDao;

    @Override
    public FichaSalud saveFichaSalud(FichaSalud fichaSalud) {
        return this.fichaSaludDao.save(fichaSalud);
    }

    @Override
    public void deleteFichaSalud(Long idfichasalud) {
        this.fichaSaludDao.deleteById(idfichasalud);
    }

    @Override
    public List<FichaSalud> getFichaSaludPaciente(CuidadoAlim idcuidadoalim) {
        return this.fichaSaludDao.findAllByIdcuidalim(idcuidadoalim);
    }

    @Override
    public VisitaMedica saveVisitaMedica(VisitaMedica visitaMedica) {
        return this.visitaMedicaDao.save(visitaMedica);
    }

    @Override
    public void deleteVisitaMedica(Long Idvisita) {
        this.visitaMedicaDao.deleteById(Idvisita);
    }

    @Override
    public List<VisitaMedica> getVisitaMedicaPaciente(FichaSalud idfichasalud) {
        return this.visitaMedicaDao.findByIdfichasalud(idfichasalud);
    }

    @Override
    public Visdomintegral saveVisdomintegral(Visdomintegral visdomintegral) {
        return this.visdomintegralDao.save(visdomintegral);
    }

    @Override
    public void deleteVisdomintegral(Long idvisdomintegral) {
        this.visdomintegralDao.deleteById(idvisdomintegral);
    }

    @Override
    public List<Visdomintegral> getVisitaDomiciliariaIntegral(FichaSalud idfichasalud) {
        return this.visdomintegralDao.findByIdfichasalud(idfichasalud);
    }

    @Override
    public CuidadoAlim saveCuidadoAlim(CuidadoAlim cuidadoAlim) {
        return this.cuidadoAlimDao.save(cuidadoAlim);
    }

    @Override
    public void deleteCuidadoAlim(Long idcuidadoalim) {
        this.cuidadoAlimDao.deleteById(idcuidadoalim);
    }

    @Override
    public List<CuidadoAlim> getCuidadoAlimentacionPaciente(Expediente idexpediente) {
        return this.cuidadoAlimDao.findAllByIdexpediente(idexpediente);
    }

}
