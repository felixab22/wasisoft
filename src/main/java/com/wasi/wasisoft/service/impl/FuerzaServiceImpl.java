package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.FuerzaDao;
import com.wasi.wasisoft.dao.FuerzaSeguimientoDao;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Fuerza;
import com.wasi.wasisoft.model.FuerzaSeguimiento;
import com.wasi.wasisoft.service.FuerzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@Service
public class FuerzaServiceImpl implements FuerzaService {

    @Autowired
    protected FuerzaDao fuerzaDao;

    @Autowired
    protected FuerzaSeguimientoDao fuerzaSeguimientoDao;

    @Override
    public Fuerza saveFuerza(Fuerza fuerza) {
        return this.fuerzaDao.save(fuerza);
    }

    @Override
    public void deleteFuerza(Long idfuerza) {
        this.fuerzaDao.deleteById(idfuerza);
    }

    @Override
    public List<Fuerza> getFuerzaPaciente(FichaTerapiaFisica idfichaterapiafisica, String anio) {
        return this.fuerzaDao.findByIdfichaterapiafisica(idfichaterapiafisica, anio);
    }

    @Override
    public List<Fuerza> listarFichasFuerzaPorFechas(FichaTerapiaFisica idficha) {
        return this.fuerzaDao.listFichaFuerzaPorFechas(idficha);
    }

    @Override
    public FuerzaSeguimiento saveFuerzaSeguimiento(FuerzaSeguimiento fuerzaSeguimiento) {
        return this.fuerzaSeguimientoDao.save(fuerzaSeguimiento);
    }

    @Override
    public void deleteSeguimientoFuerza(Long idseguimientofuerza) {
        this.fuerzaSeguimientoDao.deleteById(idseguimientofuerza);
    }

    @Override
    public List<FuerzaSeguimiento> getSeguimientoFuerzaPaciente(Fuerza idfuerza) {
        return this.fuerzaSeguimientoDao.findByFuerza(idfuerza);
    }
}
