package com.wasi.wasisoft.service.impl;


import com.wasi.wasisoft.dao.AreaDao;
import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AreaServiceImpl implements AreaService{

    @Autowired
    protected AreaDao areaDao;

    @Override
    public Area save(Area area) {
        return this.areaDao.save(area);
    }

    @Override
    public List<Area> findAll() {
        return this.areaDao.findAll();
    }

    @Override
    public void deleteArea(Long id) {
        this.areaDao.deleteById(id);
    }

    @Override
    public List<Area> getAreasReferenciaInterna() {
        return this.areaDao.findAllAreaReferenciaInterna();
    }
}
