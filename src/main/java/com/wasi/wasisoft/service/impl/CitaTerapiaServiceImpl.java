package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.CitaTerapiaDao;
import com.wasi.wasisoft.model.CitaTerapia;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.service.CitaTerapiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
public class CitaTerapiaServiceImpl implements CitaTerapiaService{

    @Autowired
    protected CitaTerapiaDao citaTerapiaDao;

    private Instant myDate = Instant.now();

    @Override
    public List<CitaTerapia> ListarCitasNoAtendidas() {
        return this.citaTerapiaDao.findByCitasNoAtendidas();
    }

    @Override
    public CitaTerapia save(CitaTerapia citaTerapia) {
        return this.citaTerapiaDao.save(citaTerapia);
    }

    @Override
    public boolean deleteCitaTerapia(Personal idpersonal) {

        this.citaTerapiaDao.deleteAllByIdpersonal(idpersonal);
        return true;
    }

    //@Override
    //public List<CitaTerapia> getCitaTerapiaPaciente(Expediente idexpediente) {
    //    return this.citaTerapiaDao.findByIdexpediente(idexpediente);
    //}

    @Override
    public List<CitaTerapia> getAgendaAtencionPorPersonal(Personal idpersonal) {
        return this.citaTerapiaDao.findAllByIdpersonal(idpersonal);
    }
}
