package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.CampaniaAtencionDao;
import com.wasi.wasisoft.dao.RegistroCampaniaDao;
import com.wasi.wasisoft.model.CampaniaAtencion;
import com.wasi.wasisoft.model.RegistroCampania;
import com.wasi.wasisoft.service.CampaniaAtencionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by airo on 1/02/2019.
 */
@Service
public class CampaniaAtencionServiceImpl implements CampaniaAtencionService {

    @Autowired
    protected CampaniaAtencionDao campaniaAtencionDao;

    @Autowired
    protected RegistroCampaniaDao registroCampaniaDao;

    @Override
    public CampaniaAtencion saveCampania(CampaniaAtencion campaniaAtencion) {
        return this.campaniaAtencionDao.save(campaniaAtencion);
    }

    @Override
    public List<CampaniaAtencion> listCampaniaAtencion() {
        return this.campaniaAtencionDao.findAll();
    }

    @Override
    public void deleteCampania(Long idcampania) {
        this.campaniaAtencionDao.deleteById(idcampania);
    }

    @Override
    public RegistroCampania saveRegistroCampania(RegistroCampania registroCampania) {
        return this.registroCampaniaDao.save(registroCampania);
    }

    @Override
    public List<RegistroCampania> ListRegistroCampaniaByFecha(String fecha) {
        return this.registroCampaniaDao.findByFregistro(fecha);
    }

    @Override
    public List<RegistroCampania> ListRegistroCampaniaByCampaniaAtencion(CampaniaAtencion idcampania) {
        return this.registroCampaniaDao.findByCampaniaatencion(idcampania);
    }

    @Override
    public RegistroCampania PacienteRegistradoEnCampania(String nombre) {
        return this.registroCampaniaDao.findByNombrepaciente(nombre);
    }
}
