package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.MarchaDao;
import com.wasi.wasisoft.dao.MarchaPiernaDao;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Marcha;
import com.wasi.wasisoft.model.MarchaPierna;
import com.wasi.wasisoft.service.MarchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@Service
public class MarchaServiceImpl implements MarchaService {

    @Autowired
    protected MarchaDao marchaDao;

    @Autowired
    protected MarchaPiernaDao marchaPiernaDao;

    @Override
    public Marcha saveMarcha(Marcha marcha) {
        return this.marchaDao.save(marcha);
    }

    @Override
    public void deleteMarcha(Long idmarcha) {
        this.marchaDao.deleteById(idmarcha);
    }

    @Override
    public List<Marcha> getMarchaPaciente(FichaTerapiaFisica idfichaterapia, String anio) {
        return this.marchaDao.findByFichaTerapiaFisica(idfichaterapia, anio);
    }

    @Override
    public List<Marcha> listarFichaMarchaPorFechas(FichaTerapiaFisica idficha) {
        return this.marchaDao.ListFichaMarchaPorFechas(idficha);
    }

    @Override
    public MarchaPierna saveMarchaPierna(MarchaPierna marchaPierna) {
        return this.marchaPiernaDao.save(marchaPierna);
    }

    @Override
    public void deleteMarchaPierna(Long idmarchapierna) {
        this.marchaPiernaDao.deleteById(idmarchapierna);
    }

    @Override
    public List<MarchaPierna> getMarchaPierna(Marcha idmarcha, Integer posicionpierna) {
        return this.marchaPiernaDao.findAllByMarchaAndPosicionpierna(idmarcha, posicionpierna);
    }


}
