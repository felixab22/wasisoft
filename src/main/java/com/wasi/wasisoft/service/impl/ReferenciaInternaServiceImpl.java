package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.ReferenciaInternaDao;
import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ReferenciaInterna;
import com.wasi.wasisoft.service.ReferenciaInternaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
@Service
public class ReferenciaInternaServiceImpl implements ReferenciaInternaService {

    @Autowired
    protected ReferenciaInternaDao referenciaInternaDao;


    private Calendar calendario = Calendar.getInstance();
    private  Integer anio = calendario.get(Calendar.YEAR);
    private String cadenaanio = Integer.toString(anio);

    @Override
    public ReferenciaInterna saveReferenciaInterna(ReferenciaInterna referenciaInterna) {
        return this.referenciaInternaDao.save(referenciaInterna);
    }

    @Override
    public void deleteReferenciaInterna(Long idreferencia) {
        this.referenciaInternaDao.deleteById(idreferencia);
    }

    @Override
    public List<ReferenciaInterna> getReferenciaInterna() {
        return this.referenciaInternaDao.findAll();
    }

    @Override
    public List<ReferenciaInterna> getReferenciaInternaPorAreas(Area idarea) {
        return this.referenciaInternaDao.findByIdareaReferencia(idarea,cadenaanio);
    }

    @Override
    public List<ReferenciaInterna> getReferenciaInternaPorPaciente(Expediente idexpediente) {
        return this.referenciaInternaDao.ObtenerReferenciaInterna(idexpediente,cadenaanio);
    }

    @Override
    public List<ReferenciaInterna> getReferenciaInternaValidar(Area idarea, Expediente idexpediente) {
        return this.referenciaInternaDao.buscarReferenciaInterna(idarea, idexpediente, cadenaanio);
    }
}
