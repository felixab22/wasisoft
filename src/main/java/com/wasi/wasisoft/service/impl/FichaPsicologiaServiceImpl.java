package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.EvpsicologicaDao;
import com.wasi.wasisoft.dao.FichaPsicologiaDao;
import com.wasi.wasisoft.model.Evpsicologica;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaPsicologia;
import com.wasi.wasisoft.service.FichaPsicologiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
@Service
public class FichaPsicologiaServiceImpl implements FichaPsicologiaService {

    @Autowired
    protected FichaPsicologiaDao fichaPsicologiaDao;

    @Autowired
    protected EvpsicologicaDao evpsicologicaDao;

    @Override
    public FichaPsicologia saveFichaPsicologia(FichaPsicologia fichaPsicologia) {
        return this.fichaPsicologiaDao.save(fichaPsicologia);
    }

    @Override
    public void deleteFichaPsicologia(Long idficha) {
        this.fichaPsicologiaDao.deleteById(idficha);
    }

    @Override
    public List<FichaPsicologia> getFichaPsicologiaPaciente(Expediente idexpediente) {
        return fichaPsicologiaDao.findByIdexpediente(idexpediente);
    }

    @Override
    public Evpsicologica saveEvPsicologica(Evpsicologica evpsicologica) {
        return this.evpsicologicaDao.save(evpsicologica);
    }

    @Override
    public void deleteEvPsicologica(Long idevpsico) {
        this.evpsicologicaDao.deleteById(idevpsico);
    }

    @Override
    public List<Evpsicologica> findAllByIdfichapsicologia(FichaPsicologia idfichapsico) {
        return this.evpsicologicaDao.findAllByIdfichapsicologia(idfichapsico);
    }
}
