package com.wasi.wasisoft.service.impl;


import com.wasi.wasisoft.dao.FichasocioeconDao;
import com.wasi.wasisoft.dao.SituacioneconDao;
import com.wasi.wasisoft.dao.VisitaDomiciliariaDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichasocioecon;
import com.wasi.wasisoft.model.Situacionecon;
import com.wasi.wasisoft.model.VisitaDomiciliaria;
import com.wasi.wasisoft.service.FichasocioeconService;
import com.wasi.wasisoft.util.ResponseArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class FichasocioeconServiceImpl implements FichasocioeconService {

    @Autowired
    protected FichasocioeconDao fichasocioeconDao;

    @Override
    public Fichasocioecon save(Fichasocioecon fichasocioecon) {

        return this.fichasocioeconDao.save(fichasocioecon);

    }

    @Override
    public List<Fichasocioecon> findAll() {
        return this.fichasocioeconDao.findAll();
    }

    @Override
    public void deleteFichasocioecon(Long id_fichasocioecon) {
        this.fichasocioeconDao.deleteById(id_fichasocioecon);
    }

    @Override
    public List<Fichasocioecon> obtenerFichaSocioEconomica(Expediente idexpediente) {
        return this.fichasocioeconDao.findByIdexpediente(idexpediente);
    }

    Calendar calendario = Calendar.getInstance();
    private  Integer anio = calendario.get(Calendar.YEAR);
    private String cadenaanio = Integer.toString(anio);

    @Override
    public ResponseArray reporteByTipoFamilia() {
        List<Integer> cantidadTipoFamiliaArrayList = this.fichasocioeconDao.reportByTipoFamilia(cadenaanio);
        List<String> tipoFamiliaArrayList = this.fichasocioeconDao.reportByTipoFamiliaTipo(cadenaanio);
        Object[] cantidad = cantidadTipoFamiliaArrayList.toArray();
        Object[] tipo = tipoFamiliaArrayList.toArray();
        return new ResponseArray(cantidad,tipo);
    }

    @Override
    public ResponseArray reportByUbicacionVivienda() {
        List<Integer> cantZonaVivienda = this.fichasocioeconDao.reportByUbicacionViviendaCantidad(cadenaanio);
        List<String> ubicacionVivienda = this.fichasocioeconDao.reportByUbicacionViviendaTipo(cadenaanio);
        Object[] cantidad = cantZonaVivienda.toArray();
        Object[] tipo = ubicacionVivienda.toArray();
        return new ResponseArray(cantidad,tipo);
    }
    @Override
    public ResponseArray reportByTenenciaVivienda() {
        List<Integer> cantTeneciaVivienda = this.fichasocioeconDao.reportByTenenciaViviendaCantidad(cadenaanio);
        List<String> teneciaVivienda = this.fichasocioeconDao.reportByTeneciaViviendaTipo(cadenaanio);
        Object[] cantidad = cantTeneciaVivienda.toArray();
        Object[] tipo = teneciaVivienda.toArray();
        return new ResponseArray(cantidad,tipo);
    }
    @Override
    public ResponseArray reportByTipoSeguro() {
        List<Integer> cantTipoSeguro = this.fichasocioeconDao.reportByTipoSeguroCantidad(cadenaanio);
        List<String> tipoSeguro = this.fichasocioeconDao.reportByTipoSeguroTipo(cadenaanio);
        Object[] cantidad = cantTipoSeguro.toArray();
        Object[] tipo = tipoSeguro.toArray();
        return new ResponseArray(cantidad,tipo);
    }

    @Override
    public ResponseArray reportByNivelEducativo() {
        List<Integer> cantNivelEducativo = this.fichasocioeconDao.reportByNivelEducativoCantidad(cadenaanio);
        List<String> nivelEducativo = this.fichasocioeconDao.reportByNivelEducativoTipo(cadenaanio);
        Object[] cantidad = cantNivelEducativo.toArray();
        Object[] tipo = nivelEducativo.toArray();
        return new ResponseArray(cantidad,tipo);
    }

    @Override
    public ResponseArray reportByTipoInstitucionEducativa() {
        List<Integer> cantIntEducativa = this.fichasocioeconDao.reportByTipoInstitucionEducativaCantidad(cadenaanio);
        List<String> instEducativa = this.fichasocioeconDao.reportByTipoInstitucionEducativaTipo(cadenaanio);
        Object[] cantidad = cantIntEducativa.toArray();
        Object[] tipo = instEducativa.toArray();
        return new ResponseArray(cantidad,tipo);
    }

    @Override
    public ResponseArray reportByCertificadoDiscapacidad() {
        List<Integer> cantCertDiscapacidad = this.fichasocioeconDao.reportByCertificadoDiscapacidadCantidad(cadenaanio);
        List<String> certifDiscapacidad = this.fichasocioeconDao.reportByCertificadoDiscapacidadTipo(cadenaanio);
        Object[] cantidad = cantCertDiscapacidad.toArray();
        Object[] tipo = certifDiscapacidad.toArray();
        return new ResponseArray(cantidad,tipo);
    }
    //    @Override
//    public VisitaDomiciliaria save(VisitaDomiciliaria visitaDomiciliaria) {
//        return this.visitaDomiciliariaDao.save(visitaDomiciliaria);
//    }
//
//    @Override
//    public List<VisitaDomiciliaria> findAllVisitaDomiciliaria() {
//        return this.visitaDomiciliariaDao.findAll();
//    }
}
