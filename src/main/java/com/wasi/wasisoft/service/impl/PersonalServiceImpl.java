package com.wasi.wasisoft.service.impl;


import com.wasi.wasisoft.dao.PersonalDao;
import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.model.User;
import com.wasi.wasisoft.service.PersonalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonalServiceImpl implements PersonalService {

    @Autowired
    protected PersonalDao personalDao;

    @Override
    public Personal save(Personal personal) {
        return this.personalDao.save(personal);
    }

    @Override
    public List<Personal> findAll() {
        return this.personalDao.findAll();
    }

    @Override
    public void deletePersonal(Long id) {
        this.personalDao.deleteById(id);
    }

    @Override
    public List<Personal> findPersonalByAreaRehabilitacion() {
        return this.personalDao.findPersonalByAreaRehabilitacion();
    }

    @Override
    public List<Personal> findAllByIdarea(Area idarea) {
        return  this.personalDao.findAllByIdarea(idarea);
    }

    @Override
    public List<Personal> listPersonalActivo() {
        return this.personalDao.listPersonalActive();
    }

      @Override
    public List<Personal> listPersonalInactivo() {
        return this.personalDao.listPersonalInactive();
    }

    @Override
    public Personal obtenerPersonalPorUsuario(String username) {
        return this.personalDao.obtenerPersonalPorUsuario(username);
    }

    @Override
    public Personal getPersonalByUser(User iduser) {
        return personalDao.getPersonalByUser(iduser);
    }
}
