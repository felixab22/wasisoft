package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.AnamnesisClinicoDao;
import com.wasi.wasisoft.model.AnamnesisClinico;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.AnamnesisClinicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnamnesisClinicoServiceImpl implements AnamnesisClinicoService{

    @Autowired
    protected AnamnesisClinicoDao anamnesisClinicoDao;

    @Override
    public AnamnesisClinico save(AnamnesisClinico anamnesisClinico) {
        return this.anamnesisClinicoDao.save(anamnesisClinico);
    }

    @Override
    public List<AnamnesisClinico> findByAnamnesisClinicoByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica) {
        return this.anamnesisClinicoDao.findByIdfichaterapiafisica(idfichaterapiafisica);
    }

    @Override
    public void delete(Long idanamnesisclinico) {
        this.anamnesisClinicoDao.deleteById(idanamnesisclinico);
    }
}
