package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.NotificacionDao;
import com.wasi.wasisoft.model.Notificacion;
import com.wasi.wasisoft.service.NotificacionService;
import com.wasi.wasisoft.util.ResponseArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by airo on 23/01/2019.
 */
@Service
public class NotificacionServiceImpl implements NotificacionService{

    @Autowired
    protected NotificacionDao notificacionDao;

    @Override
    public Notificacion save(Notificacion notificacion) {
        return this.notificacionDao.save(notificacion);
    }

    @Override
    public List<Notificacion> findAll() {
        return this.notificacionDao.findAll();
    }

    @Override
    public void deleteNotificacion(Long idnotificacion) {
        this.notificacionDao.deleteById(idnotificacion);
    }

    @Override
    public List<Notificacion> CantidadInasistencias(String nombrepaciente) {
        return this.notificacionDao.findAllByNombrepaciente(nombrepaciente);
    }

    @Override
    public List<Notificacion> reportByInasistencias() {
      return this.notificacionDao.findAll();
    }
}
