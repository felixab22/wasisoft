package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.ResultadoEscalaDao;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.ResultadoEscala;
import com.wasi.wasisoft.service.ResultadoEscalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@Service
public class ResultadoEscalaServiceImpl implements ResultadoEscalaService {

    @Autowired
    protected ResultadoEscalaDao resultadoEscalaDao;

    @Override
    public ResultadoEscala saveResultadoEscala(ResultadoEscala resultadoEscala) {
        return this.resultadoEscalaDao.save(resultadoEscala);
    }

    @Override
    public void deleteResultadoEscala(Long idResultado) {
        this.resultadoEscalaDao.deleteById(idResultado);
    }

    @Override
    public List<ResultadoEscala> getResultadoEscalaPaciente(FichaTerapiaFisica idfichaterapiafisica, String anio) {
        return this.resultadoEscalaDao.findByFichaTerapiaFisica(idfichaterapiafisica, anio);
    }

    @Override
    public List<ResultadoEscala> ListResultadoEscalaPorFechas(FichaTerapiaFisica idficha) {
        return this.resultadoEscalaDao.listaResultadoEscalaPorFechas(idficha);
    }
}
