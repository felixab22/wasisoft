package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.EspasticidadDao;
import com.wasi.wasisoft.model.Espasticidad;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.EspasticidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */

@Service
public class EspasticidadServiceImpl implements EspasticidadService {

    @Autowired
    protected EspasticidadDao espasticidadDao;

    @Override
    public Espasticidad saveEspasticidad(Espasticidad espasticidad) {
        return this.espasticidadDao.save(espasticidad);
    }

    @Override
    public void deleteEspasticidad(Long idespasticidad) {
        this.espasticidadDao.deleteById(idespasticidad);
    }

    @Override
    public List<Espasticidad> ListarEspasticidadPorAnios(FichaTerapiaFisica idficha) {
        return this.espasticidadDao.listEspasticidadPorFechas(idficha);
    }

    @Override
    public List<Espasticidad> getEspasticidadPaciente(FichaTerapiaFisica idfichaterapiafisica, String anio) {
        return this.espasticidadDao.findByFichaTerapiaFisica(idfichaterapiafisica,anio);
    }
}
