package com.wasi.wasisoft.service.impl;

//import org.jvnet.hk2.annotations.Service;

import com.wasi.wasisoft.dao.AsistenciaDao;
import com.wasi.wasisoft.model.Asistencia;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.service.AsistenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by airo on 22/01/2019.
 */
@Service
public class AsistenciaServiceImpl  implements AsistenciaService{

    @Autowired
    private AsistenciaDao asistenciaDao;

    @Override
    public Asistencia saveAsistencia(Asistencia asistencia) {
        return this.asistenciaDao.save(asistencia);
    }

    @Override
    public List<Asistencia> ListarAsistencia(String paciente) {
        return this.asistenciaDao.findAllByPaciente(paciente);
    }

    @Override
    public List<Asistencia> ListarAsistenciaPorPersonal(Personal idpersonal) {
        return this.asistenciaDao.findAllByPersonal(idpersonal);
    }


    @Override
    public List<Asistencia> ListarInasistencias(String paciente) {
        return this.asistenciaDao.listarInasistencias(paciente);

    }
    @Override
    public List<Asistencia> findAll() {
        return this.asistenciaDao.findAllAsistencia();
    }

//
//    @Override
//    public void deleteInasistenecia(Long idinasistencia) {
//        this.asistenciaDao.deleteById(idinasistencia);
//    }
}
