package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.MovilidadArticulacionesDao;
import com.wasi.wasisoft.dao.MovilidadDao;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Movilidad;
import com.wasi.wasisoft.model.MovilidadArticulaciones;
import com.wasi.wasisoft.service.MovilidadArticulacionesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

@Service
public class MovilidadArticulacionesServiceImpl implements MovilidadArticulacionesService {

    @Autowired
    protected MovilidadArticulacionesDao movilidadArticulacionesDao;

    @Autowired
    protected MovilidadDao movilidadDao;

    @Override
    public MovilidadArticulaciones save(MovilidadArticulaciones movilidadArticulaciones) {
        return this.movilidadArticulacionesDao.save(movilidadArticulaciones);
    }

    @Override
    public List<MovilidadArticulaciones> getMovilidadArticulacionesPaciente(FichaTerapiaFisica idfichaTerapiaFisica, String anio) {
        return this.movilidadArticulacionesDao.findByIdfichaterapiafisica(idfichaTerapiaFisica, anio);
    }

    @Override
    public void deleteMovilidadArticulaciones(Long idmovilidad) {
        this.movilidadArticulacionesDao.deleteById(idmovilidad);
    }

    @Override
    public List<MovilidadArticulaciones> listarMovilidadArticulacionesPorFechas(FichaTerapiaFisica idficha) {
        return this.movilidadArticulacionesDao.listMovilidadArticulacionesPorFechas(idficha);
    }

    @Override
    public Movilidad saveMovilidadPaciente(Movilidad movilidad) {
        return this.movilidadDao.save(movilidad);
    }

    @Override
    public void deleteMovilidadPaciente(Long idmovilidad) {
        this.movilidadDao.deleteById(idmovilidad);
    }

    @Override
    public List<Movilidad> getMovilidadSeguimiento(MovilidadArticulaciones movilidadArticulaciones) {
        return this.movilidadDao.findAllByIdmovarticulaciones(movilidadArticulaciones);
    }

//    @Override
//    public List<Movilidad> getMovilidadSeguimientoUno() {
//        return this.movilidadDao.getMovilidadSeguimientoUno();
//    }
//
//    @Override
//    public List<Movilidad> getMovilidadSeguimientoDos() {
//        return this.movilidadDao.getMovilidadSeguimientoDos();
//    }
//
//    @Override
//    public List<Movilidad> getMovilidadSeguimientoTres() {
//        return this.movilidadDao.getMovilidadSeguimientoTres();
//    }


//    public List<String> getTokens(String str) {
//        List<String> tokens = new ArrayList<>();
//        StringTokenizer tokenizer = new StringTokenizer(str, ",");
//        while (tokenizer.hasMoreElements()) {
//            tokens.add(tokenizer.nextToken());
//        }
//        return tokens;
//    }
}
