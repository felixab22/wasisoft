package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.MedicamentoDao;
import com.wasi.wasisoft.dao.MedicamentoVentaDao;
import com.wasi.wasisoft.model.Medicamento;
import com.wasi.wasisoft.model.MedicamentoVenta;
import com.wasi.wasisoft.service.MedicamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicamentoServiceImpl implements MedicamentoService{

    @Autowired
    protected MedicamentoDao medicamentoDao;

    @Autowired
    protected MedicamentoVentaDao medicamentoVentaDao;
    @Override
    public Medicamento save(Medicamento medicamento) {
        return this.medicamentoDao.save(medicamento) ;
    }

    @Override
    public List<Medicamento> findAll() {
        return this.medicamentoDao.findAll();
    }

    @Override
    public void deleteMedicamento(Long id) {
        this.medicamentoDao.deleteById(id);
    }

    @Override
    public MedicamentoVenta saveMedicamentoVenta(MedicamentoVenta medicamentoVenta) {
        return this.medicamentoVentaDao.save(medicamentoVenta);
    }

    @Override
    public void deleteMedicamentoVenta(Long idmedicamentoventa) {
        this.medicamentoVentaDao.deleteById(idmedicamentoventa);
    }

    @Override
    public List<MedicamentoVenta> getAllVentaMedicamento() {
        return this.medicamentoVentaDao.findAll();
    }
}
