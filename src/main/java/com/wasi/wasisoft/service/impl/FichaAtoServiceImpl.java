package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.FichaAtoDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaAto;
import com.wasi.wasisoft.service.FichaAtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Nelly on 26/04/2018.
 */
@Service
public class FichaAtoServiceImpl implements FichaAtoService {

    @Autowired
    protected FichaAtoDao fichaAtoDao;

    @Override
    public FichaAto saveFichAto(FichaAto fichaAto) {
        return this.fichaAtoDao.save(fichaAto);
    }

    @Override
    public void deleteFichaAto(Long idfichaato) {
        this.fichaAtoDao.deleteById(idfichaato);
    }

    @Override
    public List<FichaAto> getFichaAtoPaciente(Expediente idexpediente, String anio) {
        return this.fichaAtoDao.findByIdexpediente(idexpediente, anio);
    }

    @Override
    public List<FichaAto> ListarFichaAtoPacientePorFecha(Expediente idexpediante) {
        return this.fichaAtoDao.listarFichaAtoPorFechas(idexpediante);
    }
}
