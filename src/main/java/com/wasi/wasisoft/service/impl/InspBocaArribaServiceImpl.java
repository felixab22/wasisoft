package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.InspBocaArribaDao;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.InspBocaArriba;
import com.wasi.wasisoft.service.InspBocaArribaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class InspBocaArribaServiceImpl implements InspBocaArribaService {

    @Autowired
    protected InspBocaArribaDao inspBocaArribaDao;

    private Calendar calendario = Calendar.getInstance();
    private  Integer anio = calendario.get(Calendar.YEAR);
    private String cadenaanio = Integer.toString(anio);

    @Override
    public InspBocaArriba save(InspBocaArriba inspBocaArriba) {
        return this.inspBocaArribaDao.save(inspBocaArriba);
    }

    @Override
    public List<InspBocaArriba> findInspBocaArribaByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapia, String anio) {
        return this.inspBocaArribaDao.findByIdfichaterapiafisicaActual(idfichaterapia,anio);
    }

    @Override
    public void deleteInspeccionBocaArriba(Long idinspeccion) {
        this.inspBocaArribaDao.deleteById(idinspeccion);
    }

    @Override
    public List<InspBocaArriba> listarFichasInspecionPasados(FichaTerapiaFisica idfichaterapia) {
         return  this.inspBocaArribaDao.listInspeccionBocaArribaPorFechas(idfichaterapia, cadenaanio);
    }
}
