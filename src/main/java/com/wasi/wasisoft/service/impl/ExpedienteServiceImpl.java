package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.ExpedienteDao;
import com.wasi.wasisoft.dao.UbigeoDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Ubigeo;
import com.wasi.wasisoft.service.ExpedienteService;
import com.wasi.wasisoft.util.ResponseArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpedienteServiceImpl implements ExpedienteService {

    @Autowired
    protected ExpedienteDao expedienteDao;

    @Autowired
    protected UbigeoDao ubigeoDao;

    @Override
    public Expediente save(Expediente expediente) {
        return this.expedienteDao.save(expediente);
    }

    @Override
    public List<Expediente> findAll() {
        return this.expedienteDao.findAll();
    }

    @Override
    public void deleteExpediente(Long id_expediente) {
        this.expedienteDao.deleteById(id_expediente);
    }

    @Override
    public List<Expediente> findByCodigohistorial(String codigoHistoria) {
        return this.expedienteDao.findByCodigohistorial(codigoHistoria);
    }

    @Override
    public List<Expediente> findByDni(int dni) {
        return this.expedienteDao.findByDni(dni);
    }

    @Override
    public ResponseArray reporteByDiagnostico() {
        List<Integer> cantidadDiagnostico= this.expedienteDao.reportByDiagnosticoCantidad();
        List<String> tipoDiagnoostico = this.expedienteDao.reportByDiagnosticoTipo();
        Object[] cantidad = cantidadDiagnostico.toArray();
        Object[] tipo = tipoDiagnoostico.toArray();
        return new ResponseArray(cantidad,tipo);
    }
    @Override
    public ResponseArray reporteByEdadPaciente() {
        List<Integer> cantidadEdad= this.expedienteDao.reportByEdadCantidad();
        List<String> tipoEdad = this.expedienteDao.reportByEdadTipo();
        Object[] cantidad = cantidadEdad.toArray();
        Object[] tipo = tipoEdad.toArray();
        return new ResponseArray(cantidad,tipo);
    }
    @Override
    public ResponseArray reporteBySexoPaciente() {
        List<Integer> cantidadSexo= this.expedienteDao.reportBySexoCantidad();
        List<String> tipoSexo = this.expedienteDao.reportBySexoTipo();
        Object[] cantidad = cantidadSexo.toArray();
        Object[] tipo = tipoSexo.toArray();
        return new ResponseArray(cantidad,tipo);
    }


//    @Override
//    public List<Ubigeo> findById_depa(String id_depa) {
//        return this.ubigeoDao.findById_depa(id_depa);
//    }

//    @Override
//    public List<Expediente> findByNombreAndPrimer_apellido(String nombre, String primerapellido) {
//        return this.expedienteDao.findByNombreAndPrimer_apellido(nombre, primerapellido);
//    }
}
