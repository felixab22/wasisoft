package com.wasi.wasisoft.service.impl;

import com.wasi.wasisoft.dao.ReferenciaExternaDao;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ReferenciaExterna;
import com.wasi.wasisoft.service.ReferenciaExternaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReferenciaExternaServiceImpl implements ReferenciaExternaService{

    @Autowired
    protected ReferenciaExternaDao referenciaExternaDao;

    @Override
    public ReferenciaExterna save(ReferenciaExterna referenciaExterna) {
        return this.referenciaExternaDao.save(referenciaExterna);
    }

    @Override
    public List<ReferenciaExterna> findReferenciaExternaByIdexpediente(Expediente idexpediente) {
        return this.referenciaExternaDao.findByIdexpediente(idexpediente);
    }

    @Override
    public void deleteReferenciaExterna(Long idreferenciaexterna) {
        this.referenciaExternaDao.deleteById(idreferenciaexterna);
    }
}
