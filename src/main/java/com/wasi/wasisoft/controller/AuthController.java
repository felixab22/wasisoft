package com.wasi.wasisoft.controller;

import com.wasi.wasisoft.dao.RoleRepository;
import com.wasi.wasisoft.dao.UserRepository;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.model.Role;
import com.wasi.wasisoft.model.RoleName;
import com.wasi.wasisoft.model.User;
import com.wasi.wasisoft.payload.JwtAuthenticationResponse;
import com.wasi.wasisoft.payload.LoginRequest;
import com.wasi.wasisoft.payload.SignUpRequest;
import com.wasi.wasisoft.security.JwtTokenProvider;
import com.wasi.wasisoft.service.PersonalService;
import com.wasi.wasisoft.util.ResponseMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by HeverFernandez on 02/08/17.
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private PersonalService personalService;

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        //LOGGER.info(" PARAMS: '"+ loginRequest.toString() + "'");
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );
        LOGGER.info(" PARAMETROS DE LOGUEO: <<<<<<<>>>>>> '"+ loginRequest.getUsernameOrEmail() + "'");
        LOGGER.info(" PARAMETROS DE LOGUEO: <<<<<<<>>>>>> '"+ loginRequest.getPassword() + "'");

        Personal personal = this.personalService.obtenerPersonalPorUsuario(loginRequest.getUsernameOrEmail());

        LOGGER.info(" PARAMETROS DE PERSONAL : <<<<<<<>>>>>> '"+ personal.getNombrepersonal() + "'");
        LOGGER.info(" PARAMETROS DE PERSONAL : <<<<<<<>>>>>> '"+ personal.getAppmaterno() + "'" + personal.getApppaterno());

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, userDetails.getUsername(),userDetails.getAuthorities(),personal));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already in use!"),
                    HttpStatus.BAD_REQUEST);
        }
        LOGGER.info(" PARAMETROS SIGNUP: '"+ signUpRequest.toString() + "'");

        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
                passwordEncoder.encode(signUpRequest.getPassword()));

//       Set<String> strRoles = signUpRequest.getRole();

        Integer strRoles = signUpRequest.getRole();

        long valorRol = strRoles.longValue();

        Set<Role> roles = new HashSet<>();

        LOGGER.info(" PARAMETROS DE USUARIO : '"+ user.toString() + "'" + "Su rol es " + strRoles);

//        strRoles.forEach(role -> {
        switch (strRoles) {
            case 5:

                Role adminRole = roleRepository.findByDescripcionrol(RoleName.Administradora)
//                      Role adminRole = roleRepository.findByIdrol(valorRol);
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));

                String result = adminRole.getDescripcionrol().name();

                roles.add(adminRole);

                break;

            case 6:
                Role centroRole = roleRepository.findByDescripcionrol(RoleName.Jefe_de_social)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(centroRole);

                break;
            case 7:
                Role educaRole = roleRepository.findByDescripcionrol(RoleName.Jefe_centro_de_día)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(educaRole);

                break;
            case 8:
                Role fisioRole = roleRepository.findByDescripcionrol(RoleName.Jefe_de_educación)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(fisioRole);

                break;
            case 9:
                Role psicoRole = roleRepository.findByDescripcionrol(RoleName.Jefe_de_salud)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(psicoRole);

                break;
            case 10:
                Role saludRole = roleRepository.findByDescripcionrol(RoleName.Jefe_de_psicología)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(saludRole);

                break;
            case 11:
                Role socialRole = roleRepository.findByDescripcionrol(RoleName.Jefe_de_terapia_ocupacional)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(socialRole);

                break;
            case 12:
                Role ingresosRole = roleRepository.findByDescripcionrol(RoleName.Jefe_de_rehabilitación)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(ingresosRole);

                break;
            case 13:
                Role vatoRole = roleRepository.findByDescripcionrol(RoleName.Generador_de_ingresos)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(vatoRole);

                break;
            case 15:
                Role vcdiaRole = roleRepository.findByDescripcionrol(RoleName.Voluntario_terapia_ocupacional)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(vcdiaRole);

                break;
            case 16:
                Role vrehaRole = roleRepository.findByDescripcionrol(RoleName.Voluntario_rehabilitación)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(vrehaRole);

                break;
            case 17:
                Role vcraRole = roleRepository.findByDescripcionrol(RoleName.Voluntario_psicología)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(vcraRole);

                break;
            case 18:
                Role vpsicoRole = roleRepository.findByDescripcionrol(RoleName.Voluntario_centro_de_día)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(vpsicoRole);

                break;
            case 19:
                Role vcomuRole = roleRepository.findByDescripcionrol(RoleName.Voluntario_comunicación)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(vcomuRole);

                break;
            default:
                Role atoRole = roleRepository.findByDescripcionrol(RoleName.Sistema)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                roles.add(atoRole);

                break;

        }
        user.setRoles(roles);
        userRepository.save(user);

        return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
    }

}
