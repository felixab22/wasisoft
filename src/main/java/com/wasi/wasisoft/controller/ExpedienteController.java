package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.service.ExpedienteService;
import com.wasi.wasisoft.util.ResponseArray;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class ExpedienteController {

    @Autowired
    protected ExpedienteService expedienteService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateExpediente", method = RequestMethod.POST)
    public RestResponse saveOrUpdateExpediente(@RequestBody String expedienteJson)
            throws IOException {

        this.objectMapper = new ObjectMapper();

        Expediente expediente = this.objectMapper.readValue(expedienteJson, Expediente.class);

        List<Expediente> result = this.expedienteService.findByCodigohistorial(expediente.getCodigohistorial());

        if (result.size() == 0){

            this.expedienteService.save(expediente);

            return new RestResponse(HttpStatus.OK.value(), "Expediente se guardo correctamente");
        }
        return new RestResponse(HttpStatus.BAD_REQUEST.value(), "Codigo de historial duplicado");
    }

    @RequestMapping(value = "/getExpediente", method = RequestMethod.GET)
    public List<Expediente> getExpediente() {
        return this.expedienteService.findAll();
    }

    @RequestMapping(value = "/getByCodigohistorial", method = RequestMethod.GET)
    public List<Expediente> findByCodigohistorial(@RequestParam String codigo){
        return  this.expedienteService.findByCodigohistorial(codigo);
    }

    @RequestMapping(value = "/deleteExpediente", method = RequestMethod.POST)
    public void deleteExpediente(@RequestBody String expedienteJson) throws Exception {
        this.objectMapper = new ObjectMapper();
        Expediente expediente = this.objectMapper.readValue(expedienteJson, Expediente.class);
        if (expediente.getId_expediente() == null) {
            System.out.println("Registro de expediente no encontrado");
        } else {
            this.expedienteService.deleteExpediente(expediente.getId_expediente());
        }
    }
    
//--Busca el expediente por dni y retorna el expediente encontrado--
    @RequestMapping(value = "/getExpedienteByDni", method = RequestMethod.GET)
    public List<Expediente> getExpedienteByDni(@RequestParam int dni) {
        return this.expedienteService.findByDni(dni);
    }

    @GetMapping(value = "/reporteByDiagnostico")
    public ResponseArray reporteByTipoFamilia(){
        return this.expedienteService.reporteByDiagnostico();
    }
    @GetMapping(value = "/reporteByEdadPaciente")
    public ResponseArray reporteByEdadPaciente(){
        return this.expedienteService.reporteByEdadPaciente();
    }
    @GetMapping(value = "/reporteBySexoPaciente")
    public ResponseArray reporteBySexoPaciente(){
        return this.expedienteService.reporteBySexoPaciente();
    }

    private boolean validateEstado(Expediente expediente) {
        boolean isValid = true;

        if (expediente.getEstadoexpediente().equals(false)) {isValid = false;
        }
        return isValid;
    }
}