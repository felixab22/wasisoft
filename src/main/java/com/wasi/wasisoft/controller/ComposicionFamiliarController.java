package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.ComposicionFamiliar;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.service.ComposicionFamiliarService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class ComposicionFamiliarController {

    @Autowired
    protected ComposicionFamiliarService composicionFamiliarService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/getComposicionFamiliar", method = RequestMethod.GET)
    List<ComposicionFamiliar> findAll(){
        return  this.composicionFamiliarService.findAll();
    }

    @RequestMapping(value = "/saveOrUpdateComposicionFamiliar", method = RequestMethod.POST)
    public RestResponse saveOrUpdateComposicionFamiliar(@RequestBody String composicionFamiliarJson)
            throws JsonParseException, JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        ComposicionFamiliar composicionFamiliar = this.objectMapper.readValue(composicionFamiliarJson, ComposicionFamiliar.class);

        this.composicionFamiliarService.save(composicionFamiliar);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");

    }

    @RequestMapping(value = "/deleteComposicionFamiliar", method = RequestMethod.POST)
    public void deleteComposicionFamiliar(@RequestBody String composicionFamiliarJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        ComposicionFamiliar composicionFamiliar = this.objectMapper.readValue(composicionFamiliarJson, ComposicionFamiliar.class);

        if (composicionFamiliar.getId_compfamiliar() == null) {
            throw new Exception("El id esta nulo");
        }
        this.composicionFamiliarService.deleteComposicionFamiliar(composicionFamiliar.getId_compfamiliar());
    }

    @RequestMapping(value = "/getComposicionFamiliarByIdexpediente", method = RequestMethod.GET)
    public List<ComposicionFamiliar> getComposicionFamiliarByIdexpediente(@RequestParam Expediente idexpediente){
        return  this.composicionFamiliarService.findByIdexpediente(idexpediente);
    }
}
