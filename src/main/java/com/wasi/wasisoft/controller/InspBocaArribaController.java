package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.InspBocaArriba;
import com.wasi.wasisoft.service.InspBocaArribaService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

@RestController
public class InspBocaArribaController {

    @Autowired
    protected InspBocaArribaService inspBocaArribaService;

    @Autowired
    protected ObjectMapper objectMapper;

    private Calendar calendario = Calendar.getInstance();
    private  Integer anio = calendario.get(Calendar.YEAR);
    Long aniolong = new Long(anio);

    @RequestMapping(value = "/saveOrInspBocaArriba", method = RequestMethod.POST)
    public RestResponse saveOrInspBocaArriba(@RequestBody String inspBocaArribaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        InspBocaArriba inspBocaArriba = this.objectMapper.readValue(inspBocaArribaJson, InspBocaArriba.class);
//        List<InspBocaArriba> result = inspBocaArribaService.findInspBocaArribaByIdfichaterapiafisica(inspBocaArriba.getIdfichaterapiafisica());

//        System.out.println("RESULTADO " + result);

//        if(result.size() == 0) {//no existe ficha de inspecion biometrica
            this.inspBocaArribaService.save(inspBocaArriba);
            return new RestResponse(HttpStatus.OK.value(),"Nueva ficha de inspecion registrado correctamente");
//        }else
//        return new RestResponse(HttpStatus.OK.value(), "La ficha se ha actualizado correctamente");
    }

    @RequestMapping(value = "/deleteInspBocaArriba", method = RequestMethod.POST)
    public void deleteBiometriaPaciente(@RequestBody String inspBocaArribaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        InspBocaArriba inspBocaArriba = this.objectMapper.readValue(inspBocaArribaJson, InspBocaArriba.class);

        if (inspBocaArriba.getIdinspbocaarriba() == null) {
            throw new Exception("El id esta nulo");
        }
        this.inspBocaArribaService.deleteInspeccionBocaArriba(inspBocaArriba.getIdinspbocaarriba());
    }

    @RequestMapping(value = "/getInspBocaArribaPaciente", method = RequestMethod.GET)
    public List<InspBocaArriba> getInspBocaArribaPaciente(@RequestParam FichaTerapiaFisica idfichaterapiafisica,
                                                          @RequestParam String anio) {
        System.out.println(" PARAMETROS DE ENTRADA :  " + anio + " FICHA ID : " + idfichaterapiafisica );
        return this.inspBocaArribaService.findInspBocaArribaByIdfichaterapiafisica(idfichaterapiafisica, anio);
    }

    @GetMapping("/ListarInspeccionBocaArribaPorAnios")
    List<InspBocaArriba> ListarInspeccionBocaArribaPorAnios(@RequestParam FichaTerapiaFisica idfichaterapia){
        return this.inspBocaArribaService.listarFichasInspecionPasados(idfichaterapia);
    }
}
