package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.FichaTerapiaFisicaService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class FichaTerapiaFisicaController {

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @Autowired
    protected FichaTerapiaFisicaService fichaTerapiaFisicaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateFichaTerapiaFisica", method = RequestMethod.POST)
    public RestResponse saveOrUpdateFichaTerapiaFisica(@RequestBody String fichaTerapiaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + fichaTerapiaJson + "'");

        FichaTerapiaFisica fichaTerapiaFisica = this.objectMapper.readValue(fichaTerapiaJson, FichaTerapiaFisica.class);

        this.fichaTerapiaFisicaService.save(fichaTerapiaFisica);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

//    @RequestMapping(value = "/findAllPacienteNoAtendido", method = RequestMethod.GET)
//    public List<FichaTerapiaFisica> findAllPacienteNoAtendido() {
//        return this.fichaTerapiaFisicaService.findAllPacienteNoAtendido();
//    }
//
    @RequestMapping(value = "/getPacientesAtendidos", method = RequestMethod.GET)
    public List<FichaTerapiaFisica> getPacientesAtendidos(){
        return this.fichaTerapiaFisicaService.ListarPacientesAtedidos();
    }

    @RequestMapping(value = "/getFichaTerapiaFisicaByIdExpediente", method = RequestMethod.GET)
    List<FichaTerapiaFisica> getFichaTerapiaFisicaByIdExpediente(@RequestParam Expediente idexpediente){
        return this.fichaTerapiaFisicaService.obtenerFichaTerapiaFisica(idexpediente);
    }

    @RequestMapping(value = "/deleteFichaTerapaFisica", method = RequestMethod.POST)
    public void deleteFichaTerapaiFisica(@RequestBody String fichaTerapiaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        FichaTerapiaFisica fichaTerapiaFisica = this.objectMapper.readValue(fichaTerapiaJson, FichaTerapiaFisica.class);

        if (fichaTerapiaFisica.getIdfichaterapiafisica() == null) {
            throw new Exception("El id esta nulo");
        }
        this.fichaTerapiaFisicaService.deleteFichaTerapiaFisica(fichaTerapiaFisica.getIdfichaterapiafisica());
    }
}
