package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.CitaTerapia;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.service.CitaTerapiaService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@RestController
public class CitaTerapiaController {

    @Autowired
    protected CitaTerapiaService citaTerapiaService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @RequestMapping(value="/saveOrUpdateCitaTerapia", method = RequestMethod.POST)
    public RestResponse saveOrUpdateCitaTerapia(@RequestBody String citaTerapiaJson)
        throws JsonParseException, JsonMappingException, IOException {
            this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '"+ citaTerapiaJson + "'");

        CitaTerapia citaTerapia= this.objectMapper.readValue(citaTerapiaJson, CitaTerapia.class);

       // List<CitaTerapia> result = citaTerapiaService.getCitaTerapiaPaciente(citaTerapia.getIdexpediente());

        //System.out.println("RESULTADO " + result);

        //if(result.size() == 0) {//no existe cita programada para un paciente

                this.citaTerapiaService.save(citaTerapia);

                return new RestResponse(HttpStatus.OK.value(), "La cita se ha guardado correctamente");
          //   }

        //else {
          //  if(citaTerapia.getIdcitaterapia() == null){
            //    return new RestResponse(HttpStatus.EXPECTATION_FAILED.value(),"Una cita esta programada para el paciente.");
            //}

            //this.citaTerapiaService.save(citaTerapia);
            //return new RestResponse(HttpStatus.OK.value(), "La cita se ha modificado correctamente");
        //}
    }

    @RequestMapping(value = "/getCitaTerapiaNoAtendidas", method = RequestMethod.GET)
    public List<CitaTerapia>  getCitaTerapiaNoAtendidas(){

        return  this.citaTerapiaService.ListarCitasNoAtendidas();
    }

    @Transactional
    @PostMapping("/deleteAgendaByPersonal")
    public RestResponse deleteCitaTerapia(@RequestParam Personal idpersonal) throws Exception {
        this.objectMapper = new ObjectMapper();

        System.out.println(" Datos recibidos   :  " + idpersonal );

        boolean result = this.citaTerapiaService.deleteCitaTerapia(idpersonal);
        if (!result){
            return new RestResponse(HttpStatus.OK.value(), "La agenda para el personal se ha eliminado correctamente");
        }else
            return new RestResponse(HttpStatus.BAD_REQUEST.value(), "La agenda para el personal no se ha podeido eliminar");
    }

    @GetMapping("/getAgendaPersonal")
    List<CitaTerapia> getAgendaPersonal(@RequestParam Personal idpersonal){
        return this.citaTerapiaService.getAgendaAtencionPorPersonal(idpersonal);
    }
 }
