package com.wasi.wasisoft.controller;

import java.io.IOException;
import java.util.List;

import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.model.User;
import com.wasi.wasisoft.service.PersonalService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class PersonalController {

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);
    @Autowired
    protected PersonalService personalService;

    @Autowired
    private ObjectMapper mapper;

    @RequestMapping(value = "/saveOrUpdatePersonal", method = RequestMethod.POST)
    public RestResponse saveOrUpdate(@RequestBody String personaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.mapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '"+ personaJson + "'");

        Personal personal = this.mapper.readValue(personaJson, Personal.class);

        if (!this.validate(personal)) {
            return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(),
                    "Los campos obligatorios no estan diligenciados");
        }
        this.personalService.save(personal);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getPersonal", method = RequestMethod.GET)
    public List<Personal> getPersonal() {
        return this.personalService.findAll();
    }

    @RequestMapping (value = "/getPersonalByAreaRehabilitacion", method = RequestMethod.GET)
    public List<Personal> getPersonalByAreaRehabilitacion(){
        return this.personalService.findPersonalByAreaRehabilitacion();
    }

    @RequestMapping(value = "/getPersonalByIdarea", method = RequestMethod.GET)
    public List<Personal> getPersonalByIdarea(@RequestParam Area idarea){
        return this.personalService.findAllByIdarea(idarea);
    }

    @RequestMapping(value = "/deletePersonal", method = RequestMethod.POST)
    public void deletePersonal(@RequestBody String personaJson) throws Exception {
        this.mapper = new ObjectMapper();

        Personal personal = this.mapper.readValue(personaJson, Personal.class);

        if (personal.getId_personal() == null) {
            throw new Exception("El id esta nulo");
        }
        this.personalService.deletePersonal(personal.getId_personal());
    }

    @RequestMapping(value = "/listPersonalActivos", method = RequestMethod.GET)
    public List<Personal> listPersonalActivos(){
        return this.personalService.listPersonalActivo();
    }

    @RequestMapping(value = "/listPersonalInactivos", method = RequestMethod.GET)
    public List<Personal> listPersonalInactivos(){
        return this.personalService.listPersonalInactivo();
    }

    @GetMapping("/ObtenerPersonalPorSuUsuario")
    public Personal ObtenerPersonalPorSuUsuario(@RequestParam User iduser){
        return this.personalService.getPersonalByUser(iduser);
    }

    private boolean validate(Personal personal) {
        boolean isValid = true;

        if (StringUtils.trimToNull(personal.getNombrepersonal()) == null) {
            isValid = false;
        }

        if (StringUtils.trimToNull(personal.getApppaterno()) == null) {
            isValid = false;
        }
//
//        if (StringUtils.trimToNull(personal.getAppmaterno()) == null) {
//            isValid = false;
//        }
        return isValid;
    }
}
