package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.InstrEvaluacion;
import com.wasi.wasisoft.service.InstrEvaluacionService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 03/05/2018.
 */
@RestController
public class InstrEvaluacionController {

    @Autowired
    protected InstrEvaluacionService instrEvaluacionService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @PostMapping("/saveOrUpdateInstrEvaluacion" )
    public RestResponse saveOrUpdateInstrEvaluacion(@RequestBody String instrEvalucionJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + instrEvalucionJson + "'");

        InstrEvaluacion instrEvaluacion = this.objectMapper.readValue(instrEvalucionJson, InstrEvaluacion.class);

        this.instrEvaluacionService.saveInstrEvaluacion(instrEvaluacion);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa, se ha registrado el instrumento de evaluacion");
    }

    @PostMapping("/deleteInstrumentoEvaluacion")
    public void deleteInstrumentoEvaluacion (@RequestBody String instrEvalucionJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        InstrEvaluacion instrEvaluacion = this.objectMapper.readValue(instrEvalucionJson, InstrEvaluacion.class);

        if (instrEvaluacion.getIdinstrevaluacion() == null) {
            throw new Exception("El id esta nulo");
        }
        this.instrEvaluacionService.deleteInstrEvaluacion(instrEvaluacion.getIdinstrevaluacion());
    }

    @GetMapping( "/getInstrumentosDeEvaluacion")
    List<InstrEvaluacion> getInstrumentosDeEvaluacion (){
        return this.instrEvaluacionService.getInstrumentosEvaluacion();
    }
}
