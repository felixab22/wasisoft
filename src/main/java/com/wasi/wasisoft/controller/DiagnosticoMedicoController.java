package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.DiagnosticoMedico;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.DiagnosticoMedicoService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class DiagnosticoMedicoController {

    @Autowired
    protected DiagnosticoMedicoService diagnosticoMedicoService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateDiagnosticoMedico", method = RequestMethod.POST)
    public RestResponse saveOrUpdateDiagnosticoMedico(@RequestBody String diagnosticoMedicoJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        DiagnosticoMedico diagnosticoMedico = this.objectMapper.readValue(diagnosticoMedicoJson, DiagnosticoMedico.class);

        this.diagnosticoMedicoService.save(diagnosticoMedico);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/deleteDiagnosticoMedico", method = RequestMethod.POST)
    public void deleteDiagnosticoMedico(@RequestBody String diagnostcoMedicoJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        DiagnosticoMedico diagnosticoMedico = this.objectMapper.readValue(diagnostcoMedicoJson, DiagnosticoMedico.class);

        if (diagnosticoMedico.getIddiagnosticomedico() == null) {
            throw new Exception("El id esta nulo");
        }
        this.diagnosticoMedicoService.deleteDiagnosticoMedico(diagnosticoMedico.getIddiagnosticomedico());
    }

    @RequestMapping(value = "/getDiangnosticoMedicoByIdFichaTerapiaFisica", method = RequestMethod.GET)
    public List<DiagnosticoMedico> getDiangnosticoMedicoByIdFichaTerapiaFisica(@RequestParam FichaTerapiaFisica idfichaterapiafisica){
        return this.diagnosticoMedicoService.getDiagnosticoMedico(idfichaterapiafisica);
    }
}
