package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.DiagnosticoFinal;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.PlanTrabajo;
import com.wasi.wasisoft.service.DiagnosticoFinalService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 17/04/2018.
 */
@RestController
public class DiagnosticoFinalController {

    @Autowired
    protected DiagnosticoFinalService diagnosticoFinalService;

    @Autowired
    protected ObjectMapper objectMapper;


    @PostMapping("/saveOrDiagnosticoFinalPaciente")
    public RestResponse saveOrDiagnosticoFinalPaciente(@RequestBody String diagnosticoFinalJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        DiagnosticoFinal diagnosticoFinal = this.objectMapper.readValue(diagnosticoFinalJson, DiagnosticoFinal.class);
        this.diagnosticoFinalService.saveDiagnosticoFinal(diagnosticoFinal);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @PostMapping("/deleteDiagnosticoFinalPaciente")
    public void deleteDiagnosticoFinalPaciente(@RequestBody String diagnosticoFinalJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        DiagnosticoFinal diagnosticoFinal = this.objectMapper.readValue(diagnosticoFinalJson, DiagnosticoFinal.class);

        if (diagnosticoFinal.getIddiagnosticofinal() == null) {
            throw new Exception("El id esta nulo");
        }
        this.diagnosticoFinalService.deleteDiagnosticoFinal(diagnosticoFinal.getIddiagnosticofinal());
    }

    @RequestMapping("/getDiagnosticoFinalPaciente")
    public List<DiagnosticoFinal> getDiagnosticoFinalPaciente(@RequestParam FichaTerapiaFisica idficha, @RequestParam String anio) {
        return this.diagnosticoFinalService.getDiagnosticoFinal(idficha, anio);
    }

    @GetMapping("/ListarDiagnosticoFinalPorFechas")
    List<DiagnosticoFinal> ListarDiagnosticoFinalPorFechas(@RequestParam FichaTerapiaFisica idficha){
        return this.diagnosticoFinalService.listaDiagnosticoFinalPorFechas(idficha);
    }

    @PostMapping("/saveOrPlantrabajoPaciente")
    public RestResponse saveOrPlantrabajoPaciente(@RequestBody String plantrabajoJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        PlanTrabajo planTrabajo = this.objectMapper.readValue(plantrabajoJson, PlanTrabajo.class);
        this.diagnosticoFinalService.savePlanTrabajo(planTrabajo);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @PostMapping("/deletePlanTrabajoPaciente")
    public void deletePlanTrabajoPaciente(@RequestBody String plantrabajoJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        PlanTrabajo planTrabajo = this.objectMapper.readValue(plantrabajoJson, PlanTrabajo.class);

        if (planTrabajo.getIdplantrabajo() == null) {
            throw new Exception("El id esta nulo");
        }
        this.diagnosticoFinalService.deletePlanTrabajo(planTrabajo.getIdplantrabajo());
    }

    @RequestMapping("/getPlanTrabajoPaciente")
    public List<PlanTrabajo> getPlanTrabajoPaciente(@RequestParam DiagnosticoFinal iddiagnostico) {
        return this.diagnosticoFinalService.getPLanDeTrabajoDelPaciente(iddiagnostico);
    }
}
