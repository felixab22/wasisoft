package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.MaterialOrtopedico;
import com.wasi.wasisoft.model.PrestamoMaterial;
import com.wasi.wasisoft.service.MaterialOrtopedicoService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class MaterialOrtopedicoController {

    @Autowired
    protected MaterialOrtopedicoService materialOrtopedicoService;

    protected ObjectMapper mapper;

    @RequestMapping(value = "/saveOrUpdateMaterialOrtopedico", method = RequestMethod.POST)
    public RestResponse saveOrUpdate(@RequestBody String materialJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.mapper = new ObjectMapper();

        MaterialOrtopedico materialOrtopedico = this.mapper.readValue(materialJson, MaterialOrtopedico.class);

        this.materialOrtopedicoService.save(materialOrtopedico);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getMaterialOrtopedico", method = RequestMethod.GET)
    public List<MaterialOrtopedico> getMaterialOrtopedico() {
        return this.materialOrtopedicoService.findAll();
    }

    @RequestMapping(value = "/deleteMaterialOrtopedico", method = RequestMethod.POST)
    public void deleteMaterialOrtopedico(@RequestBody String materialJson) throws Exception {
        this.mapper = new ObjectMapper();

        MaterialOrtopedico materialOrtopedico = this.mapper.readValue(materialJson, MaterialOrtopedico.class);

        if (materialOrtopedico.getIdmaterialortopedico() == null) {
            throw new Exception("El id esta nulo");
        }
        this.materialOrtopedicoService.deleteMaterialOrtopedico (materialOrtopedico.getIdmaterialortopedico());
    }

    @RequestMapping(value = "/saveOrUpdatePrestamoMaterial", method = RequestMethod.POST)
    public RestResponse saveOrUpdatePrestamoMaterial(@RequestBody String prestamomaterialJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.mapper = new ObjectMapper();

        PrestamoMaterial prestamoMaterial = this.mapper.readValue(prestamomaterialJson, PrestamoMaterial.class);

        this.materialOrtopedicoService.savePrestamoMaterial(prestamoMaterial);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getMaterialEnGarantia", method = RequestMethod.GET)
    public  List<PrestamoMaterial> getMaterialPrestado(){
        return this.materialOrtopedicoService.getMaterialesEnGarantia();
    }

    @RequestMapping(value = "/getMaterialVendido", method = RequestMethod.GET)
    public List<PrestamoMaterial> getMaterialVendido(){
        return this.materialOrtopedicoService.getMaterialesVendidos();
    }
    @GetMapping(value = "/getMaterialesEnAlquiler")
    public List<PrestamoMaterial> getMaterialesEnAlquiler(){
        return this.materialOrtopedicoService.getMaterialesEnAlquiler();
    }

    @RequestMapping(value = "/deletePrestamoMaterial", method = RequestMethod.POST)
    public void deletePrestamoMaterial(@RequestBody String prestamomaterialJson) throws Exception {
        this.mapper = new ObjectMapper();

        PrestamoMaterial prestamoMaterial = this.mapper.readValue(prestamomaterialJson, PrestamoMaterial.class);

        if (prestamoMaterial.getIdprestamomaterial() == null) {
            throw new Exception("El id esta nulo");
        }
        this.materialOrtopedicoService.deletePrestamoMaterial (prestamoMaterial.getIdprestamomaterial());
    }

    @RequestMapping(value = "/getPrestamoMaterialPorPaciente", method = RequestMethod.GET)
    public List<PrestamoMaterial> getPrestamoMaterialPorPaciente(@RequestParam Expediente idexpediente){
        return this.materialOrtopedicoService.getPrestamoMaterialPorPaciente(idexpediente);
    }
}
