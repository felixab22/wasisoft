package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.wasi.wasisoft.model.Role;
import com.wasi.wasisoft.service.RolService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class RolController {

    @Autowired
    protected RolService rolService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateRol", method = RequestMethod.POST)
    public RestResponse saveOrUpdateRol(@RequestBody String rolJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        Role rol = this.objectMapper.readValue(rolJson, Role.class);

        this.rolService.save(rol);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getRol", method = RequestMethod.GET)
    public List<Role> getRol() {
        return this.rolService.findAll();
    }

    @RequestMapping(value = "/deleteRol", method = RequestMethod.POST)
    public void deleteRol(@RequestBody String rolJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Role rol = this.objectMapper.readValue(rolJson, Role.class);

        if (rol.getIdrol() == null) {
            throw new Exception("El id esta nulo");
        }
        this.rolService.deleteRol(rol.getIdrol());
    }

}
