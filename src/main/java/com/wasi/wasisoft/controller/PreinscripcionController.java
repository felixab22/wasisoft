package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Preinscripcion;
import com.wasi.wasisoft.service.PreinscripcionService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class PreinscripcionController {

    @Autowired
    protected PreinscripcionService preinscripcionService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdatePreinscripcion", method = RequestMethod.POST)
    public RestResponse saveOrUpdatePreinscripcion(@RequestBody String preinscripcionJson)
            throws  IOException {
        this.objectMapper = new ObjectMapper();

        Preinscripcion preinscripcion = this.objectMapper.readValue(preinscripcionJson, Preinscripcion.class);

        this.preinscripcionService.save(preinscripcion);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getPreinscripcion", method = RequestMethod.GET)
    public List<Preinscripcion> getPreinscripcion() {
        return this.preinscripcionService.findAllDadosDeAlta();
    }

    @RequestMapping(value = "/getPreinscripcionDadosDeBaja", method = RequestMethod.GET)
    public List<Preinscripcion> getPreinscripcionDadosDeBaja() {
        return this.preinscripcionService.findAllDadosDeBaja();
    }

    @RequestMapping(value = "/deletePreinscripcion", method = RequestMethod.POST)
    public void deletePreinscripcion(@RequestBody String preinscripcionJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Preinscripcion preinscripcion = this.objectMapper.readValue(preinscripcionJson, Preinscripcion.class);

        if (preinscripcion.getId_preinscripcion() == null) {
            throw new Exception("El id esta nulo");
        }
        this.preinscripcionService.deletePreinscripcion(preinscripcion.getId_preinscripcion());
    }
}
