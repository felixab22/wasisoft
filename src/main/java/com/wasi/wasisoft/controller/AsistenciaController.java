package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Asistencia;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.service.AsistenciaService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class AsistenciaController {

    @Autowired
    protected AsistenciaService asistenciaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @PostMapping(value = "/saveOrUpdateAsistencia")
    public RestResponse saveOrUpdateInasistencia(@RequestBody String asistenciaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        Asistencia asistencia = this.objectMapper.readValue(asistenciaJson, Asistencia.class);

        this.asistenciaService.saveAsistencia(asistencia);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @GetMapping(value = "/getAsistenciasByPacientes")
    public List<Asistencia> getAsistenciasByPacientes(@RequestParam String paciente){
        return this.asistenciaService.ListarAsistencia(paciente);
    }

    @GetMapping(value = "/getAsistenciasByPersonal")
    public List<Asistencia> getAsistenciasByPersonal(@RequestParam Personal idpersonal){
        return this.asistenciaService.ListarAsistenciaPorPersonal(idpersonal);
    }

    @GetMapping(value = "getInasistenciaPaciente")
    public RestResponse NotificarInasistencia(@RequestParam String paciente){
        List<Asistencia> result = asistenciaService.ListarInasistencias(paciente);

        return null;
    }

    @GetMapping(value = "/getAllinasistenciasPacientes")
    public List<Asistencia> getAllinasistenciasPacientesWasi(){
        return this.asistenciaService.findAll();
    }


//    @RequestMapping(value = "/deleteInasistencias", method = RequestMethod.POST)
//    public void deleteInasistencia(@RequestBody String inasistenciasJson) throws Exception {
//        this.objectMapper = new ObjectMapper();
//
//        Asistencia inasistencia = this.objectMapper.readValue(inasistenciasJson, Asistencia.class);
//
//        if (inasistencia.getIdinasistencia() == null) {
//            throw new Exception("El id esta nulo");
//        }
//        this.asistenciaService.deleteInasistenecia(inasistencia.getIdinasistencia());
//    }
}
