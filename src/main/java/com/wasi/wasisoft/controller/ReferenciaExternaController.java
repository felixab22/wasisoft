package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ReferenciaExterna;
import com.wasi.wasisoft.service.ReferenciaExternaService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class ReferenciaExternaController {

    @Autowired
    protected ReferenciaExternaService referenciaExternaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateReferenciaExterna", method = RequestMethod.POST)
    public RestResponse saveOrUpdateReferenciaExterna(@RequestBody String referenciaExternaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        ReferenciaExterna referenciaExterna = this.objectMapper.readValue(referenciaExternaJson, ReferenciaExterna.class);

        this.referenciaExternaService.save(referenciaExterna);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/deleteReferenciaExterna", method = RequestMethod.POST)
    public void deleteReferenciaExterna(@RequestBody String referenciaExternaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        ReferenciaExterna referenciaExterna = this.objectMapper.readValue(referenciaExternaJson, ReferenciaExterna.class);

        if (referenciaExterna.getIdrefexterna()== null) {
            throw new Exception("El id esta nulo");
        }
        this.referenciaExternaService.deleteReferenciaExterna(referenciaExterna.getIdrefexterna());
    }

    @RequestMapping(value = "/findReferenciaExternaByIdexpediente", method = RequestMethod.GET)
    public List<ReferenciaExterna> findReferenciaExternaByIdexpediente(@RequestParam Expediente idexpediente){
        return  this.referenciaExternaService.findReferenciaExternaByIdexpediente(idexpediente);
    }
}
