package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Movilidad;
import com.wasi.wasisoft.model.MovilidadArticulaciones;
import com.wasi.wasisoft.service.MovilidadArticulacionesService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class MovilidadArticulacionesController {

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);
    @Autowired
    protected MovilidadArticulacionesService movilidadArticulacionesService;

    @Autowired
    protected ObjectMapper objectMapper;

    @PostMapping("/saveOrUpdateMovilidadArticulaciones" )
    public RestResponse saveOrUpdateMovilidadArticulaciones(@RequestBody String movArticulacionesJson)
     throws JsonParseException, JsonMappingException, IOException {
            this.objectMapper = new ObjectMapper();

            LOGGER.info(" PARAMS: '" + movArticulacionesJson + "'");

            MovilidadArticulaciones movilidadArticulaciones = this.objectMapper.readValue(movArticulacionesJson, MovilidadArticulaciones.class);

            this.movilidadArticulacionesService.save(movilidadArticulaciones);

            return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
        }

        @PostMapping("/deleteMovilidadArticulaciones")
        public void deleteMovilidadArticulaciones (@RequestBody String movArticulacionesJson) throws Exception {
            this.objectMapper = new ObjectMapper();

            MovilidadArticulaciones movilidadArticulaciones = this.objectMapper.readValue(movArticulacionesJson, MovilidadArticulaciones.class);

            if (movilidadArticulaciones.getIdmovarticulaciones() == null) {
                throw new Exception("El id esta nulo");
            }
            this.movilidadArticulacionesService.deleteMovilidadArticulaciones(movilidadArticulaciones.getIdmovarticulaciones());
        }

    @GetMapping( "/getMovilidadArticulacionesByIdfichaterapiafisica")
    List<MovilidadArticulaciones> getMovilidadArticulacionesByIdfichaterapiafisica (@RequestParam FichaTerapiaFisica idfichaterapia,@RequestParam String anio){
        return this.movilidadArticulacionesService.getMovilidadArticulacionesPaciente(idfichaterapia, anio);
    }

    @GetMapping("/listarMovilidadArticulacionePorFechas")
    List<MovilidadArticulaciones> listarMovilidadArticulacionePorFechas(@RequestParam FichaTerapiaFisica idficha){
        return this.movilidadArticulacionesService.listarMovilidadArticulacionesPorFechas(idficha);
    }

    @PostMapping("/saveOrUpdateMovilidadPaciente" )
    public RestResponse saveOrUpdateMovilidadPaciente(@RequestBody String movilidadJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + movilidadJson + "'");

        Movilidad movilidad = this.objectMapper.readValue(movilidadJson, Movilidad.class);

        this.movilidadArticulacionesService.saveMovilidadPaciente(movilidad);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa, movilidad registrada");
    }

    @PostMapping("/deleteMovilidadPaciente")
    public void deleteMovilidadPaciente (@RequestBody String movilidadJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Movilidad movilidad = this.objectMapper.readValue(movilidadJson, Movilidad.class);

        if (movilidad.getIdmovilidad() == null) {
            throw new Exception("El id esta nulo");
        }
        this.movilidadArticulacionesService.deleteMovilidadPaciente(movilidad.getIdmovilidad());
    }

    @GetMapping( "/getMovilidadSeguimiento")
    List<Movilidad> getMovilidadSeguimiento (@RequestParam MovilidadArticulaciones movilidadArticulaciones){
        return this.movilidadArticulacionesService.getMovilidadSeguimiento(movilidadArticulaciones);
    }
}
