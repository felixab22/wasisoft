package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.AnamnesisFamiliar;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.AnamnesisFamiliarService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class AnamnesisFamiliarController {

    @Autowired
    protected AnamnesisFamiliarService anamnesisFamiliarService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateAnamnesisFamiliar", method = RequestMethod.POST)
    public RestResponse saveOrUpdateAnamnesisFamiliar(@RequestBody String anamnesisFamiliarJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();
        AnamnesisFamiliar anamnesisFamiliar = this.objectMapper.readValue(anamnesisFamiliarJson, AnamnesisFamiliar.class);
        this.anamnesisFamiliarService.save(anamnesisFamiliar);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }
    
    @RequestMapping(value = "/getAnamnesisFamiliarByIdfichaterapiafisica", method = RequestMethod.GET)
    public List<AnamnesisFamiliar> getAnamnesisFamiliarByIdfichaterapiafisica(@RequestParam FichaTerapiaFisica idfichaterapiafisica){
        return this.anamnesisFamiliarService.getAnamnesisFamiliar(idfichaterapiafisica);
    }

    @RequestMapping(value = "/deleteAnamnesisFamiliar", method = RequestMethod.POST)
    public void deleteAnamnesisFamiliar(@RequestBody String anamnesisFamiliarJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        AnamnesisFamiliar anamnesisFamiliar = this.objectMapper.readValue(anamnesisFamiliarJson, AnamnesisFamiliar.class);

        if (anamnesisFamiliar.getIdantefami() == null) {
            throw new Exception("El id esta nulo");
        }
        this.anamnesisFamiliarService.delete(anamnesisFamiliar.getIdantefami());
    }
}
