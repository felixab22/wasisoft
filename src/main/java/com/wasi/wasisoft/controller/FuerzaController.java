package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Fuerza;
import com.wasi.wasisoft.model.FuerzaSeguimiento;
import com.wasi.wasisoft.service.FuerzaService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@RestController
public class FuerzaController {

    @Autowired
    protected FuerzaService fuerzaService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @PostMapping("/saveOrUpdateFuerza" )
    public RestResponse saveOrUpdateFuerza(@RequestBody String fuerzaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + fuerzaJson + "'");

        Fuerza fuerza = this.objectMapper.readValue(fuerzaJson, Fuerza.class);

        this.fuerzaService.saveFuerza(fuerza);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa, se ha registrado la ev fuerza del pacienete");
    }

    @GetMapping( "/getFuerzaPaciente")
    List<Fuerza> getFuerzaPaciente (@RequestParam FichaTerapiaFisica idfichaterapia, @RequestParam String anio){

        return this.fuerzaService.getFuerzaPaciente(idfichaterapia,anio);
    }

    @GetMapping("/ListarFichasFuerzaPorFechas")
    List<Fuerza> ListarFichasFuerzaPorFechas(@RequestParam FichaTerapiaFisica idficha){

        return this.fuerzaService.listarFichasFuerzaPorFechas(idficha);
    }

    @PostMapping("/deleteFuerzaPaciente")
    public void deleteFuerzaPaciente (@RequestBody String fuerzaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Fuerza fuerza = this.objectMapper.readValue(fuerzaJson, Fuerza.class);

        if (fuerza.getIdfuerza() == null) {
            throw new Exception("El id esta nulo");
        }
        this.fuerzaService.deleteFuerza(fuerza.getIdfuerza());
    }

    @PostMapping("/saveOrUpdateSeguimientoFuerza" )
    public RestResponse saveOrUpdateSeguimientoFuerza(@RequestBody String fuerzaSeguimientoJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + fuerzaSeguimientoJson + "'");

        FuerzaSeguimiento fuerzaSeguimiento = this.objectMapper.readValue(fuerzaSeguimientoJson, FuerzaSeguimiento.class);

        this.fuerzaService.saveFuerzaSeguimiento(fuerzaSeguimiento);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa, se ha registrado el seguimiento de la fuerza del pacienete");
    }

    @GetMapping( "/getSeguimientoFuerza")
    List<FuerzaSeguimiento> getSeguimientoFuerza (@RequestParam Fuerza idfuerza){
        return this.fuerzaService.getSeguimientoFuerzaPaciente(idfuerza);
    }

    @PostMapping("/deleteSeguimientoFuerza")
    public void deleteSeguimientoFuerza(@RequestBody String fuerzaSeguimientoJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        FuerzaSeguimiento fuerzaSeguimiento = this.objectMapper.readValue(fuerzaSeguimientoJson, FuerzaSeguimiento.class);

        if (fuerzaSeguimiento.getIdfuerzaseguimiento() == null) {
            throw new Exception("El id esta nulo");
        }
        this.fuerzaService.deleteSeguimientoFuerza(fuerzaSeguimiento.getIdfuerzaseguimiento());
    }
}
