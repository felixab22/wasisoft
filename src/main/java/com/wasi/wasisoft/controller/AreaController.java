package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.service.AreaService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class AreaController {


    @Autowired
    protected AreaService areaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateArea", method = RequestMethod.POST)
    public RestResponse saveOrUpdateRol(@RequestBody String areaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        Area area = this.objectMapper.readValue(areaJson, Area.class);
        if (!this.validate(area)) {
            return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(),
                    "Los campos obligatorios no estan diligenciados");
        }
        this.areaService.save(area);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/getArea", method = RequestMethod.GET)
    public List<Area> getArea() {
        return this.areaService.findAll();
    }

    @RequestMapping(value = "/deleteArea", method = RequestMethod.POST)
    public void deleteArea(@RequestBody String areaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Area area = this.objectMapper.readValue(areaJson, Area.class);

        if (area.getId_area() == null) {
            throw new Exception("El id esta nulo");
        }
        this.areaService.deleteArea(area.getId_area());
    }

    private boolean validate(Area area) {
        boolean isValid = true;

        if (StringUtils.trimToNull(area.getDescripcionarea()) == null) {
            isValid = false;
        }
        return isValid;
    }

    @GetMapping("/getAllAreasDeReferenciaInterna")
    List<Area> getAllAreasDeReferenciaInterna(){
        return this.areaService.getAreasReferenciaInterna();
    }
}
