package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.CampaniaAtencion;
import com.wasi.wasisoft.model.RegistroCampania;
import com.wasi.wasisoft.service.CampaniaAtencionService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by airo on 1/02/2019.
 */
@RestController
public class CampaniaAtencionController {

    @Autowired
    protected CampaniaAtencionService campaniaAtencionService;

    @Autowired
    protected ObjectMapper objectMapper;

    @PostMapping(value = "/saveCampaniaAtencion")
    public RestResponse saveCampañaAtencion(@RequestBody String jsonCampaniaAtencion) throws IOException {
        objectMapper = new ObjectMapper();
        CampaniaAtencion campaniaAtencion = objectMapper.readValue(jsonCampaniaAtencion, CampaniaAtencion.class);
        if (!this.validateCampania(campaniaAtencion)) {
            return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(),
                    "Los campos obligatorios no estan diligenciados correctamente");
        }
        this.campaniaAtencionService.saveCampania(campaniaAtencion);
        return new RestResponse(HttpStatus.OK.value(), "Se registro correctamente la campaña");
    }

    @GetMapping(value = "/ListaCampaniaAtencion")
    public List<CampaniaAtencion> ListaCampaniaAtencion(){
        return this.campaniaAtencionService.listCampaniaAtencion();
    }

    @PostMapping(value = "/deleteCampaniaAtencion")
    public RestResponse deleteCampaniaAtencion(@RequestBody String campaniaJson) throws Exception{
        this.objectMapper = new ObjectMapper();

        CampaniaAtencion campaniaAtencion = objectMapper.readValue(campaniaJson, CampaniaAtencion.class);
        if (campaniaAtencion.getIdcampaniaatencion() == null) {
//            throw new Exception("El id esta nulo");
            return new RestResponse(HttpStatus.BAD_REQUEST.value(),"No se ha podido eliminar la campaña");
        }
        this.campaniaAtencionService.deleteCampania(campaniaAtencion.getIdcampaniaatencion());
        return  new RestResponse(HttpStatus.OK.value(),"Se ha eliminado correctamente");
    }

    private boolean validateCampania(@Validated CampaniaAtencion campaniaAtencion) {
        boolean isValid = true;

        if (StringUtils.trimToNull(campaniaAtencion.getNombrecampania()) == null) {
            isValid = false;
        }
        return isValid;
    }

    private boolean validateRegistroCampania(@Validated RegistroCampania registroCampania) {
        boolean isValid = true;

        if (StringUtils.trimToNull(registroCampania.getNombrepaciente()) == null) {
            isValid = false;
        }
        return isValid;
    }

    @PostMapping(value = "/saveRegistroCampaniaAtencion")
    public RestResponse saveRegistroCampañaAtencion(@RequestBody String jsonRegistroCampaniaAtencion) throws IOException {
        objectMapper = new ObjectMapper();
        RegistroCampania registroCampania = objectMapper.readValue(jsonRegistroCampaniaAtencion, RegistroCampania.class);
        if (!this.validateRegistroCampania(registroCampania)) {
            return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(),
                    "Los campos obligatorios no estan diligenciados correctamente");
        }
        this.campaniaAtencionService.saveRegistroCampania(registroCampania);
        return new RestResponse(HttpStatus.OK.value(), "Se guardo correctamente los datos");
    }

    @GetMapping(value = "/ListaRegistroCampaniaByFecha")
    List<RegistroCampania> ListaRegistroCampaniaByFecha(@RequestParam String fecharegistro){
        return this.campaniaAtencionService.ListRegistroCampaniaByFecha(fecharegistro);
    }
    @GetMapping(value = "/ListaRegistroCampaniaByCampania")
    List<RegistroCampania> ListaRegistroCampañaByCampaña(@RequestParam CampaniaAtencion idcampania){
        return this.campaniaAtencionService.ListRegistroCampaniaByCampaniaAtencion(idcampania);
    }
    @GetMapping(value = "/getPacienteParticipanteCampania")
    public RegistroCampania getPacienteParticipanteCampaña(@RequestParam String nombre){
        return this.campaniaAtencionService.PacienteRegistradoEnCampania(nombre);
    }
}





