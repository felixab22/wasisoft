package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Evpsicologica;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaPsicologia;
import com.wasi.wasisoft.service.FichaPsicologiaService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
@RestController
public class FichaPsicologiaController {

    @Autowired
    protected FichaPsicologiaService fichaPsicologiaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @PostMapping("/saveOrUpdateFichaPsicologia")
    public RestResponse saveOrUpdateFichaPsicologia(@RequestBody String fichaPsicologiaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();
        FichaPsicologia fichaPsicologia = this.objectMapper.readValue(fichaPsicologiaJson, FichaPsicologia.class);
        this.fichaPsicologiaService.saveFichaPsicologia(fichaPsicologia);
        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteFichaPsicologia")
    public void deleteFichaPsicologia(@RequestBody String fichaPsicologiaJson) throws Exception {
        this.objectMapper = new ObjectMapper();
        FichaPsicologia fichaPsicologia = this.objectMapper.readValue(fichaPsicologiaJson, FichaPsicologia.class);
        if (fichaPsicologia.getIdfichapsico() == null) {
            System.out.println("Registro de expediente no encontrado");
        } else {
            this.fichaPsicologiaService.deleteFichaPsicologia(fichaPsicologia.getIdfichapsico());
        }
    }

    @GetMapping("/getFichaPsicologiaPaciente")
    public List<FichaPsicologia> getFichaPsicologiaPaciente(@RequestParam Expediente idexpediente){
        return this.fichaPsicologiaService.getFichaPsicologiaPaciente(idexpediente);
    }

    //Metodo de evaluacion psicologica de familiares
    @PostMapping("/saveOrUpdateEvaluacionPsicologia")
    public RestResponse saveOrUpdateEvaluacionPsicologia(@RequestBody String EvPsicologicaJson)

            throws JsonParseException, JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        Evpsicologica evpsicologica = this.objectMapper.readValue(EvPsicologicaJson, Evpsicologica.class);

        if (!this.validate(evpsicologica)){
            return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(),
                    "Los campos obligatorios no estan diligenciados");
        }
        this.fichaPsicologiaService.saveEvPsicologica(evpsicologica);
        return new RestResponse(HttpStatus.OK.value(), "La evaluacion se guardo correctamente ");
    }

    @PostMapping("/deleteEvPsicologia")
    public void deleteEvPsicologia(@RequestBody String evPsicologiaJson) throws Exception {
        this.objectMapper = new ObjectMapper();
        Evpsicologica evpsicologica = this.objectMapper.readValue(evPsicologiaJson, Evpsicologica.class);
        if (evpsicologica.getIdevpsicologica() == null) {
            System.out.println("Registro de ficha no encontrado");
        } else {
            this.fichaPsicologiaService.deleteEvPsicologica(evpsicologica.getIdevpsicologica());
        }
    }

    @GetMapping("/getEvaluacionPsicologicaFamiliares")
    public List<Evpsicologica> getEvaluacionPsicologicaFamiliares(@RequestParam FichaPsicologia idfichapsicologia){
        return this.fichaPsicologiaService.findAllByIdfichapsicologia(idfichapsicologia);
    }

    private boolean validate(Evpsicologica evpsicologica) {
        boolean isValid = true;

        if (StringUtils.trimToNull(evpsicologica.getTipo_familia()) == null) {
            isValid = false;
        }

//        if (StringUtils.trimToNull(personal.getApppaterno()) == null) {
//            isValid = false;
//        }

//        if (StringUtils.trimToNull(personal.getAppmaterno()) == null) {
//            isValid = false;
//        }
        return isValid;
    }
}
