package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ResultadoPruebas;
import com.wasi.wasisoft.service.ResultadoPruebasService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 03/05/2018.
 */
@RestController
public class ResultadoPruebasController {

    @Autowired
    protected ResultadoPruebasService resultadoPruebasService;

    @Autowired
    protected ObjectMapper objectMapper;

    @PostMapping("/saveOrUpdateResultadoPruebas")
    public RestResponse saveOrUpdateResultadoPruebas(@RequestBody String resultadoPruebasJson)
            throws JsonParseException, JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        ResultadoPruebas resultadoPruebas = this.objectMapper.readValue(resultadoPruebasJson, ResultadoPruebas.class);

        this.resultadoPruebasService.saveResultados(resultadoPruebas);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @GetMapping("/getResultadoPruebasPaciente")
    public List<ResultadoPruebas> getResultadoPruebasPaciente(@RequestParam Expediente idexpediente, @RequestParam Area idarea) {
        return this.resultadoPruebasService.getResultadoPruebasPaciente(idexpediente, idarea);
    }

    @PostMapping("/deleteResultadoPruebas")
    public void deleteResultadoPruebas(@RequestBody String resultadoPruebasJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        ResultadoPruebas resultadoPruebas = this.objectMapper.readValue(resultadoPruebasJson, ResultadoPruebas.class);

        if (resultadoPruebas.getIdresultpruebas() == null) {
            throw new Exception("El id esta nulo");
        }
        this.resultadoPruebasService.deleleteResultados(resultadoPruebas.getIdresultpruebas());
    }

}
