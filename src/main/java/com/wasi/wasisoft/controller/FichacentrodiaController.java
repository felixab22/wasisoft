package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.CuidadoAlim;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichacentrodia;
import com.wasi.wasisoft.service.FichacentrodiaService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 27/04/2018.
 */
@RestController
public class FichacentrodiaController {

    @Autowired
    protected FichacentrodiaService fichacentrodiaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @PostMapping("/saveOrUpdateFichacentrodia")
    public RestResponse saveOrUpdateFichacentrodia(@RequestBody String fichacentrodiaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();
        Fichacentrodia fichacentrodia = this.objectMapper.readValue(fichacentrodiaJson, Fichacentrodia.class);
        this.fichacentrodiaService.saveFichcebtrodia(fichacentrodia);
        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteFichacentrodia")
    public void deleteFichacentrodia(@RequestBody String fichacentrodiaJson) throws Exception {
        this.objectMapper = new ObjectMapper();
        Fichacentrodia fichacentrodia = this.objectMapper.readValue(fichacentrodiaJson, Fichacentrodia.class);
        if (fichacentrodia.getIdcentrodia() == null) {
            System.out.println("Registro de expediente no encontrado");
        } else {
            this.fichacentrodiaService.deleteFichacentrodia(fichacentrodia.getIdcentrodia());
        }
    }

    @GetMapping("/getFichacentrodiaPaciente")
    public List<Fichacentrodia> getFichacentrodiaPaciente(@RequestParam CuidadoAlim idcuidadoalim){
        return this.fichacentrodiaService.getFichacentrodiaPaciente(idcuidadoalim);
    }
}
