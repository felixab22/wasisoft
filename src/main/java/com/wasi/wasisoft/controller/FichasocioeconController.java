package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichasocioecon;
import com.wasi.wasisoft.model.Situacionecon;
import com.wasi.wasisoft.service.FichasocioeconService;
import com.wasi.wasisoft.util.ResponseArray;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sun.plugin2.message.transport.SerializingTransport;

import javax.validation.Valid;
import javax.xml.soap.SOAPPart;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class FichasocioeconController {

    @Autowired
    protected FichasocioeconService fichasocioeconService;

    @Autowired
    protected ObjectMapper objectMapper;
    private static final Log LOGGER = LogFactory.getLog(FichasocioeconController.class);

    @RequestMapping(value = "/saveOrUpdateFichasocioecon", method = RequestMethod.POST)
    public RestResponse saveOrUpdateFichasocioecon(@RequestBody String jsonFichasocioecon)
          throws JsonParseException,JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + jsonFichasocioecon + "'");
        Fichasocioecon fichasocioecon = this.objectMapper.readValue(jsonFichasocioecon, Fichasocioecon.class);

        if (fichasocioecon.getId_socioecon() == null) {
            this.fichasocioeconService.save(fichasocioecon);
            return new RestResponse(HttpStatus.OK.value(), "La ficha socio economica se guardo correctamente");

        } else {
            this.fichasocioeconService.save(fichasocioecon);
            return new RestResponse(HttpStatus.OK.value(), "Se actualizo correctamente la ficha socioeconom");
        }
    }
            @RequestMapping(value = "/getFichasocioecon", method = RequestMethod.GET)
            public List<Fichasocioecon> getFichasocioecon () {
                return this.fichasocioeconService.findAll();
            }

            @RequestMapping(value = "/getFichaSocioEconomicaByIdexpediente", method = RequestMethod.GET)
            List<Fichasocioecon> getFichaSocioEconomicaSegunIdExpediente (@RequestParam Expediente idexpediente){

                return this.fichasocioeconService.obtenerFichaSocioEconomica(idexpediente);
            }
//
//    @RequestMapping(value = "/getByCodigoHistoriaSocial", method = RequestMethod.GET)
//    public List<HistoriaSocial> getByCodigoHistoriaSocial(@RequestParam String codigo){
//        return  this.historiaSocialService.findByCodigoHistoria(codigo);
//    }

            @RequestMapping(value = "/deleteFichasocioecon", method = RequestMethod.POST)
            public void deleteFichasocioecon (@RequestBody String fichasocioeconJson) throws Exception {

                this.objectMapper = new ObjectMapper();

                Fichasocioecon fichasocioecon = this.objectMapper.readValue(fichasocioeconJson, Fichasocioecon.class);

                if (fichasocioecon.getId_socioecon() == null) {

                    System.out.println("Registro de ficha socio economica no encontrado");

                } else {

                    this.fichasocioeconService.deleteFichasocioecon(fichasocioecon.getId_socioecon());
                }
            }

            @GetMapping(value = "/reporteByTipoFamilia")
            public ResponseArray reporteByTipoFamilia () {
                return this.fichasocioeconService.reporteByTipoFamilia();
            }
            @GetMapping(value = "/reporteByUbicacionVivienda")
            public ResponseArray reporteByUbicacionVivienda () {
                return this.fichasocioeconService.reportByUbicacionVivienda();
            }
            @GetMapping(value = "/reporteByTenenciaVivienda")
            public ResponseArray reporteByTenenciaVivienda () {
                return this.fichasocioeconService.reportByTenenciaVivienda();
            }
            @GetMapping(value = "/reporteByTipoSeguro")
            public ResponseArray reporteByTipoSeguro () {
                return this.fichasocioeconService.reportByTipoSeguro();
            }
            @GetMapping(value = "/reporteByNivelEducativo")
            public ResponseArray reporteByNivelEducativo () {
                return this.fichasocioeconService.reportByNivelEducativo();
            }
            @GetMapping(value = "/reporteByTipoInstEducativa")
            public ResponseArray reporteByTipoInstEducativa () {
                return this.fichasocioeconService.reportByTipoInstitucionEducativa();
            }
            @GetMapping(value = "/reporteByNecesidadCertificadoDiscapacidad")
            public ResponseArray reporteByNecesidadCertificadoDiscapacidad () {
                return this.fichasocioeconService.reportByCertificadoDiscapacidad();
            }

    }

