package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ReferenciaInterna;
import com.wasi.wasisoft.service.ReferenciaInternaService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
@RestController
public class ReferenciaInternaController {

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);


    @Autowired
    protected ReferenciaInternaService referenciaInternaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrUpdateReferenciaInterna", method = RequestMethod.POST)
    public RestResponse saveOrUpdateReferenciaInterna(@RequestBody String referenciaInternaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '"+ referenciaInternaJson + "'");

        ReferenciaInterna referenciaInterna = this.objectMapper.readValue(referenciaInternaJson, ReferenciaInterna.class);

        List<ReferenciaInterna> result = referenciaInternaService.getReferenciaInternaValidar(referenciaInterna.getIdarea(),referenciaInterna.getIdexpediente());
        System.out.println("RESULTADO ::" + result.size() + " DATOS : " + result.toString());

        if (result.size() == 0){
            this.referenciaInternaService.saveReferenciaInterna(referenciaInterna);
            System.out.println("OOOOPERACION EXITOSAA  " );
            return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");

        }
        else
            return new RestResponse(HttpStatus.EXPECTATION_FAILED.value(),"Ya existe una refencia para el area");
    }

    @RequestMapping(value = "/deleteReferenciaInterna", method = RequestMethod.POST)
    public void deleteReferenciaInterna(@RequestBody String referenciaInternaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        ReferenciaInterna referenciaInterna = this.objectMapper.readValue(referenciaInternaJson, ReferenciaInterna.class);

        if (referenciaInterna.getIdreferinterna() == null) {
            throw new Exception("El id esta nulo");
        }
        this.referenciaInternaService.deleteReferenciaInterna(referenciaInterna.getIdreferinterna());
    }

    @RequestMapping(value = "/getAllReferenciaInterna", method = RequestMethod.GET)
    public List<ReferenciaInterna> getAllReferenciaInterna(){
        return  this.referenciaInternaService.getReferenciaInterna();
    }

    @GetMapping("/getReferenciaInternaPorAreas")
    List<ReferenciaInterna> getReferenciaInternaPorAreas(@RequestParam Area idarea){
        return this.referenciaInternaService.getReferenciaInternaPorAreas(idarea);
    }

    @GetMapping("/getReferenciaInternaPorPaciente")
    List<ReferenciaInterna> getReferenciaInternaPorPaciente(@RequestParam Expediente idexpediente){
        return this.referenciaInternaService.getReferenciaInternaPorPaciente(idexpediente);
    }
}
