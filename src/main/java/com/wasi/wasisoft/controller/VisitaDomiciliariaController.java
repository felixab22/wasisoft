package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.VisitaDomiciliaria;
import com.wasi.wasisoft.service.VisitaDomiciliariaService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class VisitaDomiciliariaController {

    @Autowired
    protected VisitaDomiciliariaService visitaDomiciliariaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/getVisitaDomiciliaria", method = RequestMethod.GET)
    public List<VisitaDomiciliaria> getVisitaDomiciliaria(){
        return  this.visitaDomiciliariaService.findAll();
    }

    @RequestMapping(value = "/saveOrUpdateVisitaDomiciliaria", method = RequestMethod.POST)
    public RestResponse saveOrUpdateVisitaDomiciliaria(@RequestBody String visitaDomiciliariaJson)
               throws JsonParseException, JsonMappingException, IOException {
            this.objectMapper = new ObjectMapper();

            VisitaDomiciliaria visitaDomiciliaria = this.objectMapper.readValue(visitaDomiciliariaJson, VisitaDomiciliaria.class);

            this.visitaDomiciliariaService.save(visitaDomiciliaria);

            return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/deleteVisitaDomiciliaria", method = RequestMethod.POST)
    public void deleteVisitaDomiciliaria(@RequestBody String visitaDomiciliariaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        VisitaDomiciliaria visitaDomiciliaria = this.objectMapper.readValue(visitaDomiciliariaJson, VisitaDomiciliaria.class);

        if (visitaDomiciliaria.getId_visitadomiciliaria() == null) {
            throw new Exception("El id esta nulo");
        }
        this.visitaDomiciliariaService.deleteVisitaDomiciliaria(visitaDomiciliaria.getId_visitadomiciliaria());
    }

    @RequestMapping(value = "/getVisitaDomiciliariaByIdexpediente", method = RequestMethod.GET)
    public List<VisitaDomiciliaria> getVisitaDomiciliariaByIdexpediente(@RequestParam Expediente idexpediente){
        return  this.visitaDomiciliariaService.findByIdexpediente(idexpediente);
    }

    @GetMapping("/ListaVisitaDomiciliariasPorDni")
    public List<VisitaDomiciliaria> ListaVisitaDomiciliariasPorDni(@RequestParam int dni){
        return this.visitaDomiciliariaService.obtenerVisitasPorDni(dni);
    }
    @GetMapping("/ListVisitaDomiciliariaPorHistorial")
    public List<VisitaDomiciliaria> ListVisitaDomiciliariaPorHistorial(@RequestParam String historial){
        return this.visitaDomiciliariaService.obtenerVisitasPorHistorial(historial);
    }
}
