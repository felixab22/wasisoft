package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaAto;
import com.wasi.wasisoft.service.FichaAtoService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

/**
 * Created by HeverFernandez on 26/04/2018.
 */
@RestController
public class FichaAtoController {

    @Autowired
    protected FichaAtoService fichaAtoService;

    @Autowired
    protected ObjectMapper objectMapper;

    @PostMapping("/saveOrUpdateFichaAto")
    public RestResponse saveOrUpdateFichaAto(@RequestBody String fichaAtoJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();
      FichaAto fichaAto = this.objectMapper.readValue(fichaAtoJson, FichaAto.class);
          this.fichaAtoService.saveFichAto(fichaAto);
          System.out.println("Registro correctaamente guardado");
          return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteFichaAtoPaciente")
    public void deleteFichaAtoPaciente(@RequestBody String fichaAtoJson) throws Exception {
        this.objectMapper = new ObjectMapper();
        FichaAto fichaAto = this.objectMapper.readValue(fichaAtoJson, FichaAto.class);
        if (fichaAto.getIdfichaato() == null) {
            System.out.println("Registro de expediente no encontrado");
        } else {
            this.fichaAtoService.deleteFichaAto(fichaAto.getIdfichaato());
        }
    }

    @GetMapping("/getFichaAtoPaciente")
    public List<FichaAto> getFichaAtoPaciente(@RequestParam Expediente idexpediente, @RequestParam String anio){
        return this.fichaAtoService.getFichaAtoPaciente(idexpediente, anio);
    }

    @GetMapping("/ListarFichaAtoPorFechas")
    List<FichaAto> ListarFichaAtoPorFechas(@RequestParam Expediente idexpediente){
        return this.fichaAtoService.ListarFichaAtoPacientePorFecha(idexpediente);
    }
}
