package com.wasi.wasisoft.controller;

import com.wasi.wasisoft.service.UploadFileService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * Created by AITAMH on 25/01/2019.
 */
@RestController
public class UploadFileController {

    @Autowired
    private UploadFileService uploadFileService;

    private static final Log LOGGER = LogFactory.getLog(UploadFileController.class);

    @PostMapping("/upload")
    public RestResponse uploadFile(@RequestParam("file") MultipartFile file) {

        String nombre = file.getOriginalFilename();

        String extensionFileAceptados[]={"pdf", "xlsx", "docx","txt"};

        if (file.isEmpty()) {
            return new RestResponse(HttpStatus.LENGTH_REQUIRED.value(),"Seleccionar algun archivO");
        }
        try {
            uploadFileService.saveFile(file);
        } catch (IOException e) {
            e.printStackTrace();
            return new RestResponse(HttpStatus.EXPECTATION_FAILED.value(), "No se pudieron subir los archivos");
        }

        return new RestResponse(HttpStatus.OK.value(),"Archivos subidos correctamente");
    }

//    @PostMapping("/uploadMultiple")
//    public RestResponse uploadMultipleFiles(@RequestParam("files") List<MultipartFile> files){
//        LOGGER.info(" PARAMS: '"+ files.size() + "'" );
////        MultipartFile archivo;
////          archivo.getOriginalFilename();
////        String extensionFile[]={"pdf", "xlsx", "docx"};
////        String nombreArchivo[] = f
//        if(files.size() == 0){
//
//            return new RestResponse(HttpStatus.LENGTH_REQUIRED.value(),"Seleccionar algun archivO");
//        }
//        try {
//            uploadFileService.saveMultipleFiles(files);
//        } catch (IOException e) {
//            e.printStackTrace();
//            return new RestResponse(HttpStatus.EXPECTATION_FAILED.value(), "No se pudieron subir los archivos");
//        }
//        return new RestResponse(HttpStatus.OK.value(),"Archivos subidos correctamente");
//    }

    @GetMapping("/getAllFiles")
    public List<String> getListFiles(@RequestParam String nombrearea, @RequestParam String codigo) {
        return this.uploadFileService.getListFiles(nombrearea,codigo);
    }

    @GetMapping("/file/{fileName:.+}")
    public void downloadPDFResource( HttpServletResponse response,@PathVariable("fileName") String fileName,
                                     @RequestParam String nombrearea, @RequestParam String codigo) throws IOException {

            this.uploadFileService.downloadPDFResource(response, fileName, nombrearea,codigo);
    }

    @PostMapping("/deleteFile/{fileName:.+}")
    public RestResponse deleteFile(@PathVariable("fileName") String fileName,
                                   @RequestParam String nombrearea, @RequestParam String codigo) throws IOException {

        System.out.println( " IMPRIMIENDO  DATOS  :  Nombrearchivo " + fileName + " Areaa : "+ nombrearea + "codigo : "+codigo);
        boolean response = uploadFileService.deleteFile(fileName,nombrearea,codigo);
        if (!response){
            return new RestResponse(HttpStatus.OK.value(),"Se eliminó correctamenta el archivo");
        }
        return new RestResponse(HttpStatus.BAD_REQUEST.value(),"No se pudo eliminar el archivo");
    }
}

