package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Notificacion;
import com.wasi.wasisoft.model.Preinscripcion;
import com.wasi.wasisoft.service.NotificacionService;
import com.wasi.wasisoft.service.PreinscripcionService;
import com.wasi.wasisoft.util.ResponseArray;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

/**
 * Created by airo on 23/01/2019.
 */
@RestController
public class NotificacionController {

    @Autowired
    protected NotificacionService notificacionService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @PostMapping(value = "/saveOrUpdateNotificacion")
    public RestResponse saveOrUpdateNotificacion(@RequestBody String notificacionJson)
            throws IOException {
        this.objectMapper = new ObjectMapper();

        Notificacion notificacion = this.objectMapper.readValue(notificacionJson, Notificacion.class);

        this.notificacionService.save(notificacion);

        List<Notificacion> result = notificacionService.CantidadInasistencias(notificacion.getNombrepaciente());

        if (result.size()==1){
            return new RestResponse(HttpStatus.OK.value(),"No vino  " + result.size() + "   vez ");
        }
        else
            return new RestResponse(HttpStatus.OK.value(),"No vino  " + result.size() + "   veces ");
    }

//    @Transactional
    @PostMapping(value = "/deleteNotificacion")
    public void deleteNotificacion(@RequestBody String notificacionJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Notificacion notificacion = this.objectMapper.readValue(notificacionJson, Notificacion.class);

        LOGGER.info(" PARAMETROS NOTIFICACION: '"+ notificacion.toString() + "'");

        if (notificacion.getIdnotificacion() == null){
            throw new Exception("El id esta nulo");
        }
        this.notificacionService.deleteNotificacion(notificacion.getIdnotificacion());
    }

    @GetMapping(value = "/reporteByCantidadInasistencias")
    public List<Notificacion> reporteByCantidadInasistencias(){
        return this.notificacionService.reportByInasistencias();
    }
}
