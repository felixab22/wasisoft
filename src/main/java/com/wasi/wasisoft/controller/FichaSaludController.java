package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.*;
import com.wasi.wasisoft.service.FichaSaludService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
@RestController
public class FichaSaludController {

    @Autowired
    protected FichaSaludService fichaSaludService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @PostMapping("/saveOrUpdateFichaSalud")
    public RestResponse saveOrUpdateFichaSalud(@RequestBody String fichaSaludJson)
            throws JsonParseException, JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '"+ fichaSaludJson + "'");

        FichaSalud fichaSalud = this.objectMapper.readValue(fichaSaludJson, FichaSalud.class);

        this.fichaSaludService.saveFichaSalud(fichaSalud);

        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteFichaSalud")
    public void deleteFichaSalud(@RequestBody String fichaSaludJson) throws Exception {

        this.objectMapper = new ObjectMapper();

        FichaSalud fichaSalud = this.objectMapper.readValue(fichaSaludJson, FichaSalud.class);

        if (fichaSalud.getIdfichasalud() == null) {

            System.out.println("Registro de expediente no encontrado");
        } else {
            this.fichaSaludService.deleteFichaSalud(fichaSalud.getIdfichasalud());
        }
    }

    @GetMapping("/getFichaSaludPaciente")
    public List<FichaSalud> getFichaSaludPaciente(@RequestParam CuidadoAlim idcuidadoalim){

        return this.fichaSaludService.getFichaSaludPaciente(idcuidadoalim);
    }

    @PostMapping("/saveOrUpdateVisitaMedicaSalud")
    public RestResponse saveOrUpdateVisitaMedicaSalud(@RequestBody String visitaMedicaJson)
            throws JsonParseException, JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        VisitaMedica visitaMedica = this.objectMapper.readValue(visitaMedicaJson, VisitaMedica.class);

        this.fichaSaludService.saveVisitaMedica(visitaMedica);

        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteVisitaMedicaSalud")
    public void deleteVisitaMedicaSalud(@RequestBody String visitaMedicaJson) throws Exception {

        this.objectMapper = new ObjectMapper();

        VisitaMedica visitaMedica = this.objectMapper.readValue(visitaMedicaJson, VisitaMedica.class);

        if (visitaMedica.getIdvisitamedica() == null) {

            System.out.println("Registro de expediente no encontrado");

        } else {

            this.fichaSaludService.deleteVisitaMedica(visitaMedica.getIdvisitamedica());
        }
    }

    @GetMapping("/getVisitaMedicaSalud")
    public List<VisitaMedica> getVisitaMedicaSalud(@RequestParam FichaSalud idfichasalud){

        return this.fichaSaludService.getVisitaMedicaPaciente(idfichasalud);
    }

    @PostMapping("/saveOrUpdateVisitaDomiciliariaIntegral")
    public RestResponse saveOrUpdateVisitaDomiciliariaIntegral(@RequestBody String visitaDomIntegralJson)

            throws JsonParseException, JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        Visdomintegral visdomintegral = this.objectMapper.readValue(visitaDomIntegralJson, Visdomintegral.class);

        this.fichaSaludService.saveVisdomintegral(visdomintegral);

        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteVisitaDomiciliariaIntegral")
    public void deleteVisitaDomiciliariaIntegral(@RequestBody String visitaDomIntegralJson) throws Exception {

        this.objectMapper = new ObjectMapper();

        Visdomintegral visdomintegral = this.objectMapper.readValue(visitaDomIntegralJson, Visdomintegral.class);

        if (visdomintegral.getIdvisdomintegral() == null) {

            System.out.println("Registro de expediente no encontrado");

        } else {

            this.fichaSaludService.deleteVisdomintegral(visdomintegral.getIdvisdomintegral());
        }
    }

    @GetMapping("/getVisitaDomiciliariaIntegral")
    public List<Visdomintegral> getVisitaDomiciliariaIntegral(@RequestParam FichaSalud idfichasalud){

        return this.fichaSaludService.getVisitaDomiciliariaIntegral(idfichasalud);
    }

    @PostMapping("/saveOrUpdateCuidadoAlimentacion")
    public RestResponse saveOrUpdateCuidadoAlimentacion(@RequestBody String cuidalimJson)
            throws JsonParseException, JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '"+ cuidalimJson + "'");

        CuidadoAlim cuidadoAlim = this.objectMapper.readValue(cuidalimJson, CuidadoAlim.class);

        this.fichaSaludService.saveCuidadoAlim(cuidadoAlim);

        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteCuidadoAlimentacion")
    public void deleteCuidadoAlimentacion(@RequestBody String cuidalimJson) throws Exception {

        this.objectMapper = new ObjectMapper();

        CuidadoAlim cuidadoAlim = this.objectMapper.readValue(cuidalimJson, CuidadoAlim.class);

        if (cuidadoAlim.getIdcuidalim() == null) {

            System.out.println("Registro de expediente no encontrado");

        } else {

            this.fichaSaludService.deleteCuidadoAlim(cuidadoAlim.getIdcuidalim());
        }
    }

    @GetMapping("/getCuidadoAlimentacion")
    public List<CuidadoAlim> getCuidadoAlimentacion(@RequestParam Expediente expediente){

        return this.fichaSaludService.getCuidadoAlimentacionPaciente(expediente);

    }
}
