package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Biometria;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.BiometriaService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class BiometriaController {

    @Autowired
    protected BiometriaService biometriaService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrBiometriaPaciente", method = RequestMethod.POST)
    public RestResponse saveOrBiometriaPaciente(@RequestBody String BiometriaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        Biometria biometria = this.objectMapper.readValue(BiometriaJson, Biometria.class);
        this.biometriaService.saveBiometria(biometria);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/deleteBiometriaPaciente", method = RequestMethod.POST)
    public void deleteBiometriaPaciente(@RequestBody String BiometriaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Biometria biometria = this.objectMapper.readValue(BiometriaJson, Biometria.class);

        if (biometria.getIdbiometria() == null) {
            throw new Exception("El id esta nulo");
        }
        this.biometriaService.deleteBiometria(biometria.getIdbiometria());
    }

    @RequestMapping(value = "/getBiometriaPaciente", method = RequestMethod.GET)
    public List<Biometria> getBiometriaPaciente(@RequestParam FichaTerapiaFisica idfichaterapiafisica, @RequestParam String anio) {
        return this.biometriaService.getBiometriaPaciente(idfichaterapiafisica,anio);
    }

    @GetMapping("/ListarAniosDeBiometria")
    public List<Biometria> ListarBiometriaPorAnios(@RequestParam FichaTerapiaFisica fichaTerapiaFisica){
        return this.biometriaService.ListarBiometriaPorAnios(fichaTerapiaFisica);
    }
}
