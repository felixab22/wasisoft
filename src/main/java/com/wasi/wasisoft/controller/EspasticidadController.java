package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.Espasticidad;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.service.EspasticidadService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@RestController
public class EspasticidadController {

    @Autowired
    protected EspasticidadService espasticidadService;

    @Autowired
    protected ObjectMapper objectMapper;

    @RequestMapping(value = "/saveOrEspasticidadPaciente", method = RequestMethod.POST)
    public RestResponse saveOrEspasticidadPaciente(@RequestBody String espasticidadJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        Espasticidad espasticidad = this.objectMapper.readValue(espasticidadJson, Espasticidad.class);
        this.espasticidadService.saveEspasticidad(espasticidad);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa");
    }

    @RequestMapping(value = "/deleteEspasticidadPaciente", method = RequestMethod.POST)
    public void deleteEspasticidadPaciente(@RequestBody String espasticidadJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Espasticidad espasticidad = this.objectMapper.readValue(espasticidadJson, Espasticidad.class);

        if (espasticidad.getIdespasticidad() == null) {
            throw new Exception("El id esta nulo");
        }
        this.espasticidadService.deleteEspasticidad(espasticidad.getIdespasticidad());
    }

    @GetMapping(value = "/getEspasticidadPaciente")
    public List<Espasticidad> getEspasticidadPaciente(@RequestParam FichaTerapiaFisica idfichaterapiafisica, @RequestParam String anio) {
        return this.espasticidadService.getEspasticidadPaciente(idfichaterapiafisica, anio);
    }

    @GetMapping("/ListarFechasRegistroEspasticidad")
    public List<Espasticidad> ListarFechasRegistroEspasticidad(@RequestParam FichaTerapiaFisica idficha){
        return this.espasticidadService.ListarEspasticidadPorAnios(idficha);
    }
}
