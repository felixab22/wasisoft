package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.ResultadoEscala;
import com.wasi.wasisoft.service.ResultadoEscalaService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@RestController
public class ResultadoEscalaController {

    @Autowired
    protected ResultadoEscalaService resultadoEscalaService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @PostMapping("/saveOrUpdateResultadoEscala" )
    public RestResponse saveOrUpdateResultadoEscala(@RequestBody String resultadoJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + resultadoJson + "'");

        ResultadoEscala resultadoEscala = this.objectMapper.readValue(resultadoJson, ResultadoEscala.class);

        this.resultadoEscalaService.saveResultadoEscala(resultadoEscala);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa, se ha registrado los resultados de escala del pacienete");
    }

    @GetMapping( "/getResultadoEscalaPaciente")
    List<ResultadoEscala> getResultadoEscalaPaciente (@RequestParam FichaTerapiaFisica idficha, @RequestParam String anio){
        return this.resultadoEscalaService.getResultadoEscalaPaciente(idficha,anio);
    }
    @GetMapping("/listarResultadoEscalaPorFechas")
    List<ResultadoEscala> listarResultadoEscalaPorFechas(@RequestParam FichaTerapiaFisica idficha){
        return this.resultadoEscalaService.ListResultadoEscalaPorFechas(idficha);
    }

    @PostMapping("/deleteResultadoEscalaPaciente")
    public void deleteResultadoEscalaPaciente (@RequestBody String resultadoJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        ResultadoEscala resultadoEscala = this.objectMapper.readValue(resultadoJson, ResultadoEscala.class);

        if (resultadoEscala.getIdresultescala() == null) {
            throw new Exception("El id esta nulo");
        }
        this.resultadoEscalaService.deleteResultadoEscala(resultadoEscala.getIdresultescala());
    }
}
