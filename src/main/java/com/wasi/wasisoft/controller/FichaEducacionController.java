package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.*;
import com.wasi.wasisoft.service.FichaEducacionService;
import com.wasi.wasisoft.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernadez on 02/05/2018.
 */
@RestController
public class FichaEducacionController {

    @Autowired
    protected FichaEducacionService fichaEducacionService;

    @Autowired
    protected ObjectMapper objectMapper;

    @PostMapping("/saveOrUpdateFichaEducacion")
    public RestResponse saveOrUpdateFichaEducacion(@RequestBody String fichaEducacionJson) throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();
        FichaEducacion fichaEducacion = this.objectMapper.readValue(fichaEducacionJson, FichaEducacion.class);
        this.fichaEducacionService.savefichaeducacion(fichaEducacion);
        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteFichaEducacion")
    public void deleteFichaEducacion(@RequestBody String fichaEducacionJson) throws Exception {
        this.objectMapper = new ObjectMapper();
        FichaEducacion fichaEducacion = this.objectMapper.readValue(fichaEducacionJson, FichaEducacion.class);
        if (fichaEducacion.getIdfichaeduc() == null) {
            System.out.println("Registro de expediente no encontrado");
        } else {
            this.fichaEducacionService.deleteFichaEducacion(fichaEducacion.getIdfichaeduc());
        }
    }

    @GetMapping("/getFichaEducacionPaciente")
    public List<FichaEducacion> getFichaEducacionPaciente(@RequestParam Expediente idexpediente){
        return this.fichaEducacionService.getFichaEducacionPaciente(idexpediente);
    }

    @PostMapping("/saveOrUpdateDesarrolloLenguaje")
    public RestResponse saveOrUpdateDesarrolloLenguaje(@RequestBody String desaLenguajeJson) throws JsonParseException,
            JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        Desalenguaje desalenguaje = this.objectMapper.readValue(desaLenguajeJson, Desalenguaje.class);

        this.fichaEducacionService.saveDesarrolloLenguaje(desalenguaje);

        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteDesarrolloLenguaje")
    public void deleteDesarrolloLenguaje(@RequestBody String desaLenguajeJson) throws Exception {

        this.objectMapper = new ObjectMapper();

        Desalenguaje desalenguaje = this.objectMapper.readValue(desaLenguajeJson, Desalenguaje.class);

        if (desalenguaje.getIddesalenguaje() == null) {

            System.out.println("Registro no encontrado");

        } else {
            this.fichaEducacionService.deletDesarrollolenguaje(desalenguaje.getIddesalenguaje());
        }
    }

    @GetMapping("/getDesarrolloLenguaje")
    public List<Desalenguaje> getDesarrolloLenguaje(@RequestParam FichaEducacion idfichaeducacion){

        return this.fichaEducacionService.getDesarrolloLenguaje(idfichaeducacion);
    }

    @PostMapping("/saveOrUpdateDesarrolloSocial")
    public RestResponse saveOrUpdateDesarrolloSocial(@RequestBody String desaSocialJson) throws JsonParseException,
            JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        DesarrolloSocial desarrolloSocial = this.objectMapper.readValue(desaSocialJson, DesarrolloSocial.class);

        this.fichaEducacionService.saveDesarrolloSocial(desarrolloSocial);

        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteDesarrolloSocial")
    public void deleteDesarrolloSocial(@RequestBody String desaSocialJson) throws Exception {

        this.objectMapper = new ObjectMapper();

        DesarrolloSocial desarrolloSocial = this.objectMapper.readValue(desaSocialJson, DesarrolloSocial.class);

        if (desarrolloSocial.getIddesarrollosocial() == null) {

            System.out.println("Registro no encontrado");

        } else {
            this.fichaEducacionService.deleteDesarrolloSocial(desarrolloSocial.getIddesarrollosocial());
        }
    }

    @GetMapping("/getDesarrolloSocial")
    public List<DesarrolloSocial> getDesarrolloSocial(@RequestParam FichaEducacion idfichaeducacion){

        return this.fichaEducacionService.getDesarrolloSocialPaciente(idfichaeducacion);
    }

    @PostMapping("/saveOrUpdateSaludActual")
    public RestResponse saveOrUpdateSaludActal(@RequestBody String saludActualJson) throws JsonParseException,
            JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        SaludActual saludActual = this.objectMapper.readValue(saludActualJson, SaludActual.class);

        this.fichaEducacionService.saveSaludActual(saludActual);

        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteSaludActual")
    public void deleteSaludActual(@RequestBody String saludActualJson) throws Exception {

        this.objectMapper = new ObjectMapper();

        SaludActual saludActual = this.objectMapper.readValue(saludActualJson, SaludActual.class);

        if (saludActual.getIdsaludactual() == null) {

            System.out.println("Registro no encontrado");

        } else {
            this.fichaEducacionService.deleteSaludActual(saludActual.getIdsaludactual());
        }
    }

    @GetMapping("/getSaludActual")
    public List<SaludActual> getSaludActualPaciente(@RequestParam FichaEducacion idfichaeducacion){

        return this.fichaEducacionService.getSaludActualPaciente(idfichaeducacion);
    }

    @PostMapping("/saveOrUpdateEscolaridad")
    public RestResponse saveOrUpdateEscolaridad(@RequestBody String escolaridadJson) throws JsonParseException,
            JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        Escolaridad escolaridad = this.objectMapper.readValue(escolaridadJson, Escolaridad.class);

        this.fichaEducacionService.saveEscolaridad(escolaridad);

        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteEscolaridad")
    public void deleteEscolaridad(@RequestBody String escolaridadJson) throws Exception {

        this.objectMapper = new ObjectMapper();

        Escolaridad escolaridad = this.objectMapper.readValue(escolaridadJson, Escolaridad.class);

        if (escolaridad.getIdescolaridad() == null) {

            System.out.println("Registro no encontrado");

        } else {
            this.fichaEducacionService.deleteEscolaridad(escolaridad.getIdescolaridad());
        }
    }

    @GetMapping("/getEscolaridad")
    public List<Escolaridad> getEscolaridadPaciente(@RequestParam FichaEducacion idfichaeducacion){

        return this.fichaEducacionService.getEscolaridadPaciente(idfichaeducacion);
    }

    @PostMapping("/saveOrUpdateEvaluacionSemestral")
    public RestResponse saveOrUpdateEvaluacionSemestral(@RequestBody String evsemestralJson) throws JsonParseException,
            JsonMappingException, IOException {

        this.objectMapper = new ObjectMapper();

        EvalSemestral evalSemestral = this.objectMapper.readValue(evsemestralJson, EvalSemestral.class);

        this.fichaEducacionService.saveEvalSemestral(evalSemestral);

        return new RestResponse(HttpStatus.OK.value(), "La ficha se guardo correctamente");
    }

    @PostMapping("/deleteEvaluacionSemestral")
    public void deleteEvaluacionSemestral(@RequestBody String evsemestralJson) throws Exception {

        this.objectMapper = new ObjectMapper();

        EvalSemestral evalSemestral = this.objectMapper.readValue(evsemestralJson, EvalSemestral.class);

        if (evalSemestral.getIdevalsemestral() == null) {

            System.out.println("Registro no encontrado");

        } else {
            this.fichaEducacionService.deleteEvalsemestral(evalSemestral.getIdevalsemestral());
        }
    }

    @GetMapping("/getEvaluacionSemestral")
    public List<EvalSemestral> getEvaluacionSemestral(@RequestParam FichaEducacion idfichaeducacion, Integer numevaluacion){

        return this.fichaEducacionService.getEvaluacionSemestral(idfichaeducacion, numevaluacion);
    }
}
