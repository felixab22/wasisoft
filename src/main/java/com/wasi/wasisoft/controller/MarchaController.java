package com.wasi.wasisoft.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Marcha;
import com.wasi.wasisoft.model.MarchaPierna;
import com.wasi.wasisoft.service.MarchaService;
import com.wasi.wasisoft.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@RestController
public class MarchaController {

    @Autowired
    protected MarchaService marchaService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(PersonalController.class);

    @PostMapping("/saveOrUpdateMarcha" )
    public RestResponse saveOrUpdateMarcha(@RequestBody String marchaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + marchaJson + "'");

        Marcha marcha = this.objectMapper.readValue(marchaJson, Marcha.class);

        this.marchaService.saveMarcha(marcha);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa, se ha registrado la ev marcha del pacienete");
    }

    @GetMapping( "/getMarchaPaciente")
    List<Marcha> getMarchaPaciente (@RequestParam FichaTerapiaFisica idficha, @RequestParam String anio){
        return this.marchaService.getMarchaPaciente(idficha, anio);
    }
    @GetMapping("/listarFichaMarchaPorFechas")
    List<Marcha> listarFichaMarchaPorFechas(@RequestParam FichaTerapiaFisica idficha){
        return this.marchaService.listarFichaMarchaPorFechas(idficha);
    }

    @PostMapping("/deleteMarchaPaciente")
    public void deleteMarchaPaciente (@RequestBody String marchaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        Marcha marcha = this.objectMapper.readValue(marchaJson, Marcha.class);

        if (marcha.getIdmarcha() == null) {
            throw new Exception("El id esta nulo");
        }
        this.marchaService.deleteMarcha(marcha.getIdmarcha());
    }

    @PostMapping("/saveOrUpdateMarchaPierna" )
    public RestResponse saveOrUpdateMarchaPierna(@RequestBody String marchapiernaJson)
            throws JsonParseException, JsonMappingException, IOException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMS: '" + marchapiernaJson + "'");

        MarchaPierna marchaPierna = this.objectMapper.readValue(marchapiernaJson, MarchaPierna.class);

        this.marchaService.saveMarchaPierna(marchaPierna);

        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa, se ha registrado la ev marcha del pacienete");
    }

    @GetMapping( "/getMarchaPiernaPaciente")
    List<MarchaPierna> getMarchaPiernaPaciente (@RequestParam Marcha idmarcha, Integer posicionpierna){
        return this.marchaService.getMarchaPierna(idmarcha, posicionpierna);
    }

    @PostMapping("/deleteMarchaPiernaPaciente")
    public void deleteMarchaPiernaPaciente (@RequestBody String marchapiernaJson) throws Exception {
        this.objectMapper = new ObjectMapper();

        MarchaPierna marchaPierna = this.objectMapper.readValue(marchapiernaJson, MarchaPierna.class);

        if (marchaPierna.getIdmarchapierna() == null) {
            throw new Exception("El id esta nulo");
        }
        this.marchaService.deleteMarchaPierna(marchaPierna.getIdmarchapierna());
    }

}
