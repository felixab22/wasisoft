package com.wasi.wasisoft.payload;

import com.wasi.wasisoft.model.Personal;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Set;

/**
 * Created by HeverFernandez on 02/08/17.
 */

public class SignUpRequest {

    @Size(min = 4, max = 40)
    private String name;

    @Size(min = 3, max = 15)
    private String username;

    @Size(max = 40)
    @Email
    private String email;

    private Integer role;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;

//    private Personal idpersonal;
//
//    public Personal getIdpersonal() {
//        return idpersonal;
//    }
//
//    public void setIdpersonal(Personal idpersonal) {
//        this.idpersonal = idpersonal;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }
    //    public Set<String> getRole() {
//        return role;
//    }
//
//    public void setRole(Set<String> role) {
//        this.role = role;
//    }

    @Override
    public String toString() {
        return "SignUpRequest{" +
                "name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role +
                ", password='" + password + '\'' +
                '}';
    }
}
