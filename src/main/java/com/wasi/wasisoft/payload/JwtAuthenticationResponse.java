package com.wasi.wasisoft.payload;

import com.wasi.wasisoft.model.Personal;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by rajeevkumarsingh on 19/08/17.
 */
public class JwtAuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private String username;
    private Collection<? extends GrantedAuthority> authorities;
//    private String nombrepersonal;
//    private String appaterno;
//    private String appmaterno;
//    private String cargo;
//    private String hobby;
//    private  String profesion;
    private Personal personal;

//    public JwtAuthenticationResponse(String accessToken, String username, Collection<? extends GrantedAuthority> authorities) {
//        this.accessToken = accessToken;
//        this.username = username;
//        this.authorities = authorities;
//    }


//    public JwtAuthenticationResponse(String accessToken, String username, Collection<? extends GrantedAuthority> authorities, String nombrepersonal, String appaterno, String appmaterno, String cargo, String hobby, String profesion) {
//        this.accessToken = accessToken;
//        this.username = username;
//        this.authorities = authorities;
//        this.nombrepersonal = nombrepersonal;
//        this.appaterno = appaterno;
//        this.appmaterno = appmaterno;
//        this.cargo = cargo;
//        this.hobby = hobby;
//        this.profesion = profesion;
//    }


    public JwtAuthenticationResponse(String accessToken, String username, Collection<? extends GrantedAuthority> authorities, Personal personal) {
        this.accessToken = accessToken;
        this.username = username;
        this.authorities = authorities;
        this.personal = personal;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

//    public String getNombrepersonal() {
//        return nombrepersonal;
//    }
//
//    public void setNombrepersonal(String nombrepersonal) {
//        this.nombrepersonal = nombrepersonal;
//    }
//
//    public String getAppaterno() {
//        return appaterno;
//    }
//
//    public void setAppaterno(String appaterno) {
//        this.appaterno = appaterno;
//    }
//
//    public String getAppmaterno() {
//        return appmaterno;
//    }
//
//    public void setAppmaterno(String appmaterno) {
//        this.appmaterno = appmaterno;
//    }
//
//    public String getCargo() {
//        return cargo;
//    }
//
//    public void setCargo(String cargo) {
//        this.cargo = cargo;
//    }
//
//    public String getHobby() {
//        return hobby;
//    }
//
//    public void setHobby(String hobby) {
//        this.hobby = hobby;
//    }
//
//    public String getProfesion() {
//        return profesion;
//    }
//
//    public void setProfesion(String profesion) {
//        this.profesion = profesion;
//    }


    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }
}
