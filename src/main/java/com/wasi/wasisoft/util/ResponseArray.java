package com.wasi.wasisoft.util;

/**
 * Created by airo on 30/01/2019.
 */
public class ResponseArray {

    private Object[] cantidad;
    private Object[] tipo;

        public ResponseArray(Object[] cantidad,Object[] tipo)
        {
            this.tipo = tipo;
            this.cantidad = cantidad;

        }
        public Object[] getTipo() { return tipo; }
        public Object[] getCantidad() { return cantidad; }

    public void setCantidad(Object[] cantidad) {
        this.cantidad = cantidad;
    }

    public void setTipo(Object[] tipo) {
        this.tipo = tipo;
    }
}
