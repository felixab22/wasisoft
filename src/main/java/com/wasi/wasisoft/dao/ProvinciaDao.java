package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Provincia;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Nelly on 03/03/2018.
 */
public interface ProvinciaDao extends JpaRepository<Provincia, Long> {

    List<Provincia> findByIddepa (int iddepa);

}
