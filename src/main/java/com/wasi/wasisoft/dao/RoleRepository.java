package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Role;
import com.wasi.wasisoft.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by HeverFernandez on 02/08/17.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByDescripcionrol(RoleName roleName);

    Role findByIdrol(Long idrol);
}
