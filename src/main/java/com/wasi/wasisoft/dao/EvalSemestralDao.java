package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.EvalSemestral;
import com.wasi.wasisoft.model.FichaEducacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Nelly on 09/05/2018.
 */
public interface EvalSemestralDao extends JpaRepository<EvalSemestral, Long> {

    List<EvalSemestral> findAllByIdfichaeducacionAndNumevaluacion(FichaEducacion idficha, Integer numevaluacion);
}
