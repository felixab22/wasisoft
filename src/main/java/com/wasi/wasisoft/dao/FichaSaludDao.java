package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.CuidadoAlim;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaSalud;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
public interface FichaSaludDao extends JpaRepository<FichaSalud, Long> {

    List<FichaSalud> findAllByIdcuidalim(CuidadoAlim cuidadoAlim);

}
