package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.PrestamoMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface PrestamoMaterialDao extends JpaRepository<PrestamoMaterial, Long> {

    // Lista de materiales en garantia
   @Query("SELECT pm FROM PrestamoMaterial pm WHERE pm.tipooperacion=1 and pm.estado_devolucion=false ")
    List<PrestamoMaterial> getMaterialPrestado();

   // Lista de materiales vendidos
   @Query("SELECT pm FROM PrestamoMaterial pm WHERE pm.tipooperacion=0 AND SUBSTRING(pm.finiciooperacion,1,4) = :anio")
    List<PrestamoMaterial> getMaterialVendido(@Param("anio") String anio);

    //Lista los materiales en alquiler
    @Query("SELECT pm FROM PrestamoMaterial pm WHERE pm.tipooperacion=2 AND pm.estado_devolucion=false ")
    List<PrestamoMaterial> getMaterialEnAlquiler();

    List<PrestamoMaterial> findByIdexpediente(Expediente idexpediente);
}
