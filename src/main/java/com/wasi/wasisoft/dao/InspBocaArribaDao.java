package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.InspBocaArriba;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InspBocaArribaDao extends JpaRepository<InspBocaArriba, Long>{

    @Query(" SELECT i FROM InspBocaArriba i WHERE SUBSTRING(i.updatedAt,1,4) = :anio AND i.idfichaterapiafisica = :idfichaterapiafisica")
    List<InspBocaArriba> findByIdfichaterapiafisicaActual(@Param("idfichaterapiafisica") FichaTerapiaFisica idfichaterapiafisica, @Param("anio") String anio);

    @Query("SELECT b.updatedAt FROM InspBocaArriba b WHERE SUBSTRING(b.updatedAt,1,4)<= :anio AND b.idfichaterapiafisica = :idfichaterapiafisica ")
    List<InspBocaArriba> listInspeccionBocaArribaPorFechas(@Param("idfichaterapiafisica") FichaTerapiaFisica idfichaterapiafisica,
                                                           @Param("anio") String anio );
}
