package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Espasticidad;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface EspasticidadDao extends JpaRepository<Espasticidad , Long> {

//    List<Espasticidad> findByFichaTerapiaFisica(FichaTerapiaFisica fichaTerapiaFisica);

    @Query(" SELECT e FROM Espasticidad e WHERE SUBSTRING(e.updatedAt ,1,4) = :anio AND e.fichaTerapiaFisica = :idficha")
    List<Espasticidad> findByFichaTerapiaFisica( @Param("idficha") FichaTerapiaFisica idficha, @Param("anio") String anio);

    @Query("SELECT e.updatedAt FROM Espasticidad e WHERE e.fichaTerapiaFisica = :idfichaterapiafisica ")
    List<Espasticidad> listEspasticidadPorFechas(@Param("idfichaterapiafisica") FichaTerapiaFisica idfichaterapiafisica);

}
