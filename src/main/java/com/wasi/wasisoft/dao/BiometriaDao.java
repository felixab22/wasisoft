package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Biometria;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BiometriaDao extends JpaRepository<Biometria, Long>{

    @Query(" SELECT b FROM Biometria b WHERE SUBSTRING(b.updatedAt,1,4) = :anio AND b.idfichaterapiafisica = :idficha")
    List<Biometria> findByIdfichaterapiafisica( @Param("idficha") FichaTerapiaFisica idficha, @Param("anio") String anio);

    @Query("SELECT b.updatedAt FROM Biometria b WHERE b.idfichaterapiafisica = :idfichaterapiafisica ")
    List<Biometria> listBiometriaPorFechas(@Param("idfichaterapiafisica") FichaTerapiaFisica idfichaterapiafisica);

}
