package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Desalenguaje;
import com.wasi.wasisoft.model.FichaEducacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 02/05/2018.
 */
public interface DesalenguajeDao extends JpaRepository<Desalenguaje, Long> {

    List<Desalenguaje> findByIdfichaeduc(FichaEducacion idficha);
}
