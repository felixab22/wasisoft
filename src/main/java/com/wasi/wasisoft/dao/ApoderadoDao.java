package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Apoderado;
import com.wasi.wasisoft.model.Expediente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ApoderadoDao extends JpaRepository<Apoderado, Long> {

    List<Apoderado> findByIdexpediente(Expediente idexpediente);
}
