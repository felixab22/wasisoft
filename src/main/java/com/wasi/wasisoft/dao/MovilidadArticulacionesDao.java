package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.MovilidadArticulaciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MovilidadArticulacionesDao extends JpaRepository<MovilidadArticulaciones,Long>{

    @Query("SELECT m FROM MovilidadArticulaciones m WHERE SUBSTRING(m.updatedAt,1,4) = :anio AND m.idfichaterapiafisica = :idficha")
    List<MovilidadArticulaciones> findByIdfichaterapiafisica(@Param("idficha") FichaTerapiaFisica idficha, @Param("anio") String anio);

    @Query("SELECT m.updatedAt FROM MovilidadArticulaciones m WHERE m.idfichaterapiafisica = :idficha ")
    List<MovilidadArticulaciones> listMovilidadArticulacionesPorFechas(@Param("idficha") FichaTerapiaFisica idficha);

}

