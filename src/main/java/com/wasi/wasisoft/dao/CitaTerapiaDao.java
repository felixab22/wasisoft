package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.CitaTerapia;
import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Personal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.Date;
import java.util.List;

public interface CitaTerapiaDao extends JpaRepository<CitaTerapia, Long> {

//    @Query("SELECT  c FROM CitaTerapia c where c.createdAt <= myDate")
    @Query("SELECT  c FROM CitaTerapia c ")
    List<CitaTerapia> findByCitasNoAtendidas();

    //List<CitaTerapia> findByIdexpediente(Expediente idexpediente);

    List<CitaTerapia> findAllByIdpersonal(Personal idpersonal);

    void deleteAllByIdpersonal(Personal idpersonal);

}
