package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Fichasocioecon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FichasocioeconDao extends JpaRepository<Fichasocioecon, Long> {

    List<Fichasocioecon> findByIdexpediente(Expediente idexpediente);

    @Query("SELECT fs.tipoFamilia FROM Fichasocioecon fs")
    List<Fichasocioecon> getPacientesByTipoFamilia();

    @Query("SELECT f.tipoSeguro FROM Fichasocioecon f WHERE f.tipoSeguro LIKE 'No tiene%'")
    List<Fichasocioecon> getPacienteByTipoSeguro();

    //Reportes por tipo de familia
    @Query("SELECT COUNT(f.tipoFamilia) as cantidad FROM Fichasocioecon f  WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.tipoFamilia ")
    List<Integer> reportByTipoFamilia(@Param("anio") String anio);
    @Query("SELECT f.tipoFamilia as cantidad FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.tipoFamilia")
    List<String> reportByTipoFamiliaTipo(@Param("anio") String anio);

    //Reportes por zona de vivienda
    @Query("SELECT COUNT(f.zona_vivienda) as cantidad FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.zona_vivienda")
    List<Integer> reportByUbicacionViviendaCantidad(@Param("anio") String anio);
    @Query("SELECT f.zona_vivienda FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.zona_vivienda")
    List<String> reportByUbicacionViviendaTipo(@Param("anio") String anio);

    //Reportes por tenencia de vivienda
    @Query("SELECT COUNT(f.tenenciaVivienda) as cantidad FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.tenenciaVivienda")
    List<Integer> reportByTenenciaViviendaCantidad(@Param("anio") String anio);
    @Query("SELECT f.tenenciaVivienda FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.tenenciaVivienda")
    List<String> reportByTeneciaViviendaTipo(@Param("anio") String anio);

    //Reportes por Usuarios que cuentan con seguro de salud
    @Query("SELECT COUNT(f.tipoSeguro) as cantidad FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.tipoSeguro")
    List<Integer> reportByTipoSeguroCantidad(@Param("anio") String anio);
    @Query("SELECT f.tipoSeguro FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.tipoSeguro")
    List<String> reportByTipoSeguroTipo(@Param("anio") String anio);

    //Reportes por Nivel educativo alcanzado
    @Query("SELECT COUNT(f.nivelEduc) as cantidad FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.nivelEduc")
    List<Integer> reportByNivelEducativoCantidad(@Param("anio") String anio);
    @Query("SELECT f.nivelEduc FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.nivelEduc")
    List<String> reportByNivelEducativoTipo(@Param("anio") String anio);

    //Reportes por tipo de institucion
    @Query("SELECT COUNT(f.tipoInstiAsiste) as cantidad FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.tipoInstiAsiste")
    List<Integer> reportByTipoInstitucionEducativaCantidad(@Param("anio") String anio);
    @Query("SELECT f.tipoInstiAsiste FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.tipoInstiAsiste")
    List<String> reportByTipoInstitucionEducativaTipo(@Param("anio") String anio);

    //Reportes por necesidad de certificado de discapacidad
    @Query("SELECT COUNT(f.necCertifDiscap) as cantidad FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.necCertifDiscap")
    List<Integer> reportByCertificadoDiscapacidadCantidad(@Param("anio") String anio);
    @Query("SELECT f.necCertifDiscap FROM Fichasocioecon f WHERE SUBSTRING(f.updatedAt,1,4) = :anio GROUP BY f.necCertifDiscap")
    List<String> reportByCertificadoDiscapacidadTipo(@Param("anio") String anio);


}
