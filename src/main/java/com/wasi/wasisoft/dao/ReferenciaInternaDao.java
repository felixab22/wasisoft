package com.wasi.wasisoft.dao;

        import com.wasi.wasisoft.model.Area;
        import com.wasi.wasisoft.model.Expediente;
        import com.wasi.wasisoft.model.ReferenciaInterna;
        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.data.jpa.repository.Query;
        import org.springframework.data.repository.query.Param;

        import java.util.List;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
public interface ReferenciaInternaDao extends JpaRepository<ReferenciaInterna, Long>{

    @Query("SELECT r FROM ReferenciaInterna r WHERE r.idarea = :idarea and r.idexpediente = :idexpediente and SUBSTRING(r.freferencia,1,4) = :anio")
    List<ReferenciaInterna> buscarReferenciaInterna(@Param("idarea") Area idarea, @Param("idexpediente") Expediente idexpediente, @Param("anio") String anio);

    @Query("SELECT r FROM ReferenciaInterna r WHERE SUBSTRING(r.freferencia,1,4) = :anio AND r.idarea = :idarea")
    List<ReferenciaInterna> findByIdareaReferencia(@Param("idarea") Area idarea, @Param("anio") String anio);

    @Query("SELECT r FROM ReferenciaInterna r WHERE r.idexpediente = :idexpediente AND SUBSTRING(r.freferencia,1,4)= :anio")
    List<ReferenciaInterna> ObtenerReferenciaInterna(@Param("idexpediente") Expediente idexpediente, @Param("anio")String anio);
}
