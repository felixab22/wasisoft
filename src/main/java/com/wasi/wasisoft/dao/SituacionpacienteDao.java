package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Situacionpaciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SituacionpacienteDao extends JpaRepository<Situacionpaciente, Long> {

    List<Situacionpaciente> findByIdexpediente(Expediente idexpediente);


//    @Query(" SELECT  e, p.pacienteaceptado,p.estado FROM Situacionpaciente p LEFT JOIN p.Expediente e ON p.idexpediente = e.id_expediente WHERE p.estado=1 ")
//    @Query(" SELECT  e, p.pacienteaceptado,p.estado FROM Situacionpaciente p JOIN p.Expediente e  WHERE p.estado=1 ")


    @Query("SELECT  p.idexpediente FROM Situacionpaciente p WHERE  p.estado=1 and  p.pacienteaceptado=1")
    List<Situacionpaciente> findAllPacienteActivoAndAceptado();

    @Query(" SELECT  p.idexpediente FROM Situacionpaciente p WHERE  p.estado=1 ")
      List<Situacionpaciente> findAllExpedienteByPacienteactivo();

}
