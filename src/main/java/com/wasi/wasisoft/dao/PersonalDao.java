package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Area;
import com.wasi.wasisoft.model.Personal;
import com.wasi.wasisoft.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PersonalDao extends JpaRepository<Personal, Long> {
    @SuppressWarnings("unchecked")
    Personal save(Personal personal);

    @Query ("SELECT p FROM Personal  p WHERE p.idarea IN (2, 3, 4, 6) AND p.estado=1")
    List<Personal> findPersonalByAreaRehabilitacion();

    List<Personal> findAllByIdarea(Area idarea);

    @Query("SELECT p FROM Personal p WHERE p.estado=1"  )
    List<Personal> listPersonalActive();

    @Query("SELECT p FROM Personal p WHERE p.estado=0"  )
    List<Personal> listPersonalInactive();

    @Query("SELECT p FROM Personal p WHERE p.user.username = :username")
    Personal obtenerPersonalPorUsuario(@Param("username") String username);

    Personal getPersonalByUser(User user);
}


