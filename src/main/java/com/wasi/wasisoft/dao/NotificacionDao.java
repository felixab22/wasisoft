package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Notificacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


/**
 * Created by airo on 23/01/2019.
 */
public interface NotificacionDao extends JpaRepository<Notificacion,Long> {

    List<Notificacion> findAllByNombrepaciente(String nombrepaciente);

    void deleteAllByNombrepaciente(String nombrepaciente);

    //lISTA DE PACINTES QUE NO VINIERON MAS DE 2 VECES
    @Query("SELECT  n.nombrepaciente FROM Notificacion n GROUP BY n.nombrepaciente HAVING COUNT (n.nombrepaciente) > 2")
    List<String> reportByInasistenciaCantidad();
}

