package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaAto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by HeverFernandez on 26/04/2018.
 */
public interface FichaAtoDao extends JpaRepository<FichaAto, Long> {

    @Query("SELECT a FROM FichaAto a WHERE SUBSTRING(a.updatedAt,1,4) = :anio AND a.idexpediente = :idexpediente")
    List<FichaAto> findByIdexpediente(@Param("idexpediente") Expediente idexpediente, @Param("anio")String anio);

    @Query("SELECT a.updatedAt FROM FichaAto a WHERE a.idexpediente = :idexpediente")
    List<FichaAto> listarFichaAtoPorFechas(@Param("idexpediente")Expediente idexpediente);

}
