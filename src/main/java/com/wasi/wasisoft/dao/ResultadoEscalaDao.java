package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.ResultadoEscala;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface ResultadoEscalaDao extends JpaRepository<ResultadoEscala, Long>{

    @Query("SELECT r FROM ResultadoEscala r WHERE SUBSTRING(r.updatedAt,1,4) = :anio AND r.fichaTerapiaFisica = :idficha")
    List<ResultadoEscala> findByFichaTerapiaFisica(@Param("idficha") FichaTerapiaFisica idficha, @Param("anio") String anio);

    @Query("SELECT r.updatedAt FROM ResultadoEscala r WHERE r.fichaTerapiaFisica = :idficha")
    List<ResultadoEscala> listaResultadoEscalaPorFechas(@Param("idficha") FichaTerapiaFisica idficha);
}
