package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Evpsicologica;
import com.wasi.wasisoft.model.FichaPsicologia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by HeverFernandez on 10/09/2018.
 */
public interface EvpsicologicaDao extends JpaRepository<Evpsicologica, Long> {

    @Query("select e FROM Evpsicologica e where e.idfichapsicologia = :idfichapsico ORDER BY e.fevaluacion DESC")
    List<Evpsicologica>  findAllByIdfichapsicologia(@Param("idfichapsico") FichaPsicologia idfichapsico);
}

