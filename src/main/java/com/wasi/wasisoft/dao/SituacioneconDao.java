package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.Situacionecon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SituacioneconDao extends JpaRepository<Situacionecon, Long> {

//    @Query("SELECT s.id_situacionecon,s.idexpediente, (s.padreing+s.madreing+s.hermanosing+s.abuelosing+s.primosing+s.parientesing+s.tiosing+s.noparientesing), " +
//            " (s.alimentacionegr+s.viviendaegre+s.educacionegre+s.aguaegre+s.luzegre+s.telefonoegre+s.pasajesegre+s.otrosegre)" +
//            "FROM Situacionecon s")
    List<Situacionecon> findByIdexpediente(Expediente idexpediente);

}

