package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.AnamnesisFamiliar;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnamnesisFamiliarDao extends JpaRepository<AnamnesisFamiliar,Long>{

    List<AnamnesisFamiliar> findByIdfichaterapiafisica(FichaTerapiaFisica fichaTerapiaFisica);
}
