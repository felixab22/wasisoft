package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Marcha;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface MarchaDao extends JpaRepository<Marcha, Long> {

    @Query("SELECT m FROM Marcha m WHERE SUBSTRING(m.updatedAt,1,4) = :anio AND m.fichaTerapiaFisica = :idficha")
    List<Marcha> findByFichaTerapiaFisica(@Param("idficha") FichaTerapiaFisica idficha, @Param("anio") String anio);

    @Query("SELECT m.updatedAt FROM Marcha m WHERE m.fichaTerapiaFisica = :idficha")
    List<Marcha> ListFichaMarchaPorFechas(@Param("idficha") FichaTerapiaFisica idficha);
 }
