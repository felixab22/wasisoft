package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.ReferenciaExterna;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReferenciaExternaDao extends JpaRepository<ReferenciaExterna, Long> {

    List<ReferenciaExterna> findByIdexpediente(Expediente idexpediente);
}
