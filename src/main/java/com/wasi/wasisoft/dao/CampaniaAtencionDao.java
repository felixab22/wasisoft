package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.CampaniaAtencion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by airo on 1/02/2019.
 */
public interface CampaniaAtencionDao extends JpaRepository<CampaniaAtencion, Long> {
}
