package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Marcha;
import com.wasi.wasisoft.model.MarchaPierna;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 18/04/2018.
 */
public interface MarchaPiernaDao extends JpaRepository<MarchaPierna, Long> {

    List<MarchaPierna> findAllByMarchaAndPosicionpierna(Marcha marcha, Integer posicionpierna);
}
