package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Asistencia;
import com.wasi.wasisoft.model.Personal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AsistenciaDao extends JpaRepository<Asistencia, Long>{

    List<Asistencia> findAllByPaciente(String paciente);

    @Query ("SELECT a FROM Asistencia a WHERE a.paciente=:paciente and a.tipoasistencia=0")
    List<Asistencia> listarInasistencias(String paciente);

    List<Asistencia> findAllByPersonal(Personal personal);

    @Query("SELECT a FROM Asistencia a order by a.idasistencia DESC ")
    List<Asistencia> findAllAsistencia();
}



