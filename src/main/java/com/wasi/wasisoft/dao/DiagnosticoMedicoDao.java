package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.DiagnosticoMedico;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DiagnosticoMedicoDao extends JpaRepository<DiagnosticoMedico, Long> {

    List<DiagnosticoMedico> findByIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica);

    @Query("SELECT d FROM DiagnosticoMedico d WHERE d.idfichaterapiafisica.idexpediente=21")
    List<DiagnosticoMedico> findDiagnosticomedicoPaciente();

}
