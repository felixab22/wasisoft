package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Departamento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartamentoDao  extends JpaRepository<Departamento, Long>{

}
