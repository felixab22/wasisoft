package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.ComposicionFamiliar;
import com.wasi.wasisoft.model.Expediente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ComposicionFamiliarDao extends JpaRepository<ComposicionFamiliar, Long>{

    List<ComposicionFamiliar> findByIdexpediente(Expediente idexpediente);
}
