package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.FichaTerapiaFisica;
import com.wasi.wasisoft.model.Fuerza;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
public interface FuerzaDao extends JpaRepository<Fuerza, Long>{

    @Query("SELECT f FROM Fuerza f WHERE SUBSTRING(f.updatedAt,1,4) = :anio AND f.fichaTerapiaFisica = :idficha")
    List<Fuerza> findByIdfichaterapiafisica(@Param("idficha") FichaTerapiaFisica idficha, @Param("anio") String anio);

    @Query("SELECT f.updatedAt FROM Fuerza f WHERE f.fichaTerapiaFisica = :idficha ")
    List<Fuerza> listFichaFuerzaPorFechas(@Param("idficha") FichaTerapiaFisica idficha);

}
