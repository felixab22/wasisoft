package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.CampaniaAtencion;
import com.wasi.wasisoft.model.RegistroCampania;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by airo on 1/02/2019.
 */
public interface RegistroCampaniaDao extends JpaRepository<RegistroCampania, Long> {
    List<RegistroCampania> findByFregistro(String fregistro);
    List<RegistroCampania> findByCampaniaatencion(CampaniaAtencion campaniaAtencion);
    RegistroCampania findByNombrepaciente(String nombre);
}
