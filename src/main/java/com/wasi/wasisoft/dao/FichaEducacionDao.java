package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.FichaEducacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Nelly on 02/05/2018.
 */
public interface FichaEducacionDao extends JpaRepository<FichaEducacion, Long> {

    List<FichaEducacion> findAllByIdexpediente(Expediente idexpediente);
}
