package com.wasi.wasisoft.dao;


import com.wasi.wasisoft.model.Area;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AreaDao extends JpaRepository<Area, Long> {

    @Query("SELECT a FROM  Area a WHERE  a.id_area between 3 and 7")
//    @Query("SELECT a FROM Area a WHERE a.descripcionarea='Salud' OR a.descripcionarea='Terapia Ocupacional' " +
//            "OR a.descripcionarea='Educación Inclusiva' OR a.descripcionarea = 'Psicología'")
    List<Area> findAllAreaReferenciaInterna();
}
