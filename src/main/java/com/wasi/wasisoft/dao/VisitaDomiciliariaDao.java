package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.Expediente;
import com.wasi.wasisoft.model.VisitaDomiciliaria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VisitaDomiciliariaDao extends JpaRepository<VisitaDomiciliaria,Long>{

    List<VisitaDomiciliaria> findByIdexpediente(Expediente idexpediente);

    @Query("SELECT v FROM VisitaDomiciliaria v WHERE v.idexpediente.dni = :dni ")
    List<VisitaDomiciliaria> obtenerVisitasPorDni(@Param("dni") int dni);

    @Query("SELECT v FROM VisitaDomiciliaria v WHERE v.idexpediente.codigohistorial = :codigohistorial ")
    List<VisitaDomiciliaria> obtenerVisitasPorHistorial(@Param("codigohistorial") String codigohistorial);
}
