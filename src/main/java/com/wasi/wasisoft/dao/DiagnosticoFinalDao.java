package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.DiagnosticoFinal;
import com.wasi.wasisoft.model.FichaTerapiaFisica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by HeverFernandez on 17/04/2018.
 */
public interface DiagnosticoFinalDao extends JpaRepository<DiagnosticoFinal, Long> {

    @Query("SELECT d FROM DiagnosticoFinal d WHERE SUBSTRING(d.updatedAt,1,4) = :anio AND d.idfichaterapiafisica = :idficha")
    List<DiagnosticoFinal> findByIdfichaterapiafisica(@Param("idficha") FichaTerapiaFisica idficha, @Param("anio")String anio);

    @Query("SELECT d.updatedAt FROM DiagnosticoFinal d WHERE d.idfichaterapiafisica = :idficha")
    List<DiagnosticoFinal> listarDiagnosticoFinalPorFechas(@Param("idficha") FichaTerapiaFisica idficha);
}
