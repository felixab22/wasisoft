package com.wasi.wasisoft.dao;

import com.wasi.wasisoft.model.DiagnosticoFinal;
import com.wasi.wasisoft.model.PlanTrabajo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by HeverFernandez on 18/04/2018.
 */
public interface PlanTrabajoDao extends JpaRepository<PlanTrabajo, Long> {

    List<PlanTrabajo> findAllByIddiagnosticofinal(DiagnosticoFinal iddiagnosticofinal);

}
