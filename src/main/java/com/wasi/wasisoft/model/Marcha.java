package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@Entity
@Table(name = "marcha")
public class Marcha extends DateAudit implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_marcha")
    private Long idmarcha;

    @Size(max = 100)
    @Column(name = "poscabeza")
    private String poscabeza;
    @Size(max = 100)
    @Column(name = "poshombro")
    private String poshombro;
    @Size(max = 50)
    @Column(name = "difdistancia")
    private String difdistancia;
    @Size(max = 100)
    @Column(name = "anchopiernas")
    private String anchopiernas;
    @Size(max = 100)
    @Column(name = "balanceobrazos")
    private String balanceobrazos;
    @Size(max = 100)
    @Column(name = "dolor")
    private String dolor;
    @Size(max = 300)
    @Column(name = "conclusionmarcha")
    private String conclusionmarcha;
    @Size(max = 300)
    @Column(name = "obsnivelact")
    private String obsnivelact;
    @Size(max = 300)
    @Column(name = "concluactiv")
    private String concluactiv;

    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica fichaTerapiaFisica;

    public Marcha() {
    }

    public Marcha(Long idmarcha) {
        this.idmarcha = idmarcha;
    }
    public Marcha(String poscabeza, String poshombro, String difdistancia, String anchopiernas, String balanceobrazos, String dolor, String conclusionmarcha, String obsnivelact, String concluactiv, FichaTerapiaFisica fichaTerapiaFisica) {
        this.poscabeza = poscabeza;
        this.poshombro = poshombro;
        this.difdistancia = difdistancia;
        this.anchopiernas = anchopiernas;
        this.balanceobrazos = balanceobrazos;
        this.dolor = dolor;
        this.conclusionmarcha = conclusionmarcha;
        this.obsnivelact = obsnivelact;
        this.concluactiv = concluactiv;
        this.fichaTerapiaFisica = fichaTerapiaFisica;
    }

    public Long getIdmarcha() {
        return idmarcha;
    }

    public void setIdmarcha(Long idmarcha) {
        this.idmarcha = idmarcha;
    }

    public String getPoscabeza() {
        return poscabeza;
    }

    public void setPoscabeza(String poscabeza) {
        this.poscabeza = poscabeza;
    }

    public String getPoshombro() {
        return poshombro;
    }

    public void setPoshombro(String poshombro) {
        this.poshombro = poshombro;
    }

    public String getDifdistancia() {
        return difdistancia;
    }

    public void setDifdistancia(String difdistancia) {
        this.difdistancia = difdistancia;
    }

    public String getAnchopiernas() {
        return anchopiernas;
    }

    public void setAnchopiernas(String anchopiernas) {
        this.anchopiernas = anchopiernas;
    }

    public String getBalanceobrazos() {
        return balanceobrazos;
    }

    public void setBalanceobrazos(String balanceobrazos) {
        this.balanceobrazos = balanceobrazos;
    }

    public String getDolor() {
        return dolor;
    }

    public void setDolor(String dolor) {
        this.dolor = dolor;
    }

    public String getConclusionmarcha() {
        return conclusionmarcha;
    }

    public void setConclusionmarcha(String conclusionmarcha) {
        this.conclusionmarcha = conclusionmarcha;
    }

    public String getObsnivelact() {
        return obsnivelact;
    }

    public void setObsnivelact(String obsnivelact) {
        this.obsnivelact = obsnivelact;
    }

    public String getConcluactiv() {
        return concluactiv;
    }

    public void setConcluactiv(String concluactiv) {
        this.concluactiv = concluactiv;
    }

    public FichaTerapiaFisica getFichaTerapiaFisica() {
        return fichaTerapiaFisica;
    }

    public void setFichaTerapiaFisica(FichaTerapiaFisica fichaTerapiaFisica) {
        this.fichaTerapiaFisica = fichaTerapiaFisica;
    }
}
