package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
@Entity
@Table (name = "visdomintegral")
public class Visdomintegral implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_visdomintegral")
    private Long idvisdomintegral;

    @Column(name = "numfichaatenc")
    private int numfichaatenc;
    //Indica el numero de seguimineto (1, 2 y 3)
    @Column(name = "numseguimiento")
    private int numseguimiento;

    @Column(name = "edad")
    private int edad;

    @Column(name = "fechavisita")
    @Temporal(TemporalType.DATE)
    private Date fechavisita;

    @Size(max = 20)
    @Column(name = "criteriouno")
    private int criteriouno;
    @Size(max = 20)
    @Column(name = "criteriodos")
    private int criteriodos;
    @Size(max = 20)
    @Column(name = "criteriotres")
    private int criteriotres;
    @Size(max = 20)
    @Column(name = "criteriocuatro")
    private int criteriocuatro;
    @Size(max = 20)
    @Column(name = "puntajetotal")
    private int puntajetotal;

    @JoinColumn(name = "id_fichasalud", referencedColumnName = "id_fichasalud")
    @ManyToOne
    private FichaSalud idfichasalud;

    public Visdomintegral() {
    }

    public Visdomintegral( Long idvisdomintegral) {
        this.idvisdomintegral = idvisdomintegral;
    }

    public Visdomintegral(int numfichaatenc, int numseguimiento, int edad, Date fechavisita, @Size(max = 20) int criteriouno, @Size(max = 20) int criteriodos, @Size(max = 20) int criteriotres, @Size(max = 20) int criteriocuatro, @Size(max = 20) int puntajetotal, FichaSalud idfichasalud) {
        this.numfichaatenc = numfichaatenc;
        this.numseguimiento = numseguimiento;
        this.edad = edad;
        this.fechavisita = fechavisita;
        this.criteriouno = criteriouno;
        this.criteriodos = criteriodos;
        this.criteriotres = criteriotres;
        this.criteriocuatro = criteriocuatro;
        this.puntajetotal = puntajetotal;
        this.idfichasalud = idfichasalud;
    }

    public Long getIdvisdomintegral() {
        return idvisdomintegral;
    }

    public void setIdvisdomintegral(Long idvisdomintegral) {
        this.idvisdomintegral = idvisdomintegral;
    }

    public int getNumfichaatenc() {
        return numfichaatenc;
    }

    public void setNumfichaatenc(int numfichaatenc) {
        this.numfichaatenc = numfichaatenc;
    }

    public int getNumseguimiento() {
        return numseguimiento;
    }

    public void setNumseguimiento(int numseguimiento) {
        this.numseguimiento = numseguimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Date getFechavisita() {
        return fechavisita;
    }

    public void setFechavisita(Date fechavisita) {
        this.fechavisita = fechavisita;
    }

    public int getCriteriouno() {
        return criteriouno;
    }

    public void setCriteriouno(int criteriouno) {
        this.criteriouno = criteriouno;
    }

    public int getCriteriodos() {
        return criteriodos;
    }

    public void setCriteriodos(int criteriodos) {
        this.criteriodos = criteriodos;
    }

    public int getCriteriotres() {
        return criteriotres;
    }

    public void setCriteriotres(int criteriotres) {
        this.criteriotres = criteriotres;
    }

    public int getCriteriocuatro() {
        return criteriocuatro;
    }

    public void setCriteriocuatro(int criteriocuatro) {
        this.criteriocuatro = criteriocuatro;
    }

    public int getPuntajetotal() {
        return puntajetotal;
    }

    public void setPuntajetotal(int puntajetotal) {
        this.puntajetotal = puntajetotal;
    }

    public FichaSalud getIdfichasalud() {
        return idfichasalud;
    }

    public void setIdfichasalud(FichaSalud idfichasalud) {
        this.idfichasalud = idfichasalud;
    }
}
