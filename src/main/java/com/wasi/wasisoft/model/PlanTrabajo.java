package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 17/04/2018.
 */
@Entity
@Table(name = "plantrabajo")
public class PlanTrabajo implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_plantrabajo")
    private Long idplantrabajo;

    @Size(max = 400)
    @Column(name = "probderivados")
    private String probderivados;

    @Size(max = 500)
    @Column(name = "objetivo")
    private String objetivo;

    @Size(max = 400)
    @Column(name = "plandetrabajo")
    private String plandetrabajo;

    @Column(name = "fseguimiento")
    @Temporal(TemporalType.DATE)
    private Date fseguimiento;

    @Size(max = 500)
    @Column(name = "resulseg")
    private String resulseg;

    @JoinColumn(name = "id_diagnosticofinal", referencedColumnName = "id_diagnosticofinal")
    @ManyToOne
    private DiagnosticoFinal iddiagnosticofinal;

    public PlanTrabajo() {
    }


    public PlanTrabajo(Long idplantrabajo) {
        this.idplantrabajo = idplantrabajo;
    }

    public PlanTrabajo(String probderivados, String objetivo, String plandetrabajo, Date fseguimiento, String resulseg, DiagnosticoFinal iddiagnosticofinal ) {
        this.probderivados = probderivados;
        this.objetivo = objetivo;
        this.plandetrabajo = plandetrabajo;
        this.fseguimiento = fseguimiento;
        this.resulseg = resulseg;
        this.iddiagnosticofinal = iddiagnosticofinal;
    }

    public Long getIdplantrabajo() {
        return idplantrabajo;
    }

    public void setIdplantrabajo(Long idplantrabajo) {
        this.idplantrabajo = idplantrabajo;
    }

    public String getProbderivados() {
        return probderivados;
    }

    public void setProbderivados(String probderivados) {
        this.probderivados = probderivados;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getPlandetrabajo() {
        return plandetrabajo;
    }

    public void setPlandetrabajo(String plandetrabajo) {
        this.plandetrabajo = plandetrabajo;
    }

    public Date getFseguimiento() {
        return fseguimiento;
    }

    public void setFseguimiento(Date fseguimiento) {
        this.fseguimiento = fseguimiento;
    }

    public String getResulseg() {
        return resulseg;
    }

    public void setResulseg(String resulseg) {
        this.resulseg = resulseg;
    }

    public DiagnosticoFinal getIddiagnosticofinal() {
        return iddiagnosticofinal;
    }

    public void setiddiagnosticofinal(DiagnosticoFinal iddiagnosticofinal) {
        this.iddiagnosticofinal = iddiagnosticofinal;
    }
}
