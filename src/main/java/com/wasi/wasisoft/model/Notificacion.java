package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by airo on 23/01/2019.
 */
@Entity
@Table(name = "notificacion")
public class Notificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idnotificacion")
    private Long idnotificacion;

    @Column(name = "nombrepaciente")
    private String nombrepaciente;

    @Column(name = "finasistencia")
    private String finasistencia;

    @ManyToOne
    @JoinColumn(name = "id_personal")
    private Personal personal;

    public Notificacion(){}

    public Notificacion(Long idnotificacion){
        this.idnotificacion=idnotificacion;
    }

    public Notificacion(String nombrepaciente, String finasistencia, Personal personal) {
        this.nombrepaciente = nombrepaciente;
        this.finasistencia = finasistencia;
        this.personal = personal;
    }

    public Long getIdnotificacion() {
        return idnotificacion;
    }

    public void setIdnotificacion(Long idnotificacion) {
        this.idnotificacion = idnotificacion;
    }

    public String getNombrepaciente() {
        return nombrepaciente;
    }

    public void setNombrepaciente(String nombrepaciente) {
        this.nombrepaciente = nombrepaciente;
    }

    public String getFinasistencia() {
        return finasistencia;
    }

    public void setFinasistencia(String finasistencia) {
        this.finasistencia = finasistencia;
    }

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    @Override
    public String toString() {
        return "Notificacion{" +
                "idnotificacion=" + idnotificacion +
                ", nombrepaciente='" + nombrepaciente + '\'' +
                ", finasistencia='" + finasistencia + '\'' +
                '}';
    }
}

