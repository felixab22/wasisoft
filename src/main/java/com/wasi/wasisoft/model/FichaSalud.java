package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 25/04/2018.
 */
@Entity
@Table(name = "fichasalud")
public class FichaSalud extends DateAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_fichasalud")
    private Long idfichasalud;

    @Size(max = 1000)
    @Column(name = "diag_medico")
    private String diagmedico;

    @Size(max = 1000)
    @Column(name = "diag_nutricini")
    private String diagnutricini;

    @Size(max = 500)
    @Column(name = "sec_discapacidad")
    private String secdiscapacidad;

    @Size(max = 1000)
    @Column(name = "inicio")
    private String inicio;

    @Size(max = 1000)
    @Column(name = "descencadena")
    private String descencadena;

    @Size(max = 800)
    @Column(name = "observaciones")
    private String observaciones;
    @Size(max = 800)
    @Column(name = "enfer_padec_frec")
    private String enferpadecfrec;
    @Size(max = 800)
    @Column(name = "medicado_mot")
    private String medicadomot;
    @Size(max = 800)
    @Column(name = "sufre_alergia")
    private String sufrealergia;
    @Size(max = 1000)
    @Column(name = "recibe_terapia")
    private String recibeterapia;
    @Size(max = 1000)
    @Column(name = "tiempo_terap")
    private String tiempoterap;
    @Size(max = 1000)
    @Column(name = "lugar_terapia")
    private String lugarterapia;
    @Size(max = 1000)
    @Column(name = "motivo_noatencion")
    private String motivonoatencion;

    @JoinColumn(name = "id_cuidalim", referencedColumnName = "id_cuidalim")
    @ManyToOne
    private CuidadoAlim idcuidalim;

    public FichaSalud(Long idfichasalud) {
        this.idfichasalud = idfichasalud;
    }

    public FichaSalud() {
    }

    public FichaSalud(String diagmedico, String diagnutricini, String secdiscapacidad, String inicio, String descencadena, String observaciones, String enferpadecfrec, String medicadomot, String sufrealergia, String recibeterapia, String tiempoterap, String lugarterapia, String motivonoatencion, CuidadoAlim idcuidalim) {
        this.diagmedico = diagmedico;
        this.diagnutricini = diagnutricini;
        this.secdiscapacidad = secdiscapacidad;
        this.inicio = inicio;
        this.descencadena = descencadena;
        this.observaciones = observaciones;
        this.enferpadecfrec = enferpadecfrec;
        this.medicadomot = medicadomot;
        this.sufrealergia = sufrealergia;
        this.recibeterapia = recibeterapia;
        this.tiempoterap = tiempoterap;
        this.lugarterapia = lugarterapia;
        this.motivonoatencion = motivonoatencion;
        this.idcuidalim = idcuidalim;
    }

    public Long getIdfichasalud() {
        return idfichasalud;
    }

    public void setIdfichasalud(Long idfichasalud) {
        this.idfichasalud = idfichasalud;
    }

    public String getDiagmedico() {
        return diagmedico;
    }

    public void setDiagmedico(String diagmedico) {
        this.diagmedico = diagmedico;
    }

    public String getDiagnutricini() {
        return diagnutricini;
    }

    public void setDiagnutricini(String diagnutricini) {
        this.diagnutricini = diagnutricini;
    }

    public String getSecdiscapacidad() {
        return secdiscapacidad;
    }

    public void setSecdiscapacidad(String secdiscapacidad) {
        this.secdiscapacidad = secdiscapacidad;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getDescencadena() {
        return descencadena;
    }

    public void setDescencadena(String descencadena) {
        this.descencadena = descencadena;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getEnferpadecfrec() {
        return enferpadecfrec;
    }

    public void setEnferpadecfrec(String enferpadecfrec) {
        this.enferpadecfrec = enferpadecfrec;
    }

    public String getMedicadomot() {
        return medicadomot;
    }

    public void setMedicadomot(String medicadomot) {
        this.medicadomot = medicadomot;
    }

    public String getSufrealergia() {
        return sufrealergia;
    }

    public void setSufrealergia(String sufrealergia) {
        this.sufrealergia = sufrealergia;
    }

    public String getRecibeterapia() {
        return recibeterapia;
    }

    public void setRecibeterapia(String recibeterapia) {
        this.recibeterapia = recibeterapia;
    }

    public String getTiempoterap() {
        return tiempoterap;
    }

    public void setTiempoterap(String tiempoterap) {
        this.tiempoterap = tiempoterap;
    }

    public String getLugarterapia() {
        return lugarterapia;
    }

    public void setLugarterapia(String lugarterapia) {
        this.lugarterapia = lugarterapia;
    }

    public String getMotivonoatencion() {
        return motivonoatencion;
    }

    public void setMotivonoatencion(String motivonoatencion) {
        this.motivonoatencion = motivonoatencion;
    }

    public CuidadoAlim getIdcuidalim() {
        return idcuidalim;
    }

    public void setIdcuidalim(CuidadoAlim idcuidalim) {
        this.idcuidalim = idcuidalim;
    }
}
