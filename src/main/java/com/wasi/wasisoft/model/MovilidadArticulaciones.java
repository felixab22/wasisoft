package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table (name = "movarticulaciones")
public class MovilidadArticulaciones extends DateAudit implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_movarticulaciones")
    private Long idmovarticulaciones;

    @Column(name = "obscaderaext")
    private String obscadeext;

    @Column(name = "obscaderaflex")
    private String obscaderaflex;
    @Column(name = "obscaderaabduext")
    private String obscaderaabduext;
    @Column(name = "obscaderaabduflex")
    private String obscaderaabduflex;
    @Column(name = "obscaderarotext")
    private String obscaderarotext;
    @Column(name = "obscaderarotint")
    private String obscaderarotint;
    @Column(name = "obsrodillaextext")
    private String obsrodillaextext;
    @Column(name = "obsrodillaextflex")
    private String obsrodillaextflex;
    @Column(name = "obsrodillaflex")
    private String obsrodillaflex;
    @Column(name = "obstobillosoleus")
    private String obstobillosoleus;
    @Column(name = "obstobillogastoc")
    private String obstobillogastoc;
    @Column(name = "obshombroabdu")
    private String obshombroabdu;
    @Column(name = "obshombroantef")
    private String obshombroantef;
    @Column(name = "obshombroexor")
    private String obshombroexor;
    @Column(name = "obshombroendor")
    private String obshombroendor;
    @Column(name = "obscodoexte")
    private String obscodoext;
    @Column(name = "obscodosupin")
    private String obscodosupin;
    @Column(name = "obsmanodors")
    private String obsmanodorsif;
    @Column(name = "obsmanoflex")
    private String obsmanoflex;

    @Size(max = 500)
    @Column(name = "conclusion")
    private String conclusion;
    @Size(max = 400)
    @Column(name = "dif_izq_derecha")
    private String difIzqDerecha;
    @Size(max = 400)
    @Column(name = "limitaciones")
    private String limitaciones;
    @Size(max = 500)
    @Column(name = "obsfinal")
    private String obsfinal;
    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica idfichaterapiafisica;

    public MovilidadArticulaciones() {
    }

    public MovilidadArticulaciones(Long idmovarticulaciones){
        this.idmovarticulaciones = idmovarticulaciones;
    }

    public MovilidadArticulaciones(String obscadeext, String obscaderaflex, String obscaderaabduext, String obscaderaabduflex, String obscaderarotext, String obscaderarotint, String obsrodillaextext, String obsrodillaextflex, String obsrodillaflex, String obstobillosoleus, String obstobillogastoc, String obshombroabdu, String obshombroantef, String obshombroexor, String obshombroendor, String obscodoext, String obscodosupin, String obsmanodorsif, String obsmanoflex, String conclusion, String difIzqDerecha, String limitaciones, String obsfinal, FichaTerapiaFisica idfichaterapiafisica) {
        this.obscadeext = obscadeext;
        this.obscaderaflex = obscaderaflex;
        this.obscaderaabduext = obscaderaabduext;
        this.obscaderaabduflex = obscaderaabduflex;
        this.obscaderarotext = obscaderarotext;
        this.obscaderarotint = obscaderarotint;
        this.obsrodillaextext = obsrodillaextext;
        this.obsrodillaextflex = obsrodillaextflex;
        this.obsrodillaflex = obsrodillaflex;
        this.obstobillosoleus = obstobillosoleus;
        this.obstobillogastoc = obstobillogastoc;
        this.obshombroabdu = obshombroabdu;
        this.obshombroantef = obshombroantef;
        this.obshombroexor = obshombroexor;
        this.obshombroendor = obshombroendor;
        this.obscodoext = obscodoext;
        this.obscodosupin = obscodosupin;
        this.obsmanodorsif = obsmanodorsif;
        this.obsmanoflex = obsmanoflex;
        this.conclusion = conclusion;
        this.difIzqDerecha = difIzqDerecha;
        this.limitaciones = limitaciones;
        this.obsfinal = obsfinal;
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public Long getIdmovarticulaciones() {
        return idmovarticulaciones;
    }

    public void setIdmovarticulaciones(Long idmovarticulaciones) {
        this.idmovarticulaciones = idmovarticulaciones;
    }

    public String getObscadeext() {
        return obscadeext;
    }

    public void setObscadeext(String obscadeext) {
        this.obscadeext = obscadeext;
    }

    public String getObscaderaflex() {
        return obscaderaflex;
    }

    public void setObscaderaflex(String obscaderaflex) {
        this.obscaderaflex = obscaderaflex;
    }

    public String getObscaderaabduext() {
        return obscaderaabduext;
    }

    public void setObscaderaabduext(String obscaderaabduext) {
        this.obscaderaabduext = obscaderaabduext;
    }

    public String getObscaderaabduflex() {
        return obscaderaabduflex;
    }

    public void setObscaderaabduflex(String obscaderaabduflex) {
        this.obscaderaabduflex = obscaderaabduflex;
    }

    public String getObscaderarotext() {
        return obscaderarotext;
    }

    public void setObscaderarotext(String obscaderarotext) {
        this.obscaderarotext = obscaderarotext;
    }

    public String getObscaderarotint() {
        return obscaderarotint;
    }

    public void setObscaderarotint(String obscaderarotint) {
        this.obscaderarotint = obscaderarotint;
    }

    public String getObsrodillaextext() {
        return obsrodillaextext;
    }

    public void setObsrodillaextext(String obsrodillaextext) {
        this.obsrodillaextext = obsrodillaextext;
    }

    public String getObsrodillaextflex() {
        return obsrodillaextflex;
    }

    public void setObsrodillaextflex(String obsrodillaextflex) {
        this.obsrodillaextflex = obsrodillaextflex;
    }

    public String getObsrodillaflex() {
        return obsrodillaflex;
    }

    public void setObsrodillaflex(String obsrodillaflex) {
        this.obsrodillaflex = obsrodillaflex;
    }

    public String getObstobillosoleus() {
        return obstobillosoleus;
    }

    public void setObstobillosoleus(String obstobillosoleus) {
        this.obstobillosoleus = obstobillosoleus;
    }

    public String getObstobillogastoc() {
        return obstobillogastoc;
    }

    public void setObstobillogastoc(String obstobillogastoc) {
        this.obstobillogastoc = obstobillogastoc;
    }

    public String getObshombroabdu() {
        return obshombroabdu;
    }

    public void setObshombroabdu(String obshombroabdu) {
        this.obshombroabdu = obshombroabdu;
    }

    public String getObshombroantef() {
        return obshombroantef;
    }

    public void setObshombroantef(String obshombroantef) {
        this.obshombroantef = obshombroantef;
    }

    public String getObshombroexor() {
        return obshombroexor;
    }

    public void setObshombroexor(String obshombroexor) {
        this.obshombroexor = obshombroexor;
    }

    public String getObshombroendor() {
        return obshombroendor;
    }

    public void setObshombroendor(String obshombroendor) {
        this.obshombroendor = obshombroendor;
    }

    public String getObscodoext() {
        return obscodoext;
    }

    public void setObscodoext(String obscodoext) {
        this.obscodoext = obscodoext;
    }

    public String getObscodosupin() {
        return obscodosupin;
    }

    public void setObscodosupin(String obscodosupin) {
        this.obscodosupin = obscodosupin;
    }

    public String getObsmanodorsif() {
        return obsmanodorsif;
    }

    public void setObsmanodorsif(String obsmanodorsif) {
        this.obsmanodorsif = obsmanodorsif;
    }

    public String getObsmanoflex() {
        return obsmanoflex;
    }

    public void setObsmanoflex(String obsmanoflex) {
        this.obsmanoflex = obsmanoflex;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getDifIzqDerecha() {
        return difIzqDerecha;
    }

    public void setDifIzqDerecha(String difIzqDerecha) {
        this.difIzqDerecha = difIzqDerecha;
    }

    public String getLimitaciones() {
        return limitaciones;
    }

    public void setLimitaciones(String limitaciones) {
        this.limitaciones = limitaciones;
    }

    public String getObsfinal() {
        return obsfinal;
    }

    public void setObsfinal(String obsfinal) {
        this.obsfinal = obsfinal;
    }

    public FichaTerapiaFisica getIdfichaterapiafisica() {
        return idfichaterapiafisica;
    }

    public void setIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmovarticulaciones != null ? idmovarticulaciones.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MovilidadArticulaciones)) {
            return false;
        }
        MovilidadArticulaciones other = (MovilidadArticulaciones) object;
        if ((this.idmovarticulaciones == null && other.idmovarticulaciones != null) || (this.idmovarticulaciones != null && !this.idmovarticulaciones.equals(other.idmovarticulaciones))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wasi.wasisoft.model.MovilidadArticulaciones[ idMovarticulaciones=" + idmovarticulaciones + " ]";
    }
}
