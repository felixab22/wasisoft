package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "histclinico")
@Access(AccessType.FIELD)
public class HistoriaClinico implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_histclinico", unique = true, nullable = false)
    private Long id_histclinico;

    @Column(name = "fevaluacion")
    @Temporal(TemporalType.DATE)
    private Date fevaluacion;

    public Long getId_histclinico() {
        return id_histclinico;
    }

    public void setId_histclinico(Long id_histclinico) {
        this.id_histclinico = id_histclinico;
    }

    public Date getFevaluacion() {
        return fevaluacion;
    }

    public void setFevaluacion(Date fevaluacion) {
        this.fevaluacion = fevaluacion;
    }

    public CitaTerapia getId_citaterapia() {
        return id_citaterapia;
    }

    public void setId_citaterapia(CitaTerapia id_citaterapia) {
        this.id_citaterapia = id_citaterapia;
    }

    @JoinColumn(name = "id_citaterapia", referencedColumnName = "id_citaterapia")
    @ManyToOne
    private CitaTerapia id_citaterapia;


}
