package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;
import org.springframework.lang.NonNull;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "fichasocioecon")
@Access(AccessType.FIELD)
public class Fichasocioecon extends DateAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_socioecon")
    private Long id_socioecon;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "tipo_familia")
    private String tipoFamilia;

    @Size(max = 200)
    @Column(name = "evidencia_habla")
    private String evidencia_habla;
    @Size(max = 500)
    @Column(name = "diagnostico_inicial")
    private String diagnostico_inicial;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "prob_fami")
    private String probFami;
    @Size(max = 200)
    @Column(name = "observaciones")
    private String observaciones;
    @Size(max = 200)
    @Column(name = "zona_urbana")
    private String zonaUrbana;
    @Size(max = 200)
    @Column(name = "zona_rural")
    private String zonaRural;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "tenencia_vivienda")
    private String tenenciaVivienda;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "caract_vivienda")
    private String caractVivienda;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "tipo_vivienda")
    private String tipoVivienda;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "mat_pred_techo")
    private String matPredTecho;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "mat_pred_piso")
    private String matPredPiso;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "numamb_vivienda")
    private String numambVivienda;
    @Basic(optional = false)

    @Column(name = "cantdormitorios")
    private String cantdormitorios;

    @Size(max = 200)
    @Column(name = "abast_agua")
    private String abastAgua;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "tipo_alumbrado")
    private String tipoAlumbrado;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "tipo_sshh")
    private String tipoSshh;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "comb_cocinar")
    private String combCocinar;
    @Basic(optional = false)

    @Column(name = "bienes_hogar")
    private String bienesHogar;
    @Size(max = 200)
    @Column(name = "otros_patrimonio")
    private String otrosPatrimonio;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "discapacidad")
    private String discapacidad;
    @Basic(optional = false)

    @Column(name = "fecha_discapac")
    private String fechaDiscapac;
    @Size(max = 300)
    @Column(name = "referencia_disc")
    private String referenciaDisc;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "otros_problem")
    private String otrosProblem;
    @Size(max = 200)
    @Column(name = "gatos_medicamentos")
    private String gatosMedicamentos;
    @Basic(optional = false)

    @Size(max = 200)
    @Column(name = "tipo_seguro")
    private String tipoSeguro;
    @Size(max = 200)
    @Column(name = "establ_atencion")
    private String establAtencion;
    @Size(max = 200)
    @Column(name = "lugar_estsalud")
    private String lugarEstsalud;
    @Column(name = "ult_visita_medico")
    private String ultVisitaMedico;

    @Size(max = 200)
    @Column(name = "padres_salud")
    private String padresSalud;
    @Size(max = 200)
    @Column(name = "nivel_educ")
    private String nivelEduc;
    @Size(max = 200)
    @Column(name = "especif_grado")
    private String especifGrado;
    @Size(max = 200)
    @Column(name = "tipo_insti_asiste")
    private String tipoInstiAsiste;
    @Size(max = 200)
    @Column(name = "part_activ_cole")
    private String partActivCole;
    @Size(max = 200)
    @Column(name = "habil_destreza")
    private String habilDestreza;
    @Size(max = 200)
    @Column(name = "colabora_hogar")
    private String colaboraHogar;
    @Size(max = 200)
    @Column(name = "nec_certif_discap")
    private String necCertifDiscap;
    @Size(max = 200)
    @Column(name = "tipo_certif_disc")
    private String tipoCertifDisc;

    @Size(max = 200)
    @Column(name = "posee_matortopedico")
    private String poseeMatortopedico;
    @Size(max = 200)
    @Column(name = "requiere_matortopedico")
    private String requiereMatortopedico;
    @Size(max = 200)
    @Column(name = "pcd")
    private String pcd;
    @Size(max = 200)
    @Column(name = "terapia_anterior")
    private String terapiaAnterior;
    @Size(max = 200)
    @Column(name = "terapia_actual")
    private String terapia_actual;
    @Column(name = "terapiadonde")
    private String terapiadonde;
    @Size(max = 200)
    @Column(name = "programa_inscrito")
    private String programaInscrito;
    @Size(max = 200)
    @Column(name = "departamento_domicilio")
    private String departamento_domicilio;
    @Size(max = 200)
    @Column(name = "provincia_domicilio")
    private String provincia_domicilio;
    @Size(max = 200)
    @Column(name = "distrito_domicilio")
    private String distrito_domicilio;
    @Size(max = 200)
    @Column(name = "direccion_domicilio")
    private String direccion_domicilio;
    @Size(max = 200)
    @Column(name = "referencia_domicilio")
    private String referencia_domicilio;

    @Column(name = "zona_vivienda")
    private String zona_vivienda;

    @Column (name = "incidencias_familiares")
    private String incidencias_familiares;

    @Column (name = "observacion_familiares")
    private String observacion_famliares;

    @Column (name = "pagosemanal")
    private Double pagosemanal;

    @Column (name = "pagomensual")
    private Double pagomensual;

    @Column(name = "otrosbienes")
    private String otrosbienes;

    @Column(name="presentaotrosproblemas")
    private String presentaotrosproblemas;
    @Column(name="otrocolaborahogar")
    private String otrocolaborahogar;
    @Column(name="otrocuentaconcertificado")
    private String otrocuentaconcertificado;
    @Column(name="otropcd")
    private String otropcd;
    @Column(name="otroprogramassociales")
    private String otroprogramassociales;
    @Column(name="otroparticipaactividad")
    private String otroparticipaactividad;
    @Column(name="otrohabilidad")
    private String otrohabilidad;
    @Column(name="otroambiente")
    private String otroambiente;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

        public Fichasocioecon() {
    }

    public Fichasocioecon(Long id_socioecon) {
        this.id_socioecon = id_socioecon;
    }

    public Fichasocioecon(String tipoFamilia, String evidencia_habla, String diagnostico_inicial, String probFami, String observaciones, String zonaUrbana, String zonaRural, String tenenciaVivienda, String caractVivienda, String tipoVivienda, String matPredTecho, String matPredPiso, String numambVivienda, String cantdormitorios, String abastAgua, String tipoAlumbrado, String tipoSshh, String combCocinar, String bienesHogar, String otrosPatrimonio, String discapacidad, String fechaDiscapac, String referenciaDisc, String otrosProblem, String gatosMedicamentos, String tipoSeguro, String establAtencion, String lugarEstsalud, String ultVisitaMedico, String padresSalud, String nivelEduc, String especifGrado, String tipoInstiAsiste, String partActivCole, String habilDestreza, String colaboraHogar, String necCertifDiscap, String tipoCertifDisc, String poseeMatortopedico, String requiereMatortopedico, String pcd, String terapiaAnterior, String terapia_actual, String terapiadonde, String programaInscrito, String departamento_domicilio, String provincia_domicilio, String distrito_domicilio, String direccion_domicilio, String referencia_domicilio, String zona_vivienda, String incidencias_familiares, String observacion_famliares, Double pagosemanal, Double pagomensual, String otrosbienes, String presentaotrosproblemas, String otrocolaborahogar, String otrocuentaconcertificado, String otropcd, String otroprogramassociales, String otroparticipaactividad, String otrohabilidad, String otroambiente, Expediente idexpediente) {
        this.tipoFamilia = tipoFamilia;
        this.evidencia_habla = evidencia_habla;
        this.diagnostico_inicial = diagnostico_inicial;
        this.probFami = probFami;
        this.observaciones = observaciones;
        this.zonaUrbana = zonaUrbana;
        this.zonaRural = zonaRural;
        this.tenenciaVivienda = tenenciaVivienda;
        this.caractVivienda = caractVivienda;
        this.tipoVivienda = tipoVivienda;
        this.matPredTecho = matPredTecho;
        this.matPredPiso = matPredPiso;
        this.numambVivienda = numambVivienda;
        this.cantdormitorios = cantdormitorios;
        this.abastAgua = abastAgua;
        this.tipoAlumbrado = tipoAlumbrado;
        this.tipoSshh = tipoSshh;
        this.combCocinar = combCocinar;
        this.bienesHogar = bienesHogar;
        this.otrosPatrimonio = otrosPatrimonio;
        this.discapacidad = discapacidad;
        this.fechaDiscapac = fechaDiscapac;
        this.referenciaDisc = referenciaDisc;
        this.otrosProblem = otrosProblem;
        this.gatosMedicamentos = gatosMedicamentos;
        this.tipoSeguro = tipoSeguro;
        this.establAtencion = establAtencion;
        this.lugarEstsalud = lugarEstsalud;
        this.ultVisitaMedico = ultVisitaMedico;
        this.padresSalud = padresSalud;
        this.nivelEduc = nivelEduc;
        this.especifGrado = especifGrado;
        this.tipoInstiAsiste = tipoInstiAsiste;
        this.partActivCole = partActivCole;
        this.habilDestreza = habilDestreza;
        this.colaboraHogar = colaboraHogar;
        this.necCertifDiscap = necCertifDiscap;
        this.tipoCertifDisc = tipoCertifDisc;
        this.poseeMatortopedico = poseeMatortopedico;
        this.requiereMatortopedico = requiereMatortopedico;
        this.pcd = pcd;
        this.terapiaAnterior = terapiaAnterior;
        this.terapia_actual = terapia_actual;
        this.terapiadonde = terapiadonde;
        this.programaInscrito = programaInscrito;
        this.departamento_domicilio = departamento_domicilio;
        this.provincia_domicilio = provincia_domicilio;
        this.distrito_domicilio = distrito_domicilio;
        this.direccion_domicilio = direccion_domicilio;
        this.referencia_domicilio = referencia_domicilio;
        this.zona_vivienda = zona_vivienda;
        this.incidencias_familiares = incidencias_familiares;
        this.observacion_famliares = observacion_famliares;
        this.pagosemanal = pagosemanal;
        this.pagomensual = pagomensual;
        this.otrosbienes = otrosbienes;
        this.presentaotrosproblemas = presentaotrosproblemas;
        this.otrocolaborahogar = otrocolaborahogar;
        this.otrocuentaconcertificado = otrocuentaconcertificado;
        this.otropcd = otropcd;
        this.otroprogramassociales = otroprogramassociales;
        this.otroparticipaactividad = otroparticipaactividad;
        this.otrohabilidad = otrohabilidad;
        this.otroambiente = otroambiente;
        this.idexpediente = idexpediente;
    }

    public String getReferencia_domicilio() {
        return referencia_domicilio;
    }

    public void setReferencia_domicilio(String referencia_domicilio) {
        this.referencia_domicilio = referencia_domicilio;
    }

    public Long getId_socioecon() {
        return id_socioecon;
    }

    public void setId_socioecon(Long id_socioecon) {
        this.id_socioecon = id_socioecon;
    }

    public String getTipoFamilia() {
        return tipoFamilia;
    }

    public void setTipoFamilia(String tipoFamilia) {
        this.tipoFamilia = tipoFamilia;
    }

    public String getProbFami() {
        return probFami;
    }

    public void setProbFami(String probFami) {
        this.probFami = probFami;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getZonaUrbana() {
        return zonaUrbana;
    }

    public void setZonaUrbana(String zonaUrbana) {
        this.zonaUrbana = zonaUrbana;
    }

    public String getZonaRural() {
        return zonaRural;
    }

    public void setZonaRural(String zonaRural) {
        this.zonaRural = zonaRural;
    }

    public String getTenenciaVivienda() {
        return tenenciaVivienda;
    }

    public void setTenenciaVivienda(String tenenciaVivienda) {
        this.tenenciaVivienda = tenenciaVivienda;
    }

    public String getCaractVivienda() {
        return caractVivienda;
    }

    public void setCaractVivienda(String caractVivienda) {
        this.caractVivienda = caractVivienda;
    }

    public String getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public String getMatPredTecho() {
        return matPredTecho;
    }

    public void setMatPredTecho(String matPredTecho) {
        this.matPredTecho = matPredTecho;
    }

    public String getMatPredPiso() {
        return matPredPiso;
    }

    public void setMatPredPiso(String matPredPiso) {
        this.matPredPiso = matPredPiso;
    }

    public String getNumambVivienda() {
        return numambVivienda;
    }

    public void setNumambVivienda(String numambVivienda) {
        this.numambVivienda = numambVivienda;
    }

    public String getAbastAgua() {
        return abastAgua;
    }

    public void setAbastAgua(String abastAgua) {
        this.abastAgua = abastAgua;
    }

    public String getTipoAlumbrado() {
        return tipoAlumbrado;
    }

    public void setTipoAlumbrado(String tipoAlumbrado) {
        this.tipoAlumbrado = tipoAlumbrado;
    }

    public String getTipoSshh() {
        return tipoSshh;
    }

    public void setTipoSshh(String tipoSshh) {
        this.tipoSshh = tipoSshh;
    }

    public String getCombCocinar() {
        return combCocinar;
    }

    public void setCombCocinar(String combCocinar) {
        this.combCocinar = combCocinar;
    }

    public String getBienesHogar() {
        return bienesHogar;
    }

    public void setBienesHogar(String bienesHogar) {
        this.bienesHogar = bienesHogar;
    }

    public String getOtrosPatrimonio() {
        return otrosPatrimonio;
    }

    public void setOtrosPatrimonio(String otrosPatrimonio) {
        this.otrosPatrimonio = otrosPatrimonio;
    }

    public String getDiscapacidad() {
        return discapacidad;
    }

    public void setDiscapacidad(String discapacidad) {
        this.discapacidad = discapacidad;
    }

    public String getFechaDiscapac() {
        return fechaDiscapac;
    }

    public void setFechaDiscapac(String fechaDiscapac) {
        this.fechaDiscapac = fechaDiscapac;
    }

    public String getReferenciaDisc() {
        return referenciaDisc;
    }

    public void setReferenciaDisc(String referenciaDisc) {
        this.referenciaDisc = referenciaDisc;
    }

    public String getOtrosProblem() {
        return otrosProblem;
    }

    public void setOtrosProblem(String otrosProblem) {
        this.otrosProblem = otrosProblem;
    }

    public String getGatosMedicamentos() {
        return gatosMedicamentos;
    }

    public void setGatosMedicamentos(String gatosMedicamentos) {
        this.gatosMedicamentos = gatosMedicamentos;
    }

    public String getTipoSeguro() {
        return tipoSeguro;
    }

    public void setTipoSeguro(String tipoSeguro) {
        this.tipoSeguro = tipoSeguro;
    }

    public String getEstablAtencion() {
        return establAtencion;
    }

    public void setEstablAtencion(String establAtencion) {
        this.establAtencion = establAtencion;
    }

    public String getLugarEstsalud() {
        return lugarEstsalud;
    }

    public void setLugarEstsalud(String lugarEstsalud) {
        this.lugarEstsalud = lugarEstsalud;
    }

    public String getUltVisitaMedico() {
        return ultVisitaMedico;
    }

    public void setUltVisitaMedico(String ultVisitaMedico) {
        this.ultVisitaMedico = ultVisitaMedico;
    }

    public String getPadresSalud() {
        return padresSalud;
    }

    public void setPadresSalud(String padresSalud) {
        this.padresSalud = padresSalud;
    }

    public String getNivelEduc() {
        return nivelEduc;
    }

    public void setNivelEduc(String nivelEduc) {
        this.nivelEduc = nivelEduc;
    }

    public String getEspecifGrado() {
        return especifGrado;
    }

    public void setEspecifGrado(String especifGrado) {
        this.especifGrado = especifGrado;
    }

    public String getTipoInstiAsiste() {
        return tipoInstiAsiste;
    }

    public void setTipoInstiAsiste(String tipoInstiAsiste) {
        this.tipoInstiAsiste = tipoInstiAsiste;
    }

    public String getPartActivCole() {
        return partActivCole;
    }

    public void setPartActivCole(String partActivCole) {
        this.partActivCole = partActivCole;
    }

    public String getHabilDestreza() {
        return habilDestreza;
    }

    public void setHabilDestreza(String habilDestreza) {
        this.habilDestreza = habilDestreza;
    }

    public String getColaboraHogar() {
        return colaboraHogar;
    }

    public void setColaboraHogar(String colaboraHogar) {
        this.colaboraHogar = colaboraHogar;
    }

    public String getPoseeMatortopedico() {
        return poseeMatortopedico;
    }

    public void setPoseeMatortopedico(String poseeMatortopedico) {
        this.poseeMatortopedico = poseeMatortopedico;
    }

    public String getEvidencia_habla() {
        return evidencia_habla;
    }

    public void setEvidencia_habla(String evidencia_habla) {
        this.evidencia_habla = evidencia_habla;
    }

    public String getDiagnostico_inicial() {
        return diagnostico_inicial;
    }

    public void setDiagnostico_inicial(String diagnostico_inicial) {
        this.diagnostico_inicial = diagnostico_inicial;
    }

    public String getNecCertifDiscap() {
        return necCertifDiscap;
    }

    public void setNecCertifDiscap(String necCertifDiscap) {
        this.necCertifDiscap = necCertifDiscap;
    }

    public String getTipoCertifDisc() {
        return tipoCertifDisc;
    }

    public void setTipoCertifDisc(String tipoCertifDisc) {
        this.tipoCertifDisc = tipoCertifDisc;
    }

    public String getRequiereMatortopedico() {
        return requiereMatortopedico;
    }

    public void setRequiereMatortopedico(String requiereMatortopedico) {
        this.requiereMatortopedico = requiereMatortopedico;
    }

    public String getTerapiadonde() {
        return terapiadonde;
    }

    public void setTerapiadonde(String terapiadonde) {
        this.terapiadonde = terapiadonde;
    }

    public String getPcd() {
        return pcd;
    }

    public void setPcd(String pcd) {
        this.pcd = pcd;
    }

    public String getTerapiaAnterior() {
        return terapiaAnterior;
    }

    public void setTerapiaAnterior(String terapiaAnterior) {
        this.terapiaAnterior = terapiaAnterior;
    }

    public String getTerapia_actual() {
        return terapia_actual;
    }

    public void setTerapia_actual(String terapia_actual) {
        this.terapia_actual = terapia_actual;
    }

    public String getProgramaInscrito() {
        return programaInscrito;
    }

    public void setProgramaInscrito(String programaInscrito) {
        this.programaInscrito = programaInscrito;
    }

    public String getDepartamento_domicilio() {
        return departamento_domicilio;
    }

    public void setDepartamento_domicilio(String departamento_domicilio) {
        this.departamento_domicilio = departamento_domicilio;
    }

    public String getProvincia_domicilio() {
        return provincia_domicilio;
    }

    public void setProvincia_domicilio(String provincia_domicilio) {
        this.provincia_domicilio = provincia_domicilio;
    }

    public String getDistrito_domicilio() {
        return distrito_domicilio;
    }

    public void setDistrito_domicilio(String distrito_domicilio) {
        this.distrito_domicilio = distrito_domicilio;
    }

    public String getDireccion_domicilio() {
        return direccion_domicilio;
    }

    public void setDireccion_domicilio(String direccion_domicilio) {
        this.direccion_domicilio = direccion_domicilio;
    }

    public String getZona_vivienda() {
        return zona_vivienda;
    }

    public void setZona_vivienda(String zona_vivienda) {
        this.zona_vivienda = zona_vivienda;
    }

    public String getIncidencias_familiares() {
        return incidencias_familiares;
    }

    public void setIncidencias_familiares(String incidencias_familiares) {
        this.incidencias_familiares = incidencias_familiares;
    }

    public String getObservacion_famliares() {
        return observacion_famliares;
    }

    public void setObservacion_famliares(String observacion_famliares) {
        this.observacion_famliares = observacion_famliares;
    }

    public Double getPagosemanal() {
        return pagosemanal;
    }

    public void setPagosemanal(Double pagosemanal) {
        this.pagosemanal = pagosemanal;
    }

    public Double getPagomensual() {
        return pagomensual;
    }

    public void setPagomensual(Double pagomensual) {
        this.pagomensual = pagomensual;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

    public String getCantdormitorios() {
        return cantdormitorios;
    }

    public void setCantdormitorios(String cantdormitorios) {
        this.cantdormitorios = cantdormitorios;
    }

    public String getOtrosbienes() {
        return otrosbienes;
    }

    public void setOtrosbienes(String otrosbienes) {
        this.otrosbienes = otrosbienes;
    }

    public String getPresentaotrosproblemas() {
        return presentaotrosproblemas;
    }

    public void setPresentaotrosproblemas(String presentaotrosproblemas) {
        this.presentaotrosproblemas = presentaotrosproblemas;
    }

    public String getOtrocolaborahogar() {
        return otrocolaborahogar;
    }

    public void setOtrocolaborahogar(String otrocolaborahogar) {
        this.otrocolaborahogar = otrocolaborahogar;
    }

    public String getOtrocuentaconcertificado() {
        return otrocuentaconcertificado;
    }

    public void setOtrocuentaconcertificado(String otrocuentaconcertificado) {
        this.otrocuentaconcertificado = otrocuentaconcertificado;
    }

    public String getOtropcd() {
        return otropcd;
    }

    public void setOtropcd(String otropcd) {
        this.otropcd = otropcd;
    }

    public String getOtroprogramassociales() {
        return otroprogramassociales;
    }

    public void setOtroprogramassociales(String otroprogramassociales) {
        this.otroprogramassociales = otroprogramassociales;
    }

    public String getOtroparticipaactividad() {
        return otroparticipaactividad;
    }

    public void setOtroparticipaactividad(String otroparticipaactividad) {
        this.otroparticipaactividad = otroparticipaactividad;
    }

    public String getOtrohabilidad() {
        return otrohabilidad;
    }

    public void setOtrohabilidad(String otrohabilidad) {
        this.otrohabilidad = otrohabilidad;
    }

    public String getOtroambiente() {
        return otroambiente;
    }

    public void setOtroambiente(String otroambiente) {
        this.otroambiente = otroambiente;
    }
}