package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 02/05/2018.
 */
@Entity
@Table (name = "desalenguaje")
public class Desalenguaje implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_desalenguaje")
    private Long iddesalenguaje;

    @Size(max = 20)
    @Column(name = "comunicacion")
    private String comunicacion;
    @Size(max = 20)
    @Column(name = "balbucea")
    private String balbucea;
    @Size(max = 20)
    @Column(name = "vocaliza")
    private String vocaliza;
    @Size(max = 20)
    @Column(name = "emitepalabras")
    private String emitepalabras;
    @Size(max = 20)
    @Column(name = "emite_frases")
    private String emitefrases;
    @Size(max = 20)
    @Column(name = "relata_expe")
    private String relataexpe;
    @Size(max = 20)
    @Column(name = "pronun_clara")
    private String pronunclara;
    @Size(max = 20)
    @Column(name = "ident_objeto")
    private String identobjeto;
    @Size(max = 20)
    @Column(name = "ident_personas")
    private String identpersonas;
    @Size(max = 20)
    @Column(name = "compr_concep")
    private String comprconcep;
    @Size(max = 20)
    @Column(name = "resp_coherentes")
    private String respcoherentes;
    @Size(max = 100)
    @Column(name = "perdida_lenguaje")
    private String perdidalenguaje;
    @Size(max = 100)
    @Column(name = "obs_lenguaje")
    private String obslenguaje;

    @JoinColumn(name = "id_fichaeduc", referencedColumnName = "id_fichaeduc")
    @ManyToOne
    private FichaEducacion idfichaeduc;

    public Desalenguaje() {
    }

    public Desalenguaje(Long iddesalenguaje) {
        this.iddesalenguaje = iddesalenguaje;
    }

    public Desalenguaje(String comunicacion, String balbucea, String vocaliza, String emitepalabras, String emitefrases, String relataexpe, String pronunclara, String identobjeto, String identpersonas, String comprconcep, String respcoherentes, String perdidalenguaje, String obslenguaje, FichaEducacion idfichaeduc) {
        this.comunicacion = comunicacion;
        this.balbucea = balbucea;
        this.vocaliza = vocaliza;
        this.emitepalabras = emitepalabras;
        this.emitefrases = emitefrases;
        this.relataexpe = relataexpe;
        this.pronunclara = pronunclara;
        this.identobjeto = identobjeto;
        this.identpersonas = identpersonas;
        this.comprconcep = comprconcep;
        this.respcoherentes = respcoherentes;
        this.perdidalenguaje = perdidalenguaje;
        this.obslenguaje = obslenguaje;
        this.idfichaeduc = idfichaeduc;
    }

    public Long getIddesalenguaje() {
        return iddesalenguaje;
    }

    public void setIddesalenguaje(Long iddesalenguaje) {
        this.iddesalenguaje = iddesalenguaje;
    }

    public String getComunicacion() {
        return comunicacion;
    }

    public void setComunicacion(String comunicacion) {
        this.comunicacion = comunicacion;
    }

    public String getBalbucea() {
        return balbucea;
    }

    public void setBalbucea(String balbucea) {
        this.balbucea = balbucea;
    }

    public String getVocaliza() {
        return vocaliza;
    }

    public void setVocaliza(String vocaliza) {
        this.vocaliza = vocaliza;
    }

    public String getEmitepalabras() {
        return emitepalabras;
    }

    public void setEmitepalabras(String emitepalabras) {
        this.emitepalabras = emitepalabras;
    }

    public String getEmitefrases() {
        return emitefrases;
    }

    public void setEmitefrases(String emitefrases) {
        this.emitefrases = emitefrases;
    }

    public String getRelataexpe() {
        return relataexpe;
    }

    public void setRelataexpe(String relataexpe) {
        this.relataexpe = relataexpe;
    }

    public String getPronunclara() {
        return pronunclara;
    }

    public void setPronunclara(String pronunclara) {
        this.pronunclara = pronunclara;
    }

    public String getIdentobjeto() {
        return identobjeto;
    }

    public void setIdentobjeto(String identobjeto) {
        this.identobjeto = identobjeto;
    }

    public String getIdentpersonas() {
        return identpersonas;
    }

    public void setIdentpersonas(String identpersonas) {
        this.identpersonas = identpersonas;
    }

    public String getComprconcep() {
        return comprconcep;
    }

    public void setComprconcep(String comprconcep) {
        this.comprconcep = comprconcep;
    }

    public String getRespcoherentes() {
        return respcoherentes;
    }

    public void setRespcoherentes(String respcoherentes) {
        this.respcoherentes = respcoherentes;
    }

    public String getPerdidalenguaje() {
        return perdidalenguaje;
    }

    public void setPerdidalenguaje(String perdidalenguaje) {
        this.perdidalenguaje = perdidalenguaje;
    }

    public String getObslenguaje() {
        return obslenguaje;
    }

    public void setObslenguaje(String obslenguaje) {
        this.obslenguaje = obslenguaje;
    }

    public FichaEducacion getIdfichaeduc() {
        return idfichaeduc;
    }

    public void setIdfichaeduc(FichaEducacion idfichaeduc) {
        this.idfichaeduc = idfichaeduc;
    }
}
