package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "fichaterapiafisica")
@Access(AccessType.FIELD)

public class FichaTerapiaFisica extends DateAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_fichaterapiafisica")
    private Long idfichaterapiafisica;

    @Column(name = "fregistro")
    @Temporal(TemporalType.DATE)
    private Date fregistro;

    @Column(name = "usuariocreacion")
    private String usuariocreacion;

    @Column(name = "estado_atencion")
    private Boolean estadoatencion;

//    @JoinColumn(name = "id_citaterapia", referencedColumnName = "id_citaterapia")
//    @OneToOne
//    private CitaTerapia idcitaterapia;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public FichaTerapiaFisica() {
    }

    public FichaTerapiaFisica(Long idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public FichaTerapiaFisica(Date fregistro, String usuariocreacion, Boolean estadoatencion, Expediente idexpediente) {
        this.fregistro = fregistro;
        this.usuariocreacion = usuariocreacion;
        this.estadoatencion = estadoatencion;
        this.idexpediente = idexpediente;
//        this.diagnostico = diagnostico;
    }

    public Long getIdfichaterapiafisica() {
        return idfichaterapiafisica;
    }

    public void setIdfichaterapiafisica(Long idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public Date getFregistro() {
        return fregistro;
    }

    public void setFregistro(Date fregistro) {
        this.fregistro = fregistro;
    }

    public String getUsuariocreacion() {
        return usuariocreacion;
    }

    public void setUsuariocreacion(String usuariocreacion) {
        this.usuariocreacion = usuariocreacion;
    }

    public Boolean getEstadoatencion() {
        return estadoatencion;
    }

    public void setEstadoatencion(Boolean estadoatencion) {
        this.estadoatencion = estadoatencion;
    }
//
//    public CitaTerapia getIdcitaterapia() {
//        return idcitaterapia;
//    }
//
//    public void setIdcitaterapia(CitaTerapia idcitaterapia) {
//        this.idcitaterapia = idcitaterapia;
//    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

}
