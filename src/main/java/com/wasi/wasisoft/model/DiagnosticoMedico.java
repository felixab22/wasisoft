package com.wasi.wasisoft.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "diagmedico")
@Access(AccessType.FIELD)
public class DiagnosticoMedico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_diagnostico")
    private Long iddiagnosticomedico;

    @Size(max = 500)
    @Column(name = "diagnostico")
    private String diagnostico;

    @Size(max = 200)
    @Column(name = "nombre_medico")
    private String nombre_medico;

    @Column(name = "fdiagnostico")
    @Temporal(TemporalType.DATE)
    private Date fdiagnostico;

    @Size(max = 250)
    @Column(name = "lugar_diagnostico")
    private String lugar_diagnostico;

    @Size(max = 500)
    @Column(name = "sospecha_diagnostico")
    private String sospecha_diagnostico;
    @Size(max = 500)
    @Column(name = "segun_sospecha")
    private String segun_sospecha;
    @Lob
    @Column(name = "evidencia_diagnostico")
    private String evidencia_diagnostico;
    
    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica idfichaterapiafisica;

    public DiagnosticoMedico() {
    }

    public Long getIddiagnosticomedico() {
        return iddiagnosticomedico;
    }

    public void setIddiagnosticomedico(Long iddiagnosticomedico) {
        this.iddiagnosticomedico = iddiagnosticomedico;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }


    public String getNombre_medico() {
        return nombre_medico;
    }

    public void setNombre_medico(String nombre_medico) {
        this.nombre_medico = nombre_medico;
    }

    public Date getFdiagnostico() {
        return fdiagnostico;
    }

    public void setFdiagnostico(Date fdiagnostico) {
        this.fdiagnostico = fdiagnostico;
    }

    public String getLugar_diagnostico() {
        return lugar_diagnostico;
    }

    public void setLugar_diagnostico(String lugar_diagnostico) {
        this.lugar_diagnostico = lugar_diagnostico;
    }

    public String getSospecha_diagnostico() {
        return sospecha_diagnostico;
    }

    public void setSospecha_diagnostico(String sospecha_diagnostico) {
        this.sospecha_diagnostico = sospecha_diagnostico;
    }

    public String getSegun_sospecha() {
        return segun_sospecha;
    }

    public void setSegun_sospecha(String segun_sospecha) {
        this.segun_sospecha = segun_sospecha;
    }

    public String getEvidencia_diagnostico() {
        return evidencia_diagnostico;
    }

    public void setEvidencia_diagnostico(String evidencia_diagnostico) {
        this.evidencia_diagnostico = evidencia_diagnostico;
    }

    public FichaTerapiaFisica getIdfichaterapiafisica() {
        return idfichaterapiafisica;
    }

    public void setIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public DiagnosticoMedico(String diagnostico, String nombre_medico, Date fdiagnostico, String lugar_diagnostico, String sospecha_diagnostico, String segun_sospecha, String evidencia_diagnostico, FichaTerapiaFisica idfichaterapiafisica) {
        this.diagnostico = diagnostico;
        this.nombre_medico = nombre_medico;
        this.fdiagnostico = fdiagnostico;
        this.lugar_diagnostico = lugar_diagnostico;
        this.sospecha_diagnostico = sospecha_diagnostico;
        this.segun_sospecha = segun_sospecha;
        this.evidencia_diagnostico = evidencia_diagnostico;
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddiagnosticomedico != null ? iddiagnosticomedico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DiagnosticoMedico)) {
            return false;
        }
        DiagnosticoMedico other = (DiagnosticoMedico) object;
        if ((this.iddiagnosticomedico == null && other.iddiagnosticomedico != null) || (this.iddiagnosticomedico != null && !this.iddiagnosticomedico.equals(other.iddiagnosticomedico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wasi.wasisoft.model.Diagmedico[ iddiagnosticomedico=" + iddiagnosticomedico + " ]";
    }

}
