package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Nelly on 26/04/2018.
 */
@Entity
@Table(name = "fichaato")
public class FichaAto extends DateAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_fichaato")
    private Long idfichaato;

    @Size(max = 100)
    @Column(name = "art_hombro")
    private String arthombro;

    @Size(max = 100)
    @Column(name = "art_codo")
    private String artcodo;

    @Size(max = 100)
    @Column(name = "art_muneca")
    private String artmuneca;

    @Size(max = 100)
    @Column(name = "prono_supinacion")
    private String pronosupinacion;

    @Size(max = 100)
    @Column(name = "disoc_movim")
    private String disocmovim;

    @Size(max = 100)
    @Column(name = "falanges")
    private String falanges;

    @Size(max = 100)
    @Column(name = "caminar")
    private String caminar;

    @Size(max = 100)
    @Column(name = "atencion")
    private String atencion;

    @Size(max = 100)
    @Column(name = "memoria")
    private String memoria;

    @Size(max = 100)
    @Column(name = "resol_problem")
    private String resolproblem;

    @Size(max = 100)
    @Column(name = "toleran_esfuer")
    private String toleranesfuer;

    @Size(max = 100)
    @Column(name = "toleran_frustra")
    private String toleranfrustra;

    @Size(max = 100)
    @Column(name = "funcion_habla")
    private String funcionhabla;

    @Size(max = 100)
    @Column(name = "num_hermanos")
    private String numhermanos;

    @Size(max = 20)
    @Column(name = "asiste_cole")
    private String asisteCole;

    @Size(max = 300)
    @Column(name = "observaciones")
    private String observaciones;

    @JoinColumn(name = "id_referinterna", referencedColumnName = "id_referinterna")
    @ManyToOne
    private ReferenciaInterna idreferenciainterna;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public FichaAto() {
    }

    public FichaAto(Long idfichaato) {
        this.idfichaato = idfichaato;
    }

    public FichaAto(String arthombro, String artcodo, String artmuneca, String pronosupinacion, String disocmovim, String falanges, String caminar, String atencion, String memoria, String resolproblem, String toleranesfuer, String toleranfrustra, String funcionhabla, String numhermanos, String asisteCole, String observaciones, ReferenciaInterna idreferenciainterna, Expediente idexpediente) {
        this.arthombro = arthombro;
        this.artcodo = artcodo;
        this.artmuneca = artmuneca;
        this.pronosupinacion = pronosupinacion;
        this.disocmovim = disocmovim;
        this.falanges = falanges;
        this.caminar = caminar;
        this.atencion = atencion;
        this.memoria = memoria;
        this.resolproblem = resolproblem;
        this.toleranesfuer = toleranesfuer;
        this.toleranfrustra = toleranfrustra;
        this.funcionhabla = funcionhabla;
        this.numhermanos = numhermanos;
        this.asisteCole = asisteCole;
        this.observaciones = observaciones;
        this.idreferenciainterna = idreferenciainterna;
        this.idexpediente = idexpediente;
    }

    public Long getIdfichaato() {
        return idfichaato;
    }

    public void setIdfichaato(Long idfichaato) {
        this.idfichaato = idfichaato;
    }

    public String getArthombro() {
        return arthombro;
    }

    public void setArthombro(String arthombro) {
        this.arthombro = arthombro;
    }

    public String getArtcodo() {
        return artcodo;
    }

    public void setArtcodo(String artcodo) {
        this.artcodo = artcodo;
    }

    public String getArtmuneca() {
        return artmuneca;
    }

    public void setArtmuneca(String artmuneca) {
        this.artmuneca = artmuneca;
    }

    public String getPronosupinacion() {
        return pronosupinacion;
    }

    public void setPronosupinacion(String pronosupinacion) {
        this.pronosupinacion = pronosupinacion;
    }

    public String getDisocmovim() {
        return disocmovim;
    }

    public void setDisocmovim(String disocmovim) {
        this.disocmovim = disocmovim;
    }

    public String getFalanges() {
        return falanges;
    }

    public void setFalanges(String falanges) {
        this.falanges = falanges;
    }

    public String getCaminar() {
        return caminar;
    }

    public void setCaminar(String caminar) {
        this.caminar = caminar;
    }

    public String getAtencion() {
        return atencion;
    }

    public void setAtencion(String atencion) {
        this.atencion = atencion;
    }

    public String getMemoria() {
        return memoria;
    }

    public void setMemoria(String memoria) {
        this.memoria = memoria;
    }

    public String getResolproblem() {
        return resolproblem;
    }

    public void setResolproblem(String resolproblem) {
        this.resolproblem = resolproblem;
    }

    public String getToleranesfuer() {
        return toleranesfuer;
    }

    public void setToleranesfuer(String toleranesfuer) {
        this.toleranesfuer = toleranesfuer;
    }

    public String getToleranfrustra() {
        return toleranfrustra;
    }

    public void setToleranfrustra(String toleranfrustra) {
        this.toleranfrustra = toleranfrustra;
    }

    public String getFuncionhabla() {
        return funcionhabla;
    }

    public void setFuncionhabla(String funcionhabla) {
        this.funcionhabla = funcionhabla;
    }

    public String getNumhermanos() {
        return numhermanos;
    }

    public void setNumhermanos(String numhermanos) {
        this.numhermanos = numhermanos;
    }

    public String getAsisteCole() {
        return asisteCole;
    }

    public void setAsisteCole(String asisteCole) {
        this.asisteCole = asisteCole;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public ReferenciaInterna getIdreferenciainterna() {
        return idreferenciainterna;
    }

    public void setIdreferenciainterna(ReferenciaInterna idreferenciainterna) {
        this.idreferenciainterna = idreferenciainterna;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }
}
