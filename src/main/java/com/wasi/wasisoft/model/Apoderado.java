package com.wasi.wasisoft.model;


import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "apoderado")
//@Access(AccessType.FIELD)
public class Apoderado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_apoderado")
    private Long id_apoderado;

    @Column(name = "tipo_apoderado")
    private String tipo_apoderado;
    @Size(max = 20)
    @Column(name = "nombre_apoderado")
    private String nombre_apoderado;
    @Size(max = 50)
    @Column(name = "apellido_apoderado")
    private String apellido_apoderado;
    @Column(name = "fnacimiento_apod")
    @Temporal(TemporalType.DATE)
    private Date fnacimiento_apod;
    @Column(name = "dni_apoderado")
    private Integer dni_apoderado;
    @Size(max = 20)
    @Column(name = "idioma_apoderado")
    private String idioma_apoderado;
    @Size(max = 20)
    @Column(name = "estcivil_apoderado")
    private String estcivil_apoderado;
    @Size(max = 50)
    @Column(name = "departamento_apoderado")
    private String departamento_apoderado;

    @Column(name = "provincia_apoderado")
    private String provincia_apoderado;


    @Column(name = "distrito_apoderado")
    private String distrito_apoderado;

    @Column(name = "nivelinst_apoderado")
    private String nivelinst_apoderado;

    @Column(name = "religion_apoderado")
    private String religion_apoderado;
    @Size(max = 100)
    @Column(name = "ocupacion_apoderado")
    private String ocupacion_apoderado;
    @Column(name = "celular")
    private Integer celular;
    @Column(name = "telefono_fijo")
    private Integer telefonoFijo;

    @Column(name = "condlaboral_apod")
    private String condlaboral_apod;

    @Column(name = "funcion_apoderado")
    private String funcion_apoderado;

    @Column(name = "horariotrabajo")
    private String horariotrabajo;
    @Column(name = "adiccion")
    private String adiccion;
    @Column(name = "caracter")
    private String caracter;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public Apoderado(Long id_apoderado) {
        this.id_apoderado = id_apoderado;
    }


    public String getHorariotrabajo() {
        return horariotrabajo;
    }

    public void setHorariotrabajo(String horariotrabajo) {
        this.horariotrabajo = horariotrabajo;
    }

    public String getAdiccion() {
        return adiccion;
    }

    public void setAdiccion(String adiccion) {
        this.adiccion = adiccion;
    }

    public String getCaracter() {
        return caracter;
    }

    public void setCaracter(String caracter) {
        this.caracter = caracter;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Long getId_apoderado() {
        return id_apoderado;
    }

    public void setId_apoderado(Long id_apoderado) {
        this.id_apoderado = id_apoderado;
    }

    public String getTipo_apoderado() {
        return tipo_apoderado;
    }

    public void setTipo_apoderado(String tipo_apoderado) {
        this.tipo_apoderado = tipo_apoderado;
    }

    public String getNombre_apoderado() {
        return nombre_apoderado;
    }

    public void setNombre_apoderado(String nombre_apoderado) {
        this.nombre_apoderado = nombre_apoderado;
    }

    public String getApellido_apoderado() {
        return apellido_apoderado;
    }

    public void setApellido_apoderado(String apellido_apoderado) {
        this.apellido_apoderado = apellido_apoderado;
    }

    public Date getFnacimiento_apod() {
        return fnacimiento_apod;
    }

    public void setFnacimiento_apod(Date fnacimiento_apod) {
        this.fnacimiento_apod = fnacimiento_apod;
    }

    public Integer getDni_apoderado() {
        return dni_apoderado;
    }

    public void setDni_apoderado(Integer dni_apoderado) {
        this.dni_apoderado = dni_apoderado;
    }

    public String getIdioma_apoderado() {
        return idioma_apoderado;
    }

    public void setIdioma_apoderado(String idioma_apoderado) {
        this.idioma_apoderado = idioma_apoderado;
    }

    public String getEstcivil_apoderado() {
        return estcivil_apoderado;
    }

    public void setEstcivil_apoderado(String estcivil_apoderado) {
        this.estcivil_apoderado = estcivil_apoderado;
    }

    public String getNivelinst_apoderado() {
        return nivelinst_apoderado;
    }

    public void setNivelinst_apoderado(String nivelinst_apoderado) {
        this.nivelinst_apoderado = nivelinst_apoderado;
    }

    public String getReligion_apoderado() {
        return religion_apoderado;
    }

    public void setReligion_apoderado(String religion_apoderado) {
        this.religion_apoderado = religion_apoderado;
    }

    public String getOcupacion_apoderado() {
        return ocupacion_apoderado;
    }

    public void setOcupacion_apoderado(String ocupacion_apoderado) {
        this.ocupacion_apoderado = ocupacion_apoderado;
    }

    public Integer getCelular() {
        return celular;
    }

    public void setCelular(Integer celular) {
        this.celular = celular;
    }

    public Integer getTelefonoFijo() {
        return telefonoFijo;
    }

    public void setTelefonoFijo(Integer telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    public String getCondlaboral_apod() {
        return condlaboral_apod;
    }

    public void setCondlaboral_apod(String condlaboral_apod) {
        this.condlaboral_apod = condlaboral_apod;
    }

    public String getFuncion_apoderado() {
        return funcion_apoderado;
    }

    public void setFuncion_apoderado(String funcion_apoderado) {
        this.funcion_apoderado = funcion_apoderado;
    }

    public String getDepartamento_apoderado() {
        return departamento_apoderado;
    }

    public void setDepartamento_apoderado(String departamento_apoderado) {
        this.departamento_apoderado = departamento_apoderado;
    }

    public String getProvincia_apoderado() {
        return provincia_apoderado;
    }

    public void setProvincia_apoderado(String provincia_apoderado) {
        this.provincia_apoderado = provincia_apoderado;
    }

    public String getDistrito_apoderado() {
        return distrito_apoderado;
    }

    public void setDistrito_apoderado(String distrito_apoderado) {
        this.distrito_apoderado = distrito_apoderado;
    }

    public Apoderado() {
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Apoderado)) {
            return false;
        }
        Apoderado other = (Apoderado) object;
        if ((this.id_apoderado == null && other.id_apoderado != null) || (this.id_apoderado != null && !this.id_apoderado.equals(other.id_apoderado))) {
            return false;
        }
        return true;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_apoderado != null ? id_apoderado.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Apoderado{" +
                "id_apoderado=" + id_apoderado +
                ", tipo_apoderado='" + tipo_apoderado + '\'' +
                ", nombre_apoderado='" + nombre_apoderado + '\'' +
                ", apellido_apoderado='" + apellido_apoderado + '\'' +
                ", fnacimiento_apod=" + fnacimiento_apod +
                ", dni_apoderado=" + dni_apoderado +
                ", idioma_apoderado='" + idioma_apoderado + '\'' +
                ", estcivil_apoderado='" + estcivil_apoderado + '\'' +
                ", departamento_apoderado='" + departamento_apoderado + '\'' +
                ", provincia_apoderado='" + provincia_apoderado + '\'' +
                ", distrito_apoderado='" + distrito_apoderado + '\'' +
                ", nivelinst_apoderado='" + nivelinst_apoderado + '\'' +
                ", religion_apoderado='" + religion_apoderado + '\'' +
                ", ocupacion_apoderado='" + ocupacion_apoderado + '\'' +
                ", celular=" + celular +
                ", telefonoFijo=" + telefonoFijo +
                ", condlaboral_apod='" + condlaboral_apod + '\'' +
                ", funcion_apoderado='" + funcion_apoderado + '\'' +
                ", idexpediente=" + idexpediente +
                '}';
    }
}
