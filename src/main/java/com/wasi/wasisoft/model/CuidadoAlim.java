package com.wasi.wasisoft.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 26/04/2018.
 */
@Entity
@Table(name = "cuidalim")
public class CuidadoAlim implements Serializable {

        private static final long serialVersionUID = 1L;
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Basic(optional = false)
        @Column(name = "id_cuidalim")
        private Long idcuidalim;

        @Size(max = 800)
        @Column(name = "cuid_terceros")
        private String cuidterceros;
        @Size(max = 800)
        @Column(name = "cuidado_menor")
        private String cuidadomenor;
        @Size(max = 800)
        @Column(name = "apoyo_familia")
        private String apoyofamilia;
        @Size(max = 800)
        @Column(name = "dific_cuidado")
        private String dificcuidado;
        @Size(max = 800)
        @Column(name = "apego_fam")
        private String apegofamilia;
        @Size(max = 800)
        @Column(name = "maltrato_fam")
        private String maltratofam;
        @Size(max = 800)
        @Column(name = "aspectos_cuid")
        private String aspectoscuid;

        @Column(name = "hora_despierta")
        private String horadespierta;

        @Size(max = 800)
        @Column(name = "veces_duerme")
        private String vecesduerme;

        @Size(max = 800)
        @Column(name = "apoyo_act")
        private String apoyoact;
        @Size(max = 800)
        @Column(name = "frec_alimen")
        private String frecalimen;
        @Size(max = 800)
        @Column(name = "cant_alimen")
        private String cantalimen;
        @Size(max = 800)
        @Column(name = "consis_alimen")
        private String consisalimen;
        @Size(max = 800)
        @Column(name = "comp_alimen")
        private String compAlimen;
        @Size(max = 800)
        @Column(name = "hora_alimen")
        private String horaalimen;
        @Size(max = 800)
        @Column(name = "alim_notolera")
        private String alimnotolera;
        @Size(max = 800)
        @Column(name = "tipo_alim")
        private String tipoalim;
        @Size(max = 800)
        @Column(name = "apoyo_alimen")
        private String apoyoalimen;
        @Size(max = 800)
        @Column(name = "toma_mater")
        private String tomamaterial;
        @Size(max = 800)
        @Column(name = "liquido_gusta")
        private String liquidogusta;
        @Size(max = 800)
        @Column(name = "veces_toma")
        private String vecestoma;
        @Size(max = 800)
        @Column(name = "consis_tomar")
        private String consistomar;
        @Size(max = 800)
        @Column(name = "posicion_comer")
        private String posicioncomer;
        @Size(max = 800)

        @Column(name = "veces_deposic")
        private String vecesdeposic;
        @Size(max = 800)
        @Column(name = "veces_aseo")
        private String vecesaseo;
        @Size(max = 800)
        @Column(name = "veces_aseobucal")
        private String vecesaseobucal;
         @Column(name = "otros")
         private String otros;
        @Size(max = 800)
        @Column(name = "sufre_estre")
        private String sufreestre;
        @Column(name = "comobebe")
        private String comobebe;
        @Column (name = "tiempolimenta")
        private String tiempolimenta;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
        @ManyToOne
        private Expediente idexpediente;

    public CuidadoAlim() {
    }

    public CuidadoAlim(Long idcuidalim) {
        this.idcuidalim = idcuidalim;
    }

    public CuidadoAlim(String cuidterceros, String cuidadomenor, String apoyofamilia, String dificcuidado, String apegofamilia, String maltratofam, String aspectoscuid, String horadespierta, String vecesduerme, String apoyoact, String frecalimen, String cantalimen, String consisalimen, String compAlimen, String horaalimen, String alimnotolera, String tipoalim, String apoyoalimen, String tomamaterial, String liquidogusta, String vecestoma, String consistomar, String posicioncomer, String vecesdeposic, String vecesaseo, String vecesaseobucal, String otros, String sufreestre, String comobebe, String tiempolimenta, Expediente idexpediente) {
        this.cuidterceros = cuidterceros;
        this.cuidadomenor = cuidadomenor;
        this.apoyofamilia = apoyofamilia;
        this.dificcuidado = dificcuidado;
        this.apegofamilia = apegofamilia;
        this.maltratofam = maltratofam;
        this.aspectoscuid = aspectoscuid;
        this.horadespierta = horadespierta;
        this.vecesduerme = vecesduerme;
        this.apoyoact = apoyoact;
        this.frecalimen = frecalimen;
        this.cantalimen = cantalimen;
        this.consisalimen = consisalimen;
        this.compAlimen = compAlimen;
        this.horaalimen = horaalimen;
        this.alimnotolera = alimnotolera;
        this.tipoalim = tipoalim;
        this.apoyoalimen = apoyoalimen;
        this.tomamaterial = tomamaterial;
        this.liquidogusta = liquidogusta;
        this.vecestoma = vecestoma;
        this.consistomar = consistomar;
        this.posicioncomer = posicioncomer;
        this.vecesdeposic = vecesdeposic;
        this.vecesaseo = vecesaseo;
        this.vecesaseobucal = vecesaseobucal;
        this.otros = otros;
        this.sufreestre = sufreestre;
        this.comobebe = comobebe;
        this.tiempolimenta = tiempolimenta;
        this.idexpediente = idexpediente;
    }

    public String getTiempolimenta() {
        return tiempolimenta;
    }

    public void setTiempolimenta(String tiempolimenta) {
        this.tiempolimenta = tiempolimenta;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Long getIdcuidalim() {
        return idcuidalim;
    }

    public void setIdcuidalim(Long idcuidalim) {
        this.idcuidalim = idcuidalim;
    }

    public String getCuidterceros() {
        return cuidterceros;
    }

    public void setCuidterceros(String cuidterceros) {
        this.cuidterceros = cuidterceros;
    }

    public String getCuidadomenor() {
        return cuidadomenor;
    }

    public void setCuidadomenor(String cuidadomenor) {
        this.cuidadomenor = cuidadomenor;
    }

    public String getApoyofamilia() {
        return apoyofamilia;
    }

    public void setApoyofamilia(String apoyofamilia) {
        this.apoyofamilia = apoyofamilia;
    }

    public String getDificcuidado() {
        return dificcuidado;
    }

    public void setDificcuidado(String dificcuidado) {
        this.dificcuidado = dificcuidado;
    }

    public String getApegofamilia() {
        return apegofamilia;
    }

    public void setApegofamilia(String apegofamilia) {
        this.apegofamilia = apegofamilia;
    }

    public String getMaltratofam() {
        return maltratofam;
    }

    public void setMaltratofam(String maltratofam) {
        this.maltratofam = maltratofam;
    }

    public String getAspectoscuid() {
        return aspectoscuid;
    }

    public void setAspectoscuid(String aspectoscuid) {
        this.aspectoscuid = aspectoscuid;
    }

    public String getHoradespierta() {
        return horadespierta;
    }

    public void setHoradespierta(String horadespierta) {
        this.horadespierta = horadespierta;
    }

    public String getVecesduerme() {
        return vecesduerme;
    }

    public void setVecesduerme(String vecesduerme) {
        this.vecesduerme = vecesduerme;
    }

    public String getApoyoact() {
        return apoyoact;
    }

    public void setApoyoact(String apoyoact) {
        this.apoyoact = apoyoact;
    }

    public String getFrecalimen() {
        return frecalimen;
    }

    public void setFrecalimen(String frecalimen) {
        this.frecalimen = frecalimen;
    }

    public String getCantalimen() {
        return cantalimen;
    }

    public void setCantalimen(String cantalimen) {
        this.cantalimen = cantalimen;
    }

    public String getConsisalimen() {
        return consisalimen;
    }

    public void setConsisalimen(String consisalimen) {
        this.consisalimen = consisalimen;
    }

    public String getCompAlimen() {
        return compAlimen;
    }

    public void setCompAlimen(String compAlimen) {
        this.compAlimen = compAlimen;
    }

    public String getHoraalimen() {
        return horaalimen;
    }

    public void setHoraalimen(String horaalimen) {
        this.horaalimen = horaalimen;
    }

    public String getAlimnotolera() {
        return alimnotolera;
    }

    public void setAlimnotolera(String alimnotolera) {
        this.alimnotolera = alimnotolera;
    }

    public String getTipoalim() {
        return tipoalim;
    }

    public void setTipoalim(String tipoalim) {
        this.tipoalim = tipoalim;
    }

    public String getApoyoalimen() {
        return apoyoalimen;
    }

    public void setApoyoalimen(String apoyoalimen) {
        this.apoyoalimen = apoyoalimen;
    }

    public String getTomamaterial() {
        return tomamaterial;
    }

    public void setTomamaterial(String tomamaterial) {
        this.tomamaterial = tomamaterial;
    }

    public String getLiquidogusta() {
        return liquidogusta;
    }

    public void setLiquidogusta(String liquidogusta) {
        this.liquidogusta = liquidogusta;
    }

    public String getVecestoma() {
        return vecestoma;
    }

    public void setVecestoma(String vecestoma) {
        this.vecestoma = vecestoma;
    }

    public String getConsistomar() {
        return consistomar;
    }

    public void setConsistomar(String consistomar) {
        this.consistomar = consistomar;
    }

    public String getPosicioncomer() {
        return posicioncomer;
    }

    public void setPosicioncomer(String posicioncomer) {
        this.posicioncomer = posicioncomer;
    }

    public String getVecesdeposic() {
        return vecesdeposic;
    }

    public void setVecesdeposic(String vecesdeposic) {
        this.vecesdeposic = vecesdeposic;
    }

    public String getVecesaseo() {
        return vecesaseo;
    }

    public void setVecesaseo(String vecesaseo) {
        this.vecesaseo = vecesaseo;
    }

    public String getVecesaseobucal() {
        return vecesaseobucal;
    }

    public void setVecesaseobucal(String vecesaseobucal) {
        this.vecesaseobucal = vecesaseobucal;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public String getSufreestre() {
        return sufreestre;
    }

    public void setSufreestre(String sufreestre) {
        this.sufreestre = sufreestre;
    }

    public String getComobebe() {
        return comobebe;
    }

    public void setComobebe(String comobebe) {
        this.comobebe = comobebe;
    }
}
