package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 02/05/2018.
 */
@Entity
@Table(name = "saludactual")
public class SaludActual implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_saludactual")
    private Long idsaludactual;

    @Size(max = 100)
    @Column(name = "vacunas_dia")
    private String vacunasdia;
    @Size(max = 100)
    @Column(name = "trastorno_motor")
    private String trastornomotor;
    @Size(max = 100)
    @Column(name = "epilepsia")
    private String epilepsia;
    @Size(max = 100)
    @Column(name = "prob_cardiaco")
    private String probcardiaco;
    @Size(max = 100)
    @Column(name = "prob_broncoresp")
    private String probbroncoresp;
    @Size(max = 100)
    @Column(name = "enfer_contagio")
    private String enfercontagio;
    @Size(max = 100)
    @Column(name = "paraplejia")
    private String paraplejia;
    @Size(max = 100)
    @Column(name = "perd_auditiva")
    private String perdauditiva;
    @Size(max = 100)
    @Column(name = "perd_visual")
    private String perdvisual;
    @Size(max = 100)
    @Column(name = "transtor_emoci")
    private String transtoremoci;
    @Size(max = 100)
    @Column(name = "trantorn_conduc")
    private String trantornconduc;
    @Size(max = 200)
    @Column(name = "otro")
    private String otro;
    @Size(max = 200)
    @Column(name = "recibn_tratam")
    private String recibntratam;
    @Size(max = 200)
    @Column(name = "alimentacion")
    private String alimentacion;
    @Size(max = 200)
    @Column(name = "peso")
    private String peso;
    @Size(max = 200)
    @Column(name = "sueno")
    private String sueno;
    @Column(name = "horas_duerme")
    private Double horasduerme;
    @Size(max = 200)
    @Column(name = "duerme_compania")
    private String duermecompania;
    @Size(max = 100)
    @Column(name = "sufreinsomio")
    private String sufreinsomio;
    @Size(max = 800)
    @Column(name = "obs_salud")
    private String obsSalud;

    @JoinColumn(name = "id_fichaeduc", referencedColumnName = "id_fichaeduc")
    @ManyToOne
    private FichaEducacion idfichaeduc;

    public SaludActual() {
    }

    public SaludActual(Long idsaludactual) {
        this.idsaludactual = idsaludactual;
    }

    public SaludActual(String vacunasdia, String trastornomotor, String epilepsia, String probcardiaco, String probbroncoresp, String enfercontagio, String paraplejia, String perdauditiva, String perdvisual, String transtoremoci, String trantornconduc, String otro, String recibntratam, String alimentacion, String peso, String sueno, Double horasduerme, String duermecompania, String sufreinsomio, String obsSalud, FichaEducacion idfichaeduc) {
        this.vacunasdia = vacunasdia;
        this.trastornomotor = trastornomotor;
        this.epilepsia = epilepsia;
        this.probcardiaco = probcardiaco;
        this.probbroncoresp = probbroncoresp;
        this.enfercontagio = enfercontagio;
        this.paraplejia = paraplejia;
        this.perdauditiva = perdauditiva;
        this.perdvisual = perdvisual;
        this.transtoremoci = transtoremoci;
        this.trantornconduc = trantornconduc;
        this.otro = otro;
        this.recibntratam = recibntratam;
        this.alimentacion = alimentacion;
        this.peso = peso;
        this.sueno = sueno;
        this.horasduerme = horasduerme;
        this.duermecompania = duermecompania;
        this.sufreinsomio = sufreinsomio;
        this.obsSalud = obsSalud;
        this.idfichaeduc = idfichaeduc;
    }

    public Long getIdsaludactual() {
        return idsaludactual;
    }

    public void setIdsaludactual(Long idsaludactual) {
        this.idsaludactual = idsaludactual;
    }

    public String getVacunasdia() {
        return vacunasdia;
    }

    public void setVacunasdia(String vacunasdia) {
        this.vacunasdia = vacunasdia;
    }

    public String getTrastornomotor() {
        return trastornomotor;
    }

    public void setTrastornomotor(String trastornomotor) {
        this.trastornomotor = trastornomotor;
    }

    public String getEpilepsia() {
        return epilepsia;
    }

    public void setEpilepsia(String epilepsia) {
        this.epilepsia = epilepsia;
    }

    public String getProbcardiaco() {
        return probcardiaco;
    }

    public void setProbcardiaco(String probcardiaco) {
        this.probcardiaco = probcardiaco;
    }

    public String getProbbroncoresp() {
        return probbroncoresp;
    }

    public void setProbbroncoresp(String probbroncoresp) {
        this.probbroncoresp = probbroncoresp;
    }

    public String getEnfercontagio() {
        return enfercontagio;
    }

    public void setEnfercontagio(String enfercontagio) {
        this.enfercontagio = enfercontagio;
    }

    public String getParaplejia() {
        return paraplejia;
    }

    public void setParaplejia(String paraplejia) {
        this.paraplejia = paraplejia;
    }

    public String getPerdauditiva() {
        return perdauditiva;
    }

    public void setPerdauditiva(String perdauditiva) {
        this.perdauditiva = perdauditiva;
    }

    public String getPerdvisual() {
        return perdvisual;
    }

    public void setPerdvisual(String perdvisual) {
        this.perdvisual = perdvisual;
    }

    public String getTranstoremoci() {
        return transtoremoci;
    }

    public void setTranstoremoci(String transtoremoci) {
        this.transtoremoci = transtoremoci;
    }

    public String getTrantornconduc() {
        return trantornconduc;
    }

    public void setTrantornconduc(String trantornconduc) {
        this.trantornconduc = trantornconduc;
    }

    public String getOtro() {
        return otro;
    }

    public void setOtro(String otro) {
        this.otro = otro;
    }

    public String getRecibntratam() {
        return recibntratam;
    }

    public void setRecibntratam(String recibntratam) {
        this.recibntratam = recibntratam;
    }

    public String getAlimentacion() {
        return alimentacion;
    }

    public void setAlimentacion(String alimentacion) {
        this.alimentacion = alimentacion;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getSueno() {
        return sueno;
    }

    public void setSueno(String sueno) {
        this.sueno = sueno;
    }

    public Double getHorasduerme() {
        return horasduerme;
    }

    public void setHorasduerme(Double horasduerme) {
        this.horasduerme = horasduerme;
    }

    public String getDuermecompania() {
        return duermecompania;
    }

    public void setDuermecompañia(String duermecompañia) {
        this.duermecompania = duermecompania;
    }

    public String getSufreinsomio() {
        return sufreinsomio;
    }

    public void setSufreinsomio(String sufreinsomio) {
        this.sufreinsomio = sufreinsomio;
    }

    public String getObsSalud() {
        return obsSalud;
    }

    public void setObsSalud(String obsSalud) {
        this.obsSalud = obsSalud;
    }

    public FichaEducacion getIdfichaeduc() {
        return idfichaeduc;
    }

    public void setIdfichaeduc(FichaEducacion idfichaeduc) {
        this.idfichaeduc = idfichaeduc;
    }
}
