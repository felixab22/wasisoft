package com.wasi.wasisoft.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by airo on 1/02/2019.
 */
@Entity
@Table(name = "campaniaatencion")
public class CampaniaAtencion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_campaniaatencion")
    private Long idcampaniaatencion;

    @NotNull
    @Column(name = "nombrecampania")
    private String nombrecampania;

    @Column(name = "servicio1")
    private String servicio1;
    @Column(name = "servicio2")
    private String servicio2;
    @Column(name = "servicio3")
    private String servicio3;
    @Column(name = "servicio4")
    private String servicio4;
    @Column(name = "servicio5")
    private String servicio5;
    @Column(name = "servicio6")
    private String servicio6;
    @Column(name = "fcampania")
    private String fcampania;

    public CampaniaAtencion() {
    }

    public CampaniaAtencion(Long idcampaniaatencion) {
        this.idcampaniaatencion=idcampaniaatencion;
    }
    public CampaniaAtencion(@NotNull String nombrecampania, String servicio1, String servicio2, String servicio3, String servicio4, String servicio5, String servicio6, String fcampania) {
        this.nombrecampania = nombrecampania;
        this.servicio1 = servicio1;
        this.servicio2 = servicio2;
        this.servicio3 = servicio3;
        this.servicio4 = servicio4;
        this.servicio5 = servicio5;
        this.servicio6 = servicio6;
        this.fcampania = fcampania;
    }

    public Long getIdcampaniaatencion() {
        return idcampaniaatencion;
    }

    public void setIdcampaniaatencion(Long idcampaniaatencion) {
        this.idcampaniaatencion = idcampaniaatencion;
    }

    public String getNombrecampania() {
        return nombrecampania;
    }

    public void setNombrecampania(String nombrecampania) {
        this.nombrecampania = nombrecampania;
    }

    public String getServicio1() {
        return servicio1;
    }

    public void setServicio1(String servicio1) {
        this.servicio1 = servicio1;
    }

    public String getServicio2() {
        return servicio2;
    }

    public void setServicio2(String servicio2) {
        this.servicio2 = servicio2;
    }

    public String getServicio3() {
        return servicio3;
    }

    public void setServicio3(String servicio3) {
        this.servicio3 = servicio3;
    }

    public String getServicio4() {
        return servicio4;
    }

    public void setServicio4(String servicio4) {
        this.servicio4 = servicio4;
    }

    public String getServicio5() {
        return servicio5;
    }

    public void setServicio5(String servicio5) {
        this.servicio5 = servicio5;
    }

    public String getServicio6() {
        return servicio6;
    }

    public void setServicio6(String servicio6) {
        this.servicio6 = servicio6;
    }

    public String getFcampania() {
        return fcampania;
    }

    public void setFcampania(String fcampania) {
        this.fcampania = fcampania;
    }

    @Override
    public String toString() {
        return "CampaniaAtencion{" +
                "idcampaniaatencion=" + idcampaniaatencion +
                ", nombrecampania='" + nombrecampania + '\'' +
                ", servicio1='" + servicio1 + '\'' +
                ", servicio2='" + servicio2 + '\'' +
                ", servicio3='" + servicio3 + '\'' +
                ", servicio4='" + servicio4 + '\'' +
                ", servicio5='" + servicio5 + '\'' +
                ", servicio6='" + servicio6 + '\'' +
                ", fcampania='" + fcampania + '\'' +
                '}';
    }
}
