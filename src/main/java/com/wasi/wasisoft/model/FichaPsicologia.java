package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
@Entity
@Table(name = "fichapsico")
public class FichaPsicologia extends DateAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_fichapsico")
    private Long idfichapsico;

    @Size(max = 200)
    @Column(name = "motivoconsulta")
    private String motivoconsulta;

    @Column(name = "tipofamilia ")
    private String tipofamilia;

    @Size(max = 50)
    @Column(name = "relacionpadres")
    private String relacionpadres;
    @Size(max = 100)
    @Column(name = "climafamiliar")
    private String climafamiliar;
    @Size(max = 200)
    @Column(name = "antecpsiquiatricos")
    private String antecpsiquiatricos;
    @Size(max = 10000)
    @Column(name = "etapa_prenatal")
    private String etapaPrenatal;
    @Size(max = 10000)
    @Column(name = "etapa_postnatal")
    private String etapaPostnatal;
    @Size(max = 10000)
    @Column(name = "desarr_motor")
    private String desarrMotor;
    @Size(max = 10000)
    @Column(name = "lenguaje")
    private String lenguaje;
    @Size(max = 10000)
    @Column(name = "desarr_cognitivo")
    private String desarrCognitivo;
    @Size(max = 100)
    @Column(name = "desarr_personal")
    private String desarrPersonal;
    @Size(max = 300)
    @Column(name = "desarr_dia")
    private String desarrDia;
    @Size(max = 100)
    @Column(name = "historia_educ")
    private String historiaEduc;
    @Size(max = 300)
    @Column(name = "diagn_presuntivo")
    private String diagnPresuntivo;
    @Size(max = 300)
    @Column(name = "recomendaciones")
    private String recomendaciones;

    @JoinColumn(name = "id_referinterna", referencedColumnName = "id_referinterna")
    @ManyToOne
    private ReferenciaInterna idreferenciainterna;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    public FichaPsicologia() {
    }

    public FichaPsicologia(Long idfichapsico) {
        this.idfichapsico = idfichapsico;
    }

    public FichaPsicologia(String motivoconsulta, String relacionpadres, String climafamiliar, String antecpsiquiatricos, String etapaPrenatal, String etapaPostnatal, String desarrMotor, String lenguaje, String desarrCognitivo, String desarrPersonal, String desarrDia, String historiaEduc, String diagnPresuntivo, String recomendaciones, ReferenciaInterna idreferenciainterna, Expediente idexpediente) {
        this.motivoconsulta = motivoconsulta;
        this.relacionpadres = relacionpadres;
        this.climafamiliar = climafamiliar;
        this.antecpsiquiatricos = antecpsiquiatricos;
        this.etapaPrenatal = etapaPrenatal;
        this.etapaPostnatal = etapaPostnatal;
        this.desarrMotor = desarrMotor;
        this.lenguaje = lenguaje;
        this.desarrCognitivo = desarrCognitivo;
        this.desarrPersonal = desarrPersonal;
        this.desarrDia = desarrDia;
        this.historiaEduc = historiaEduc;
        this.diagnPresuntivo = diagnPresuntivo;
        this.recomendaciones = recomendaciones;
        this.idreferenciainterna = idreferenciainterna;
        this.idexpediente = idexpediente;
    }

    public Long getIdfichapsico() {
        return idfichapsico;
    }

    public void setIdfichapsico(Long idfichapsico) {
        this.idfichapsico = idfichapsico;
    }

    public String getMotivoconsulta() {
        return motivoconsulta;
    }

    public void setMotivoconsulta(String motivoconsulta) {
        this.motivoconsulta = motivoconsulta;
    }

    public String getTipofamilia() {
        return tipofamilia;
    }

    public void setTipofamilia(String tipofamilia) {
        this.tipofamilia = tipofamilia;
    }

    public String getRelacionpadres() {
        return relacionpadres;
    }

    public void setRelacionpadres(String relacionpadres) {
        this.relacionpadres = relacionpadres;
    }

    public String getClimafamiliar() {
        return climafamiliar;
    }

    public void setClimafamiliar(String climafamiliar) {
        this.climafamiliar = climafamiliar;
    }

    public String getAntecpsiquiatricos() {
        return antecpsiquiatricos;
    }

    public void setAntecpsiquiatricos(String antecpsiquiatricos) {
        this.antecpsiquiatricos = antecpsiquiatricos;
    }

    public String getEtapaPrenatal() {
        return etapaPrenatal;
    }

    public void setEtapaPrenatal(String etapaPrenatal) {
        this.etapaPrenatal = etapaPrenatal;
    }

    public String getEtapaPostnatal() {
        return etapaPostnatal;
    }

    public void setEtapaPostnatal(String etapaPostnatal) {
        this.etapaPostnatal = etapaPostnatal;
    }

    public String getDesarrMotor() {
        return desarrMotor;
    }

    public void setDesarrMotor(String desarrMotor) {
        this.desarrMotor = desarrMotor;
    }

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    public String getDesarrCognitivo() {
        return desarrCognitivo;
    }

    public void setDesarrCognitivo(String desarrCognitivo) {
        this.desarrCognitivo = desarrCognitivo;
    }

    public String getDesarrPersonal() {
        return desarrPersonal;
    }

    public void setDesarrPersonal(String desarrPersonal) {
        this.desarrPersonal = desarrPersonal;
    }

    public String getDesarrDia() {
        return desarrDia;
    }

    public void setDesarrDia(String desarrDia) {
        this.desarrDia = desarrDia;
    }

    public String getHistoriaEduc() {
        return historiaEduc;
    }

    public void setHistoriaEduc(String historiaEduc) {
        this.historiaEduc = historiaEduc;
    }

    public String getDiagnPresuntivo() {
        return diagnPresuntivo;
    }

    public void setDiagnPresuntivo(String diagnPresuntivo) {
        this.diagnPresuntivo = diagnPresuntivo;
    }

    public String getRecomendaciones() {
        return recomendaciones;
    }

    public void setRecomendaciones(String recomendaciones) {
        this.recomendaciones = recomendaciones;
    }

    public ReferenciaInterna getIdreferenciainterna() {
        return idreferenciainterna;
    }

    public void setIdreferenciainterna(ReferenciaInterna idreferenciainterna) {
        this.idreferenciainterna = idreferenciainterna;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }
}
