package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 02/05/2018.
 */
@Entity
@Table(name = "fichaeduc")
public class FichaEducacion extends DateAudit implements Serializable{

        private static final long serialVersionUID = 1L;
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Basic(optional = false)
        @Column(name = "id_fichaeduc")
        private Long idfichaeduc;

        @Column(name = "fregistro")
        @Temporal(TemporalType.DATE)
        private Date fregistro;

        @Size(max = 50)
        @Column(name = "personal_registro")
        private String personalregistro;

        @Size(max = 20)
        @Column(name = "lenguamaterna")
        private String lenguamaterna;
        @Size(max = 20)
        @Column(name = "gradodominiolm")
        private String gradodominiolm;
        @Size(max = 20)
        @Column(name = "lenguauso")
        private String lenguauso;
        @Size(max = 50)
        @Column(name = "gradodominiolu")
        private String gradodominiolu;
        @Size(max = 50)
        @Column(name = "escolactual")
        private String escolactual;
        @Size(max = 50)
        @Column(name = "insteduc")
        private String insteduc;
        @Size(max = 100)
        @Column(name = "nombreinfor")
        private String nombreinfor;
        @Size(max = 20)
        @Column(name = "relacionpaciente")
        private String relacionpaciente;
        @Column(name = "motivoentre")
        private String motivoentre;
        @Size(max = 20)
        @Column(name = "diagn_previo")
        private String diagnprevio;
        @Size(max = 100)
        @Column(name = "espec_diagn")
        private String especDiagn;
        @Size(max = 100)
        @Column(name = "otrosdiagn")
        private String otrosdiagn;
        @Column(name = "pediatria")
        private String pediatria;
        @Column(name = "genetico")
        private String genetico;
        @Column(name = "neurologia")
        private String neurologia;
        @Column(name = "psicopedagogico")
        private String psicopedagogico;
        @Column(name = "psicologia")
        private String psicologia;
        @Column(name = "terapiaocupa")
        private String terapiaocupa;

    @JoinColumn(name = "id_referinterna", referencedColumnName = "id_referinterna")
        @ManyToOne
        private ReferenciaInterna idreferinterna;
        @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
        @ManyToOne
        private Expediente idexpediente;

    public FichaEducacion() {
    }

    public FichaEducacion(Long idfichaeduc) {
        this.idfichaeduc = idfichaeduc;
    }

    public FichaEducacion(Date fregistro, String personalregistro, String lenguamaterna, String gradodominiolm, String lenguauso, String gradodominiolu, String escolactual, String insteduc, String nombreinfor, String relacionpaciente, String motivoentre, String diagnprevio, String especDiagn, String otrosdiagn, String pediatria, String genetico, String neurologia, String psicopedagogico, String psicologia, String terapiaocupa, ReferenciaInterna idreferinterna, Expediente idexpediente) {
        this.fregistro = fregistro;
        this.personalregistro = personalregistro;
        this.lenguamaterna = lenguamaterna;
        this.gradodominiolm = gradodominiolm;
        this.lenguauso = lenguauso;
        this.gradodominiolu = gradodominiolu;
        this.escolactual = escolactual;
        this.insteduc = insteduc;
        this.nombreinfor = nombreinfor;
        this.relacionpaciente = relacionpaciente;
        this.motivoentre = motivoentre;
        this.diagnprevio = diagnprevio;
        this.especDiagn = especDiagn;
        this.otrosdiagn = otrosdiagn;
        this.pediatria = pediatria;
        this.genetico = genetico;
        this.neurologia = neurologia;
        this.psicopedagogico = psicopedagogico;
        this.psicologia = psicologia;
        this.terapiaocupa = terapiaocupa;
        this.idreferinterna = idreferinterna;
        this.idexpediente = idexpediente;
    }

    public Long getIdfichaeduc() {
        return idfichaeduc;
    }

    public void setIdfichaeduc(Long idfichaeduc) {
        this.idfichaeduc = idfichaeduc;
    }

    public Date getFregistro() {
        return fregistro;
    }

    public void setFregistro(Date fregistro) {
        this.fregistro = fregistro;
    }

    public String getPersonalregistro() {
        return personalregistro;
    }

    public void setPersonalregistro(String personalregistro) {
        this.personalregistro = personalregistro;
    }

    public String getLenguamaterna() {
        return lenguamaterna;
    }

    public void setLenguamaterna(String lenguamaterna) {
        this.lenguamaterna = lenguamaterna;
    }

    public String getPediatria() {
        return pediatria;
    }

    public void setPediatria(String pediatria) {
        this.pediatria = pediatria;
    }

    public String getGenetico() {
        return genetico;
    }

    public void setGenetico(String genetico) {
        this.genetico = genetico;
    }

    public String getNeurologia() {
        return neurologia;
    }

    public void setNeurologia(String neurologia) {
        this.neurologia = neurologia;
    }

    public String getPsicopedagogico() {
        return psicopedagogico;
    }

    public void setPsicopedagogico(String psicopedagogico) {
        this.psicopedagogico = psicopedagogico;
    }

    public String getPsicologia() {
        return psicologia;
    }

    public void setPsicologia(String psicologia) {
        this.psicologia = psicologia;
    }

    public String getTerapiaocupa() {
        return terapiaocupa;
    }

    public void setTerapiaocupa(String terapiaocupa) {
        this.terapiaocupa = terapiaocupa;
    }

    public String getGradodominiolm() {
        return gradodominiolm;
    }

    public void setGradodominiolm(String gradodominiolm) {
        this.gradodominiolm = gradodominiolm;
    }

    public String getLenguauso() {
        return lenguauso;
    }

    public void setLenguauso(String lenguauso) {
        this.lenguauso = lenguauso;
    }

    public String getGradodominiolu() {
        return gradodominiolu;
    }

    public void setGradodominiolu(String gradodominiolu) {
        this.gradodominiolu = gradodominiolu;
    }

    public String getEscolactual() {
        return escolactual;
    }

    public void setEscolactual(String escolactual) {
        this.escolactual = escolactual;
    }

    public String getInsteduc() {
        return insteduc;
    }

    public void setInsteduc(String insteduc) {
        this.insteduc = insteduc;
    }

    public String getNombreinfor() {
        return nombreinfor;
    }

    public void setNombreinfor(String nombreinfor) {
        this.nombreinfor = nombreinfor;
    }

    public String getRelacionpaciente() {
        return relacionpaciente;
    }

    public void setRelacionpaciente(String relacionpaciente) {
        this.relacionpaciente = relacionpaciente;
    }

    public String getMotivoentre() {
        return motivoentre;
    }

    public void setMotivoentre(String motivoentre) {
        this.motivoentre = motivoentre;
    }

    public String getDiagnprevio() {
        return diagnprevio;
    }

    public void setDiagnprevio(String diagnprevio) {
        this.diagnprevio = diagnprevio;
    }

    public String getEspecDiagn() {
        return especDiagn;
    }

    public void setEspecDiagn(String especDiagn) {
        this.especDiagn = especDiagn;
    }

    public String getOtrosdiagn() {
        return otrosdiagn;
    }

    public void setOtrosdiagn(String otrosdiagn) {
        this.otrosdiagn = otrosdiagn;
    }

    public ReferenciaInterna getIdreferinterna() {
        return idreferinterna;
    }

    public void setIdreferinterna(ReferenciaInterna idreferinterna) {
        this.idreferinterna = idreferinterna;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }
}
