package com.wasi.wasisoft.model;

import javax.persistence.*;

@Entity

@Table(name = "citaterapia")
@Access(AccessType.FIELD)
public class CitaTerapia {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    @Column(name = "id_citaterapia", unique = true, nullable = false)
    private Long idcitaterapia;

    @Column(name = "hora")
    private String hora;

    @Column(name = "lunes")
    private String lunes;

    @Column(name = "martes")
    private String martes;

    @Column(name = "miercoles")
    private String miercoles;

    @Column(name = "jueves")
    private String jueves;

    @Column(name = "viernes")
    private String viernes;

    @Column(name = "orden")
    private Integer orden;

    @Column(name = "lunescobro")
    private Double lunescobro;
    @Column(name = "martescobro")
    private Double martescobro;
    @Column(name = "miercolescobro")
    private Double miercolescobro;
    @Column(name = "juevescobro")
    private Double juevescobro;
    @Column(name = "viernescobro")
    private Double viernescobro;

    //@JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    //@ManyToOne
    //private Expediente idexpediente;

    @JoinColumn(name = "id_personal", referencedColumnName = "id_personal")
    @ManyToOne
    private Personal idpersonal;

    public CitaTerapia() {
    }

    public CitaTerapia(Long idcitaterapia) {
        this.idcitaterapia = idcitaterapia;
    }

    public CitaTerapia(String hora, String lunes, String martes, String miercoles, String jueves, String viernes, Integer orden, Double lunescobro, Double martescobro, Double miercolescobro, Double juevescobro, Double viernescobro, Personal idpersonal) {
        this.hora = hora;
        this.lunes = lunes;
        this.martes = martes;
        this.miercoles = miercoles;
        this.jueves = jueves;
        this.viernes = viernes;
        this.orden = orden;
        this.lunescobro = lunescobro;
        this.martescobro = martescobro;
        this.miercolescobro = miercolescobro;
        this.juevescobro = juevescobro;
        this.viernescobro = viernescobro;
        this.idpersonal = idpersonal;
    }

    public Long getIdcitaterapia() {
        return idcitaterapia;
    }

    public void setIdcitaterapia(Long idcitaterapia) {
        this.idcitaterapia = idcitaterapia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getLunes() {
        return lunes;
    }

    public void setLunes(String lunes) {
        this.lunes = lunes;
    }

    public String getMartes() {
        return martes;
    }

    public void setMartes(String martes) {
        this.martes = martes;
    }

    public String getMiercoles() {
        return miercoles;
    }

    public void setMiercoles(String miercoles) {
        this.miercoles = miercoles;
    }

    public String getJueves() {
        return jueves;
    }

    public void setJueves(String jueves) {
        this.jueves = jueves;
    }

    public String getViernes() {
        return viernes;
    }

    public void setViernes(String viernes) {
        this.viernes = viernes;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Personal getIdpersonal() {
        return idpersonal;
    }

    public void setIdpersonal(Personal idpersonal) {
        this.idpersonal = idpersonal;
    }

    public Double getLunescobro() {
        return lunescobro;
    }

    public void setLunescobro(Double lunescobro) {
        this.lunescobro = lunescobro;
    }

    public Double getMartescobro() {
        return martescobro;
    }

    public void setMartescobro(Double martescobro) {
        this.martescobro = martescobro;
    }

    public Double getMiercolescobro() {
        return miercolescobro;
    }

    public void setMiercolescobro(Double miercolescobro) {
        this.miercolescobro = miercolescobro;
    }

    public Double getJuevescobro() {
        return juevescobro;
    }

    public void setJuevescobro(Double juevescobro) {
        this.juevescobro = juevescobro;
    }

    public Double getViernescobro() {
        return viernescobro;
    }

    public void setViernescobro(Double viernescobro) {
        this.viernescobro = viernescobro;
    }
}
