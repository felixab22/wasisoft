package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Nelly on 24/04/2018.
 */
@Entity
@Table(name = "instrevaluacion")
public class InstrEvaluacion implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_instrevaluacion")
    private Long idinstrevaluacion;

    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "nombreprueba")
    private String nombreprueba;

    //Registra la inf. a quien se evalua con una prueba (0 = evalua a madres, 1= evalua a niños)
    @Size(max = 20)
    @Column(name = "evalua_a")
    private Boolean evaluaa;
////
//    @JoinColumn(name = "id_area", referencedColumnName = "id_area")
//    @ManyToOne
//    private Area area;

    public InstrEvaluacion() {
    }

    public InstrEvaluacion(Long idinstrevaluacion) {
        this.idinstrevaluacion = idinstrevaluacion;
    }

    public InstrEvaluacion(String nombreprueba, Boolean evaluaa) {
        this.nombreprueba = nombreprueba;
        this.evaluaa = evaluaa;
    }

    public Long getIdinstrevaluacion() {
        return idinstrevaluacion;
    }

    public void setIdinstrevaluacion(Long idinstrevaluacion) {
        this.idinstrevaluacion = idinstrevaluacion;
    }

    public String getNombreprueba() {
        return nombreprueba;
    }

    public void setNombreprueba(String nombreprueba) {
        this.nombreprueba = nombreprueba;
    }

    public Boolean getEvaluaa() {
        return evaluaa;
    }

    public void setEvaluaa(Boolean evaluaa) {
        this.evaluaa = evaluaa;
    }

//    public Area getArea() {
//        return area;
//    }
//
//    public void setArea(Area area) {
//        this.area = area;
//    }
}
