package com.wasi.wasisoft.model;

/**
 * Created by HeverFernandez on 07/12/17.
 */
public enum  RoleName {
    Administradora,
    Generador_de_ingresos,
    Jefe_centro_de_día,
    Jefe_de_psicología,
    Jefe_de_rehabilitación,
    Jefe_de_salud,
    Jefe_de_social,
    Jefe_de_terapia_ocupacional,
    Jefe_de_educación,
    Sistema,
    Voluntario_centro_de_día,
    Voluntario_comunicación,
    Voluntario_psicología,
    Voluntario_rehabilitación,
    Voluntario_terapia_ocupacional

    }
