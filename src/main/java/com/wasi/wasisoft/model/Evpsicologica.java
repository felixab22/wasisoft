package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by HeverFernandez on 10/09/2018.
 */
@Entity
@Table(name = "evpsicologica")
public class Evpsicologica {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idevpsicologica")
    private Long idevpsicologica;

    @Column(name = "tipo_familia")
    private String tipo_familia;

    @Column(name = "fevaluacion")
    @Temporal(TemporalType.DATE)
    private Date fevaluacion;

    @Column(name = "act_realizada")
    private String actrealizada;

    @Column(name = "trab_psico")
    private String trab_psico;

    @Column(name = "progreso")
    private String progreso;

    @JoinColumn(name = "id_fichapsico", referencedColumnName = "id_fichapsico")
    @ManyToOne
    private FichaPsicologia idfichapsicologia;

    public Evpsicologica() {
    }

    public Evpsicologica(Long idevpsicologica) {
        this.idevpsicologica = idevpsicologica;

    }

    public Evpsicologica(String tipo_familia, Date fevaluacion, String actrealizada, String trab_psico, String progreso, FichaPsicologia idfichapsicologia) {
        this.tipo_familia = tipo_familia;
        this.fevaluacion = fevaluacion;
        this.actrealizada = actrealizada;
        this.trab_psico = trab_psico;
        this.progreso = progreso;
        this.idfichapsicologia = idfichapsicologia;
    }

    public Long getIdevpsicologica() {
        return idevpsicologica;
    }

    public void setIdevpsicologica(Long idevpsicologica) {
        this.idevpsicologica = idevpsicologica;
    }

    public String getTipo_familia() {
        return tipo_familia;
    }

    public void setTipo_familia(String tipo_familia) {
        this.tipo_familia = tipo_familia;
    }

    public Date getFevaluacion() {
        return fevaluacion;
    }

    public void getFevaluacion(Date fevaluacion) {
        this.fevaluacion = fevaluacion;
    }

    public String getActrealizada() {
        return actrealizada;
    }

    public void setActrealizada(String actrealizada) {
        this.actrealizada = actrealizada;
    }

    public String getTrab_psico() {
        return trab_psico;
    }

    public void setTrab_psico(String trab_psico) {
        this.trab_psico = trab_psico;
    }

    public String getProgreso() {
        return progreso;
    }

    public void setProgreso(String progreso) {
        this.progreso = progreso;
    }

    public FichaPsicologia getIdfichapsicologia() {
        return idfichapsicologia;
    }

    public void setIdfichapsicologia(FichaPsicologia idfichapsicologia) {
        this.idfichapsicologia = idfichapsicologia;
    }
}

