package com.wasi.wasisoft.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by airo on 1/02/2019.
 */
@Entity
@Table(name = "registrocampania")
public class RegistroCampania implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registrocampania")
    private Long idregistrocampania;

    @NotNull
    @Column(name = "nombrepaciente")
    private String nombrepaciente;

    @Column(name = "fregistro")
    private String fregistro;
    @Column(name = "motivoconsulta")
    private String motivoconsulta;
    @Column(name = "anamnesis")
    private String anamnesis;
    @Column(name = "examenfisico")
    private String examenfisico;
    @Column(name = "diagnostico")
    private String diagnostico;
    @Column(name = "tratamiento")
    private String tratamiento;
    @Column(name = "recomendaciones")
    private String recomendaciones;
    @Column(name = "cita")
    private String cita;
    @Column(name = "edad")
    private int edad;
    @Column(name = "peso")
    private Double peso;

    @JoinColumn(name = "id_campaniaatencion", referencedColumnName = "id_campaniaatencion")
    @ManyToOne(fetch = FetchType.LAZY)
    private CampaniaAtencion campaniaatencion;

    public RegistroCampania() {
    }

    public RegistroCampania(@NotNull String nombrepaciente, String fregistro, String motivoconsulta, String anamnesis, String examenfisico, String diagnostico, String tratamiento, String recomendaciones, String cita, int edad, Double peso,CampaniaAtencion campaniaatencion) {
        this.nombrepaciente = nombrepaciente;
        this.fregistro = fregistro;
        this.motivoconsulta = motivoconsulta;
        this.anamnesis = anamnesis;
        this.examenfisico = examenfisico;
        this.diagnostico = diagnostico;
        this.tratamiento = tratamiento;
        this.recomendaciones = recomendaciones;
        this.cita = cita;
        this.edad=edad;
        this.peso=peso;
        this.campaniaatencion = campaniaatencion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Long getIdregistrocampania() {
        return idregistrocampania;
    }

    public void setIdregistrocampania(Long idregistrocampania) {
        this.idregistrocampania = idregistrocampania;
    }

    public String getNombrepaciente() {
        return nombrepaciente;
    }

    public void setNombrepaciente(String nombrepaciente) {
        this.nombrepaciente = nombrepaciente;
    }


    public String getFregistro() {
        return fregistro;
    }

    public void setFregistro(String fregistro) {
        this.fregistro = fregistro;
    }

    public String getMotivoconsulta() {
        return motivoconsulta;
    }

    public void setMotivoconsulta(String motivoconsulta) {
        this.motivoconsulta = motivoconsulta;
    }

    public String getAnamnesis() {
        return anamnesis;
    }

    public void setAnamnesis(String anamnesis) {
        this.anamnesis = anamnesis;
    }

    public String getExamenfisico() {
        return examenfisico;
    }

    public void setExamenfisico(String examenfisico) {
        this.examenfisico = examenfisico;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getRecomendaciones() {
        return recomendaciones;
    }

    public void setRecomendaciones(String recomendaciones) {
        this.recomendaciones = recomendaciones;
    }

    public String getCita() {
        return cita;
    }

    public void setCita(String cita) {
        this.cita = cita;
    }

     public CampaniaAtencion getCampaniaatencion() {
        return campaniaatencion;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public void setCampaniaatencion(CampaniaAtencion campaniaatencion) {
        this.campaniaatencion = campaniaatencion;
    }
}
