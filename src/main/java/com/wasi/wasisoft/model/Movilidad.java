package com.wasi.wasisoft.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.xml.internal.ws.spi.db.DatabindingException;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "movilidad")
public class Movilidad implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmovilidad")
    private Long idmovilidad;

    @Column(name = "numseguimiento")
    private Integer numseguimiento;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    @Temporal(TemporalType.DATE)
    @Column(name = "fseguimiento", nullable = false)
    private Date fseguimiento;
    /**
     * Atributos de movilidad izquierda
     */
    @Column(name = "caderaextiz")
    private String caderaextiz;
    @Column(name = "caderaflexiz")
    private String caderaflexiz;
    @Column(name = "caderaabextiz")
    private String  caderaabexciz;
    @Column(name = "caderaabfleiz")
    private String caderaabfleiz;
    @Column(name = "caderaroextiz")
    private String caderarotextiz;
    @Column(name = "caderarotinttiz")
    private String caderarotintiz;
    @Column(name = "rodillaextiz")
    private String rodillaextiz;
    @Column(name = "rodillaextflexiz")
    private String rodillaextflexiz;
    @Column(name = "rodillaflexiz")
    private String rodillaflexiz;
    @Column(name = "tobillosoleusiz")
    private String tobillosoleusiz;
    @Column(name = "tobillogastociz")
    private String tobillogastociz;
    @Column(name = "hombroabdiz")
    private String hombroabdiz;
    @Column(name = "hombroanteiz")
    private String hombroanteiz;
    @Column(name = "hombroexoriz")
    private String hombroexoriz;
    @Column(name = "hombroedoriz")
    private String hombroendoriz;
    @Column(name = "codoexteiz")
    private String codoentiz;
    @Column(name = "codosupiniz")
    private String codosupiniz;
    @Column(name = "manodorsifiz")
    private String manodorsifiz;
    @Column(name = "manoflexioniz")
    private String manoflexioniz;
/**
 * Atributos de movilidad derecha
 */
    @Column(name = "caderaextder")
    private String caderaextder;
    @Column(name = "caderaflexder")
    private String caderaflexder;
    @Column(name = "caderaabextder")
    private String  caderaabexcder;
    @Column(name = "caderaabfleder")
    private String caderaabfleder;
    @Column(name = "caderaroextder")
    private String caderarotextder;
    @Column(name = "caderarotinttder")
    private String caderarotintder;
    @Column(name = "rodillaextder")
    private String rodillaextder;
    @Column(name = "rodillaextflexder")
    private String rodillaextflexder;
    @Column(name = "rodillaflexder")
    private String rodillaflexder;
    @Column(name = "tobillosoleusder")
    private String tobillosoleusder;
    @Column(name = "tobillogastocder")
    private String tobillogastocider;
    @Column(name = "hombroabdder")
    private String hombroabdder;
    @Column(name = "hombroanteder")
    private String hombroanteider;
    @Column(name = "hombroexorder")
    private String hombroexorder;
    @Column(name = "hombroedorder")
    private String hombroendorder;
    @Column(name = "codoexteder")
    private String codoentder;
    @Column(name = "codosupinder")
    private String codosupinder;
    @Column(name = "manodorsifder")
    private String manodorsifder;
    @Column(name = "manoflexionder")
    private String manoflexionder;

    @JoinColumn(name = "id_movarticulaciones", referencedColumnName = "id_movarticulaciones")
    @ManyToOne
    private MovilidadArticulaciones idmovarticulaciones;

    public Movilidad() {
    }

    public Movilidad(Long idmovilidad) {
        this.idmovilidad = idmovilidad;
    }

    public Movilidad(String caderaextiz,Date fseguimiento, Integer numseguimiento, String caderaflexiz, String caderaabexciz, String caderaabfleiz, String caderarotextiz, String caderarotintiz, String rodillaextiz, String rodillaextflexiz, String rodillaflexiz, String tobillosoleusiz, String tobillogastociz, String hombroabdiz, String hombroanteiz, String hombroexoriz, String hombroendoriz, String codoentiz, String codosupiniz, String manodorsifiz, String manoflexioniz, String caderaextder, String caderaflexder, String caderaabexcder, String caderaabfleder, String caderarotextder, String caderarotintder, String rodillaextder, String rodillaextflexder, String rodillaflexder, String tobillosoleusder, String tobillogastocider, String hombroabdder, String hombroanteider, String hombroexorder, String hombroendorder, String codoentder, String codosupinder, String manodorsifder, String manoflexionder, MovilidadArticulaciones idmovarticulaciones) {
        this.fseguimiento = fseguimiento;
        this.numseguimiento = numseguimiento;
        this.caderaextiz = caderaextiz;
        this.caderaflexiz = caderaflexiz;
        this.caderaabexciz = caderaabexciz;
        this.caderaabfleiz = caderaabfleiz;
        this.caderarotextiz = caderarotextiz;
        this.caderarotintiz = caderarotintiz;
        this.rodillaextiz = rodillaextiz;
        this.rodillaextflexiz = rodillaextflexiz;
        this.rodillaflexiz = rodillaflexiz;
        this.tobillosoleusiz = tobillosoleusiz;
        this.tobillogastociz = tobillogastociz;
        this.hombroabdiz = hombroabdiz;
        this.hombroanteiz = hombroanteiz;
        this.hombroexoriz = hombroexoriz;
        this.hombroendoriz = hombroendoriz;
        this.codoentiz = codoentiz;
        this.codosupiniz = codosupiniz;
        this.manodorsifiz = manodorsifiz;
        this.manoflexioniz = manoflexioniz;
        this.caderaextder = caderaextder;
        this.caderaflexder = caderaflexder;
        this.caderaabexcder = caderaabexcder;
        this.caderaabfleder = caderaabfleder;
        this.caderarotextder = caderarotextder;
        this.caderarotintder = caderarotintder;
        this.rodillaextder = rodillaextder;
        this.rodillaextflexder = rodillaextflexder;
        this.rodillaflexder = rodillaflexder;
        this.tobillosoleusder = tobillosoleusder;
        this.tobillogastocider = tobillogastocider;
        this.hombroabdder = hombroabdder;
        this.hombroanteider = hombroanteider;
        this.hombroexorder = hombroexorder;
        this.hombroendorder = hombroendorder;
        this.codoentder = codoentder;
        this.codosupinder = codosupinder;
        this.manodorsifder = manodorsifder;
        this.manoflexionder = manoflexionder;
        this.idmovarticulaciones = idmovarticulaciones;
    }

    public Long getIdmovilidad() {
        return idmovilidad;
    }

    public void setIdmovilidad(Long idmovilidad) {
        this.idmovilidad = idmovilidad;
    }

    public String getCaderaextiz() {
        return caderaextiz;
    }

    public void setCaderaextiz(String caderaextiz) {
        this.caderaextiz = caderaextiz;
    }

    public String getCaderaflexiz() {
        return caderaflexiz;
    }

    public void setCaderaflexiz(String caderaflexiz) {
        this.caderaflexiz = caderaflexiz;
    }

    public String getCaderaabexciz() {
        return caderaabexciz;
    }

    public void setCaderaabexciz(String caderaabexciz) {
        this.caderaabexciz = caderaabexciz;
    }

    public String getCaderaabfleiz() {
        return caderaabfleiz;
    }

    public void setCaderaabfleiz(String caderaabfleiz) {
        this.caderaabfleiz = caderaabfleiz;
    }

    public String getCaderarotextiz() {
        return caderarotextiz;
    }

    public void setCaderarotextiz(String caderarotextiz) {
        this.caderarotextiz = caderarotextiz;
    }

    public String getCaderarotintiz() {
        return caderarotintiz;
    }

    public void setCaderarotintiz(String caderarotintiz) {
        this.caderarotintiz = caderarotintiz;
    }

    public String getRodillaextiz() {
        return rodillaextiz;
    }

    public void setRodillaextiz(String rodillaextiz) {
        this.rodillaextiz = rodillaextiz;
    }

    public String getRodillaextflexiz() {
        return rodillaextflexiz;
    }

    public void setRodillaextflexiz(String rodillaextflexiz) {
        this.rodillaextflexiz = rodillaextflexiz;
    }

    public String getRodillaflexiz() {
        return rodillaflexiz;
    }

    public void setRodillaflexiz(String rodillaflexiz) {
        this.rodillaflexiz = rodillaflexiz;
    }

    public String getTobillosoleusiz() {
        return tobillosoleusiz;
    }

    public void setTobillosoleusiz(String tobillosoleusiz) {
        this.tobillosoleusiz = tobillosoleusiz;
    }

    public String getTobillogastociz() {
        return tobillogastociz;
    }

    public void setTobillogastociz(String tobillogastociz) {
        this.tobillogastociz = tobillogastociz;
    }

    public String getHombroabdiz() {
        return hombroabdiz;
    }

    public void setHombroabdiz(String hombroabdiz) {
        this.hombroabdiz = hombroabdiz;
    }

    public String getHombroanteiz() {
        return hombroanteiz;
    }

    public void setHombroanteiz(String hombroanteiz) {
        this.hombroanteiz = hombroanteiz;
    }

    public String getHombroexoriz() {
        return hombroexoriz;
    }

    public void setHombroexoriz(String hombroexoriz) {
        this.hombroexoriz = hombroexoriz;
    }

    public String getHombroendoriz() {
        return hombroendoriz;
    }

    public void setHombroendoriz(String hombroendoriz) {
        this.hombroendoriz = hombroendoriz;
    }

    public String getCodoentiz() {
        return codoentiz;
    }

    public void setCodoentiz(String codoentiz) {
        this.codoentiz = codoentiz;
    }

    public String getCodosupiniz() {
        return codosupiniz;
    }

    public void setCodosupiniz(String codosupiniz) {
        this.codosupiniz = codosupiniz;
    }

    public String getManodorsifiz() {
        return manodorsifiz;
    }

    public void setManodorsifiz(String manodorsifiz) {
        this.manodorsifiz = manodorsifiz;
    }

    public String getManoflexioniz() {
        return manoflexioniz;
    }

    public void setManoflexioniz(String manoflexioniz) {
        this.manoflexioniz = manoflexioniz;
    }

    public String getCaderaextder() {
        return caderaextder;
    }

    public void setCaderaextder(String caderaextder) {
        this.caderaextder = caderaextder;
    }

    public String getCaderaflexder() {
        return caderaflexder;
    }

    public void setCaderaflexder(String caderaflexder) {
        this.caderaflexder = caderaflexder;
    }

    public String getCaderaabexcder() {
        return caderaabexcder;
    }

    public void setCaderaabexcder(String caderaabexcder) {
        this.caderaabexcder = caderaabexcder;
    }

    public String getCaderaabfleder() {
        return caderaabfleder;
    }

    public void setCaderaabfleder(String caderaabfleder) {
        this.caderaabfleder = caderaabfleder;
    }

    public String getCaderarotextder() {
        return caderarotextder;
    }

    public void setCaderarotextder(String caderarotextder) {
        this.caderarotextder = caderarotextder;
    }

    public String getCaderarotintder() {
        return caderarotintder;
    }

    public void setCaderarotintder(String caderarotintder) {
        this.caderarotintder = caderarotintder;
    }

    public String getRodillaextder() {
        return rodillaextder;
    }

    public void setRodillaextder(String rodillaextder) {
        this.rodillaextder = rodillaextder;
    }

    public String getRodillaextflexder() {
        return rodillaextflexder;
    }

    public void setRodillaextflexder(String rodillaextflexder) {
        this.rodillaextflexder = rodillaextflexder;
    }

    public String getRodillaflexder() {
        return rodillaflexder;
    }

    public void setRodillaflexder(String rodillaflexder) {
        this.rodillaflexder = rodillaflexder;
    }

    public String getTobillosoleusder() {
        return tobillosoleusder;
    }

    public void setTobillosoleusder(String tobillosoleusder) {
        this.tobillosoleusder = tobillosoleusder;
    }

    public String getTobillogastocider() {
        return tobillogastocider;
    }

    public void setTobillogastocider(String tobillogastocider) {
        this.tobillogastocider = tobillogastocider;
    }

    public String getHombroabdder() {
        return hombroabdder;
    }

    public void setHombroabdder(String hombroabdder) {
        this.hombroabdder = hombroabdder;
    }

    public String getHombroanteider() {
        return hombroanteider;
    }

    public void setHombroanteider(String hombroanteider) {
        this.hombroanteider = hombroanteider;
    }

    public String getHombroexorder() {
        return hombroexorder;
    }

    public void setHombroexorder(String hombroexorder) {
        this.hombroexorder = hombroexorder;
    }

    public String getHombroendorder() {
        return hombroendorder;
    }

    public void setHombroendorder(String hombroendorder) {
        this.hombroendorder = hombroendorder;
    }

    public String getCodoentder() {
        return codoentder;
    }

    public void setCodoentder(String codoentder) {
        this.codoentder = codoentder;
    }

    public String getCodosupinder() {
        return codosupinder;
    }

    public void setCodosupinder(String codosupinder) {
        this.codosupinder = codosupinder;
    }

    public String getManodorsifder() {
        return manodorsifder;
    }

    public void setManodorsifder(String manodorsifder) {
        this.manodorsifder = manodorsifder;
    }

    public String getManoflexionder() {
        return manoflexionder;
    }

    public void setManoflexionder(String manoflexionder) {
        this.manoflexionder = manoflexionder;
    }

    public Integer getNumseguimiento() {
        return numseguimiento;
    }

    public void setNumseguimiento(Integer numseguimiento) {
        this.numseguimiento = numseguimiento;
    }

    public Date getFseguimiento() {
        return fseguimiento;
    }

    public void setFseguimiento(Date fseguimiento) {
        this.fseguimiento = fseguimiento;
    }

    public MovilidadArticulaciones getIdmovarticulaciones() {
        return idmovarticulaciones;
    }

    public void setIdmovarticulaciones(MovilidadArticulaciones idmovarticulaciones) {
        this.idmovarticulaciones = idmovarticulaciones;
    }
}
