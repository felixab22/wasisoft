package com.wasi.wasisoft.model;

import javax.persistence.*;

@Entity
@Table(name = "provincia")
@Access(AccessType.FIELD)
public class Provincia {

        private static final long serialVersionUID = 1L;
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Basic(optional = false)
        @Column(name = "id_prov")
        private Long id_prov;

        @Column(name = "provincia")
        private String nombre_provincia;

        @Column(name = "id_depa")
        private Integer iddepa;

    public Long getId_prov() {
        return id_prov;
    }

    public void setId_prov(Long id_prov) {
        this.id_prov = id_prov;
    }

    public String getNombre_provincia() {
        return nombre_provincia;
    }

    public void setNombre_provincia(String nombre_provincia) {
        this.nombre_provincia = nombre_provincia;
    }

    public Integer getId_depa() {
        return iddepa;
    }

    public void setId_depa(Integer id_depa) {
        this.iddepa = id_depa;
    }
}
