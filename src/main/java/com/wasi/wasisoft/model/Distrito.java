package com.wasi.wasisoft.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "distrito")
@Access(AccessType.FIELD)
public class Distrito {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_dist")
    private Long id_dist;

    @Column(name = "distrito")
    private String nombre_distrito;

    @Column(name = "id_prov")
    private Integer idprov;

    public Long getId_dist() {
        return id_dist;
    }

    public String getNombre_distrito() {
        return nombre_distrito;
    }

    public void setNombre_distrito(String nombre_distrito) {
        this.nombre_distrito = nombre_distrito;
    }

    public void setId_dist(Long id_dist) {
        this.id_dist = id_dist;
    }

    public Integer getId_prov() {
        return idprov;
    }

    public void setId_prov(Integer id_prov) {
        this.idprov = id_prov;
    }
}
