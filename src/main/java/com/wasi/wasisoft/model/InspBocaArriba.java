package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "inspbocaarriba")
public class InspBocaArriba extends DateAudit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_inspbocaarriba")
    private Long idinspbocaarriba;
    @Size(max = 500)
    @Column(name = "posicion")
    private String posicion;
    @Size(max = 500)
    @Column(name = "tronco")
    private String tronco;
    @Size(max = 500)
    @Column(name = "tono")
    private String tono;
    @Size(max = 500)
    @Column(name = "patronos")
    private String patrones;
    @Size(max = 500)
    @Column(name = "movimientos")
    private String movimientos;
    @Size(max = 500)
    @Column(name = "estado_alerte")
    private String estado_alerte;
    @Size(max = 500)
    @Column(name = "emociones")
    private String emociones;
    @Size(max = 500)
    @Column (name = "visto")
    private String visto;
    @Size(max = 500)
    @Column (name = "otro")
    private String otro;
    @Size(max = 500)
    @Column(name = "especifico")
    private String especifico;
    @Size(max = 500)
    @Column(name = "oido")
    private String oido;
    @Size(max = 500)
    @Column(name = "conclusion")
    private String conclusion;

    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica idfichaterapiafisica;

    public InspBocaArriba() {
    }


    public InspBocaArriba(Long idinspbocaarriba) {
        this.idinspbocaarriba = idinspbocaarriba;
    }

    public InspBocaArriba(String posicion, String tronco, String tono, String patrones, String movimientos, String estado_alerte, String emociones, String visto, String otro, String especifico, String conclusion, String oido, FichaTerapiaFisica idfichaterapiafisica) {
        this.posicion = posicion;
        this.tronco = tronco;
        this.tono = tono;
        this.patrones = patrones;
        this.movimientos = movimientos;
        this.estado_alerte = estado_alerte;
        this.emociones = emociones;
        this.visto = visto;
        this.otro = otro;
        this.especifico = especifico;
        this.conclusion = conclusion;
        this.oido = oido;
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public Long getIdinspbocaarriba() {
        return idinspbocaarriba;
    }

    public void setIdinspbocaarriba(Long idinspbocaarriba) {
        this.idinspbocaarriba = idinspbocaarriba;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public String getTronco() {
        return tronco;
    }

    public void setTronco(String tronco) {
        this.tronco = tronco;
    }

    public String getTono() {
        return tono;
    }

    public void setTono(String tono) {
        this.tono = tono;
    }

    public String getPatrones() {
        return patrones;
    }

    public void setPatrones(String patrones) {
        this.patrones = patrones;
    }

    public String getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(String movimientos) {
        this.movimientos = movimientos;
    }

    public String getEstado_alerte() {
        return estado_alerte;
    }

    public void setEstado_alerte(String estado_alerte) {
        this.estado_alerte = estado_alerte;
    }

    public String getEmociones() {
        return emociones;
    }

    public void setEmociones(String emociones) {
        this.emociones = emociones;
    }

    public String getVisto() {
        return visto;
    }

    public void setVisto(String visto) {
        this.visto = visto;
    }

    public String getOtro() {
        return otro;
    }

    public void setOtro(String otro) {
        this.otro = otro;
    }

    public String getEspecifico() {
        return especifico;
    }

    public void setEspecifico(String especifico) {
        this.especifico = especifico;
    }

    public String getConclusion() {
        return conclusion;
    }

    public String getOido() {
        return oido;
    }

    public void setOido(String oido) {
        this.oido = oido;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public FichaTerapiaFisica getIdfichaterapiafisica() {
        return idfichaterapiafisica;
    }

    public void setIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }


}
