package com.wasi.wasisoft.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "fuerzaseg")
public class FuerzaSeguimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idfuerzaseguimiento")
    private Long idfuerzaseguimiento;

    @Column(name = "numseguimientofuerza")
    private Integer numseguimientofuerza;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    @Temporal(TemporalType.DATE)
    @Column(name = "fseguimientofuerza", nullable = false)
    private Date fseguimientofuerza;
    /**
     * Atributos de seguimineto de la fuerza izquierda
     */
    @Column(name = "caderaextiz")
    private String caderaextiz;
    @Column(name = "caderaflexiz")
    private String caderaflexiz;
    @Column(name = "caderaabdglutiz")
    private String caderaabdglutiz;
    @Column(name = "caderaabducextiz")
    private String caderaabducextiz;
    @Column(name = "caderaabducflexiz")
    private String caderaabducflexiz;

    @Column(name = "rodillaextiz")
    private String rodillaextiz;
    @Column(name = "rodillaflexiz")
    private String rodillaflexiz;

    @Column(name = "tobillodorsiiz")
    private String tobillodorsiiz;

    @Column(name = "tobilloplantiz")
    private String tobilloplantiz;

    @Column(name = "hombrosanteiz")
    private String hombrosanteiz;
    @Column(name = "hombrosabduiz")
    private String hombrosabduiz;
    @Column(name = "codoextiz")
    private String codextiz;
    @Column(name = "codoflexiz")
    private String codoflexiz;
    /**
     * Atributos del seguimineto de la fuerza de la derecha
     */
    @Column(name = "caderaextder")
    private String caderaextder;
    @Column(name = "caderaflexder")
    private String caderaflexder;
    @Column(name = "caderaabdglutder")
    private String caderaabdglutder;
    @Column(name = "caderaabducextder")
    private String caderaabducextder;
    @Column(name = "caderaabducflexder")
    private String caderaabducflexder;

    @Column(name = "rodillaextder")
    private String rodillaextder;

    @Column(name = "rodillaflexder")
    private String rodillaflexder;

    @Column(name = "tobillodorsider")
    private String tobillodorsider;

    @Column(name = "tobilloplantder")
    private String tobilloplantder;

    @Column(name = "hombrosanteder")
    private String hombrosanteder;
    @Column(name = "hombrosabduder")
    private String hombrosabduder;
    @Column(name = "codoextder")
    private String codextder;
    @Column(name = "codoflexder")
    private String codoflexder;

    @Column(name = "abdominales")
    private String abdominales;
    @Column(name = "extespalda")
    private String extespalda;

    @JoinColumn(name = "id_fuerza", referencedColumnName = "id_fuerza")
    @ManyToOne
    private Fuerza fuerza;

    public FuerzaSeguimiento() {
    }

    public FuerzaSeguimiento(Long idfuerzaseguimiento) {
        this.idfuerzaseguimiento = idfuerzaseguimiento;
    }

    public FuerzaSeguimiento(Integer numseguimientofuerza, Date fseguimientofuerza, String caderaextiz, String caderaflexiz, String caderaabdglutiz, String caderaabducextiz, String caderaabducflexiz, String rodillaextiz, String rodillaflexiz, String tobillodorsiiz, String tobilloplantiz, String hombrosanteiz, String hombrosabduiz, String codextiz, String codoflexiz, String caderaextder, String caderaflexder, String caderaabdglutder, String caderaabducextder, String caderaabducflexder, String rodillaextder, String rodillaflexder, String tobillodorsider, String tobilloplantder, String hombrosanteder, String hombrosabduder, String codextder, String codoflexder, String abdominales, String extespalda, Fuerza fuerza) {
        this.numseguimientofuerza = numseguimientofuerza;
        this.fseguimientofuerza = fseguimientofuerza;
        this.caderaextiz = caderaextiz;
        this.caderaflexiz = caderaflexiz;
        this.caderaabdglutiz = caderaabdglutiz;
        this.caderaabducextiz = caderaabducextiz;
        this.caderaabducflexiz = caderaabducflexiz;
        this.rodillaextiz = rodillaextiz;
        this.rodillaflexiz = rodillaflexiz;
        this.tobillodorsiiz = tobillodorsiiz;
        this.tobilloplantiz = tobilloplantiz;
        this.hombrosanteiz = hombrosanteiz;
        this.hombrosabduiz = hombrosabduiz;
        this.codextiz = codextiz;
        this.codoflexiz = codoflexiz;
        this.caderaextder = caderaextder;
        this.caderaflexder = caderaflexder;
        this.caderaabdglutder = caderaabdglutder;
        this.caderaabducextder = caderaabducextder;
        this.caderaabducflexder = caderaabducflexder;
        this.rodillaextder = rodillaextder;
        this.rodillaflexder = rodillaflexder;
        this.tobillodorsider = tobillodorsider;
        this.tobilloplantder = tobilloplantder;
        this.hombrosanteder = hombrosanteder;
        this.hombrosabduder = hombrosabduder;
        this.codextder = codextder;
        this.codoflexder = codoflexder;
        this.abdominales = abdominales;
        this.extespalda = extespalda;
        this.fuerza = fuerza;
    }

    public Long getIdfuerzaseguimiento() {
        return idfuerzaseguimiento;
    }

    public void setIdfuerzaseguimiento(Long idfuerzaseguimiento) {
        this.idfuerzaseguimiento = idfuerzaseguimiento;
    }

    public Integer getNumseguimientofuerza() {
        return numseguimientofuerza;
    }

    public void setNumseguimientofuerza(Integer numseguimientofuerza) {
        this.numseguimientofuerza = numseguimientofuerza;
    }

    public Date getFseguimientofuerza() {
        return fseguimientofuerza;
    }

    public void setFseguimientofuerza(Date fseguimientofuerza) {
        this.fseguimientofuerza = fseguimientofuerza;
    }

    public String getCaderaextiz() {
        return caderaextiz;
    }

    public void setCaderaextiz(String caderaextiz) {
        this.caderaextiz = caderaextiz;
    }

    public String getCaderaflexiz() {
        return caderaflexiz;
    }

    public void setCaderaflexiz(String caderaflexiz) {
        this.caderaflexiz = caderaflexiz;
    }

    public String getCaderaabdglutiz() {
        return caderaabdglutiz;
    }

    public void setCaderaabdglutiz(String caderaabdglutiz) {
        this.caderaabdglutiz = caderaabdglutiz;
    }

    public String getCaderaabducextiz() {
        return caderaabducextiz;
    }

    public void setCaderaabducextiz(String caderaabducextiz) {
        this.caderaabducextiz = caderaabducextiz;
    }

    public String getCaderaabducflexiz() {
        return caderaabducflexiz;
    }

    public void setCaderaabducflexiz(String caderaabducflexiz) {
        this.caderaabducflexiz = caderaabducflexiz;
    }

    public String getRodillaextiz() {
        return rodillaextiz;
    }

    public void setRodillaextiz(String rodillaextiz) {
        this.rodillaextiz = rodillaextiz;
    }

    public String getRodillaflexiz() {
        return rodillaflexiz;
    }

    public void setRodillaflexiz(String rodillaflexiz) {
        this.rodillaflexiz = rodillaflexiz;
    }

    public String getTobillodorsiiz() {
        return tobillodorsiiz;
    }

    public void setTobillodorsiiz(String tobillodorsiiz) {
        this.tobillodorsiiz = tobillodorsiiz;
    }

    public String getTobilloplantiz() {
        return tobilloplantiz;
    }

    public void setTobilloplantiz(String tobilloplantiz) {
        this.tobilloplantiz = tobilloplantiz;
    }

    public String getTobillodorsider() {
        return tobillodorsider;
    }

    public void setTobillodorsider(String tobillodorsider) {
        this.tobillodorsider = tobillodorsider;
    }

    public String getTobilloplantder() {
        return tobilloplantder;
    }

    public void setTobilloplantder(String tobilloplantder) {
        this.tobilloplantder = tobilloplantder;
    }

    public String getHombrosanteiz() {
        return hombrosanteiz;
    }

    public void setHombrosanteiz(String hombrosanteiz) {
        this.hombrosanteiz = hombrosanteiz;
    }

    public String getHombrosabduiz() {
        return hombrosabduiz;
    }

    public void setHombrosabduiz(String hombrosabduiz) {
        this.hombrosabduiz = hombrosabduiz;
    }

    public String getCodextiz() {
        return codextiz;
    }

    public void setCodextiz(String codextiz) {
        this.codextiz = codextiz;
    }

    public String getCodoflexiz() {
        return codoflexiz;
    }

    public void setCodoflexiz(String codoflexiz) {
        this.codoflexiz = codoflexiz;
    }

    public String getCaderaextder() {
        return caderaextder;
    }

    public void setCaderaextder(String caderaextder) {
        this.caderaextder = caderaextder;
    }

    public String getCaderaflexder() {
        return caderaflexder;
    }

    public void setCaderaflexder(String caderaflexder) {
        this.caderaflexder = caderaflexder;
    }

    public String getCaderaabdglutder() {
        return caderaabdglutder;
    }

    public void setCaderaabdglutder(String caderaabdglutder) {
        this.caderaabdglutder = caderaabdglutder;
    }

    public String getCaderaabducextder() {
        return caderaabducextder;
    }

    public void setCaderaabducextder(String caderaabducextder) {
        this.caderaabducextder = caderaabducextder;
    }

    public String getCaderaabducflexder() {
        return caderaabducflexder;
    }

    public void setCaderaabducflexder(String caderaabducflexder) {
        this.caderaabducflexder = caderaabducflexder;
    }

    public String getRodillaextder() {
        return rodillaextder;
    }

    public void setRodillaextder(String rodillaextder) {
        this.rodillaextder = rodillaextder;
    }

    public String getRodillaflexder() {
        return rodillaflexder;
    }

    public void setRodillaflexder(String rodillaflexder) {
        this.rodillaflexder = rodillaflexder;
    }

    public String getHombrosanteder() {
        return hombrosanteder;
    }

    public void setHombrosanteder(String hombrosanteder) {
        this.hombrosanteder = hombrosanteder;
    }

    public String getHombrosabduder() {
        return hombrosabduder;
    }

    public void setHombrosabduder(String hombrosabduder) {
        this.hombrosabduder = hombrosabduder;
    }

    public String getCodextder() {
        return codextder;
    }

    public void setCodextder(String codextder) {
        this.codextder = codextder;
    }

    public String getCodoflexder() {
        return codoflexder;
    }

    public void setCodoflexder(String codoflexder) {
        this.codoflexder = codoflexder;
    }

    public String getAbdominales() {
        return abdominales;
    }

    public void setAbdominales(String abdominales) {
        this.abdominales = abdominales;
    }

    public String getExtespalda() {
        return extespalda;
    }

    public void setExtespalda(String extespalda) {
        this.extespalda = extespalda;
    }

    public Fuerza getFuerza() {
        return fuerza;
    }

    public void setFuerza(Fuerza fuerza) {
        this.fuerza = fuerza;
    }
}
