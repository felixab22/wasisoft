package com.wasi.wasisoft.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by HeverFernandez on 16/02/2018.
 */
@Entity
@Table(name = "expediente")
//@Access(AccessType.FIELD)
public class Expediente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_expediente")
    private Long id_expediente;

    @Column(name = "codigoexpediente", nullable = false, length = 20)
    private String codigohistorial;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @Column(name = "fechaapertura", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaapertura;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "primerapellido", nullable = false, length = 50)
    private String primer_apellido;

    @Column(name = "segundoapellido", nullable = false, length = 50)
    private String segundoapellido;

    @Column(name = "edad")
    private Integer edad;

    @Column(name = "foto")
    private String foto;

    @Column(name = "sexo")
    private String sexo;

    @Column(name = "fnacimiento")
    @Temporal(TemporalType.DATE)
//    @JsonFormat(pattern = "dd::MM::yyyy")
    private Date fechanacimiento;

    @Column (name = "dni")
    private Integer dni;

    @Column(name = "idioma")
    private String idioma;

    @Column (name = "departamentonacimiento")
    private String departamentonacimiento;

    @Column (name = "provincianacimiento")
    private String provincianacimiento;

    @Column (name = "distritonacimiento")
    private String distritonacimiento;

    @Column (name = "estado")
    private Boolean estadoexpediente;

    @Column (name = "lugar_nacimiento")
    private  String lugarnacimiento;

    @Column(name = "diagnostico")
    private String diagnostico;

    @Column(name = "edad_meses")
    private Integer edadmeses;

//    @OneToMany(mappedBy="idexpediente")
//    private List<FichaTerapiaFisica> fichaTerapiaFisicas;

//    @OneToMany(cascade = {CascadeType.ALL},fetch= FetchType.LAZY, mappedBy = "idexpediente")
//    private Set<Fichasocioecon> fichasocioecons;

//  @JoinColumn(name = "idusercreacion", referencedColumnName = "id_personal")
//    @ManyToOne
//    private Personal idusercreacion;

//    @JoinColumn(name = "id_ubigeo", referencedColumnName = "id_ubigeo")
//    @ManyToOne
//    private Ubigeo id_ubigeo;

    public Expediente(Long id_expediente) {
        this.id_expediente = id_expediente;
    }

    public Expediente(String codigohistorial, Date fechaapertura, String nombre, String primer_apellido, String segundoapellido, Integer edad, String foto, String sexo, Date fechanacimiento, Integer dni, String idioma, String departamentonacimiento, String provincianacimiento, String distritonacimiento, Boolean estadoexpediente, String lugarnacimiento, String diagnostico, Integer edadmeses) {
        this.codigohistorial = codigohistorial;
        this.fechaapertura = fechaapertura;
        this.nombre = nombre;
        this.primer_apellido = primer_apellido;
        this.segundoapellido = segundoapellido;
        this.edad = edad;
        this.foto = foto;
        this.sexo = sexo;
        this.fechanacimiento = fechanacimiento;
        this.dni = dni;
        this.idioma = idioma;
        this.departamentonacimiento = departamentonacimiento;
        this.provincianacimiento = provincianacimiento;
        this.distritonacimiento = distritonacimiento;
        this.estadoexpediente = estadoexpediente;
        this.lugarnacimiento = lugarnacimiento;
        this.diagnostico = diagnostico;
        this.edadmeses = edadmeses;

    }

    public String getDepartamentonacimiento() {
        return departamentonacimiento;
    }

    public void setDepartamentonacimiento(String departamentonacimiento) {
        this.departamentonacimiento = departamentonacimiento;
    }

    public String getProvincianacimiento() {
        return provincianacimiento;
    }

    public void setProvincianacimiento(String provincianacimiento) {
        this.provincianacimiento = provincianacimiento;
    }

    public String getDistritonacimiento() {
        return distritonacimiento;
    }

    public void setDistritonacimiento(String distritonacimiento) {
        this.distritonacimiento = distritonacimiento;
    }


//    public Ubigeo getId_ubigeo() {
//        return id_ubigeo;
//    }
//
//    public void setId_ubigeo(Ubigeo id_ubigeo) {
//        this.id_ubigeo = id_ubigeo;
//    }

    public Expediente() {
    }


    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Long getId_expediente() {
        return id_expediente;
    }

    public void setId_expediente(Long id_expediente) {
        this.id_expediente = id_expediente;
    }

    public Date getFechaapertura() {
        return fechaapertura;
    }

    public void setFechaapertura(Date fechaapertura) {
        this.fechaapertura = fechaapertura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundoapellido() {
        return segundoapellido;
    }

    public void setSegundoapellido(String segundoapellido) {
        this.segundoapellido = segundoapellido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getCodigohistorial() {
        return codigohistorial;
    }

    public void setCodigohistorial(String codigohistorial) {
        this.codigohistorial = codigohistorial;
    }

    public Boolean getEstadoexpediente() {
        return estadoexpediente;
    }

    public void setEstadoexpediente(Boolean estadoexpediente) {
        this.estadoexpediente = estadoexpediente;
    }

    public String getLugarnacimiento() {
        return lugarnacimiento;
    }

    public void setLugarnacimiento(String lugarnacimiento) {
        this.lugarnacimiento = lugarnacimiento;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public Integer getEdadmeses() {
        return edadmeses;
    }

    public void setEdadmeses(Integer edadmeses) {
        this.edadmeses = edadmeses;
    }


    //    public List<FichaTerapiaFisica> getFichaTerapiaFisicas() {
//        return fichaTerapiaFisicas;
//    }
//
//    public void setFichaTerapiaFisicas(List<FichaTerapiaFisica> fichaTerapiaFisicas) {
//        this.fichaTerapiaFisicas = fichaTerapiaFisicas;
//    }

    @Override
    public String toString() {
        return "com.wasi.wasisoft.model.Expediente[ id_expediente=" + id_expediente + " ]";
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_expediente != null ? id_expediente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expediente)) {
            return false;
        }
        Expediente other = (Expediente) object;
        if ((this.id_expediente == null && other.id_expediente != null) || (this.id_expediente != null && !this.id_expediente.equals(other.id_expediente))) {
            return false;
        }
        return true;
    }
}
