package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 26/04/2018.
 */
@Entity
@Table (name = "fichacentrodia")
public class Fichacentrodia extends DateAudit implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_centrodia")
    private Long idcentrodia;

    @Column(name = "fregistro")
    private Date fregistro;

    @Size(max = 800)
    @Column(name = "asistioalbergue")
    private String asistioalbergue;

    @Size(max = 800)
    @Column(name = "terapiaanterior")
    private String terapiaanterior;

    @Size(max = 800)
    @Column(name = "anterioroperacion")
    private String anterioroperacion;

    @Size(max = 800)
    @Column(name = "anteriorenfer")
    private String anteriorenfer;

    @Size(max = 800)
    @Column(name = "medicenterior")
    private String medicanterior;

    @Size(max = 800)
    @Column(name = "motivo_ingreso")
    private String motivoingreso;
    @Size(max = 800)
    @Column(name = "opinion_alberque")
    private String opinionalberque;
    @Size(max = 800)
    @Column(name = "confianza_personal")
    private String confianzapersonal;
    @Size(max = 800)
    @Column(name = "refer_trabajo")
    private String refertrabajo;
    @Size(max = 800)

    @Column(name = "caracter_menor")
    private String caractermenor;
    @Size(max = 800)
    @Column(name = "gusta_jugar")
    private String gustajugar;
    @Size(max = 800)
    @Column(name = "juego_pref")
    private String juegpref;
    @Size(max = 800)
    @Column(name = "juego_preferido")
    private String juegopreferido;
    @Size(max = 800)
    @Column(name = "otros")
    private String otros;

    @JoinColumn(name = "id_cuidalim", referencedColumnName = "id_cuidalim")
    @ManyToOne
    private CuidadoAlim idcuidalim;

    public Fichacentrodia(Long idcentrodia) {
        this.idcentrodia=idcentrodia;
    }

    public Fichacentrodia() {
    }

    public Fichacentrodia(Date fregistro, String asistioalbergue, String terapiaanterior, String anterioroperacion, String anteriorenfer, String medicanterior, String motivoingreso, String opinionalberque, String confianzapersonal, String refertrabajo, String caractermenor, String gustajugar, String juegpref, String juegopreferido, String otros, CuidadoAlim idcuidalim) {
        this.fregistro = fregistro;
        this.asistioalbergue = asistioalbergue;
        this.terapiaanterior = terapiaanterior;
        this.anterioroperacion = anterioroperacion;
        this.anteriorenfer = anteriorenfer;
        this.medicanterior = medicanterior;
        this.motivoingreso = motivoingreso;
        this.opinionalberque = opinionalberque;
        this.confianzapersonal = confianzapersonal;
        this.refertrabajo = refertrabajo;
        this.caractermenor = caractermenor;
        this.gustajugar = gustajugar;
        this.juegpref = juegpref;
        this.juegopreferido = juegopreferido;
        this.otros = otros;
        this.idcuidalim = idcuidalim;
    }

    public Long getIdcentrodia() {
        return idcentrodia;
    }

    public void setIdcentrodia(Long idcentrodia) {
        this.idcentrodia = idcentrodia;
    }

    public Date getFregistro() {
        return fregistro;
    }

    public void setFregistro(Date fregistro) {
        this.fregistro = fregistro;
    }

    public String getAsistioalbergue() {
        return asistioalbergue;
    }

    public void setAsistioalbergue(String asistioalbergue) {
        this.asistioalbergue = asistioalbergue;
    }

    public String getTerapiaanterior() {
        return terapiaanterior;
    }

    public void setTerapiaanterior(String terapiaanterior) {
        this.terapiaanterior = terapiaanterior;
    }

    public String getAnterioroperacion() {
        return anterioroperacion;
    }

    public void setAnterioroperacion(String anterioroperacion) {
        this.anterioroperacion = anterioroperacion;
    }

    public String getAnteriorenfer() {
        return anteriorenfer;
    }

    public void setAnteriorenfer(String anteriorenfer) {
        this.anteriorenfer = anteriorenfer;
    }

    public String getMedicanterior() {
        return medicanterior;
    }

    public void setMedicanterior(String medicanterior) {
        this.medicanterior = medicanterior;
    }

    public String getMotivoingreso() {
        return motivoingreso;
    }

    public void setMotivoingreso(String motivoingreso) {
        this.motivoingreso = motivoingreso;
    }

    public String getOpinionalberque() {
        return opinionalberque;
    }

    public void setOpinionalberque(String opinionalberque) {
        this.opinionalberque = opinionalberque;
    }

    public String getConfianzapersonal() {
        return confianzapersonal;
    }

    public void setConfianzapersonal(String confianzapersonal) {
        this.confianzapersonal = confianzapersonal;
    }

    public String getRefertrabajo() {
        return refertrabajo;
    }

    public void setRefertrabajo(String refertrabajo) {
        this.refertrabajo = refertrabajo;
    }

    public String getCaractermenor() {
        return caractermenor;
    }

    public void setCaractermenor(String caractermenor) {
        this.caractermenor = caractermenor;
    }

    public String getGustajugar() {
        return gustajugar;
    }

    public void setGustajugar(String gustajugar) {
        this.gustajugar = gustajugar;
    }

    public String getJuegpref() {
        return juegpref;
    }

    public void setJuegpref(String juegpref) {
        this.juegpref = juegpref;
    }

    public String getJuegopreferido() {
        return juegopreferido;
    }

    public void setJuegopreferido(String juegopreferido) {
        this.juegopreferido = juegopreferido;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public CuidadoAlim getIdcuidalim() {
        return idcuidalim;
    }

    public void setIdcuidalim(CuidadoAlim idcuidalim) {
        this.idcuidalim = idcuidalim;
    }
}
