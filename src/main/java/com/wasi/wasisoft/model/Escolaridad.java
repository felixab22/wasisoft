package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Nelly on 02/05/2018.
 */
@Entity
@Table(name = "escolaridad")
public class Escolaridad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_escolari")
    private Long idescolaridad;

    @Column(name = "edad_ingreso")
    private Integer edadingreso;
    @Size(max = 4)
    @Column(name = "asistio_jardin")
    private String asistiojardin;
    @Column(name = "num_colegios")
    private Integer numcolegios;
    @Size(max = 4)
    @Column(name = "modensenanza")
    private String modensenanza;
    @Size(max = 100)
    @Column(name = "motivo_cambio")
    private String motivocambio;
    @Size(max = 200)
    @Column(name = "difmatricula")
    private String difmatricula;
    @Size(max = 4)
    @Column(name = "repite_cursos")
    private String repitecursos;
    @Size(max = 100)
    @Column(name = "cursos_rep")
    private String cursosrep;
    @Size(max = 100)
    @Column(name = "motivo_repite")
    private String motivorepite;
    @Size(max = 20)
    @Column(name = "nivel_actual")
    private String nivelactual;
    @Size(max = 4)
    @Column(name = "difaprendizaje")
    private String difaprendizaje;
    @Size(max = 4)
    @Column(name = "dif_participar")
    private String difparticipar;
    @Size(max = 4)
    @Column(name = "conductadisr")
    private String conductadisr;
    @Size(max = 4)
    @Column(name = "asiste_reg")
    private String asistereg;
    @Size(max = 4)
    @Column(name = "asiste_agrado")
    private String asisteagrado;
    @Size(max = 4)
    @Column(name = "apoyo_familiar")
    private String apoyofamiliar;
    @Size(max = 4)
    @Column(name = "amigos")
    private String amigos;
    @Size(max = 20)
    @Column(name = "necesbano")
    private String necesbano;
    @Size(max = 20)
    @Column(name = "neces_vestido")
    private String necesestido;
    @Size(max = 20)
    @Column(name = "neces_alim")
    private String necesalim;
    @Size(max = 150)
    @Column(name = "obser_cole")
    private String obsercole;
    @Size(max = 20)
    @Column(name = "eval_desempeno")
    private String evaldesempeno;
    @Size(max = 20)
    @Column(name = "resp_dificultades")
    private String respdificultades;
    @Size(max = 20)
    @Column(name = "resp_exitos")
    private String respexitos;
    @Size(max = 20)
    @Column(name = "apoyo_aprendizaje")
    private String apoyoaprendizaje;
    @Size(max = 20)
    @Column(name = "expectativas")
    private String expectativas;
    @Size(max = 300)
    @Column(name = "comentarios")
    private String comentarios;

    @JoinColumn(name = "id_fichaeduc", referencedColumnName = "id_fichaeduc")
    @ManyToOne
    private FichaEducacion idfichaeduc;

    public Escolaridad() {
    }

    public Escolaridad(Long idescolaridad) {
        this.idescolaridad = idescolaridad;
    }

    public Escolaridad(Integer edadingreso, String asistiojardin, Integer numcolegios, String modensenanza, String motivocambio, String difmatricula, String repitecursos, String cursosrep, String motivorepite, String nivelactual, String difaprendizaje, String difparticipar, String conductadisr, String asistereg, String asisteagrado, String apoyofamiliar, String amigos, String necesbano, String necesestido, String necesalim, String obsercole, String evaldesempeno, String respdificultades, String respexitos, String apoyoaprendizaje, String expectativas, String comentarios, FichaEducacion idfichaeduc) {
        this.edadingreso = edadingreso;
        this.asistiojardin = asistiojardin;
        this.numcolegios = numcolegios;
        this.modensenanza = modensenanza;
        this.motivocambio = motivocambio;
        this.difmatricula = difmatricula;
        this.repitecursos = repitecursos;
        this.cursosrep = cursosrep;
        this.motivorepite = motivorepite;
        this.nivelactual = nivelactual;
        this.difaprendizaje = difaprendizaje;
        this.difparticipar = difparticipar;
        this.conductadisr = conductadisr;
        this.asistereg = asistereg;
        this.asisteagrado = asisteagrado;
        this.apoyofamiliar = apoyofamiliar;
        this.amigos = amigos;
        this.necesbano = necesbano;
        this.necesestido = necesestido;
        this.necesalim = necesalim;
        this.obsercole = obsercole;
        this.evaldesempeno = evaldesempeno;
        this.respdificultades = respdificultades;
        this.respexitos = respexitos;
        this.apoyoaprendizaje = apoyoaprendizaje;
        this.expectativas = expectativas;
        this.comentarios = comentarios;
        this.idfichaeduc = idfichaeduc;
    }

    public Long getIdescolaridad() {
        return idescolaridad;
    }

    public void setIdescolaridad(Long idescolaridad) {
        this.idescolaridad = idescolaridad;
    }

    public Integer getEdadingreso() {
        return edadingreso;
    }

    public void setEdadingreso(Integer edadingreso) {
        this.edadingreso = edadingreso;
    }

    public String getAsistiojardin() {
        return asistiojardin;
    }

    public void setAsistiojardin(String asistiojardin) {
        this.asistiojardin = asistiojardin;
    }

    public Integer getNumcolegios() {
        return numcolegios;
    }

    public void setNumcolegios(Integer numcolegios) {
        this.numcolegios = numcolegios;
    }

    public String getModensenanza() {
        return modensenanza;
    }

    public void setModensenanza(String modensenanza) {
        this.modensenanza = modensenanza;
    }

    public String getMotivocambio() {
        return motivocambio;
    }

    public void setMotivocambio(String motivocambio) {
        this.motivocambio = motivocambio;
    }

    public String getDifmatricula() {
        return difmatricula;
    }

    public void setDifmatricula(String difmatricula) {
        this.difmatricula = difmatricula;
    }

    public String getRepitecursos() {
        return repitecursos;
    }

    public void setRepitecursos(String repitecursos) {
        this.repitecursos = repitecursos;
    }

    public String getCursosrep() {
        return cursosrep;
    }

    public void setCursosrep(String cursosrep) {
        this.cursosrep = cursosrep;
    }

    public String getMotivorepite() {
        return motivorepite;
    }

    public void setMotivorepite(String motivorepite) {
        this.motivorepite = motivorepite;
    }

    public String getNivelactual() {
        return nivelactual;
    }

    public void setNivelactual(String nivelactual) {
        this.nivelactual = nivelactual;
    }

    public String getDifaprendizaje() {
        return difaprendizaje;
    }

    public void setDifaprendizaje(String difaprendizaje) {
        this.difaprendizaje = difaprendizaje;
    }

    public String getDifparticipar() {
        return difparticipar;
    }

    public void setDifparticipar(String difparticipar) {
        this.difparticipar = difparticipar;
    }

    public String getConductadisr() {
        return conductadisr;
    }

    public void setConductadisr(String conductadisr) {
        this.conductadisr = conductadisr;
    }

    public String getAsistereg() {
        return asistereg;
    }

    public void setAsistereg(String asistereg) {
        this.asistereg = asistereg;
    }

    public String getAsisteagrado() {
        return asisteagrado;
    }

    public void setAsisteagrado(String asisteagrado) {
        this.asisteagrado = asisteagrado;
    }

    public String getApoyofamiliar() {
        return apoyofamiliar;
    }

    public void setApoyofamiliar(String apoyofamiliar) {
        this.apoyofamiliar = apoyofamiliar;
    }

    public String getAmigos() {
        return amigos;
    }

    public void setAmigos(String amigos) {
        this.amigos = amigos;
    }

    public String getNecesbano() {
        return necesbano;
    }

    public void setNecesbano(String necesbano) {
        this.necesbano = necesbano;
    }

    public String getNecesestido() {
        return necesestido;
    }

    public void setNecesestido(String necesestido) {
        this.necesestido = necesestido;
    }

    public String getNecesalim() {
        return necesalim;
    }

    public void setNecesalim(String necesalim) {
        this.necesalim = necesalim;
    }

    public String getObsercole() {
        return obsercole;
    }

    public void setObsercole(String obsercole) {
        this.obsercole = obsercole;
    }

    public String getEvaldesempeno() {
        return evaldesempeno;
    }

    public void setEvaldesempeno(String evaldesempeno) {
        this.evaldesempeno = evaldesempeno;
    }

    public String getRespdificultades() {
        return respdificultades;
    }

    public void setRespdificultades(String respdificultades) {
        this.respdificultades = respdificultades;
    }

    public String getRespexitos() {
        return respexitos;
    }

    public void setRespexitos(String respexitos) {
        this.respexitos = respexitos;
    }

    public String getApoyoaprendizaje() {
        return apoyoaprendizaje;
    }

    public void setApoyoaprendizaje(String apoyoaprendizaje) {
        this.apoyoaprendizaje = apoyoaprendizaje;
    }

    public String getExpectativas() {
        return expectativas;
    }

    public void setExpectativas(String expectativas) {
        this.expectativas = expectativas;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public FichaEducacion getIdfichaeduc() {
        return idfichaeduc;
    }

    public void setIdfichaeduc(FichaEducacion idfichaeduc) {
        this.idfichaeduc = idfichaeduc;
    }
}
