package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 17/04/2018.
 */
@Entity
@Table(name = "diagnosticofinal")
public class DiagnosticoFinal extends DateAudit implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_diagnosticofinal")
    private Long iddiagnosticofinal;

    @Size(max = 200)
    @Column(name = "probpacientcasa")
    private String probpacientcasa;
    @Size(max = 200)
    @Column(name = "probpacienteduc")
    private String probpacienteduc;
    @Size(max = 200)
    @Column(name = "probpacientsocied")
    private String probpacientsocied;
    @Size(max = 200)
    @Column(name = "factpersonales")
    private String factpersonales;
    @Size(max = 200)
    @Column(name = "factexteriores")
    private String factexteriores;
    @Size(max = 100)
    @Column(name = "probterapbiom")
    private String probterapbiom;
    @Size(max = 100)
    @Column(name = "probterapmovili")
    private String probterapmovili;
    @Size(max = 100)
    @Column(name = "probterapfuerza")
    private String probterapfuerza;
    @Size(max = 100)
    @Column(name = "probterapespecif")
    private String probterapespecif;
    @Size(max = 300)
    @Column(name = "probterapfinal")
    private String probterapfinal;
    @Size(max = 200)
    @Column(name = "probprincipal")
    private String probprincipal;
    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica idfichaterapiafisica;

    public DiagnosticoFinal() {
    }

    public DiagnosticoFinal(Long iddiagnosticofinal) {
        this.iddiagnosticofinal = iddiagnosticofinal;
    }

    public DiagnosticoFinal(String probpacientcasa, String probpacienteduc, String probpacientsocied, String factpersonales, String factexteriores, String probterapbiom, String probterapmovili, String probterapfuerza, String probterapespecif, String probterapfinal,String probprincipal, FichaTerapiaFisica idfichaterapiafisica) {
        this.probpacientcasa = probpacientcasa;
        this.probpacienteduc = probpacienteduc;
        this.probpacientsocied = probpacientsocied;
        this.factpersonales = factpersonales;
        this.factexteriores = factexteriores;
        this.probterapbiom = probterapbiom;
        this.probterapmovili = probterapmovili;
        this.probterapfuerza = probterapfuerza;
        this.probterapespecif = probterapespecif;
        this.probterapfinal = probterapfinal;
        this.probprincipal = probprincipal;
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public Long getIddiagnosticofinal() {
        return iddiagnosticofinal;
    }

    public void setIddiagnosticofinal(Long iddiagnosticofinal) {
        this.iddiagnosticofinal = iddiagnosticofinal;
    }

    public String getProbpacientcasa() {
        return probpacientcasa;
    }

    public void setProbpacientcasa(String probpacientcasa) {
        this.probpacientcasa = probpacientcasa;
    }

    public String getProbpacienteduc() {
        return probpacienteduc;
    }

    public void setProbpacienteduc(String probpacienteduc) {
        this.probpacienteduc = probpacienteduc;
    }

    public String getProbpacientsocied() {
        return probpacientsocied;
    }

    public void setProbpacientsocied(String probpacientsocied) {
        this.probpacientsocied = probpacientsocied;
    }

    public String getFactpersonales() {
        return factpersonales;
    }

    public void setFactpersonales(String factpersonales) {
        this.factpersonales = factpersonales;
    }

    public String getFactexteriores() {
        return factexteriores;
    }

    public void setFactexteriores(String factexteriores) {
        this.factexteriores = factexteriores;
    }

    public String getProbterapbiom() {
        return probterapbiom;
    }

    public void setProbterapbiom(String probterapbiom) {
        this.probterapbiom = probterapbiom;
    }

    public String getProbterapmovili() {
        return probterapmovili;
    }

    public void setProbterapmovili(String probterapmovili) {
        this.probterapmovili = probterapmovili;
    }

    public String getProbterapfuerza() {
        return probterapfuerza;
    }

    public void setProbterapfuerza(String probterapfuerza) {
        this.probterapfuerza = probterapfuerza;
    }

    public String getProbterapespecif() {
        return probterapespecif;
    }

    public void setProbterapespecif(String probterapespecif) {
        this.probterapespecif = probterapespecif;
    }

    public String getProbterapfinal() {
        return probterapfinal;
    }

    public void setProbterapfinal(String probterapfinal) {
        this.probterapfinal = probterapfinal;
    }

    public String getProbprincipal() {
        return probprincipal;
    }

    public void setProbprincipal(String probprincipal) {
        this.probprincipal = probprincipal;
    }

    public FichaTerapiaFisica getIdfichaterapiafisica() {
        return idfichaterapiafisica;
    }

    public void setIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }
}
