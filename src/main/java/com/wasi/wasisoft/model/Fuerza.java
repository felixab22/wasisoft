package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "fuerza")
public class Fuerza extends DateAudit implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_fuerza")
    private Long idfuerza;

    @Column(name = "obscaderaext")
    private String obscaderaext;
    @Column(name = "obscaderaflex")
    private String obscaderaflex;
    @Column(name = "obscaderaabducglut")
    private String obscaderaabducglut;
    @Column(name = "obscaderaabducext")
    private String obscaderaabducext;
    @Column(name = "obscaderaabducflex")
    private String obscaderaabducflex;
    @Column(name = "obsrodillaext")
    private String obsrodillaext;
    @Column(name = "obsrodillaflex")
    private String obsrodillaflex;

    @Column(name = "obstobillodors")
    private String obstobillodors;
    @Column(name = "obstobilloplant")
    private String obstobilloplant;
    @Column(name = "obshombrosante")
    private String obshombrosante;
    @Column(name = "obshombrosabdu")
    private String obshombrosabdu;

    @Column(name = "obscodoexte")
    private String obscodoext;
    @Column(name = "obscodoflex")
    private String obscodoflex;
    @Column(name = "obsabdominales")
    private String  obsabdominales;
    @Column(name = "obsextensores")
    private String obsextensores;

    @Size(max = 400)
    @Column(name = "conclusiones")
    private String conclusiones;
    @Size(max = 500)
    @Column(name = "difizquierda")
    private String difizquierda;
    @Size(max = 500)
    @Column(name = "reducciones")
    private String reducciones;
    @Size(max = 500)
    @Column(name = "obsfinal")
    private String obsfinal;

    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica fichaTerapiaFisica;

    public Fuerza(Long idfuerza) {
        this.idfuerza = idfuerza;
    }

    public Fuerza() {
    }

    public Fuerza(String obscaderaext, String obscaderaflex, String obscaderaabducglut, String obscaderaabducext, String obscaderaabducflex, String obsrodillaext, String obsrodillaflex, String obstobillodors, String obstobilloplant, String obshombrosante, String obshombrosabdu, String obscodoext, String obscodoflex, String obsabdominales, String obsextensores, String conclusiones, String difizquierda, String reducciones, String obsfinal, FichaTerapiaFisica fichaTerapiaFisica) {
        this.obscaderaext = obscaderaext;
        this.obscaderaflex = obscaderaflex;
        this.obscaderaabducglut = obscaderaabducglut;
        this.obscaderaabducext = obscaderaabducext;
        this.obscaderaabducflex = obscaderaabducflex;
        this.obsrodillaext = obsrodillaext;
        this.obsrodillaflex = obsrodillaflex;
        this.obstobillodors = obstobillodors;
        this.obstobilloplant = obstobilloplant;
        this.obshombrosante = obshombrosante;
        this.obshombrosabdu = obshombrosabdu;
        this.obscodoext = obscodoext;
        this.obscodoflex = obscodoflex;
        this.obsabdominales = obsabdominales;
        this.obsextensores = obsextensores;
        this.conclusiones = conclusiones;
        this.difizquierda = difizquierda;
        this.reducciones = reducciones;
        this.obsfinal = obsfinal;
        this.fichaTerapiaFisica = fichaTerapiaFisica;
    }

    public Long getIdfuerza() {
        return idfuerza;
    }

    public void setIdfuerza(Long idfuerza) {
        this.idfuerza = idfuerza;
    }

    public String getObscaderaext() {
        return obscaderaext;
    }

    public void setObscaderaext(String obscaderaext) {
        this.obscaderaext = obscaderaext;
    }

    public String getObscaderaflex() {
        return obscaderaflex;
    }

    public void setObscaderaflex(String obscaderaflex) {
        this.obscaderaflex = obscaderaflex;
    }

    public String getObscaderaabducglut() {
        return obscaderaabducglut;
    }

    public void setObscaderaabducglut(String obscaderaabducglut) {
        this.obscaderaabducglut = obscaderaabducglut;
    }

    public String getObscaderaabducext() {
        return obscaderaabducext;
    }

    public void setObscaderaabducext(String obscaderaabducext) {
        this.obscaderaabducext = obscaderaabducext;
    }

    public String getObscaderaabducflex() {
        return obscaderaabducflex;
    }

    public void setObscaderaabducflex(String obscaderaabducflex) {
        this.obscaderaabducflex = obscaderaabducflex;
    }

    public String getObsrodillaext() {
        return obsrodillaext;
    }

    public void setObsrodillaext(String obsrodillaext) {
        this.obsrodillaext = obsrodillaext;
    }

    public String getObsrodillaflex() {
        return obsrodillaflex;
    }

    public void setObsrodillaflex(String obsrodillaflex) {
        this.obsrodillaflex = obsrodillaflex;
    }

    public String getObstobillodors() {
        return obstobillodors;
    }

    public void setObstobillodors(String obstobillodors) {
        this.obstobillodors = obstobillodors;
    }

    public String getObstobilloplant() {
        return obstobilloplant;
    }

    public void setObstobilloplant(String obstobilloplant) {
        this.obstobilloplant = obstobilloplant;
    }

    public String getObshombrosante() {
        return obshombrosante;
    }

    public void setObshombrosante(String obshombrosante) {
        this.obshombrosante = obshombrosante;
    }

    public String getObshombrosabdu() {
        return obshombrosabdu;
    }

    public void setObshombrosabdu(String obshombrosabdu) {
        this.obshombrosabdu = obshombrosabdu;
    }

    public String getObscodoext() {
        return obscodoext;
    }

    public void setObscodoext(String obscodoext) {
        this.obscodoext = obscodoext;
    }

    public String getObscodoflex() {
        return obscodoflex;
    }

    public void setObscodoflex(String obscodoflex) {
        this.obscodoflex = obscodoflex;
    }

    public String getConclusiones() {
        return conclusiones;
    }

    public void setConclusiones(String conclusiones) {
        this.conclusiones = conclusiones;
    }

    public String getDifizquierda() {
        return difizquierda;
    }

    public void setDifizquierda(String difizquierda) {
        this.difizquierda = difizquierda;
    }

    public String getReducciones() {
        return reducciones;
    }

    public void setReducciones(String reducciones) {
        this.reducciones = reducciones;
    }

    public String getObsfinal() {
        return obsfinal;
    }

    public void setObsfinal(String obsfinal) {
        this.obsfinal = obsfinal;
    }

    public String getObsabdominales() {
        return obsabdominales;
    }

    public void setObsabdominales(String obsabdominales) {
        this.obsabdominales = obsabdominales;
    }

    public String getObsextensores() {
        return obsextensores;
    }

    public void setObsextensores(String obsextensores) {
        this.obsextensores = obsextensores;
    }

    public FichaTerapiaFisica getFichaTerapiaFisica() {
        return fichaTerapiaFisica;
    }

    public void setFichaTerapiaFisica(FichaTerapiaFisica fichaTerapiaFisica) {
        this.fichaTerapiaFisica = fichaTerapiaFisica;
    }
}
