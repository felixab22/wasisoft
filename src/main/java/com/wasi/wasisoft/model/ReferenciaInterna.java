package com.wasi.wasisoft.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 24/04/2018.
 */
@Entity
@Table(name = "referinterna")
public class ReferenciaInterna implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_referinterna")
    private Long idreferinterna;

    @Size(max = 200)
    @Column(name = "motivoreferencia")
    private String motivoreferencia;

    @Column(name = "freferencia")
    @Temporal(TemporalType.DATE)
    private Date freferencia;

    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne
    private Expediente idexpediente;

    @JoinColumn(name = "id_area", referencedColumnName = "id_area")
    @ManyToOne
    private Area idarea;

    public ReferenciaInterna() {
    }

    public ReferenciaInterna(Long idreferinterna) {
        this.idreferinterna = idreferinterna;
    }

    public ReferenciaInterna(String motivoreferencia, Date freferencia, Expediente idexpediente, Area idarea) {
        this.motivoreferencia = motivoreferencia;
        this.freferencia = freferencia;
        this.idexpediente = idexpediente;
        this.idarea = idarea;
    }

    public Long getIdreferinterna() {
        return idreferinterna;
    }

    public void setIdreferinterna(Long idreferinterna) {
        this.idreferinterna = idreferinterna;
    }

    public String getMotivoreferencia() {
        return motivoreferencia;
    }

    public void setMotivoreferencia(String motivoreferencia) {
        this.motivoreferencia = motivoreferencia;
    }

    public Date getFreferencia() {
        return freferencia;
    }

    public void setFreferencia(Date freferencia) {
        this.freferencia = freferencia;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Area getIdarea() {
        return idarea;
    }

    public void setIdarea(Area idarea) {
        this.idarea = idarea;
    }
}
