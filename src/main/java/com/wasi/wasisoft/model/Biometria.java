package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "biometria")

public class Biometria extends DateAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_biometria")
    private Long idbiometria;

    /**
     * Datos dfe prmera evaluacion
     */
    @Column(name = "longitudizq")
    private String longitudizq;
    @Column(name = "longitudder")
    private String longitudder;
    @Column(name = "diferencia")
    private String diferencia;
    @Column(name = "circunfcabeza")
    private String circunfcabeza;
    /**
     * Datos de segunda evaluacion
     */
    @Column(name = "longitudizqdos")
    private String longitudizqdos;
    @Column(name = "longitudderdos")
    private String longitudderdos;
    @Column(name = "diferenciados")
    private String diferenciados;
    @Column(name = "circunfcabezados")
    private String circunfcabezados;
    /**
     * Datos de la tercera evaluacion
     */
    @Column(name = "longitudizqtres")
    private String longitudizqtres;
    @Column(name = "longituddertres")
    private String longituddertres;
    @Column(name = "diferenciatres")
    private String diferenciatres;
    @Column(name = "circunfcabezatres")
    private String circunfcabezatres;
    /**
     * Otros datos
     */
    @Size(max = 500)
    @Column(name = "obslongitud")
    private String obslongitud;
    @Size(max = 500)
    @Column(name = "obscircunfcabeza")
    private String obscircunfcabeza;
    @Size(max = 500)
    @Column(name = "conclusion")
    private String conclusion;
    @Size(max = 400)
    @Column(name = "difizquierda")
    private String difizquierda;
    @Size(max = 400)
    @Column(name = "reducciones")
    private String reducciones;
    @Size(max = 500)
    @Column(name = "observaciones")
    private String observaciones;

    @Temporal(TemporalType.DATE)
    @Column(name = "fbioseguno")
    private Date fbioseguno;

    @Temporal(TemporalType.DATE)
    @Column(name = "fbiosegdos")
    private Date fbiosegdos;

    @Temporal(TemporalType.DATE)
    @Column(name = "fbiosegtres")
    private Date fbiosegtres;

    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica idfichaterapiafisica;

    public Biometria() {
    }

    public Biometria(Long idbiometria) {
        this.idbiometria = idbiometria;
    }

    public Biometria(String longitudizq, String longitudder, String diferencia, String circunfcabeza, String longitudizqdos, String longitudderdos, String diferenciados, String circunfcabezados, String longitudizqtres, String longituddertres, String diferenciatres, String circunfcabezatres, @Size(max = 500) String obslongitud, @Size(max = 500) String obscircunfcabeza, @Size(max = 500) String conclusion, @Size(max = 400) String difizquierda, @Size(max = 400) String reducciones, @Size(max = 500) String observaciones, Date fbioseguno, Date fbiosegdos, Date fbiosegtres, FichaTerapiaFisica idfichaterapiafisica) {
        this.longitudizq = longitudizq;
        this.longitudder = longitudder;
        this.diferencia = diferencia;
        this.circunfcabeza = circunfcabeza;
        this.longitudizqdos = longitudizqdos;
        this.longitudderdos = longitudderdos;
        this.diferenciados = diferenciados;
        this.circunfcabezados = circunfcabezados;
        this.longitudizqtres = longitudizqtres;
        this.longituddertres = longituddertres;
        this.diferenciatres = diferenciatres;
        this.circunfcabezatres = circunfcabezatres;
        this.obslongitud = obslongitud;
        this.obscircunfcabeza = obscircunfcabeza;
        this.conclusion = conclusion;
        this.difizquierda = difizquierda;
        this.reducciones = reducciones;
        this.observaciones = observaciones;
        this.fbioseguno = fbioseguno;
        this.fbiosegdos = fbiosegdos;
        this.fbiosegtres = fbiosegtres;
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public Long getIdbiometria() {
        return idbiometria;
    }

    public void setIdbiometria(Long idbiometria) {
        this.idbiometria = idbiometria;
    }

    public String getLongitudizq() {
        return longitudizq;
    }

    public void setLongitudizq(String longitudizq) {
        this.longitudizq = longitudizq;
    }

    public String getLongitudder() {
        return longitudder;
    }

    public void setLongitudder(String longitudder) {
        this.longitudder = longitudder;
    }

    public String getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(String diferencia) {
        this.diferencia = diferencia;
    }

    public String getCircunfcabeza() {
        return circunfcabeza;
    }

    public void setCircunfcabeza(String circunfcabeza) {
        this.circunfcabeza = circunfcabeza;
    }

    public String getLongitudizqdos() {
        return longitudizqdos;
    }

    public void setLongitudizqdos(String longitudizqdos) {
        this.longitudizqdos = longitudizqdos;
    }

    public String getLongitudderdos() {
        return longitudderdos;
    }

    public void setLongitudderdos(String longitudderdos) {
        this.longitudderdos = longitudderdos;
    }

    public String getDiferenciados() {
        return diferenciados;
    }

    public void setDiferenciados(String diferenciados) {
        this.diferenciados = diferenciados;
    }

    public String getCircunfcabezados() {
        return circunfcabezados;
    }

    public void setCircunfcabezados(String circunfcabezados) {
        this.circunfcabezados = circunfcabezados;
    }

    public String getLongitudizqtres() {
        return longitudizqtres;
    }

    public void setLongitudizqtres(String longitudizqtres) {
        this.longitudizqtres = longitudizqtres;
    }

    public String getLongituddertres() {
        return longituddertres;
    }

    public void setLongituddertres(String longituddertres) {
        this.longituddertres = longituddertres;
    }

    public String getDiferenciatres() {
        return diferenciatres;
    }

    public void setDiferenciatres(String diferenciatres) {
        this.diferenciatres = diferenciatres;
    }

    public String getCircunfcabezatres() {
        return circunfcabezatres;
    }

    public void setCircunfcabezatres(String circunfcabezatres) {
        this.circunfcabezatres = circunfcabezatres;
    }

    public String getObslongitud() {
        return obslongitud;
    }

    public void setObslongitud(String obslongitud) {
        this.obslongitud = obslongitud;
    }

    public String getObscircunfcabeza() {
        return obscircunfcabeza;
    }

    public void setObscircunfcabeza(String obscircunfcabeza) {
        this.obscircunfcabeza = obscircunfcabeza;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getDifizquierda() {
        return difizquierda;
    }

    public void setDifizquierda(String difizquierda) {
        this.difizquierda = difizquierda;
    }

    public String getReducciones() {
        return reducciones;
    }

    public void setReducciones(String reducciones) {
        this.reducciones = reducciones;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public FichaTerapiaFisica getIdfichaterapiafisica() {
        return idfichaterapiafisica;
    }

    public void setIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public Date getFbioseguno() {
        return fbioseguno;
    }

    public void setFbioseguno(Date fbioseguno) {
        this.fbioseguno = fbioseguno;
    }

    public Date getFbiosegdos() {
        return fbiosegdos;
    }

    public void setFbiosegdos(Date fbiosegdos) {
        this.fbiosegdos = fbiosegdos;
    }

    public Date getFbiosegtres() {
        return fbiosegtres;
    }

    public void setFbiosegtres(Date fbiosegtres) {
        this.fbiosegtres = fbiosegtres;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idbiometria != null ? idbiometria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Biometria)) {
            return false;
        }
        Biometria other = (Biometria) object;
        if ((this.idbiometria == null && other.idbiometria != null) || (this.idbiometria != null && !this.idbiometria.equals(other.idbiometria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wasi.wasisoft.model.BiometriaService[ idBiometria=" + idbiometria + " ]";
    }
}
