package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "asistencia")
public class Asistencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_asistencia")
    private Long idasistencia;

    //Registra el nombre del paciente y el codigo
    @Column(name = "paciente")
    private String paciente;

    //Registra dia de la semana de asistencia
    @Column(name = "dia")
    private String dia;

    //Fecha en que se registra el control de asistencia
    @Column(name = "fecha")
    private String fecha;

    //Registra si el páciente vino, no vino, llego tarde o no vino pero aviso.
    @Column(name = "descripcion")
    private String descripcion;

    //(No Vino = 0, Vino = 1, Llego tarde = 2, No vino pero aviso = 3)
    @Column(name = "tipo_asistencia")
    private int tipoasistencia;

//    @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne
    @JoinColumn(name = "id_personal")
    private Personal personal;

    public Asistencia() {    }

    public Asistencia(String paciente, String dia, String fecha, String descripcion, int tipoasistencia, Personal personal) {
        this.paciente = paciente;
        this.dia = dia;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.tipoasistencia = tipoasistencia;
        this.personal = personal;
    }

    public Long getIdasistencia() {
        return idasistencia;
    }

    public void setIdasistencia(Long idasistencia) {
        this.idasistencia = idasistencia;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getFecha() {
        return fecha;
    }

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getTipoasistencia() {
        return tipoasistencia;
    }

    public void setTipoasistencia(int tipoasistencia) {
        this.tipoasistencia = tipoasistencia;
    }

    @Override
    public String toString() {
        return "Asistencia{" +
                "idasistencia=" + idasistencia +
                ", paciente='" + paciente + '\'' +
                ", dia='" + dia + '\'' +
                ", fecha='" + fecha + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", tipoasistencia='" + tipoasistencia + '\'' +
                '}';
    }


}
