package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@Entity
@Table(name = "espasticidad")
public class Espasticidad extends DateAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_espasticidad")
    private Long idespasticidad;
    /**
     * Datos de primera evaluacion
     */
    @Column(name = "unocaderaiz")
    private  String unocaderaiz;
    @Column(name = "unorodillaflexiz")
    private  String unorodillaflexiz;
    @Column(name = "unorodillaextiz")
    private  String unorodillaextiz;
    @Column(name = "unotobilloiz")
    private  String unotobilloiz;
    @Column(name = "unocodoiz")
    private  String unocodoiz;
    @Column(name = "unocaderader")
    private  String unocaderader;
    @Column(name = "unorodillaflexder")
    private  String unorodillaflexder;
    @Column(name = "unorodillaextder")
    private  String unorodillaextder;
    @Column(name = "unotobilloder")
    private  String unotobilloder;
    @Column(name = "unocododer")
    private  String unocododer;
    /**
     * Datos de la segunda evaluacion
     */
    @Column(name = "doscaderaiz")
    private  String doscaderaiz;
    @Column(name = "dosrodillaflexiz")
    private  String dosrodillaflexiz;
    @Column(name = "dosrodillaextiz")
    private  String dosrodillaextiz;
    @Column(name = "dostobilloiz")
    private  String dostobilloiz;
    @Column(name = "doscodoiz")
    private  String doscodoiz;
    @Column(name = "doscaderader")
    private  String doscaderader;
    @Column(name = "dosrodillaflexder")
    private  String dosrodillaflexder;
    @Column(name = "dosrodillaextder")
    private  String dosrodillaextder;
    @Column(name = "dostobilloder")
    private  String dostobilloder;
    @Column(name = "doscododer")
    private  String doscododer;
    /**
     * Observaciones
     */
    @Column(name = "obscadera")
    private String obscadera;
    @Column(name = "obsrodillaflex")
    private String obsrodillaflex;
    @Column(name = "obsrodillaext")
    private String obsrodillaext;
    @Column(name = "obstobillo")
    private String obstobillo;
    @Column(name = "obscodo")
    private String obscodo;
    /**
     * Otros datos
     */
    @Size(max = 500)
    @Column(name = "conclusion")
    private String conclusion;
    @Size(max = 400)
    @Column(name = "difizquierda")
    private String difizquierda;
    @Size(max = 400)
    @Column(name = "otro")
    private String otro;

    @Temporal(TemporalType.DATE)
    @Column(name = "fespseguno")
    private Date fespseguno;

    @Temporal(TemporalType.DATE)
    @Column(name = "fespsegdos")
    private Date fespsegdos;

    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica fichaTerapiaFisica;

    public Espasticidad() {
    }

    public Espasticidad(Long idespasticidad) {
        this.idespasticidad = idespasticidad;
    }

    public Espasticidad(String unocaderaiz, String unorodillaflexiz, String unorodillaextiz, String unotobilloiz, String unocodoiz, String unocaderader, String unorodillaflexder, String unorodillaextder, String unotobilloder, String unocododer, String doscaderaiz, String dosrodillaflexiz, String dosrodillaextiz, String dostobilloiz, String doscodoiz, String doscaderader, String dosrodillaflexder, String dosrodillaextder, String dostobilloder, String doscododer, String obscadera, String obsrodillaflex, String obsrodillaext, String obstobillo, String obscodo, @Size(max = 500) String conclusion, @Size(max = 400) String difizquierda, @Size(max = 400) String otro, Date fespseguno, Date fespsegdos, FichaTerapiaFisica fichaTerapiaFisica) {
        this.unocaderaiz = unocaderaiz;
        this.unorodillaflexiz = unorodillaflexiz;
        this.unorodillaextiz = unorodillaextiz;
        this.unotobilloiz = unotobilloiz;
        this.unocodoiz = unocodoiz;
        this.unocaderader = unocaderader;
        this.unorodillaflexder = unorodillaflexder;
        this.unorodillaextder = unorodillaextder;
        this.unotobilloder = unotobilloder;
        this.unocododer = unocododer;
        this.doscaderaiz = doscaderaiz;
        this.dosrodillaflexiz = dosrodillaflexiz;
        this.dosrodillaextiz = dosrodillaextiz;
        this.dostobilloiz = dostobilloiz;
        this.doscodoiz = doscodoiz;
        this.doscaderader = doscaderader;
        this.dosrodillaflexder = dosrodillaflexder;
        this.dosrodillaextder = dosrodillaextder;
        this.dostobilloder = dostobilloder;
        this.doscododer = doscododer;
        this.obscadera = obscadera;
        this.obsrodillaflex = obsrodillaflex;
        this.obsrodillaext = obsrodillaext;
        this.obstobillo = obstobillo;
        this.obscodo = obscodo;
        this.conclusion = conclusion;
        this.difizquierda = difizquierda;
        this.otro = otro;
        this.fespseguno = fespseguno;
        this.fespsegdos = fespsegdos;
        this.fichaTerapiaFisica = fichaTerapiaFisica;
    }

    public Long getIdespasticidad() {
        return idespasticidad;
    }

    public void setIdespasticidad(Long idespasticidad) {
        this.idespasticidad = idespasticidad;
    }

    public String getUnocaderaiz() {
        return unocaderaiz;
    }

    public void setUnocaderaiz(String unocaderaiz) {
        this.unocaderaiz = unocaderaiz;
    }

    public String getUnorodillaflexiz() {
        return unorodillaflexiz;
    }

    public void setUnorodillaflexiz(String unorodillaflexiz) {
        this.unorodillaflexiz = unorodillaflexiz;
    }

    public String getUnorodillaextiz() {
        return unorodillaextiz;
    }

    public void setUnorodillaextiz(String unorodillaextiz) {
        this.unorodillaextiz = unorodillaextiz;
    }

    public String getUnotobilloiz() {
        return unotobilloiz;
    }

    public void setUnotobilloiz(String unotobilloiz) {
        this.unotobilloiz = unotobilloiz;
    }

    public String getUnocodoiz() {
        return unocodoiz;
    }

    public void setUnocodoiz(String unocodoiz) {
        this.unocodoiz = unocodoiz;
    }

    public String getUnocaderader() {
        return unocaderader;
    }

    public void setUnocaderader(String unocaderader) {
        this.unocaderader = unocaderader;
    }

    public String getUnorodillaflexder() {
        return unorodillaflexder;
    }

    public void setUnorodillaflexder(String unorodillaflexder) {
        this.unorodillaflexder = unorodillaflexder;
    }

    public String getUnorodillaextder() {
        return unorodillaextder;
    }

    public void setUnorodillaextder(String unorodillaextder) {
        this.unorodillaextder = unorodillaextder;
    }

    public String getUnotobilloder() {
        return unotobilloder;
    }

    public void setUnotobilloder(String unotobilloder) {
        this.unotobilloder = unotobilloder;
    }

    public String getUnocododer() {
        return unocododer;
    }

    public void setUnocododer(String unocododer) {
        this.unocododer = unocododer;
    }

    public String getDoscaderaiz() {
        return doscaderaiz;
    }

    public void setDoscaderaiz(String doscaderaiz) {
        this.doscaderaiz = doscaderaiz;
    }

    public String getDosrodillaflexiz() {
        return dosrodillaflexiz;
    }

    public void setDosrodillaflexiz(String dosrodillaflexiz) {
        this.dosrodillaflexiz = dosrodillaflexiz;
    }

    public String getDosrodillaextiz() {
        return dosrodillaextiz;
    }

    public void setDosrodillaextiz(String dosrodillaextiz) {
        this.dosrodillaextiz = dosrodillaextiz;
    }

    public String getDostobilloiz() {
        return dostobilloiz;
    }

    public void setDostobilloiz(String dostobilloiz) {
        this.dostobilloiz = dostobilloiz;
    }

    public String getDoscodoiz() {
        return doscodoiz;
    }

    public void setDoscodoiz(String doscodoiz) {
        this.doscodoiz = doscodoiz;
    }

    public String getDoscaderader() {
        return doscaderader;
    }

    public void setDoscaderader(String doscaderader) {
        this.doscaderader = doscaderader;
    }

    public String getDosrodillaflexder() {
        return dosrodillaflexder;
    }

    public void setDosrodillaflexder(String dosrodillaflexder) {
        this.dosrodillaflexder = dosrodillaflexder;
    }

    public String getDosrodillaextder() {
        return dosrodillaextder;
    }

    public void setDosrodillaextder(String dosrodillaextder) {
        this.dosrodillaextder = dosrodillaextder;
    }

    public String getDostobilloder() {
        return dostobilloder;
    }

    public void setDostobilloder(String dostobilloder) {
        this.dostobilloder = dostobilloder;
    }

    public String getDoscododer() {
        return doscododer;
    }

    public void setDoscododer(String doscododer) {
        this.doscododer = doscododer;
    }

    public String getObscadera() {
        return obscadera;
    }

    public void setObscadera(String obscadera) {
        this.obscadera = obscadera;
    }

    public String getObsrodillaflex() {
        return obsrodillaflex;
    }

    public void setObsrodillaflex(String obsrodillaflex) {
        this.obsrodillaflex = obsrodillaflex;
    }

    public String getObsrodillaext() {
        return obsrodillaext;
    }

    public void setObsrodillaext(String obsrodillaext) {
        this.obsrodillaext = obsrodillaext;
    }

    public String getObstobillo() {
        return obstobillo;
    }

    public void setObstobillo(String obstobillo) {
        this.obstobillo = obstobillo;
    }

    public String getObscodo() {
        return obscodo;
    }

    public void setObscodo(String obscodo) {
        this.obscodo = obscodo;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getDifizquierda() {
        return difizquierda;
    }

    public void setDifizquierda(String difizquierda) {
        this.difizquierda = difizquierda;
    }

    public String getOtro() {
        return otro;
    }

    public void setOtro(String otro) {
        this.otro = otro;
    }

    public FichaTerapiaFisica getFichaTerapiaFisica() {
        return fichaTerapiaFisica;
    }

    public void setFichaTerapiaFisica(FichaTerapiaFisica fichaTerapiaFisica) {
        this.fichaTerapiaFisica = fichaTerapiaFisica;
    }

    public Date getFespseguno() {
        return fespseguno;
    }

    public void setFespseguno(Date fespseguno) {
        this.fespseguno = fespseguno;
    }

    public Date getFespsegdos() {
        return fespsegdos;
    }

    public void setFespsegdos(Date fespsegdos) {
        this.fespsegdos = fespsegdos;
    }
}