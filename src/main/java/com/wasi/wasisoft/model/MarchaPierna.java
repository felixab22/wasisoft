package com.wasi.wasisoft.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@Entity
@Table(name = "marchapierna")
public class MarchaPierna implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_marchapierna")
    private Long idmarchapierna;

    // Indica si es la pierna izquierda o derecha(0 = pierna derecha y 1 = pierna izquierda)
    @Column(name = "posicionpierna")
    private Integer posicionpierna;

    @Column(name = "fsegpierna")
    @Temporal(TemporalType.DATE)
    private Date fsegpierna;

    //Registra los seguimientos de la pierna (0, 1 y 2)
    @Column(name = "numsegpierna")
    private Integer numsegpierna;
    /**
     * Datos de fase de apoyo ( pf = plano frontal, ps = planosagital, ap=apoyo, imp= impulsion)
     */
    @Column(name = "troncoincap")
    private String troncoincap;
    @Column(name = "troncorotap")
    private String troncorotapyo;
    @Column(name = "pelvissubeap")
    private String pelvissubeap;
    @Column(name = "pelviscontraap")
    private String pelviscontraap;
    @Column(name = "caderarotap")
    private String caderarotap;
    @Column(name = "caderaabduap")
    private String caderaabduap;
    @Column(name = "pstroncoap")
    private String pstroncoap;
    @Column(name = "pspelvisap")
    private String pspelvisap;
    @Column(name = "pscaderaap")
    private String pscaderaap;
    @Column(name = "psrodillflexap")
    private String psrodillflexap;
    @Column(name = "psrodillhiperap")
    private String psrodillhiperap;
    @Column(name = "pstobillantpieap")
    private String pstobillantpieap;
    @Column(name = "pstobillpieap")
    private String pstobillpieap;
    @Column(name = "pstobilldedosap")
    private String pstobilldedosap;
    /**
     * Datos de la fase impulsion(imp)
     */
    @Column(name = "pftroncoincimp")
    private String pftroncoincimp;
    @Column(name = "pftroncorotimp")
    private String pftroncorotimp;
    @Column(name = "pfpelvissubeimp")
    private String pfpelvissubeimp;
    @Column(name = "pfpelviscontraimp")
    private String pfpelviscontraimp;
    @Column(name = "pfcaderarotimp")
    private String pfcaderarotimp;
    @Column(name = "pfcaderaabduimp")
    private String pfcaderaabduimp;
    @Column(name = "pstroncoimp")
    private String pstroncoapimp;
    @Column(name = "pspelvisimp")
    private String pspelvisimp;
    @Column(name = "pscaderaimp")
    private String pscaderaimp;
    @Column(name = "psrodillfleximp")
    private String psrodillfleximp;
    @Column(name = "psrodillhiperimp")
    private String psrodillhiperimp;
    @Column(name = "pstobillntepieimp")
    private String pstobillantepieimp;
    @Column(name = "pstobillpieimp")
    private String pstobillpieimp;
    @Column(name = "pstobilldedosimp")
    private String pstobilldedosimp;

    @JoinColumn(name = "id_marcha", referencedColumnName = "id_marcha")
    @ManyToOne
    private Marcha marcha;

    public MarchaPierna() {
    }

    public MarchaPierna(Long idmarchapierna ) {
        this.idmarchapierna = idmarchapierna;
    }

    public MarchaPierna(Integer posicionpierna, Date fsegpierna, Integer numsegpierna, String troncoincap, String troncorotapyo, String pelvissubeap, String pelviscontraap, String caderarotap, String caderaabduap, String pstroncoap, String pspelvisap, String pscaderaap, String psrodillflexap, String psrodillhiperap, String pstobillantpieap, String pstobillpieap, String pstobilldedosap, String pftroncoincimp, String pftroncorotimp, String pfpelvissubeimp, String pfpelviscontraimp, String pfcaderarotimp, String pfcaderaabduimp, String pstroncoapimp, String pspelvisimp, String pscaderaimp, String psrodillfleximp, String psrodillhiperimp, String pstobillantepieimp, String pstobillpieimp, String pstobilldedosimp, Marcha marcha) {
        this.posicionpierna = posicionpierna;
        this.fsegpierna = fsegpierna;
        this.numsegpierna = numsegpierna;
        this.troncoincap = troncoincap;
        this.troncorotapyo = troncorotapyo;
        this.pelvissubeap = pelvissubeap;
        this.pelviscontraap = pelviscontraap;
        this.caderarotap = caderarotap;
        this.caderaabduap = caderaabduap;
        this.pstroncoap = pstroncoap;
        this.pspelvisap = pspelvisap;
        this.pscaderaap = pscaderaap;
        this.psrodillflexap = psrodillflexap;
        this.psrodillhiperap = psrodillhiperap;
        this.pstobillantpieap = pstobillantpieap;
        this.pstobillpieap = pstobillpieap;
        this.pstobilldedosap = pstobilldedosap;
        this.pftroncoincimp = pftroncoincimp;
        this.pftroncorotimp = pftroncorotimp;
        this.pfpelvissubeimp = pfpelvissubeimp;
        this.pfpelviscontraimp = pfpelviscontraimp;
        this.pfcaderarotimp = pfcaderarotimp;
        this.pfcaderaabduimp = pfcaderaabduimp;
        this.pstroncoapimp = pstroncoapimp;
        this.pspelvisimp = pspelvisimp;
        this.pscaderaimp = pscaderaimp;
        this.psrodillfleximp = psrodillfleximp;
        this.psrodillhiperimp = psrodillhiperimp;
        this.pstobillantepieimp = pstobillantepieimp;
        this.pstobillpieimp = pstobillpieimp;
        this.pstobilldedosimp = pstobilldedosimp;
        this.marcha = marcha;
    }

    public Long getIdmarchapierna() {
        return idmarchapierna;
    }

    public void setIdmarchapierna(Long idmarchapierna) {
        this.idmarchapierna = idmarchapierna;
    }

    public Integer getPosicionpierna() {
        return posicionpierna;
    }

    public void setPosicionpierna(Integer posicionpierna) {
        this.posicionpierna = posicionpierna;
    }

    public String getTroncoincap() {
        return troncoincap;
    }

    public void setTroncoincap(String troncoincap) {
        this.troncoincap = troncoincap;
    }

    public String getTroncorotapyo() {
        return troncorotapyo;
    }

    public void setTroncorotapyo(String troncorotapyo) {
        this.troncorotapyo = troncorotapyo;
    }

    public String getPelvissubeap() {
        return pelvissubeap;
    }

    public void setPelvissubeap(String pelvissubeap) {
        this.pelvissubeap = pelvissubeap;
    }

    public String getPelviscontraap() {
        return pelviscontraap;
    }

    public void setPelviscontraap(String pelviscontraap) {
        this.pelviscontraap = pelviscontraap;
    }

    public String getCaderarotap() {
        return caderarotap;
    }

    public void setCaderarotap(String caderarotap) {
        this.caderarotap = caderarotap;
    }

    public String getCaderaabduap() {
        return caderaabduap;
    }

    public void setCaderaabduap(String caderaabduap) {
        this.caderaabduap = caderaabduap;
    }

    public String getPstroncoap() {
        return pstroncoap;
    }

    public void setPstroncoap(String pstroncoap) {
        this.pstroncoap = pstroncoap;
    }

    public String getPspelvisap() {
        return pspelvisap;
    }

    public void setPspelvisap(String pspelvisap) {
        this.pspelvisap = pspelvisap;
    }

    public String getPscaderaap() {
        return pscaderaap;
    }

    public void setPscaderaap(String pscaderaap) {
        this.pscaderaap = pscaderaap;
    }

    public String getPsrodillflexap() {
        return psrodillflexap;
    }

    public void setPsrodillflexap(String psrodillflexap) {
        this.psrodillflexap = psrodillflexap;
    }

    public String getPsrodillhiperap() {
        return psrodillhiperap;
    }

    public void setPsrodillhiperap(String psrodillhiperap) {
        this.psrodillhiperap = psrodillhiperap;
    }

    public String getPstobillantpieap() {
        return pstobillantpieap;
    }

    public void setPstobillantpieap(String pstobillantpieap) {
        this.pstobillantpieap = pstobillantpieap;
    }

    public String getPstobillpieap() {
        return pstobillpieap;
    }

    public void setPstobillpieap(String pstobillpieap) {
        this.pstobillpieap = pstobillpieap;
    }

    public String getPstobilldedosap() {
        return pstobilldedosap;
    }

    public void setPstobilldedosap(String pstobilldedosap) {
        this.pstobilldedosap = pstobilldedosap;
    }

    public String getPftroncoincimp() {
        return pftroncoincimp;
    }

    public void setPftroncoincimp(String pftroncoincimp) {
        this.pftroncoincimp = pftroncoincimp;
    }

    public String getPftroncorotimp() {
        return pftroncorotimp;
    }

    public void setPftroncorotimp(String pftroncorotimp) {
        this.pftroncorotimp = pftroncorotimp;
    }

    public String getPfpelvissubeimp() {
        return pfpelvissubeimp;
    }

    public void setPfpelvissubeimp(String pfpelvissubeimp) {
        this.pfpelvissubeimp = pfpelvissubeimp;
    }

    public String getPfpelviscontraimp() {
        return pfpelviscontraimp;
    }

    public void setPfpelviscontraimp(String pfpelviscontraimp) {
        this.pfpelviscontraimp = pfpelviscontraimp;
    }

    public String getPfcaderarotimp() {
        return pfcaderarotimp;
    }

    public void setPfcaderarotimp(String pfcaderarotimp) {
        this.pfcaderarotimp = pfcaderarotimp;
    }

    public String getPfcaderaabduimp() {
        return pfcaderaabduimp;
    }

    public void setPfcaderaabduimp(String pfcaderaabduimp) {
        this.pfcaderaabduimp = pfcaderaabduimp;
    }

    public String getPstroncoapimp() {
        return pstroncoapimp;
    }

    public void setPstroncoapimp(String pstroncoapimp) {
        this.pstroncoapimp = pstroncoapimp;
    }

    public String getPspelvisimp() {
        return pspelvisimp;
    }

    public void setPspelvisimp(String pspelvisimp) {
        this.pspelvisimp = pspelvisimp;
    }

    public String getPscaderaimp() {
        return pscaderaimp;
    }

    public void setPscaderaimp(String pscaderaimp) {
        this.pscaderaimp = pscaderaimp;
    }

    public String getPsrodillfleximp() {
        return psrodillfleximp;
    }

    public void setPsrodillfleximp(String psrodillfleximp) {
        this.psrodillfleximp = psrodillfleximp;
    }

    public String getPsrodillhiperimp() {
        return psrodillhiperimp;
    }

    public void setPsrodillhiperimp(String psrodillhiperimp) {
        this.psrodillhiperimp = psrodillhiperimp;
    }

    public String getPstobillantepieimp() {
        return pstobillantepieimp;
    }

    public void setPstobillantepieimp(String pstobillantepieimp) {
        this.pstobillantepieimp = pstobillantepieimp;
    }

    public String getPstobillpieimp() {
        return pstobillpieimp;
    }

    public void setPstobillpieimp(String pstobillpieimp) {
        this.pstobillpieimp = pstobillpieimp;
    }

    public String getPstobilldedosimp() {
        return pstobilldedosimp;
    }

    public void setPstobilldedosimp(String pstobilldedosimp) {
        this.pstobilldedosimp = pstobilldedosimp;
    }

    public Marcha getMarcha() {
        return marcha;
    }

    public void setMarcha(Marcha marcha) {
        this.marcha = marcha;
    }

    public Date getFsegpierna() {
        return fsegpierna;
    }

    public void setFsegpierna(Date fsegpierna) {
        this.fsegpierna = fsegpierna;
    }

    public Integer getNumsegpierna() {
        return numsegpierna;
    }

    public void setNumsegpierna(Integer numsegpierna) {
        this.numsegpierna = numsegpierna;
    }
}
