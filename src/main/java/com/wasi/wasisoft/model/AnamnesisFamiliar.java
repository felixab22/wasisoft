package com.wasi.wasisoft.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "antefami")
public class AnamnesisFamiliar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_antefami")
    private Long idantefami;

    @Column(name = "parentesco")
    private String parentesco;

    @Column(name = "salud_familiar")
    private  String salud_familiar;

    @Column(name = "edad_concepcion")
    private  Integer edad_concepcion;

    @Column(name = "patologias_fam")
    private  String patologias_fam;

    @Column(name = "num_hermanos")
    private  Integer num_hermanos;

    @Column(name = "desc_adicional")
    private  String desc_adicional;

    @Column(name = "anamnesisfamiliarrehab")
    private String anamnesisFamiliar_rehab;

    @Column (name = "problemasactuales")
    private String problemasactuales;

    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica idfichaterapiafisica;

    public AnamnesisFamiliar(Long idantefami) {
        this.idantefami = idantefami;
    }

    public AnamnesisFamiliar() {
    }

    public AnamnesisFamiliar(String parentesco, String salud_familiar, Integer edad_concepcion, String patologias_fam,String problemasactuales, Integer num_hermanos, String desc_adicional, String anamnesisFamiliar_rehab, FichaTerapiaFisica idfichaterapiafisica) {
        this.parentesco = parentesco;
        this.salud_familiar = salud_familiar;
        this.edad_concepcion = edad_concepcion;
        this.patologias_fam = patologias_fam;
        this.num_hermanos = num_hermanos;
        this.desc_adicional = desc_adicional;
        this.anamnesisFamiliar_rehab = anamnesisFamiliar_rehab;
        this.problemasactuales = problemasactuales;
        this.idfichaterapiafisica = idfichaterapiafisica;
    }

    public Long getIdantefami() {
        return idantefami;
    }

    public void setIdantefami(Long idantefami) {
        this.idantefami = idantefami;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getSalud_familiar() {
        return salud_familiar;
    }

    public void setSalud_familiar(String salud_familiar) {
        this.salud_familiar = salud_familiar;
    }

    public Integer getEdad_concepcion() {
        return edad_concepcion;
    }

    public void setEdad_concepcion(Integer edad_concepcion) {
        this.edad_concepcion = edad_concepcion;
    }

    public String getPatologias_fam() {
        return patologias_fam;
    }

    public void setPatologias_fam(String patologias_fam) {
        this.patologias_fam = patologias_fam;
    }

    public Integer getNum_hermanos() {
        return num_hermanos;
    }

    public void setNum_hermanos(Integer num_hermanos) {
        this.num_hermanos = num_hermanos;
    }

    public String getDesc_adicional() {
        return desc_adicional;
    }

    public void setDesc_adicional(String desc_adicional) {
        this.desc_adicional = desc_adicional;
    }

    public String getAnamnesisFamiliar_rehab() {
        return anamnesisFamiliar_rehab;
    }

    public void setAnamnesisFamiliar_rehab(String anamnesisFamiliar_rehab) {
        this.anamnesisFamiliar_rehab = anamnesisFamiliar_rehab;
    }

    public String getProblemasactuales() {
        return problemasactuales;
    }

    public void setProblemasactuales(String problemasactuales) {
        this.problemasactuales = problemasactuales;
    }

    public FichaTerapiaFisica getIdfichaterapiafisica() {
        return idfichaterapiafisica;
    }

    public void setIdfichaterapiafisica(FichaTerapiaFisica idfichaterapiafisica) {
        this.idfichaterapiafisica = idfichaterapiafisica;
    }
}
