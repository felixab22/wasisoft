package com.wasi.wasisoft.model;

import com.wasi.wasisoft.model.audit.DateAudit;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by HeverFernandez on 16/04/2018.
 */
@Entity
@Table(name = "resultescala")
public class ResultadoEscala extends DateAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_resultescala")
    private Long idresultescala;

    @Size(max = 300)
    @Column(name = "aims")
    private String aims;

    @Column(name = "imgunoaims")
    private String imgunoaims;

    @Column(name = "imgdosaims")
    private String imgdosaims;

    @Column(name = "imgtresaims")
    private String imgtresaims;

    @Column(name = "imgcuatroaims")
    private String imgcuatroaims;
    @Size(max = 300)
    @Column(name = "gmfma")
    private String gmfma;
    @Column(name = "gmfmb")
    private String gmfmb;
    @Column(name = "gmfmc")
    private String gmfmc;
    @Column(name = "gmfmd")
    private String gmfmd;
    @Column(name = "gmfme")
    private String gmfme;


    @Column(name = "imgunogmfm")
    private String imgunogmfm;

    @Column(name = "imgdosgmfm")
    private String imgdosgmfm;
    @Size(max = 200)
    @Column(name = "gmfcs")
    private String gmfcs;
    @Size(max = 200)
    @Column(name = "macs")
    private String macs;
    @JoinColumn(name = "id_fichaterapiafisica", referencedColumnName = "id_fichaterapiafisica")
    @ManyToOne
    private FichaTerapiaFisica fichaTerapiaFisica;

    public ResultadoEscala() {
    }

    public ResultadoEscala(Long idresultescala ) {
        this.idresultescala = idresultescala;
    }

    public ResultadoEscala(String aims, String imgunoaims, String imgdosaims, String imgtresaims, String imgcuatroaims, String gmfma, String gmfmb, String gmfmc, String gmfmd, String gmfme, String imgunogmfm, String imgdosgmfm, String gmfcs, String macs, FichaTerapiaFisica fichaTerapiaFisica) {
        this.aims = aims;
        this.imgunoaims = imgunoaims;
        this.imgdosaims = imgdosaims;
        this.imgtresaims = imgtresaims;
        this.imgcuatroaims = imgcuatroaims;
        this.gmfma = gmfma;
        this.gmfmb = gmfmb;
        this.gmfmc = gmfmc;
        this.gmfmd = gmfmd;
        this.gmfme = gmfme;
        this.imgunogmfm = imgunogmfm;
        this.imgdosgmfm = imgdosgmfm;
        this.gmfcs = gmfcs;
        this.macs = macs;
        this.fichaTerapiaFisica = fichaTerapiaFisica;
    }

    public Long getIdresultescala() {
        return idresultescala;
    }

    public void setIdresultescala(Long idresultescala) {
        this.idresultescala = idresultescala;
    }

    public String getAims() {
        return aims;
    }

    public void setAims(String aims) {
        this.aims = aims;
    }

    public String getImgunoaims() {
        return imgunoaims;
    }

    public void setImgunoaims(String imgunoaims) {
        this.imgunoaims = imgunoaims;
    }

    public String getImgdosaims() {
        return imgdosaims;
    }

    public void setImgdosaims(String imgdosaims) {
        this.imgdosaims = imgdosaims;
    }

    public String getImgtresaims() {
        return imgtresaims;
    }

    public void setImgtresaims(String imgtresaims) {
        this.imgtresaims = imgtresaims;
    }

    public String getImgcuatroaims() {
        return imgcuatroaims;
    }

    public void setImgcuatroaims(String imgcuatroaims) {
        this.imgcuatroaims = imgcuatroaims;
    }

    public String getGmfma() {
        return gmfma;
    }

    public void setGmfma(String gmfma) {
        this.gmfma = gmfma;
    }

    public String getGmfmb() {
        return gmfmb;
    }

    public void setGmfmb(String gmfmb) {
        this.gmfmb = gmfmb;
    }

    public String getGmfmc() {
        return gmfmc;
    }

    public void setGmfmc(String gmfmc) {
        this.gmfmc = gmfmc;
    }

    public String getGmfmd() {
        return gmfmd;
    }

    public void setGmfmd(String gmfmd) {
        this.gmfmd = gmfmd;
    }

    public String getGmfme() {
        return gmfme;
    }

    public void setGmfme(String gmfme) {
        this.gmfme = gmfme;
    }

    public String getImgunogmfm() {
        return imgunogmfm;
    }

    public void setImgunogmfm(String imgunogmfm) {
        this.imgunogmfm = imgunogmfm;
    }

    public String getImgdosgmfm() {
        return imgdosgmfm;
    }

    public void setImgdosgmfm(String imgdosgmfm) {
        this.imgdosgmfm = imgdosgmfm;
    }

    public String getGmfcs() {
        return gmfcs;
    }

    public void setGmfcs(String gmfcs) {
        this.gmfcs = gmfcs;
    }

    public String getMacs() {
        return macs;
    }

    public void setMacs(String macs) {
        this.macs = macs;
    }

    public FichaTerapiaFisica getFichaTerapiaFisica() {
        return fichaTerapiaFisica;
    }

    public void setFichaTerapiaFisica(FichaTerapiaFisica fichaTerapiaFisica) {
        this.fichaTerapiaFisica = fichaTerapiaFisica;
    }
}
