package com.wasi.wasisoft.model;

import org.hibernate.annotations.NaturalId;
import javax.persistence.*;

/**
 * Created by HeverFernandez on 01/08/17.
 */
@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idrol;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(length = 60)
    private RoleName descripcionrol;

    public Role() {

    }

    public Role(RoleName descripcionrol) {
        this.descripcionrol = descripcionrol;
    }

    public Long getIdrol() {
        return idrol;
    }

    public void setIdrol(Long idrol) {
        this.idrol = idrol;
    }

    public RoleName getDescripcionrol() {
        return descripcionrol;
    }

    public void setDescripcionrol(RoleName descripcionrol) {
        this.descripcionrol = descripcionrol;
    }
}
